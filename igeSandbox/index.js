// Require the vm
vm = require('vm');
path = require('path');

console.log('Sandbox starting from folder: ' + __dirname);

// Update the config data with the passed cli arguments
if (process.argv[2]) {
	
	// Extract the arguments
	var argArray = [];
	for (var i = 2; i < process.argv.length; i += 2) {
		argArray[process.argv[i]] = process.argv[i + 1];
	}
	
	// Set the config data based upon the arguments
	var configFile = argArray['-c'] || __dirname + '/config.js';
	
	// Config file & JSON processing library
	if (path.existsSync(configFile)) {
		console.log('Loading configuration data from: ' + configFile);
		require(configFile);
	} else {
		console.log('Cannot load configuration file "' + configFile + '", the file does not exist!'); 
		process.exit();
	}
	
	// Set the required options
	var sandboxUser = argArray['-u'];
	var sandboxGame = argArray['-g'];
	var sandboxPort = argArray['-p'];
	
	// Set the optional flags
	if (!argArray['-nodb']) {
		igeConfig.db.database = argArray['-dbname'] || igeConfig.db.database;
		igeConfig.db.port = argArray['-dbport'] || igeConfig.db.port;
		igeConfig.db.host = argArray['-dbhost'] || igeConfig.db.host;
		igeConfig.db.user = argArray['-dbuser'] || igeConfig.db.user;
		igeConfig.db.pass = argArray['-dbpass'] || igeConfig.db.pass;
	} else {
		// Remove the db object and the engine will assume non-persist
		delete igeConfig.db;
	}
	
	// Set config defaults
	igeConfig.mode = argArray['-m'] || igeConfig.mode;
	igeConfig.maxClients = argArray['-mc'] || igeConfig.maxClients;
	
	igeConfig.dir_root = argArray['-ige'] || igeConfig.dir_root;
	igeConfig.dir_node_modules = argArray['-nm'] || igeConfig.dir_node_modules;
	igeConfig.dir_users = argArray['-up'] || igeConfig.dir_users;
	igeConfig.dir_engine = argArray['-ep'] || igeConfig.dir_engine;
	igeConfig.dir_engine_modules = argArray['-mp'] || igeConfig.dir_engine_modules;
	igeConfig.dir_node_modules = argArray['-nm'] || igeConfig.dir_node_modules;
	
	// Set the game config settings based upon the command line arguments
	igeConfig.dir_game_root = igeConfig.dir_users + '/' + sandboxUser + '/' + sandboxGame; // The absolute path to the game folder
	igeConfig.dir_game_client = igeConfig.dir_users + '/' + sandboxUser + '/' + sandboxGame; // The absolute path where the client game files are located
	igeConfig.dir_game_server = igeConfig.dir_users + '/' + sandboxUser + '/' + sandboxGame; // The absolute path where the server game files are located
	igeConfig.dir_game_assets = igeConfig.dir_users + '/' + sandboxUser + '/' + sandboxGame + '/assets'; // The absolute path where the asset files are located
	
	igeConfig.port = sandboxPort;
	
	// Check if we have a complete override for the game script path
	igeConfig.game_script_path = argArray['-gs'] || igeConfig.dir_users + '/' + sandboxUser + '/' + sandboxGame + '/index.js';
	
	igeConfig.init();
	
	// Check for required node modules before proceeding
	if (typeof (igeConfig.nodeModules) == 'object') {
		var modulesMissing = 0;
		
		for (var i in igeConfig.nodeModules) {
			var modName = igeConfig.nodeModules[i];
			if (!path.existsSync(igeConfig.mapUrl('/node_modules/' + modName))) {
				// Module does not exist! Throw an error
				console.log('Error - the node module "' + modName + '" does not exist at ' + igeConfig.mapUrl('/node_modules/' + modName));
				modulesMissing++;
			}
		}
		
		if (modulesMissing) {
			console.log('Total of ' + modulesMissing + ' modules are missing. Exiting!');
			process.exit();
		}
	}
	
	// Check that the index.js exists for the specified user and game combo
	var gameScriptExists = path.existsSync(igeConfig.game_script_path);
	
	if (gameScriptExists) {
		console.log('Starting sandbox instance for game: ' + igeConfig.game_script_path + ' on port ' + sandboxPort);
		
		if (!argArray['-nodb']) {
			igeConfig.db.database = argArray['-dbname'] || sandboxGame;
		}
		
		// onBoot function - Called by the bootstrap process when all files are loaded into memory
		var onBoot = function () {
			
			// Define the sandbox class
			var IgeSandbox = new IgeClass({
				
				engine: null,
				
				// Constructor
				init: function (engine) {
					this._className = 'IgeSandbox';
					this.engine = engine;
					
					// Hook the serverStarted event
					this.engine.on('initVM', this.bind(this.startVm));
					
					// Start the server engine
					this.engine.start(igeConfig);
				},
				
				startVm: function () {
					// Start the sandbox
					this.sandbox = {
						IgeClass: IgeClass,
						ige: this.engine,
						console: console,
						log: console.log,
						require: require,
						
						///////////////
						// Constants //
						///////////////
						
						// Asset Constants
						ASSET_RENDER_MODE_2D: 0,
						ASSET_RENDER_MODE_ISOMETRIC: 1,
						
						// Persist Constants
						PERSIST_DISABLED: false,
						PERSIST_ENABLED: true,
						
						// Locale Constants
						LOCALE_EVERYWHERE: 0,
						LOCALE_ALL_CLIENTS: 1,
						LOCALE_SINGLE_CLIENT: 2,
						LOCALE_SERVER_ONLY: 4,
						LOCALE_RESERVED1: 8,
						LOCALE_RESERVED2: 16,
						LOCALE_RESERVED3: 32,
						LOCALE_DB: 64,
						
						LOCALE_DEFAULT: 0, // Set the default locale setting when none is supplied
						
						// Entity Constants
						ENTITY_TYPE_BACKGROUND: 0,
						ENTITY_TYPE_TILE: 1,
						ENTITY_TYPE_OBJECT: 2,
						ENTITY_TYPE_SPRITE: 3,
						ENTITY_TYPE_UI: 4,
						
						ENTITY_TB_NOBLOCK_NOCHECK: 1,
						ENTITY_TB_NOBLOCK_CHECK: 2,
						ENTITY_TB_BLOCK_NOCHECK: 3,
						ENTITY_TB_BLOCK_CHECK: 4,
						
						NET_MODE_FIXED_MOTION: 0,
						NET_MODE_FREE_MOTION: 1,
						
						// Layer Constants
						LAYER_BACKGROUNDS: 0,
						LAYER_TILES: 1,
						LAYER_OBJECTS: 2,
						LAYER_SPRITES: 2,
						LAYER_UI: 3,
						
						LAYER_AUTO_NONE: 0,
						LAYER_AUTO_CULL: 1,
						LAYER_AUTO_REQUEST: 2,
						
						LAYER_TYPE_CANVAS: 0,
						LAYER_TYPE_HTML: 1,
						LAYER_TYPE_WEBGL: 2,
						LAYER_TYPE_AUTO: 3,
						
						// Map Constants
						MAP_RENDER_MODE_2D: 0,
						MAP_RENDER_MODE_ISOMETRIC: 1,
						
						MAP_NO_DIRTY: 0,
						MAP_USE_DIRTY: 1,
						MAP_DEBUG_DIRTY: 2,
						MAP_DEBUG_ENTITIES: 4,
						
						MAP_TYPE_FLAT: 0,
						MAP_TYPE_POINTS: 1,
						
						// Direction Constants
						DIRECTION_NONE: 0,
						DIRECTION_N: 1,
						DIRECTION_E: 2,
						DIRECTION_S: 4,
						DIRECTION_W: 8,
						DIRECTION_NE: 3,
						DIRECTION_SE: 6,
						DIRECTION_NW: 9,
						DIRECTION_SW: 12,
						
						// Propagate Constants
						PROPAGATE_CREATE: 1,
						PROPAGATE_UPDATE: 2,
						PROPAGATE_DELETE: 3,
						
						// Path Constants
						PATH_TYPE_ENTITY: 0,
						PATH_TYPE_CAMERA: 1,
						
						// Internal Engine Constants
						CLEAN_MOVE_DIR_NONE: 0,
						CLEAN_MOVE_DIR_LEFT: 1,
						CLEAN_MOVE_DIR_RIGHT: 2,
						CLEAN_MOVE_DIR_UP: 3,
						CLEAN_MOVE_DIR_DOWN: 4,
						
						// Tile corner points
						TILE_CORNER_1: 0,
						TILE_CORNER_2: 1,
						TILE_CORNER_3: 2,
						TILE_CORNER_4: 3,
						
						// Sound constants
						SOUND_TYPE_PLAYLIST: 1,
						SOUND_TYPE_TRACK: 2,
						SOUND_TYPE_EFFECT: 3,
						
						SOUND_STATE_LOADING: 1,
						SOUND_STATE_FAILED: 2,
						SOUND_STATE_LOADED: 3,
						
						// Engine states
						ENGINE_STATE_STOPPED: 0,
						ENGINE_STATE_STARTED: 1,
					}
					
					//try {
						// Load the sandboxed script into the sandbox object
						var sandboxSourcePath = igeConfig.game_script_path;
						
						if (path.existsSync(sandboxSourcePath)) {
							vm.runInNewContext(fs.readFileSync(sandboxSourcePath), this.sandbox);
							
							// Create the server script instance and pass the ige as the engine
							if (this.sandbox.GameServer) {
								this.sandbox.server = new this.sandbox.GameServer(ige);
							} else {
								this.sandbox.server = {};
								this.log('Starting VM without a GameServer because it does not exist in the index.js.');
							}
							
							// Create the client script instance and pass the ige as the engine
							this.sandbox.client = {}; //new GameClient(ige);
							
							// Create the game script instance and pass the client and server instance variables
							this.sandbox.game = new this.sandbox.Game(this.sandbox.ige, this.sandbox.client, this.sandbox.server);
							
							//this.sandbox.game.bind = this.bind;
							this.sandbox.game.log = this.log;
							
							// Setup network settings in the sandbox
							this.safeCall(this.sandbox.game, 'networkInit');
							
							// Hook the networkProviderUp method
							var netReadyMethod = this.bind(function () {
								// Setup engine hooks in the sandbox
								this.safeCall(this.sandbox.game, 'engineHooks');
								
								// Register the game network commands
								this.safeCall(this.sandbox.game, 'networkCommands');
								
								// Register the game network properties
								this.safeCall(this.sandbox.game, 'networkProperties');
								
								// Load the sandbox modules
								this.safeCall(this.sandbox.game, 'modules');
								
								// Setup engine hooks in the sandbox
								this.safeCall(this.sandbox.game, 'moduleHooks');
								
								// Hook the data loaded event and then call the data method
								this.engine.on('_loadDataFinished', this.bind(function () {
									// Check if the assets class has work to do
									if (this.engine.assets.imagesLoading < 1) {
										// Assets has no work so call ready immediately
										this.log('Calling game script ready method...');
										this.safeCall(this.sandbox.game, 'ready');
									} else {
										// Assets still has work to do so wait for the allAssetsLoaded event
										this.engine.assets.on('allAssetsLoaded', this.bind(function () {
											this.log('Calling game script ready method...');
											this.safeCall(this.sandbox.game, 'ready');
										}), true);
									}
								}), true);
								
								this.safeCall(this.sandbox.game, 'data');
							});
							
							if (typeof(this.sandbox.game.networkInit) == 'function') {
								this.engine.on('networkProviderUp', netReadyMethod);
							} else {
								netReadyMethod();
							}
						} else {
							this.log('Cannot load game script because the file does not exist at ' + sandboxSourcePath, 'error');
							process.exit();
						}
					//} catch (err) {
						// VM could not start external script, display error to user
						//this.log('External script error: ' + err);
						//console.log(err.stack);
					//}
				},
				
			});
			
			// Create a new engine instance
			var ige = new IgeEngine({config:igeConfig});
			
			// Start the image generator
			imgGen.start(ige);
			imgGen.startImgGenServer(igeConfig);
			
			// Create the new instance of the game control class, passing the engine as the parameter
			var igeSandbox = new IgeSandbox(ige);
		
		}
		
		require(igeConfig.dir_engine + '/IgeBootstrap');
		igeBootstrap = new IgeBootstrap(onBoot);
		igeBootstrap.init(igeConfig.serverDeploy);
		igeBootstrap.require(igeConfig.dir_node_modules + '/ige_image_gen', 'imgGen');
		igeBootstrap.process();
	} else {
		console.log('Cannot start sandbox, specified game does not exist or game\'s index.js does not exist at ' + igeConfig.dir_users + '/' + sandboxUser + '/' + sandboxGame + '/index.js');
		process.exit();
	}
} else {
	console.log('Usage: node index.js -u user -g game -p port [options] [paths]');
	console.log('');
	console.log('*required*');
	console.log('\t-u user\t\tThe user folder to load the game from');
	console.log('\t-g game\t\tThe game folder to load the game\'s index.js file from');
	console.log('\t-p port\t\tThe port to run the game server on');
	console.log('');
	console.log('\tE.G. To run the game called iso_city for the user rob@irrelon.com on\n\tport 9001 you would type:');
	console.log('');
	console.log('\tnode index.js -u rob@irrelon.com -g iso_city -p 9001');
	console.log('');
	console.log('All flags below will override any values specified in the config.js file');
	console.log('[options]');
	console.log('\t-c path\t\tPath to config.js (default: ./config.js)');
	console.log('\t-m mode\t\tMode to start in: debug or release (default: release)');
	console.log('\t-mc num\t\tMax number of clients to allow (default: 1000)');
	console.log('');
	console.log('[paths]');
	console.log('\t-ige path\tPath to ige root (default: /ige)');
	console.log('\t-nm path\tPath to node_modules (default: /ige/node_modules)');
	console.log('\t-up path\tPath to users (default: /ige/users)');
	console.log('\t-ep path\tPath to engine (default: /ige/engine)');
	console.log('\t-mp path\tPath to engine modules (default: /ige/engine/modules)');
	// -gs is not fully implemented yet, do not log it
	//console.log('\t-gs path\tPath to the game script to run, overrides user details');
	console.log('');
	console.log('[db]');
	console.log('\t-dbname name\tThe database name');
	console.log('\t-dbhost host\tThe database server host domain or IP');
	console.log('\t-dbport port\tThe database server port');
	console.log('\t-dbuser user\tThe database username');
	console.log('\t-dbpass pass\tThe database password');
	
	process.exit();
}