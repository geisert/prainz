///////////////////////////////////////////////////////////////////////////////////////
// This is the server configuration file for the Isogenic Game Engine Node.js Server //
///////////////////////////////////////////////////////////////////////////////////////

// Define the global configuration object
igeConfig = {
	
	// This init method provides a good place to execute any code specific to the config
	init: function () {
		// Configure regular expression searches to map urls to internal paths
		this.requestMapping = [
			{name: 'engine', regEx: /^\/engine\//gi, mapTo: this.dir_engine + '/'},
			{name: 'assets', regEx: /^\/assets\//gi, mapTo: this.dir_game_assets + '/'},
			{name: 'modules', regEx: /^\/modules\//gi, mapTo: this.dir_engine_modules + '/'},
			{name: 'nodeModules', regEx: /^\/node_modules\//gi, mapTo: this.dir_node_modules + '/'},
			{name: 'client', regEx: /^\/client\//gi, mapTo: this.dir_game_client + '/'},
			{name: 'client', regEx: /^\//gi, mapTo: this.dir_game_client + '/'},
		];
	},
	
	mapUrl: function (url, name) {
		for (var i in this.requestMapping) {
			var map = this.requestMapping[i];
			if (url.match(map.regEx)) {
				if (name) {
					return [map.name, url.replace(map.regEx, map.mapTo), map.mapTo];
				} else {
					return url.replace(map.regEx, map.mapTo);
				}
			}
		}
	},
	
	///////////////////////////////////////////////////////////////////////////////////////
	// Required node.js modules
	///////////////////////////////////////////////////////////////////////////////////////
	nodeModules: [          
		'canvas',
		'cli-color',
		'express',
		'facebook-graph',
		'forever',
		'http-proxy',
		'imagemagick',
		'ige_image_gen',
		'mkdirp',
		'mongodb',
		'mysql',
		'node-static',
		'pusher-pipe',
		'rimraf',
		'socket.io',
		'uglify-js',
		'wrench',
	],
	
	///////////////////////////////////////////////////////////////////////////////////////
	// Server Configuration
	///////////////////////////////////////////////////////////////////////////////////////
	sandbox: false, // Set to true to disable all external libraries and go in to sandbox mode
	
	version:'0.2.5', // Client must match to connect and authorise
	mode:'release', // Either 'debug' or 'release'. In debug mode, scripts are not obfuscated and server code is not removed
	
	// Root and node.js
	dir_root:'/ige', // The absolute path where the IGE is located
	dir_node_modules:'/ige/node_modules', // The absolute path where the node modules are located
	dir_users:'/ige/users', // The absolute path to the users folder (usually /[dir_root]/users)
	
	// Engine
	dir_engine:'/ige/engine', // The absolute path where the engine files are located
	dir_engine_modules:'/ige/engine/modules', // The absolute path where modules are located
	
	// HTTP server
	serveClient: '/', // The virtual root to serve the client files from. Calls to this folder will be routed to the dir_game_client folder.
	serveAssets: '/assets/', // The virtual root to serve the asset files from. Calls to this folder will be routed to the dir_game_root + '/assets' folder.
	serveEngine: '/engine/', // The virtual root to serve the engine files from. Calls to this folder will be routed to the dir_engine folder.
	
	port: 3000, // The port that you want to listen to socket connections from clients on
	maxClients: 1000, // Max number of client game connections to accept at once
	
	// Turn on or off server console debugging
	debug: true,
	debugLevel: ['log', 'info', 'warning', 'error'],
	debugBreakOnError: true, // Set to true to terminate the engine runtime when an engine error is thrown
	
	// Node.js File Server Configuration
	serveFile: [],
	
	///////////////////////////////////////////////////////////////////////////////////////
	// Database configuration
	///////////////////////////////////////////////////////////////////////////////////////
	
	// Mongo conection details
	db: {
		type:'mongo',
		host:'localhost',
		database:'iso_city',
		strict:false,
		nativeParser:false,
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Server Engine Deployment Files - Used by the server-engine to load all the required files
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	serverDeploy: [
		// Libraries
		'/ige/engine/lib_json',
		'/ige/engine/lib_bison',
		// Engine Core classes
		'/ige/engine/IgeConstants',
		'/ige/engine/IgeClass',
		'/ige/engine/IgeBase',
		'/ige/engine/IgeEnum',
		'/ige/engine/IgeEvents',
		'/ige/engine/IgeCollection',
		'/ige/engine/IgeItem',
		// Networking classes
		'/ige/engine/IgeNetwork2',
		'/ige/engine/IgeNetwork2_Packet',
		'/ige/engine/IgeNetwork2_FileServer',
		'/ige/engine/IgeNetwork2_GameServer',
		'/ige/engine/IgeNetworkItem',
		'/ige/engine/IgeNetworkProvider_SocketIO',
		'/ige/engine/IgeNetworkProvider_Pusher',
		'/ige/engine/IgeNetworkProvider_Offline',
		// Database etc
		'/ige/engine/IgeDatabase',
		'/ige/engine/IgeDbMongo',
		'/ige/engine/IgeObfuscate',
		// Engine Game classes
		'/ige/engine/IgeTemplates',
		'/ige/engine/IgeAnimations',
		'/ige/engine/IgePaths',
		'/ige/engine/IgeUsers',
		'/ige/engine/IgeAssets',
		'/ige/engine/IgeEntities',
		'/ige/engine/IgeScreens',
		'/ige/engine/IgeDirtyRects',
		'/ige/engine/IgeMaps',
		'/ige/engine/IgeCameras',
		'/ige/engine/IgeViewports',
		'/ige/engine/IgeBackgrounds',
		'/ige/engine/IgeRenderer',
		'/ige/engine/IgeTime',
		'/ige/engine/IgeIdFactory',
		'/ige/engine/IgePallete',
		'/ige/engine/IgeWindow',
		'/ige/engine/IgeEngine'
	],
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Client Engine Deployment Files - Used to generate a single file deployment of the client-side engine
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
 	clientDeploy: [
		// Libraries
		'/engine/lib_json',
		'/engine/lib_bison',
		'/engine/lib_stack',
		// Engine Core Classes
		'/engine/IgeConstants',
		'/engine/IgeClass',
		'/engine/IgeBase',
		'/engine/IgeEnum',
		'/engine/IgeEvents',
		'/engine/IgeCollection',
		'/engine/IgeItem',
		// Networking classes
		'/engine/IgeNetwork2',
		'/engine/IgeNetwork2_Packet',
		'/engine/IgeNetwork2_GameClient',
		'/engine/IgeNetworkItem',
		'/engine/IgeNetworkProvider_SocketIO',
		'/engine/IgeNetworkProvider_Pusher',
		'/engine/IgeNetworkProvider_Offline',
		// Render classes
		'/engine/IgeCanvas',
		'/engine/IgeHtml',
		// Engine Game Classes
		'/engine/IgeTemplates',
		'/engine/IgeAnimations',
		'/engine/IgePaths',
		'/engine/IgeUsers',
		'/engine/IgeAssets',
		'/engine/IgeEntities',
		'/engine/IgeScreens',
		'/engine/IgeDirtyRects',
		'/engine/IgeMaps',
		'/engine/IgeCameras',
		'/engine/IgeViewports',
		'/engine/IgeBackgrounds',
		'/engine/IgeRenderer',
		'/engine/IgeTime',
		'/engine/IgeIdFactory',
		'/engine/IgePallete',
		'/engine/IgeWindow',
		'/engine/IgeEngine',
		'/client/index',
	],
	
}