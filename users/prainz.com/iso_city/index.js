// Declare your global debug settings here (only if we are on the client)
if (typeof(window) != 'undefined') {
	window.igeDebug = true;
	window.igeDebugLog = [];
	//window.igeDebugLevel = ['info', 'warning', 'error', 'log'];
	window.igeDebugLevel = ['info'];
	window.igeDebugBreakOnError = true;
}

// Your game code goes that runs on both client AND server goes inside this class
Game = new IgeClass({
	
	ige: null,
	client: null,
	server: null,

	init: function (engine, client, server) {
		this._className = 'Game';
		this.ige = engine;
		this.client = client;
		this.server = server;
	},
	
	///////////////////////////////////////////////////////////////////////////////////////////
	// START OF REQUIRED METHODS
	///////////////////////////////////////////////////////////////////////////////////////////
	networkInit: function () {
		this.log('networkInit');
		
		this.ige.network.useBison(false);
		this.ige.network.useManifest(true);
		this.ige.network.setProvider('socketio');
		this.ige.network.debug = false;
		
		switch (this.ige.network.networkProvider) {
			case 'offline':
				this.ige.network.providerInit();
			break;
			
			case 'socketio':
				if (this.ige.isServer) {
					this.ige.network.providerInit();
					this.ige.network.start();
				}
				if (!this.ige.isServer) {
					this.ige.network.useStats(true);
					this.ige.network.providerInit();
				}
			break;
			
			case 'pusher':
				if (this.ige.isServer) {
					this.ige.network.setOptions({
						key: '',
						secret: '',
						app_id: 0,
						debug: false,
					});
					this.ige.network.providerInit();
					this.ige.network.start();
				}
				
				if (!this.ige.isServer) {
					this.ige.network.setOptions({
						key: '',
						host:'ws.darling.pusher.com',
						port:'80',
						sslPort:'443',
					});
					this.ige.network.useStats(true);
					this.ige.network.providerInit();
				}
			break;
		}
		
		// Add a load of manifest data to speed up networking
		this.ige.network.addToManifest([
			'bakery1',
			'bakery1Iso',
			'bank1',
			'bank1Iso',
			'burgerShop1',
			'burgerShop1Iso',
			'butcherShop1',
			'butcherShop1Iso',
			'cdStore1',
			'cdStore1Iso',
			'casino1',
			'casino1Iso',
			'clothesStore1',
			'clothesStore1Iso',
			'clothesStore2',
			'clothesStore2Iso',
			'coffeeShop1',
			'coffeeShop1Iso',
			'factory1',
			'factory1Iso',
			'hospital1',
			'hospital1Iso',
			'house1',
			'house1Iso',
			'house2',
			'house2Iso',
			'newsAgent1',
			'newsAgent1Iso',
			'office1',
			'office1Iso',
			'pizza1',
			'pizza1Iso',
			'restaurant1',
			'restaurant1Iso',
			'shoeStore1',
			'shoeStore1Iso',
			'stadium1',
			'stadium1Iso',
			'tower2',
			'tower2Iso',
			'travelAgent1',
			'travelAgent1Iso',
			'grassBackground',
			'road',
			'roadSheet',
			'womanWalk',
			'woman_sheet2',
			'tilePavement',
			'dirtSheet',
			'mapView',
			'testMap1',
		]);
	},
	
	// This is called when the engine wants you to hook engine events
	engineHooks: function () {
		/* CEXCLUDE */
		if (this.ige.isServer) {
			this.server.engineHooks();
		}
		/* CEXCLUDE */
		if (!this.ige.isServer) {
			this.client.engineHooks();
		}
	},
	
	// This is called when the engine wants you to register all your network commands
	networkCommands: function () {
		////////////////////////////////////////////////
		// Create some game specific network commands //
		////////////////////////////////////////////////
		
		// Client listeners
		this.ige.network.registerCommand('hardRefreshGame', this.bind(this._hardRefreshGame), false);
		this.ige.network.registerCommand('requestLogin', this.bind(this._requestLogin), false);
		this.ige.network.registerCommand('clientLoggedIn', this.bind(this._clientLoggedIn), false);
		this.ige.network.registerCommand('entityCount', this.bind(this._entityCount), false);
		this.ige.network.registerCommand('allDone', this.bind(this.allDone), false);
		this.ige.network.registerCommand('placeItemFailed', this.bind(this._placeItemFailed), false);
		this.ige.network.registerCommand('moveItemFailed', this.bind(this._moveItemFailed), false);
		this.ige.network.registerCommand('deleteItemFailed', this.bind(this._deleteItemFailed), false);
		this.ige.network.registerCommand('assetsLoaded', this.bind(this._assetsFinishedStream), this.bind(this._clientAssetsLoaded, true));
		
		// Server listeners
		this.ige.network.registerCommand('gameData', this.bind(this._gameData));
		this.ige.network.registerCommand('reconnect', this.bind(this._clientReconnect));
		this.ige.network.registerCommand('placeItem', null, this.bind(this._placeItem, true));
		this.ige.network.registerCommand('moveItem', null, this.bind(this.server._moveItem, true));
		this.ige.network.registerCommand('deleteItem', this.bind(this.server._deleteItem, true));
	},
	
	// This is called when the engine wants you to load all your modules
	modules: function () {
		/* CEXCLUDE */
		if (this.ige.isServer) {
			this.server.modules();
		}
		/* CEXCLUDE */
	},
	
	// This is called when the engine wants you to hook module events
	moduleHooks: function () {
		/* CEXCLUDE */
		if (this.ige.isServer) {
			this.server.moduleHooks();
		}
		/* CEXCLUDE */
	},
	
	// This is called when the engine wants you to load all your data
	data: function () {
		/* CEXCLUDE */
		if (this.ige.isServer) {
			this.server.data();
		}
		/* CEXCLUDE */
	},
	
	// This is called when the engine is ready to use
	ready: function () {
		/* CEXCLUDE */
		if (this.ige.isServer) {
			this.server.ready();
		}
		/* CEXCLUDE */
		
		if (!this.ige.isServer) {
			this.client.ready();
		}
	},
	///////////////////////////////////////////////////////////////////////////////////////////
	// END OF REQUIRED METHODS
	///////////////////////////////////////////////////////////////////////////////////////////
	
	_hardRefreshGame: function () {
		this.client._hardRefreshGame.apply(this.client, arguments);
	},
	
	_requestLogin: function () {
		this.client._requestLogin.apply(this.client, arguments);
	},
	
	_clientLoggedIn: function () {
		this.client._clientLoggedIn.apply(this.client, arguments);
	},
	
	_entityCount: function () {
		this.client._entityCount.apply(this.client, arguments);
	},
	
	allDone: function () {
		this.client.allDone.apply(this.client, arguments);
	},
	
	_placeItemFailed: function () {
		this.client._placeItemFailed.apply(this.client, arguments);
	},
	
	_moveItemFailed: function () {
		this.client._moveItemFailed.apply(this.client, arguments);
	},
	
	_deleteItemFailed: function () {
		this.client._deleteItemFailed.apply(this.client, arguments);
	},
	
	_assetsFinishedStream: function () {
		this.client._assetsFinishedStream.apply(this.client, arguments);
	},
	
	// Server
	_gameData: function () {
		this.server._gameData.apply(this.server, arguments);
	},
	
	_clientReconnect: function () {
		this.server._clientReconnect.apply(this.server, arguments);
	},
	
	_clientAssetsLoaded: function () {
		this.server._clientAssetsLoaded.apply(this.server, arguments);
	},
	
	_placeItem: function () {
		this.server._placeItem.apply(this.server, arguments);
	},
	
	_moveItem: function () {
		this.server._moveItem.apply(this.server, arguments);
	},
	
	_deleteItem: function () {
		this.server._deleteItem.apply(this.server, arguments);
	},
	
});

// Define the global server instance variable
server = {};

/* CEXCLUDE */
// Your server-specific code goes in this class
GameServer = new IgeClass({
	ige: null,
	
	init: function (engine) {
		this._className = 'GameServer';
		this.ige = engine;
	},
	
	///////////////////////////////////////////////////////////////////////////////////////////
	// START OF REQUIRED METHODS
	///////////////////////////////////////////////////////////////////////////////////////////
	engineHooks: function () {
		// Network events
		this.ige.network.on('clientConnect', this.bind(this.clientConnect));
		this.ige.network.on('clientDisconnect', this.bind(this.clientDisconnect));
	},
	
	moduleHooks: function () {
		this.ige.facebook.on('loggedIn', this.bind(this._clientLogin));
	},
	
	modules: function () {
		// Ask the engine to load the modules we want to use
		this.ige.includeModule('IgeFacebook');
		//this.ige.includeModule('IgeSound');
	},
	
	// This is called when the engine wants you to load all your data
	data: function () {
		this.ige.loadData({
			'templates':{},
			'screens':{},
			'animations':{},
			'assets':{},
			'users':{},
			'entities':{}
		});
		
		this.setupWorldData();
	},
	
	// This is called when the engine is ready to use
	ready: function () {
		// Setup out world in script
		this.setupWorld();
		
		//this.ige.entities.saveEntityDataAsJson('testMap1', '/client/testMap1');
		
		// Set the server to allow client connections
		this.ige.network.allowConnections(true);
	},
	
	///////////////////////////////////////////////////////////////////////////////////////////
	// END OF REQUIRED METHODS
	///////////////////////////////////////////////////////////////////////////////////////////
	
	// Called when the clientConnect event is fired by the network class
	clientConnect: function (sessionId) {
		this.log('-----------------------------------------------------');
		this.log('Client Connected with session: ' + sessionId);
		this.log('-----------------------------------------------------');
		
		// Create client object storage
		this.igeStore = this.igeStore || [];
		this.igeStore[sessionId] = this.igeStore[sessionId] || {};
	},
	
	_clientReconnect: function () {
		this.log('A client is trying to reconnect from a lost connection');
		
		// Send the client a message telling them to do a complete refresh
		this.ige.network.send('hardRefreshGame');
	},
	
	_gameData: function (data, sessionId) {
		this.log('Client requesting game data...');
		if (!this.igeStore[sessionId]._clientAssetsLoaded) {
			// Send the client all the game data that all clients require
			// when all the client's assets have loaded the _clientAssetsLoaded
			// will be called
			this.log('Sending all game data to client...');
			this.ige.templates.netSendAll(sessionId);
			
			if (this.ige.assets.byIndex.length) {
				this.ige.assets.netSendAll(sessionId);
				this.ige.network.send('assetsLoaded', null, sessionId);
			} else {
				this.ige.network.send('assetsLoaded', null, sessionId);
				this._clientAssetsLoaded({}, sessionId);
			}
		} else {
			this.log('Client ' + sessionId + ' is requesting game data but has already been sent it, telling client to refresh browser.');
			this.ige.network.send('hardRefreshGame');
		}
	},
	
	_clientAssetsLoaded: function (data, sessionId) {
		// Mark this client as having received the startup objects
		// which will stop subsequent "assetsLoaded" events from sending
		// all this data again
		this.igeStore[sessionId]._clientAssetsLoaded = true;
		
		// Tell the client to load all the engine modules in use
		this.ige.sendClientModuleList(sessionId);
		
		this.log('Sending camera data...');
		this.ige.cameras.netSendAll(sessionId);
		this.ige.animations.netSendAll(sessionId);
		this.ige.screens.netSendAll(sessionId);
		this.ige.maps.netSendAll(sessionId);
		this.ige.viewports.netSendAll(sessionId);
		
		// Ask the client to display the login screen
		this.log('Asking client to display login screen');
		this.ige.network.send('requestLogin', null, sessionId);
	},
	
	_clientLogin: function (data, sessionId) {
		this.log('Client Facebook login success');
		
		var details = data.details;
		
		var userData = {
			user_id: details.id,
			user_first_name: details.first_name,
			user_last_name: details.last_name,
			user_profile_link: details.link,
			user_username: details.username,
			user_birthday: details.birthday,
			user_gender: details.gender,
			user_email: details.email,
			user_timezone: details.timezone,
			user_geo_locale: details.locale,
			user_first_name: details.first_name,
			user_locale: LOCALE_SERVER_ONLY + LOCALE_DB,
			user_persist: true,
			user_logged_in: true,
			session_id: sessionId
		};
		
		var tempUser = this.ige.users.read(userData);
		
		if (!tempUser) {
			// This is a new user so give them some cash!
			userData.cashBalance = 5000;
			
			this.ige.users.create(userData, this.bind(function (user) {
				if (user) {
					this.log('User created successfully', 'log', user);
					this.ige.network.send('clientLoggedIn', null, sessionId);
					this.clientLoggedIn(sessionId);
				} else {
					this.log('User didnt create', 'error', user);
				}
			}));
		} else {
			// The user already exists, update the user's session and logon status
			this.ige.users.update(tempUser, {session_id:sessionId, user_logged_in: true}, this.bind(function (data) {
				this.ige.network.send('clientLoggedIn', null, sessionId);
				this.clientLoggedIn(sessionId);
			}));
		}
	},
	
	// Client logged in
	clientLoggedIn: function (sessionId) {
		// Send all the required game data to the client engine
		this.ige.backgrounds.netSendAll(sessionId);
		
		this.ige.network.send('entityCount', this.ige.entities.byIndex.length, sessionId);
		this.ige.entities.netSendAll(sessionId);
		
		// Switch to the loading screen
		this.ige.screens.setCurrent('mapView', sessionId);
		
		// Tell the client to start engine
		this.ige.startClient(sessionId);
		
		// Tell all clients to add this client
		this.ige.network.send('clientConnect', null, sessionId);
		
		// Tell the client it is ready to run!
		this.ige.network.send('allDone', null, sessionId);
	},
	
	// What to do when a client disconnects
	clientDisconnect: function (sessionId) {
		// Tell all clients to remove this client and it's associated non-persistent stuff
		this.ige.network.send('clientDisconnect', sessionId);
		
		// Remove all the stuff from the server engine data
		this.ige.entities.removeBySearch({session_id:sessionId});
		
		// Remove any non-persistent entities for the disconnecting client
		this.ige.database.remove('entity', {entity_persist:PERSIST_DISABLED, session_id:sessionId});
		
		// Remove client from users
		var tempUser = this.ige.users.readBySessionId(sessionId);
		if (tempUser && tempUser.user_id) {
			this.ige.users.update(tempUser, {user_logged_in: false}, this.bind(function (data) {
				this.log('User successfully logged out and DB updated: ' + tempUser.user_id);
			}));
		}
	},
	
	/* _placeItem - Client has sent a request to place an item. */
	_placeItem: function (data, sessionId) {
		/* Psuedo-code of method
		1: Retrieve the cost for this placement
		2: Check if the player has enough money to make this placement
		3: If yes
			1: Deduct the money from the player's cash
			2: Create the placement entity
		4: If no
			1: Send network message back to client rejecting the placement
		*/
		var user = this.ige.users.readBySessionId(sessionId);
		
		if (user && user.user_id) {
			var placeX = data[0];
			var placeY = data[1];
			var templateId = data[2];
			var sheetId = data[3];
			
			// Retrieve the cost for this placement
			var cost = 0;
			
			// Check if the player has enough money to make this placement
			var money = this.ige.getData(user, 'cashBalance') || 0;
			
			if (money >= cost) {
				// Deduct the money from the player's cash
				money -= cost;
				this.ige.putData(user, 'cashBalance', money)
				
				// Create the entity
				this.ige.entities.create({
					template_id: templateId,
					entity_x: placeX,
					entity_y: placeY,
					entity_locale: LOCALE_EVERYWHERE + LOCALE_DB,
					entity_persist: PERSIST_ENABLED,
					asset_sheet_frame: sheetId,
					map_id: 'testMap1',
					user_id: user.user_id,
					//propagate_trace: true,
					entity_net_mode: NET_MODE_FREE_MOTION,
				});
			} else {
				// The user doesn't have enough money to build this item!
				
			}
		}
	},
	
	/* _moveItem - Client has sent a request to move an item. */
	_moveItem: function (data, sessionId) {
		var user = this.ige.users.readBySessionId(sessionId);
		//console.log('Move action requested from ' + sessionId, data);
		if (user && user.user_id) {
			var itemId = data[0];
			var placeX = data[1];
			var placeY = data[2];
			
			var entity = this.ige.entities.read(itemId);
			
			// Check that we found an entity and that the entity is owned by the user
			if (entity && entity.entity_id && entity.user_id == user.user_id) {
				// Move the entity
				//console.log('Moving entity', entity, placeX, placeY);
				entity.entity_net_mode = NET_MODE_FREE_MOTION;
				this.ige.entities.moveToTile(entity, placeX, placeY);
				delete entity.propagate_trace;
				// Update the entity in the database
				this.ige.entities.update(entity);
			} else {
				// Send a move failure command to the client
				console.log('Cannot move entity!', entity, user);
			}
		} else {
			// User is not logged in to the game
			console.log('Attempt to move entity from non-logged in user', user, data, sessionId);
		}
	},
	
	/* _deleteItem - Client has sent a request to delete an item. */
	_deleteItem: function (data, sessionId) {
		var user = this.ige.users.readBySessionId(sessionId);
		
		if (user && user.user_id) {
			var itemId = data[0];
			
			var entity = this.ige.entities.read(itemId);
			
			// Check that we found an entity and that the entity is owned by the user
			if (entity && entity.entity_id && entity.user_id == user.user_id) {
				// Delete the entity
				this.ige.entities.remove(entity);
			} else {
				// Send a delete failure command to the client
				//this.log('Delete failed!');
			}			
		} else {
			this.log('Attempted to delete object not owned by this player', 'info', [data, entity, user]);
		}
	},
	
	setupWorldData: function () {
		this.createAssetAndTemplate('bakery1', 'iso', [97, 66], 4, 4, 0.79);
		this.createAssetAndTemplate('bank1', 'iso', [98, 99], 4, 4, 0.73);
		this.createAssetAndTemplate('burgerShop1', 'iso', [93, 59], 4, 4, 0.82);
		this.createAssetAndTemplate('butcherShop1', 'iso', [103, 45], 4, 4, 0.83);
		this.createAssetAndTemplate('cdStore1', 'iso', [102, 50], 4, 4, 0.82);
		this.createAssetAndTemplate('casino1', 'iso', [97, 105], 4, 4, 0.86);
		this.createAssetAndTemplate('clothesStore1', 'iso', [111, 49], 4, 5, 1);
		this.createAssetAndTemplate('clothesStore2', 'iso', [109, 94], 4, 4, 0.78);
		this.createAssetAndTemplate('coffeeShop1', 'iso', [96, 69], 4, 4, 0.75);
		this.createAssetAndTemplate('factory1', 'iso', [149, 70], 6, 6, 1);
		this.createAssetAndTemplate('hospital1', 'iso', [101, 51], 4, 4, 1);
		this.createAssetAndTemplate('house1', 'iso', [87, 54], 4, 3, 1);
		this.createAssetAndTemplate('house2', 'iso', [102, 45], 5, 5, 0.83);
		this.createAssetAndTemplate('newsAgent1', 'iso', [412, 257], 4, 8, 0.85);
		this.createAssetAndTemplate('office1', 'iso', [90, 51], 5, 4, 1);
		this.createAssetAndTemplate('pizza1', 'iso', [100, 53], 4, 4, 0.96);
		this.createAssetAndTemplate('restaurant1', 'iso', [111, 59], 4, 5, 1);
		this.createAssetAndTemplate('shoeStore1', 'iso', [101, 101], 4, 4, 0.98);
		this.createAssetAndTemplate('tower2', 'iso', [113, 144], 3, 4, 1);
		this.createAssetAndTemplate('travelAgent1', 'iso', [100, 54], 4, 4, 0.97);		

		this.ige.assets.create({
			"asset_id" : "roadSheet",
			"asset_image_url" : "/assets/tiles/roadSheet.png",
			"asset_sheet_enabled" : true,
			"asset_anchor_points" : [ 
			[ 
			50, 
			12 ] ],
			"asset_sheet_width" : 9,
			"asset_sheet_height" : 1,
			"asset_render_mode" : ASSET_RENDER_MODE_ISOMETRIC,
			"asset_locale" : LOCALE_EVERYWHERE,
			"asset_persist" : PERSIST_DISABLED
		});
		
		this.ige.assets.create({
			"asset_id" : "dirtSheet",
			"asset_image_url" : "/assets/tiles/dirtSheet.png",
			"asset_sheet_enabled" : true,
			"asset_anchor_points" : [ 
			[ 
			50, 
			25 ] ],
			"asset_sheet_width" : 4,
			"asset_sheet_height" : 1,
			"asset_render_mode" : ASSET_RENDER_MODE_ISOMETRIC,
			"asset_locale" : LOCALE_EVERYWHERE,
			"asset_persist" : PERSIST_DISABLED
		});			

		this.ige.assets.create({
			"asset_id" : "woman_sheet2",
			"asset_image_url" : "/assets/people/woman_sheet2_small.png",
			"asset_sheet_enabled" : true,
			"asset_sheet_width" : 9,
			"asset_sheet_height" : 8,
			"asset_anchor_points" : [ 
			[ 
			24, 
			80 ] ],
			"asset_render_mode" : ASSET_RENDER_MODE_ISOMETRIC,
			"asset_locale" : LOCALE_EVERYWHERE,
			"asset_persist" : PERSIST_DISABLED,
			"asset_scale" : 0.3
		});
		
		/*
		this.ige.assets.create({
			asset_id : "grassBackground",
			asset_image_url : "/assets/backgrounds/grassLarge.png",
			asset_sheet_enabled : false,
			asset_anchor_points : [ 
			[ 600, 15 ] ],
			asset_render_mode : ASSET_RENDER_MODE_2D,
			asset_locale: LOCALE_EVERYWHERE,
			asset_persist : PERSIST_DISABLED
		});
		*/
		
		this.ige.assets.create({
			asset_id : "grassRepeat",
			asset_image_url : "/assets/backgrounds/grassRepeat.png",
			asset_sheet_enabled : false,
			asset_anchor_points : [ 
			[ 600, 15 ] ],
			asset_render_mode : ASSET_RENDER_MODE_2D,
			asset_locale: LOCALE_EVERYWHERE,
			asset_persist : PERSIST_DISABLED
		});
		
		/*
		this.ige.templates.create({
			template_id: 'grassBackground',
			template_contents: {
				entity_type:ENTITY_TYPE_BACKGROUND,
				entity_layer:0,
				entity_tile_width:20,
				entity_tile_height:20,
				entity_z:0,
				entity_tile_block: ENTITY_TB_NOBLOCK_NOCHECK,
				asset_id: 'grassBackground',
				map_id: 'testMap1',
			},
		});
		*/
		
		this.ige.templates.create({
			template_id: 'tilePavement',
			template_contents: {
				entity_type:ENTITY_TYPE_TILE,
				entity_layer:1,
				entity_tile_width:1,
				entity_tile_height:1,
				entity_z:0,
				entity_tile_block: ENTITY_TB_BLOCK_CHECK,
				asset_id: 'dirtSheet',
				path_class: ['walk'],
			},
		});
		
		// Create all the data that isn't in the DB at the moment - useful for testing
		this.ige.templates.create({
			template_id: 'road',
			template_contents: {
				entity_type:ENTITY_TYPE_TILE,
				entity_layer:1,
				entity_tile_width:2,
				entity_tile_height:2,
				entity_z:0,
				entity_tile_block: ENTITY_TB_BLOCK_CHECK,
				asset_id: 'roadSheet',
				path_class: ['drive'],
				map_id: 'testMap1',
			},
		});			

		// Create a new animation
		this.ige.animations.create({
			animation_id: 'womanWalkS',
			animation_frame_from: 1,
			animation_frame_to: 9,
			animation_fps: 9,
			animation_loop: true,
		});
		
		this.ige.animations.create({
			animation_id: 'womanWalkSW',
			animation_frame_from: 10,
			animation_frame_to: 18,
			animation_fps: 9,
			animation_loop: true,
		});
		
		this.ige.animations.create({
			animation_id: 'womanWalkW',
			animation_frame_from: 19,
			animation_frame_to: 27,
			animation_fps: 9,
			animation_loop: true,
		});
		
		this.ige.animations.create({
			animation_id: 'womanWalkNW',
			animation_frame_from: 28,
			animation_frame_to: 36,
			animation_fps: 9,
			animation_loop: true,
		});
		
		this.ige.animations.create({
			animation_id: 'womanWalkN',
			animation_frame_from: 37,
			animation_frame_to: 45,
			animation_fps: 9,
			animation_loop: true,
		});
		
		this.ige.animations.create({
			animation_id: 'womanWalkNE',
			animation_frame_from: 46,
			animation_frame_to: 54,
			animation_fps: 9,
			animation_loop: true,
		});
		
		this.ige.animations.create({
			animation_id: 'womanWalkE',
			animation_frame_from: 55,
			animation_frame_to: 63,
			animation_fps: 9,
			animation_loop: true,
		});
		
		this.ige.animations.create({
			animation_id: 'womanWalkSE',
			animation_frame_from: 64,
			animation_frame_to: 72,
			animation_fps: 9,
			animation_loop: true,
		});
		
		this.ige.templates.create({
			template_id: 'womanWalk',
			template_contents: {
				// Entity stuff
				entity_type:ENTITY_TYPE_SPRITE,
				entity_layer:2,
				entity_tile_width:1,
				entity_tile_height:1,
				entity_z:0,
				// Animation stuff
				animation_id: 'womanWalkSE',
				animation_dirty: true,
				// Asset stuff
				asset_sheet_frame:1,
				asset_id: 'woman_sheet2',
				// Map stuff
				map_id: 'testMap1',
			},
		});
		
		// Create splash screen
		this.ige.screens.create({
			screen_id:'splash',
			screen_background_color:'#000',
			screen_html: '/html/mainMenu.html',
			screen_persist:PERSIST_DISABLED,
		});
		
		// Create map screen
		this.ige.screens.create({
			screen_id:'mapView',
			screen_background_color:'#000',
			screen_html: '',
			screen_parent_id: 'mainView',
			screen_persist:PERSIST_DISABLED,
		});
		
		// Create map
		this.ige.maps.create({
			map_id:'testMap1',
			map_tilesize:40,
			map_use_dirty:true,
			map_dirty_grid:false,
			map_debug_dirty:false,
			map_debug_entities:false,
			map_dirty_width:150,
			map_dirty_height:150,
			map_render_mode:MAP_RENDER_MODE_ISOMETRIC,
			map_render:true,
			map_layers:[
				{
					layer_auto_mode:LAYER_AUTO_NONE,
					layer_type:LAYER_TYPE_HTML,
					layer_entity_types: LAYER_BACKGROUNDS
				},
				{
					layer_auto_mode:LAYER_AUTO_NONE, //LAYER_AUTO_CULL + LAYER_AUTO_REQUEST,
					layer_type:LAYER_TYPE_AUTO,
					layer_entity_types: LAYER_TILES,
					//layer_draw_grid: true,
				},
				{
					layer_auto_mode:LAYER_AUTO_NONE, //LAYER_AUTO_CULL + LAYER_AUTO_REQUEST,
					layer_type:LAYER_TYPE_AUTO,
					layer_entity_types: LAYER_SPRITES
				},
				{
					layer_auto_mode:LAYER_AUTO_NONE,
					layer_type:LAYER_TYPE_AUTO,
					layer_entity_types: LAYER_UI
				},
			],
			map_persist:PERSIST_DISABLED,
		});
		
		// Create camera
		this.ige.cameras.create({
			camera_id:'mainCam',
			camera_x: 0,
			camera_y: 0,
			camera_z: 0,
			camera_scale: 1,
			camera_zClipping:{
				near: 0,
				far: 1,
			},
			camera_persist: PERSIST_DISABLED,
		});
		
		// Create viewport
		this.ige.viewports.create({
			viewport_id: 'mainVp',
			viewport_tile_ratio: 0.5,
			viewport_clipping: true,
			/*viewport_background_color:'#005aff',*/
			viewport_anchor_point: [0, 0],
			viewport_autoSize: true,
			viewport_container: { width: 800, height: 600 },
			viewport_locale: LOCALE_EVERYWHERE + LOCALE_DB,
			viewport_persist: PERSIST_DISABLED,
			panLayer:{
				id: 'panLayer',
				padding: 0.5,
				zStart: 0,
			},
			screen_id: 'mapView',
			map_id: 'testMap1',
			camera_id: 'mainCam',
		});
		
		// Create camera
		this.ige.cameras.create({
			camera_id:'cam2',
			camera_x: 0,
			camera_y: 0,
			camera_z: 0,
			camera_scale: 1,
			camera_zClipping:{
				near: 0,
				far: 1,
			},
			camera_persist: PERSIST_DISABLED,
		});
		
		// Create viewport
		this.ige.viewports.create({
			viewport_id: 'miniVp',
			viewport_tile_ratio: 0.5,
			viewport_clipping: true,
			/*viewport_background_color:'#005aff',*/
			viewport_anchor_point: [0, 0],
			viewport_autoSize: false,
			viewport_container: { width: 400, height: 200 },
			viewport_locale: LOCALE_EVERYWHERE + LOCALE_DB,
			viewport_persist: PERSIST_DISABLED,
			viewport_hide:true,
			panLayer:{
				id: 'panLayer',
				padding: 0,
			},
			screen_id: 'mapView',
			map_id: 'testMap1',
			camera_id: 'cam2',
			css_zindex: 10,
		});	
	},
	
	setupWorld: function () {
		var gIndex = 0;
		this.ige.backgrounds.create({
			asset_id: 'grassRepeat',
			map_id: 'testMap1',
			background_id: 'g' + gIndex,
			//background_target: 'vpLayer0',
			background_repeat: 'repeat',
			background_offset_x: -400,
			background_offset_y: -10,
			background_cam_speed_x: 1,
			background_cam_speed_y: 1,
			background_layer: LAYER_BACKGROUNDS,
			background_locale: LOCALE_EVERYWHERE,
		});
		
		// Create the entity
		/*this.ige.entities.create({
			template_id: 'bank1',
			entity_x: 0,
			entity_y: 0,
			entity_locale: LOCALE_EVERYWHERE,
			entity_persist: PERSIST_DISABLED,
			map_id: 'testMap1',
		});*/
		
		this.log('+++++++++++++++++++ All data loaded and ready - Engine online +++++++++++++++++++');
	},
	
	createAssetAndTemplate: function (assetId, mode, anchorPoint, tileWidth, tileHeight, scale) {
		if (!scale) { scale = 1; }
		switch (mode) {
			case 'iso':
				var fileName = assetId;
				var assetId = assetId + 'Iso';
				var renderMode = ASSET_RENDER_MODE_ISOMETRIC;
			break;
			
			case '2d':
				var fileName = assetId;
				var assetId = assetId + '2d';
				var renderMode = ASSET_RENDER_MODE_2D;
			break;
		}
		
		this.ige.assets.create({
			asset_id: assetId,
			asset_image_url: '/assets/buildings/' + fileName + '.png',
			asset_sheet_enabled: false,
			asset_anchor_points: [anchorPoint],
			asset_render_mode: renderMode,
			asset_locale: LOCALE_EVERYWHERE,
			asset_persist: PERSIST_DISABLED,
			asset_scale: scale,
		});
		
		this.ige.templates.create({
			template_id: fileName,
			template_contents: {
				entity_type: ENTITY_TYPE_OBJECT,
				entity_layer: 2,
				entity_tile_width: tileWidth,
				entity_tile_height: tileHeight,
				entity_z: 0,
				entity_tile_block: ENTITY_TB_BLOCK_CHECK,
				asset_id: assetId,
			},
		});
	},
	
});
/* CEXCLUDE */

// Define the global client instance variable
client = {};

// Your client-specific code goes in this class
GameClient = new IgeClass({
	ige: null,
	
	GAME_MM_SELECT: 1,
	GAME_MM_MOVE: 2,
	GAME_MM_DELETE: 3,
	GAME_MM_PLACE: 4,
	GAME_MM_EDIT: 5,
	
	mousePanMode: 0,
	
	init: function (engine) {
		this._className = 'GameClient';
		this.ige = engine;
		
		// Define some UI tool-tip text
		this._tipState = true;
		
		this.tips = [];
		this.tips[0] = [];
		this.tips[0][0] = 'Select Tool';
		this.tips[0][1] = 'Used to select objects';
		
		this.tips[1] = [];
		this.tips[1][0] = 'Move Tool';
		this.tips[1][1] = 'Click an object that you own, then click where you want to move it';
		
		this.tips[2] = [];
		this.tips[2][0] = 'Delete Tool';
		this.tips[2][1] = 'Click an object that you own to delete it';
		
		this.tips[3] = [];
		this.tips[3][0] = 'Pavement Menu';
		this.tips[3][1] = 'Tiles for your little sim people to walk from one place to another';
		
		this.tips[4] = [];
		this.tips[4][0] = 'Road Menu';
		this.tips[4][1] = 'Tiles for road-based transport';
		
		this.tips[5] = [];
		this.tips[5][0] = 'Buildings Menu';
		this.tips[5][1] = 'Houses, offices, shops and stores for your little sim people';
	},
	
	///////////////////////////////////////////////////////////////////////////////////////////
	// START OF REQUIRED METHODS
	///////////////////////////////////////////////////////////////////////////////////////////
	engineHooks: function () {
		// Network events
		this.ige.network.on('serverConnect', this.bind(this.serverConnect));
		this.ige.network.on('serverDisconnect', this.bind(this.serverDisconnect));
		
		this.ige.on('moduleLoaded', this.bind(this._moduleLoaded));
		this.ige.viewports.on('mousedown', this.bind(this.viewportMouseDown));
		this.ige.viewports.on('mousemove', this.bind(this.viewportMouseMove));
		this.ige.viewports.on('mouseup', this.bind(this.viewportMouseUp));
		this.ige.viewports.on('mousewheel', this.bind(this.viewportMouseWheel));
		
		this.ige.entities.on('directionChange', this.bind(this.directionChange));
		this.ige.paths.on('pathComplete', this.bind(this.pathComplete));
	},
	
	ready: function () {
		this.log('Game script ready to run');
		this.ige.network.start();
	},
	///////////////////////////////////////////////////////////////////////////////////////////
	// END OF REQUIRED METHODS
	///////////////////////////////////////////////////////////////////////////////////////////
	serverConnect: function () {
		// We have connected to the server successfully!
		this.log('Connected to server successfully!');
		
		// Are we re-connecting or connecting first-time?
		if (!this._didFullConnect) {
			// We haven't connected before
			this.log('Requesting server game data...');
			this.ige.network.send('gameData');
		} else {
			// We are reconnecting
			this.log('Requesting server reconnection advice...');
			this.ige.network.send('reconnect');
		}
	},
	
	_hardRefreshGame: function () {
		if (!this._doingHardRefresh) {
			this.log('Hard Refresh');
			this._doingHardRefresh = true;
			alert('The game lost its connection to the interwebs! We\'re gonna try and get it back...\n\nClick OK to refresh now!');
			window.location.reload(true);
		}
	},
	
	_entityCount: function (data) {
		// Server has told us how many entities it will send over initially
		// so we can display a progress bar
		this._loadingProgressMax = data;
		this._loadingProgressInterval = setInterval(this.bind(this.updateProgressBar), 200);
	},
	
	updateProgressBar: function () {
		var ratio = this._loadingProgressMax / this.ige.entities.byIndex.length;
		var width = 250 / ratio;
		$('#loadingProgressBar').css('width', width + 'px');
		
		if (ratio <= 1) { clearInterval(this._loadingProgressInterval); }
	},
	
	updateWalkTiles: function () {
		var tiles = false;
		
		if (this.ige.entities.byMapIdAndLayer['testMap1']) {
			tiles = this.ige.entities.byMapIdAndLayer['testMap1'][LAYER_TILES];
		}
		
		if (tiles) {
			// Get an array of tiles that match the "walk" class
			this.walkTiles = [];
			for (var i = 0; i < tiles.length; i++) {
				var tile = tiles[i];
				if (tile.path_class != null) {
					if (tile.path_class.indexOf('walk') > -1) {
						// Tile has walk class
						this.walkTiles.push(tile);
					}
				}
			}
		}
	},
	
	_assetsFinishedStream: function () {
		this.log('-----------------------------------------------------');
		this.log('Asset stream stage 1, waiting for image data...');
		this.log('-----------------------------------------------------');
		this.__allAssetsListener = this.ige.assets.on('allAssetsLoaded', this.bind(this.assetsLoaded));
		
		if (this.ige.assets.imagesLoading < 1) { this.assetsLoaded(); }
	},
	
	assetsLoaded: function () {
		this.ige.assets.cancelOn('allAssetsLoaded', this.__allAssetsListener);

		// All the assets have finished loading so tell the server
		this.log('-----------------------------------------------------');
		this.log('All assets have finished loading, entering stage 2...');
		this.log('-----------------------------------------------------');
		this.ige.network.send('assetsLoaded');
	},
	
	_moduleLoaded: function (moduleName) {
		switch (moduleName) {
			case 'IgeFacebook':
				// Facebook module has loaded so start it with our app details
				this.ige.facebook.start($('body'), '196290723783045', false);
				this.ige.facebook.on('loggedOut', this.bind(this._clientLoggedOut));
				this.ige.facebook.on('userUnknown', this.bind(this._clientUnknown));
				
			break;
		}
	},
	
	_requestLogin: function () {
		// The server has asked us to login
		//$('#loadingMain').css('display', 'none');
		//$('#mainMenu').css('display', 'block');
	},
	
	_clientLoggedIn: function () {
		// Facebook says the user has logged in
		$('#loadingMain').show();
		$('#mainMenu').hide();
		this.ige.facebook.insert('profilePic', '#uiFbControls', 'fbProfilePic');
	},
	
	_clientLoggedOut: function () {
		// Facebook says the user has logged out so
		// now we need to show the player the login screen
		this.log('Facebook says we are logged out, displaying the login screen...');
		this._displayFacebookLogin();
	},
	
	_clientUnknown: function () {
		// Facebook says the user is unknown to our game so
		// now we need to show the player the login screen
		this.log('Facebook says we are unknown, displaying the login screen...');
		this._displayFacebookLogin();
	},
	
	_displayFacebookLogin: function () {
		$('#mainMenu_faceBookLogin').html('');
		this.ige.facebook.insertLoginButton('#mainMenu_faceBookLogin', null, 'email,user_birthday,status_update,publish_stream,user_hometown,user_location');
		
		$('#loadingMain').hide();
		$('#mainMenu').show();
	},
	
	allDone: function () {
		// Set the client's flag to true so if we reconnect we do not request
		// all the game data again
		this._didFullConnect = true;
		
		/*
		$('#loadingMain').fadeTo(1000, 0, function () {
			$('#loadingMain').css('display', 'none');
			clearInterval(window.loadingTextInterval);
		});
		*/
		clearInterval(window.loadingTextInterval);
		$('#loadingMain').hide();
		
		$('#ui').show();
		$('#mainView').show();
		
		if (this.ige.sound != null && this.ige.sound.state()) {
			this.ige.sound.create({
				sound_id: 'backgroundMusic1',
				sound_url: '/assets/audio/back1.mp3',
				sound_volume: 10,
				sound_auto_load: true,
				sound_auto_play: true,
				sound_buffer: 5,
				sound_multi_play: false,
				sound_type: SOUND_TYPE_TRACK,
			}, {
				scope: this,
				onLoaded: function (sound) {
					this.log('Sound module says sound is now completely loaded with id: ' + sound.sound_id);
					//this.ige.sound.play(sound);
				}
			});
		} else {
			this.log('Cannot play sound, sound module is not loaded', 'info', this.ige.sound);
		}
		
		
		// Get all tiles on the tile layer
		var tiles = false;
		
		if (this.ige.entities.byMapIdAndLayer['testMap1']) {
			tiles = this.ige.entities.byMapIdAndLayer['testMap1'][LAYER_TILES];
		}
		
		if (tiles) {
			// Get an array of tiles that match the "walk" class
			this.walkTiles = [];
			for (var i = 0; i < tiles.length; i++) {
				var tile = tiles[i];
				if (tile.path_class != null) {
					if (tile.path_class.indexOf('walk') > -1) {
						// Tile has walk class
						this.walkTiles.push(tile);
					}
				}
			}
			
			// Create some random citizens
			for (var pi = 0; pi < 100; pi++) {
				// Pick a random tile and get it's x and y
				var randomTileIndex = Math.floor((Math.random() * this.walkTiles.length));
				var randomTile = this.walkTiles[randomTileIndex];
				//console.log('Creating new woman');
				this.ige.entities.create({
					template_id: 'womanWalk',
					entity_x:randomTile.entity_x,
					entity_y:randomTile.entity_y,
					entity_locale: LOCALE_EVERYWHERE,
				}, this.bind(function (entity) {
					//console.log('Woman created with ID', entity.entity_id);
					
					if (pi == 0) {
						this.ige.cameras.trackTarget('cam2', entity.entity_id);
					}
					
					if (pi == 1) {
						//this.ige.cameras.trackTarget('cam3', entity.entity_id);
					}
					
					var map = entity.$local.$map;
					this.ige.paths.stopPath(entity, 0);
					this.ige.paths.create(entity);
					
					var startPoint = [entity.entity_x, entity.entity_y];
					//var startPoint = [33, 24];
					
					var randomTileIndex = Math.floor((Math.random() * this.walkTiles.length));
					var randomTile = this.walkTiles[randomTileIndex];
					var endPoint = [randomTile.entity_x, randomTile.entity_y];
					
					var pathPoints = this.ige.paths.localGeneratePath(map, 1, startPoint, endPoint, 'walk', true, false);
					
					//if (pathPoints.length) {
						for (var i = 0; i < pathPoints.length; i++) {
							this.ige.paths.addPathPoint(entity, pathPoints[i][0], pathPoints[i][1], 30);
						}
						
						// Start path processing with autoStop enabled
						this.ige.paths.startPath(entity, PATH_TYPE_ENTITY, new Date().getTime(), true);
					//}
				}));
			}
		}
		
		//setTimeout(this.bind(this.ige.viewports.makeViewportDirty(this.ige.viewports.byIndex[0])), 500);
		
		setInterval(this.bind(this.updateWalkTiles), 1000);
	},
	
	shareOnWall: function () {
		this.ige.facebook.newWallPost({
				name: 'IsoCity - By Irrelon Software',
				link: 'http://bit.ly/qeFyJW',
				picture: 'https://fbcdn-photos-a.akamaihd.net/photos-ak-snc1/v43/87/191864000870979/app_1_191864000870979_96.gif',
				caption: 'A multiplayer city-builder, currently in demo mode.',
				description: 'IsoCity is a new multiplayer city-builder game! It\'s written in JavaScript and uses the Isogenic Game Engine. It\'s still just a demo at the moment so why not check out the beginnings of something awesome?',
				privacy: 'EVERYONE',
				actions: [{name:'Open', link:'http://bit.ly/qeFyJW'}],
			},
			true
		);
	},
	
	toggleMiniVp: function () {
		if ($('#miniVp').css('display') == 'block') {
			this.ige.viewports.byId['miniVp'].viewport_hide = true;
			$('#miniVp').hide();
		} else {
			this.ige.viewports.byId['miniVp'].viewport_hide = false;
			$('#miniVp').show();
		}
	},
	
	toggleViewportClipping: function () {
		if (this.ige.viewports.byIndex[0].viewport_clipping) {
			this.ige.viewports.byIndex[0].viewport_clipping = false;
			this.ige.viewports.makeViewportDirty(this.ige.viewports.byIndex[0]);
			this.log('Viewport clipping disabled');
		} else {
			this.ige.viewports.byIndex[0].viewport_clipping = true;
			this.ige.viewports.makeViewportDirty(this.ige.viewports.byIndex[0]);
			this.log('Viewport clipping enabled');
		}
	},
	
	viewportMouseDown: function (event, touch) {
		event.preventDefault ? event.preventDefault() : event.returnValue = false;
		
		var elementOffset = $('#' + event.viewport.viewport_id).offset();
		var clientX = event.pageX;
		var clientY = event.pageY;
		
		if (event.button == 0) {
			if (!this.mousePanMode) {
				this.mousePanMode = 1;
				this.mousePanStartPosition = [clientX, clientY];
			}
		}
		
	},
	
	viewportMouseMove: function (event) {
		var viewport = event.viewport;
		var elementOffset = $('#' + viewport.viewport_id).offset();
		
		var clientX = event.pageX;
		var clientY = event.pageY;
		
		this.currentPanPort = event.viewport;
		var tileCords = this.ige.renderer.screenToMap[event.map.map_render_mode](clientX - elementOffset.left, clientY - elementOffset.top, event.viewport);
		
		if (document.getElementById('igeCordDiv') != null) {
			$('#igeCordDiv').html(tileCords[0] + ', ' + tileCords[1]);
		}
		
		if (event.button == 0) {
			switch (this.mousePanMode) {
				case 1:
					// Check if the mouse has moved enough to consider it a panning event
					if (Math.abs(this.mousePanStartPosition[0] - clientX) > 4 || Math.abs(this.mousePanStartPosition[1] - clientY) > 4) {
						// The user pressed the mouse-down in SELECT mode then moved the mouse,
						// so now we want to transition into a pan operation
						this.currentPanPort = event.viewport;
						this.ige.viewports.panStart(event.viewport, clientX, clientY);
						
						this.mousePanMode = 2;
						return;
					}
				break;
				
				case 2:
					this.ige.viewports.panTo(this.currentPanPort, clientX, clientY);
					return;
				break;
			}
		}
		
		// Check if we are currently placing an entity and if so, move it to the current tile
		if (this.currentCursor != null && this.currentCursorEntity != null) {
			this.ige.entities.moveToTile(this.currentCursorEntity, tileCords[0], tileCords[1]);
		}
		
		switch (this.mouseMode) {
			case this.GAME_MM_SELECT:
				this.ige.entities.highlightAtTileXY(event.viewport, tileCords[0], tileCords[1]);
			break;
			
			case this.GAME_MM_EDIT:
				if (this.mouseModeEditEntity == null) {
					// Pick up the entity
					var entityArray = this.ige.entities.atTileXY(event.viewport, tileCords[0], tileCords[1]);
					var gotOne = false;
					
					// Loop the array and check for non-sprite entities
					for (var i in entityArray) {
						var entity = entityArray[i][0];
						if (entity.entity_type != ENTITY_TYPE_SPRITE) {
							this.ige.entities.highlightOne(entity);
							gotOne = true;
							break;
						}
					}
					
					if (!gotOne) {
						this.ige.entities.unHighlightAll();
					}
				} else {
					// We already have an entity so alter it's anchor point
					var entity = this.mouseModeEditEntity;
					
					var diffX = this.mouseModeEditStartX - clientX;
					var diffY = this.mouseModeEditStartY - clientY;
					
					entity.$local.$asset.asset_anchor_points[0][0] = this.mouseModeEditEntityAnchor[0] + diffX;
					entity.$local.$asset.asset_anchor_points[0][1] = this.mouseModeEditEntityAnchor[1] + diffY;
					
					this.ige.entities.markDirtyByAssetId(this.mouseModeEditEntity.asset_id);
				}
			break;
			
			case this.GAME_MM_MOVE:
				if (this.mouseModeMoveEntity == null) {
					// Pick up the entity
					var entityArray = this.ige.entities.atTileXY(event.viewport, tileCords[0], tileCords[1]);
					var gotOne = false;
					
					// Loop the array and check for non-sprite entities
					for (var i in entityArray) {
						var entity = entityArray[i][0];
						if (entity.entity_type != ENTITY_TYPE_SPRITE) {
							// Check if we own this item
							if (entity.user_id == this.ige.facebook._user.session.uid) {
								this.ige.entities.highlightOne(entity);
								gotOne = true;
								break;
							}
						}
					}
					
					if (!gotOne) {
						this.ige.entities.unHighlightAll();
					}
				} else {
					// We already have an entity so move it
					this.ige.entities.moveToTile(this.mouseModeMoveEntity, tileCords[0], tileCords[1]);
				}
			break;
			
			case this.GAME_MM_DELETE:
				// Pick up the entity
				var entityArray = this.ige.entities.atTileXY(event.viewport, tileCords[0], tileCords[1]);
				var gotOne = false;
				
				// Loop the array and check for ownership
				for (var i in entityArray) {
					var entity = entityArray[i][0];
					if (entity.entity_type != ENTITY_TYPE_SPRITE) {
						// Check if we own this item
						if (entity.user_id == this.ige.facebook._user.session.uid) {
							this.ige.entities.highlightOne(entity);
							gotOne = true;
							break;
						}
					}
				}
				
				if (!gotOne) {
					this.ige.entities.unHighlightAll();
				}
			break;
		}
		
	},
	
	viewportMouseUp: function (event, touch) {
		var elementOffset = $('#' + event.viewport.viewport_id).offset();
		var clientX = event.pageX;
		var clientY = event.pageY;
		
		var tileCords = this.ige.renderer.screenToMap[event.map.map_render_mode](clientX - elementOffset.left, clientY - elementOffset.top, event.viewport);
		
		if (event.button == 0) {
			
			if (this.mousePanMode == 2) {
				this.ige.viewports.panEnd(this.currentPanPort, clientX, clientY);
				this.mousePanMode = 0;
				return;
			} else {
				// User has clicked on the map so end pre-pan mode
				//this.ige.cameras.lookBy(event.viewport.$local.$camera, clientX + event.viewport.panLayer.centerX, clientY + event.viewport.panLayer.centerY);
				this.mousePanMode = 0;
			}
			
			switch (this.mouseMode) {
				case this.GAME_MM_SELECT:
					this.ige.entities.highlightAtTileXY(event.viewport, tileCords[0], tileCords[1]);
				break;
				
				case this.GAME_MM_EDIT:
					if (this.mouseModeEditEntity == null) {
						// Pick up the entity
						var entityArray = this.ige.entities.atTileXY(event.viewport, tileCords[0], tileCords[1]);
						
						// Loop the array and check for non-sprite entities
						for (var i in entityArray) {
							var entity = entityArray[i][0];
							if (entity.entity_type != ENTITY_TYPE_SPRITE) {
								// Pick up the entity
								this.mouseModeEditEntity = entity;
								this.mouseModeEditEntityAnchor = [entity.$local.$asset.asset_anchor_points[0][0], entity.$local.$asset.asset_anchor_points[0][1]];
								this.mouseModeEditEntityScale = entity.$local.$asset.asset_scale;
								
								this.mouseModeEditStartX = clientX;
								this.mouseModeEditStartY = clientY;
								
								this.ige.entities.unHighlight(this.mouseModeEditEntity);
								this.mouseModeEditEntity.entity_base_draw = [0, '#ff0000'];
								break;
							}
						}				
					} else {
						// We already have an entity so place it back down
						var entity = this.mouseModeEditEntity;
						this.ige.entities.markDirtyByAssetId(this.mouseModeEditEntity.asset_id);
						
						// Output the new anchor and scale data to the console
						console.log(entity.$local.$asset.asset_anchor_points[0][0], entity.$local.$asset.asset_anchor_points[0][1], entity.$local.$asset.asset_scale);
						
						// Clear the selection variables
						delete this.mouseModeEditEntity.entity_base_draw;
						this.mouseModeEditEntity = null;
					}
				break;				
				
				case this.GAME_MM_MOVE:
					if (this.mouseModeMoveEntity == null) {
						// Pick up the entity
						var entityArray = this.ige.entities.atTileXY(event.viewport, tileCords[0], tileCords[1]);
						
						// Loop the array and check for non-sprite entities
						for (var i in entityArray) {
							var entity = entityArray[i][0];
							if (entity.entity_type != ENTITY_TYPE_SPRITE) {
								// Check if we own this item
								if (entity.user_id == this.ige.facebook._user.session.uid) {
									// Pick up the entity
									this.mouseModeMoveEntity = entity;
									this.mouseModeMoveEntityBlocking = entity.entity_tile_block;
									if (entity.entity_tile_block == ENTITY_TB_BLOCK_NOCHECK || entity.entity_tile_block == ENTITY_TB_BLOCK_CHECK) {
										this.ige.entities.unBlockTiles(entity);
										entity.entity_tile_block = ENTITY_TB_NOBLOCK_CHECK;
									}
									
									//this.mouseModeMoveEntity.entity_base_draw = [1, '#0098e4'];
									this.ige.entities.highlight(this.mouseModeMoveEntity);
									
									break;
								}
							}
						}						
					} else {
						// We already have an entity so place it back down
						var entity = this.mouseModeMoveEntity;
						
						// Check if we own this item
						if (entity.user_id == this.ige.facebook._user.session.uid) {
							this.ige.entities.moveToTile(entity, tileCords[0], tileCords[1]);
							
							// Send the move request over the network
							this.ige.network.send('moveItem', [entity.entity_id, entity.entity_x, entity.entity_y]);
							
							// Sort out entity tile blocking
							entity.entity_tile_block = this.mouseModeMoveEntityBlocking;
							if (entity.entity_tile_block == ENTITY_TB_BLOCK_NOCHECK || entity.entity_tile_block == ENTITY_TB_BLOCK_CHECK) {
								this.ige.entities.blockTiles(entity);
							}
							
						}
						
						//delete entity.entity_base_draw;
						this.ige.entities.unHighlight(entity);
						
						// Clear the selection variables
						this.mouseModeMoveEntity = null;
						this.mouseModeMoveEntityBlocking = null;
					}
				break;
				
				case this.GAME_MM_DELETE:
					// Delete the entity
					var entityArray = this.ige.entities.atTileXY(event.viewport, tileCords[0], tileCords[1]);
					
					// Loop the array and check for non-sprite entities
					for (var i in entityArray) {
						var entity = entityArray[i][0];
						if (entity.entity_type != ENTITY_TYPE_SPRITE) {
							// Check if we own this item
							if (entity.user_id == this.ige.facebook._user.session.uid) {
								// Send the move request over the network
								this.ige.network.send('deleteItem', [entity.entity_id]);
								
								// Remove the entity
								this.ige.entities.remove(entity);
							}
						}
					}
				break;
				
				case this.GAME_MM_PLACE:
					if (this.currentCursor && this.currentCursorEntity) {
						this.ige.entities.moveToTile(this.currentCursorEntity, tileCords[0], tileCords[1]);
						if (this.ige.network) {
							this.ige.network.send('placeItem', [this.currentCursorEntity.entity_x, this.currentCursorEntity.entity_y, this.currentCursor[0], this.currentCursor[1]]);
						} else {
							this._placeItem([this.currentCursorEntity.entity_x, this.currentCursorEntity.entity_y, this.currentCursor[0], this.currentCursor[1]]);
						}
						var ccType = this.currentCursor[0];
						var ccSheet = this.currentCursor[1];
						
						this.selectCursor(ccType, ccSheet);
					}
				break;
			}

		}
	},
	
	viewportMouseWheel: function (event, touch) {
		switch (this.mouseMode) {
			case this.GAME_MM_EDIT:
				if (this.mouseModeEditEntity != null) {
					if (event.wheelDelta < 0) {
						this.mouseModeEditEntity.$local.$asset.asset_scale -= 0.01;
					}
					
					if (event.wheelDelta > 0) {
						this.mouseModeEditEntity.$local.$asset.asset_scale += 0.01;
					}
					
					this.ige.entities.markDirtyByAssetId(this.mouseModeEditEntity.asset_id);
				}
			break;
		}
	},
	
	selectMouseMode: function (mode) {
		// Turn off the current mode cleanly first
		switch (this.mouseMode) {
			case this.GAME_MM_SELECT:
				this.selectCursor(null);
				$('#uiMenuButton_select').css('box-shadow', '');
			break;
			
			case this.GAME_MM_EDIT:
				$('#uiMenuButton_select').css('box-shadow', '');
			break;
			
			case this.GAME_MM_MOVE:
				this.selectCursor(null);
				$('#uiMenuButton_move').css('box-shadow', '');
			break;
			
			case this.GAME_MM_DELETE:
				this.selectCursor(null);
				$('#uiMenuButton_delete').css('box-shadow', '');
			break;
			
			case this.GAME_MM_PLACE:
				// We are in place mode so kill the cursor if there is one
				this.selectCursor(null);
			break;
		}
		
		// Assign the new mode
		this.mouseMode = mode;
		
		// Run any mode code now we have a new mode
		switch (this.mouseMode) {
			case this.GAME_MM_SELECT:
				this.log('Selecting cursor mode SELECT');
				$('#uiMenuButton_select').css('box-shadow', '0px 0px 15px 0px #fff');
			break;
			
			case this.GAME_MM_EDIT:
				this.log('Selecting cursor mode EDIT');
				$('#uiMenuButton_select').css('box-shadow', '0px 0px 15px 0px #ff0000');
			break;
			
			case this.GAME_MM_MOVE:
				this.log('Selecting cursor mode MOVE');
				$('#uiMenuButton_move').css('box-shadow', '0px 0px 15px 0px #fff');
			break;
			
			case this.GAME_MM_DELETE:
				this.log('Selecting cursor mode DELETE');
				$('#uiMenuButton_delete').css('box-shadow', '0px 0px 15px 0px #fff');
			break;
		}
	},
	
	selectCursor: function (cursorName, sheetId) {
		// Check that the temp cursor location is not being used
		/*
		var blockingEntity = this.ige.maps.getTileMark('testMap1', -40, -40);
		if (blockingEntity) {
			this.log('Removing temp blocking entity');
			this.ige.entities.remove(blockingEntity);
		}
		*/
		if (this.currentCursorEntity != null) {
			// We have a current entity that has been the temp entity for the previous cursor so remove it
			//this.log('Deselecting cursor');
			var tempCursor = this.currentCursorEntity;
			this.currentCursorEntity = null;
			this.currentCursor = null;
			this.ige.entities.remove(tempCursor);
			this.selectCursor(cursorName, sheetId);
		} else {
			// The cursor needs to change and we need to create a temporary entity that will follow the cursor
			if (this.currentCursorEntity == null && this.currentCursor == null) {
				if (cursorName != null) {
					// Set the mouse-mode
					this.selectMouseMode(this.GAME_MM_PLACE);
					this.log('Selecting new cursor', cursorName, sheetId);
					
					// Create a temporary entity that will represent the one the
					// user wants to create. We make this an object even if the type
					// is usually a tile because we want more than one to exist on the 
					// same tile and tiles don't allow stacking like objects do
					this.ige.entities.create({
						template_id: cursorName,
						entity_x:-40,
						entity_y:-40,
						entity_type: ENTITY_TYPE_OBJECT,
						entity_locale: LOCALE_SINGLE_CLIENT,
						entity_tile_block: ENTITY_TB_NOBLOCK_NOCHECK,
						map_id: 'testMap1',
						asset_sheet_frame: sheetId,
						temp: true,
					}, this.bind(function (entity) {
						if (entity) {
							// Set this entity to non-blocking as it is temporary
							entity.entity_tile_block = ENTITY_TB_NOBLOCK_CHECK;
							this.currentCursorEntity = entity;
							this.log('Created cursor with ID:', entity.entity_id);
						} else {
							this.log('Error creating entity');
						}
					}));
					
					this.currentCursor = [cursorName, sheetId];
					
				} else {
					this.currentCursor = null;
				}
			} else {
				if (this.currentCursorEntity != null) {
					var tempCursor = this.currentCursorEntity;
					this.currentCursorEntity = null;
					this.currentCursor = null;
					this.ige.entities.remove(tempCursor);
				}
				this.currentCursor = null;
				this.log('Cannot select new cursor whilst old one is still in existance.');
			}
		}
	},
	
	toggleMenu: function (menuId, groupId) {
		// Ensure all the groups toggles are off but don't alter the one specified by menuId
		this.closeAll(groupId, menuId);
		
		if (menuId) {
			if (!$('#uiMenu_' + menuId).data('igeState')) {
				this.openMenu('#uiMenu_' + menuId);
				this._tipState = false;
				this.hideTip();
			} else {
				this.closeMenu('#uiMenu_' + menuId);
				this._tipState = true;
			}
		}
	},
	
	openMenu: function (id) {
		$(id).data('igeState', 1);
		$(id).show();
		/*$(id).animate({"opacity": '1'}, 100);*/
	},
	
	closeMenu: function (id) {
		$(id).data('igeState', 0);
		$(id).hide();
		/*
		$(id).animate({"opacity": '0'}, {
			duration:100,
			complete:function () {
				$(this).hide();
			}
		});
		*/
	},
	
	closeAll: function (groupId, menuId) {
		var groupArray = $('.' + groupId);
		for (var i = 0; i < groupArray.length; i++) {
			if (menuId) {
				if (groupArray[i].id != 'uiMenu_' + menuId) {
					this.closeMenu('#' + groupArray[i].id);
				}
			} else {
				this.closeMenu('#' + groupArray[i].id);
			}
		}
		this._tipState = true;
	},
	
	showTip: function (element, tipIndex) {
		if (this._tipState) {
			$('#uiGameTip').html('<span class="tipTitle">' + this.tips[tipIndex][0] + '</span><span class="tipText">' + this.tips[tipIndex][1] + '</span>');
			$('#uiGameTip').show();
		}
	},
	
	hideTip: function () {
		$('#uiGameTip').hide();
	},
	
	pathComplete: function (obj) {
		//console.log('Path completed');
		var map = obj.$local.$map;
		
		if (obj.path.points.length) {
			var endTile = obj.path.points[obj.path.points.length - 1].tile;
		} else {
			endTile = [];
		}
		
		var startPoint = [obj.entity_x, obj.entity_y];
		
		var randomTileIndex = Math.floor((Math.random() * this.walkTiles.length));
		var randomTile = this.walkTiles[randomTileIndex];
		
		endPoint = [randomTile.entity_x, randomTile.entity_y];
		
		this.ige.paths.create(obj);
		
		var pathPoints = this.ige.paths.localGeneratePath(map, 1, startPoint, endPoint, 'walk', true, false);
		//console.log('New path:', pathPoints);
		
		if (pathPoints) {
			for (var i = 0; i < pathPoints.length; i++) {
				this.ige.paths.addPathPoint(obj, pathPoints[i][0], pathPoints[i][1], 30);
			}
		}
		
		// Start path processing with autoStop enabled
		this.ige.paths.startPath(obj, PATH_TYPE_ENTITY, new Date().getTime(), true);
	},
	
	directionChange: function (entity) {
		switch (entity.template_id) {
			
			case 'womanWalk':
				// Entity is a person sprite
				switch (entity.entity_direction) {
					case DIRECTION_N:
						this.ige.entities.setAnimation(entity, 'womanWalkN');
					break;
					
					case DIRECTION_E:
						this.ige.entities.setAnimation(entity, 'womanWalkE');
					break;
					
					case DIRECTION_NE:
						this.ige.entities.setAnimation(entity, 'womanWalkNE');
					break;					
					
					case DIRECTION_S:
						this.ige.entities.setAnimation(entity, 'womanWalkS');
					break;
					
					case DIRECTION_SE:
						this.ige.entities.setAnimation(entity, 'womanWalkSE');
					break;
					
					case DIRECTION_W:
						this.ige.entities.setAnimation(entity, 'womanWalkW');
					break;		
					
					case DIRECTION_NW:
						this.ige.entities.setAnimation(entity, 'womanWalkNW');
					break;						
					
					case DIRECTION_SW:
						this.ige.entities.setAnimation(entity, 'womanWalkSW');
					break;		
				}
			break;
			
		}
	},
	
});