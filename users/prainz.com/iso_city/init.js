// Define our engine and game instance variables
window.ige = null;

// Create the bootstrap instance and ask it to load our engine files
window.igeBootstrap = new IgeBootstrap(onBoot);

var mode = 0;

if (mode == 1) {
	// Initalise the bootstrap instance with the file list to load
	window.igeBootstrap.init([
		// Libraries
		'/engine/lib_json',
		'/engine/lib_bison',
		'/engine/lib_stack',
		// Engine Core Classes
		'/engine/IgeConstants',
		'/engine/IgeClass',
		'/engine/IgeBase',
		'/engine/IgeEnum',
		'/engine/IgeEvents',
		'/engine/IgeCollection',
		'/engine/IgeItem',
		// Networking classes
		'/engine/IgeNetwork2',
		'/engine/IgeNetwork2_Packet',
		'/engine/IgeNetwork2_GameClient',
		'/engine/IgeNetworkItem',
		'/engine/IgeNetworkProvider_SocketIO',
		'/engine/IgeNetworkProvider_Pusher',
		'/engine/IgeNetworkProvider_Offline',
		// Render classes
		'/engine/IgeCanvas',
		'/engine/IgeHtml',
		// Engine Game Classes
		'/engine/IgeTemplates',
		'/engine/IgeAnimations',
		'/engine/IgePaths',
		'/engine/IgeUsers',
		'/engine/IgeAssets',
		'/engine/IgeEntities',
		'/engine/IgeScreens',
		'/engine/IgeDirtyRects',
		'/engine/IgeMaps',
		'/engine/IgeCameras',
		'/engine/IgeViewports',
		'/engine/IgeBackgrounds',
		'/engine/IgeRenderer',
		'/engine/IgeTime',
		'/engine/IgeIdFactory',
		'/engine/IgePallete',
		'/engine/IgeWindow',
		'/engine/IgeEngine',
		'/client/index',
	]);
} else {
	window.igeBootstrap.init([
		'_deploy',
	]);
}

setTimeout(function () {
	console.log('-----------------------------------------------------');
	console.log('Isogenic Game Engine - http://www.isogenicengine.com');
	console.log('(C)opyright 2011 Irrelon Software Limited');
	console.log('-----------------------------------------------------');
	window.igeBootstrap.process.apply(window.igeBootstrap);
}, 200);

function onBoot () {
	$(document).ready(function () {
		Init = new IgeClass({
			init: function () {
				this._className = 'Init';
				this.log('jQuery says DOM is ready');
				
				// Setup engine instance
				this.log('Setting up engine instance');
				window.ige = new IgeEngine();
				window.ige.defaultContext = '2d'; // Set default canvas context
				
				// Create the server script instance and pass the ige as the engine
				window.server = {}; //new this.sandbox.GameServer(ige);
				// Create the client script instance and pass the ige as the engine
				window.client = new GameClient(ige);
				// Create the game script instance and pass the client and server instance variables
				window.game = new Game(window.ige, window.client, window.server);
				
				// Setup network
				this.log('Setting up network provider');
				safeCall(window.game, 'networkInit');
				
				window.ige.on('networkProviderReady', this.bind(function () {
					// Setup engine hooks
					this.log('Setting up engine hooks');
					safeCall(window.game, 'engineHooks');
					
					// Register the game network commands
					this.log('Registering network commands');
					safeCall(window.game, 'networkCommands');
					
					// Connect to the server
					this.log('Setting engine as ready');
					safeCall(window.game, 'ready');
				}), null, true);
			}
		});
		init = new Init();
	});
}