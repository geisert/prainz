/** IgeItem - Provides helper methods to other classes such as IgeEntities
that inherit this classes methods. {
	engine_ver:"0.2.2",
	category:"class",
} **/
IgeItem = new IgeClass({
	init: function () {},
	
	/** create - Creates a new item. {
		category:"method",
		engine_ver:"0.2.4",
		arguments: [{
			type:"object",
			name:"pItem",
			desc:"The item to be created.",
			link:"entityData",
		}, {
			type:"method",
			name:"pCb",
			desc:"A callback method to be called when the item has been successfully created.",
			flags:"optional",
		}],
	} **/
	create: function (pItem, pCb) {
		
		// Apply templates
		if (pItem.template_id) {
			this.engine.templates.applyTemplate(pItem);
		}
		
		// Ensure defaults
		if (this.safeCall(this, '_itemDefaults', [pItem]) == true) {
			// Ensure our own defaults
			this.ensureLocalExists(pItem);
			/* CEXCLUDE */
			if (this.engine.isServer) { this.ensurePersistExists(pItem); }
			/* CEXCLUDE */
			
			// Check integrity
			if (typeof(this._itemIntegrity) != 'function' || this.safeCall(this, '_itemIntegrity', [pItem]) == true) {
				
				// Ensure that the item has an id!
				if (pItem[this.collectionId + '_id']) {
					
					// Emit the beforeCreate event and act upon a cancel request
					if (!this.emit('beforeCreate', pItem)) {
						// No receiver methods requested a cancel operation
						if (typeof(pCb) == 'function') {
							// The callback is a function so store it in the item object for later use
							pItem.$local.create_callback = pCb;
						}
						
						// Check if this class has propagation
						if (typeof(this._propagate) == 'function') {
							// Pass it to propagation
							this._propagate(pItem, PROPAGATE_CREATE);
						} else {
							// No propagation enabled in this class so just create the item!
							this._create(pItem);
						}
					} else {
						// Call the callback with no argument meaning there was a cancellation or error
						if (typeof (pCb) == 'function') { pCb(); }
					}
				} else {
					this.log('Cannot create item because it has no id! (IgeItem)', 'warning', pItem);
				}
			} else {
				//this.log('Bugging out of create because the item failed an integrity check!', 'warning', pItem);
			}
		}
	},
	
	/** read - Returns the object whose id is passed, or if an object is passed, will return the object with the id
	that matches the object's property collectionId + '_id'. {
		category:"method",
		engine_ver:"0.2.2",
		return: {
			type:"object",
			desc:"Returns the corresponding object to the passed parameter or false on error.",
		},
		arguments: [{
			type:"multi",
			name:"itemIdOrObj",
			desc:"Either an object or a string that identifies the object (by it's id) to return.",
		}],
	} **/
	read: function (itemIdOrObj) {
		// Check if the argument passed is null and return immediately if so
		if (itemIdOrObj == null) { return false; }
		// Check if the argument is an object or a string
		if (typeof(itemIdOrObj) == 'object') {
			// We have an object, see if we can get an id from it
			return this.byId[itemIdOrObj[this.collectionId + '_id']] || false;
		} else {
			// We have a string, see if the string matches an id for this collection
			return this.byId[itemIdOrObj] || false;
		}
	},
	
	/** update - Updates the class collection item with the matching id. The item is updated
	with all properties of the pItem parameter. DO NOT use this method to alter the position of
	an entity, use the IgeEntities moveTo* methods instead. {
		category:"method",
		engine_ver:"0.2.2",
		return: {
			type:"bool",
			desc:"Returns false on failure or the updated item on success.",
		},
		arguments: [{
			type:"multi",
			name:"pItem",
			desc:"The object containing update data.",
		}, {
			type:"object",
			name:"pData",
			desc:"The object containing update data.",
		}],
	} **/
	update: function (pItem, pData, pCb) {
		var pItem = this.read(pItem);
		
		// Emit the beforeUpdate event and act upon a cancel request
		if (!this.emit('beforeUpdate', pItem)) {
			// No receiver methods requested a cancel operation
			if (typeof(pCb) == 'function') {
				// The callback is a function so store it in the item object for later use
				pItem.$local.update_callback = pCb;
			}
			
			this._update(pItem, pData);
			
			// Check if this class has propagation
			if (typeof(this._propagate) == 'function') {
				// Pass it to propagation
				this._propagate(pItem, PROPAGATE_UPDATE);
			}
		} else {
			// Call the callback with no argument meaning there was a cancellation or error
			if (typeof(pCb) == 'function') { pCb(); }
		}
	},
	
	/** remove - Removes an item from the engine based upon its _locale
	and _persist property values. {
		category:"method",
		engine_ver:"0.2.2",
		return: {
			type:"bool",
			desc:"Returns false on failure or null for any other reason.",
		},
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The item object to be removed.",
			link:"entityData",
		}, {
			type:"method",
			name:"callback",
			desc:"The method to call when the item has been successfully removed from the engine.",
			flags:"optional",
		}],
	} **/
	remove: function (pItem, pCb) {
		var pItem = this.read(pItem);
		
		// Check if a callback was specified and if so, attach it to the item
		if (typeof(pCb) == 'function') {
			pItem.$local.remove_callback = pCb;
		}
		
		this._propagate(pItem, PROPAGATE_DELETE);
	},
	
	/* CEXCLUDE */
	/** ensurePersistExists - Takes an object and makes sure that the _persist property has a value,
	even if that value is just false. This makes sure that the database has a value to scan against
	when checking persist values. {
		category:"method",
		flags:"server",
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object to check.",
		}],
	} **/
	ensurePersistExists: function (obj) {
		if (!obj[this.collectionId + '_persist']) { obj[this.collectionId + '_persist'] = PERSIST_DISABLED; }
	},
	/* CEXCLUDE */
	
	/** ensureIdExists - Takes an object and makes sure that the _id property has a value. If no value
	currently exists, a new ID is generated from the IgeFactory.js class method newId(). {
		category:"method",
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object to check.",
		}],
	} **/	
	ensureIdExists: function (obj) {
		if (!obj[this.collectionId + '_id']) { obj[this.collectionId + '_id'] = this.engine.idFactory.newId(); }
	},
	
	/** ensureLocalExists - Takes an object and makes sure that the $local property is an object. If no value
	currently exists, a new object is assigned. {
		category:"method",
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object to check.",
		}],
	} **/	
	ensureLocalExists: function (obj) {
		if (obj && typeof(obj.$local) != 'object') {
			obj.$local = {};
		}
	},
	
});