IgeNetworkItem = new IgeClass({
	init: function () {
		
	},
	
	/** _propagate - Determines what action to take when a collection gets a CRUD operation call.
	If this function returns true, it will instruct the calling method to return as	well, 
	therefore exiting the method without executing any further code. {
		category:"method",
		engine_ver:"0.1.2",
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object to evaluate for network propagation.",
		}, {
			type:"string",
			name:"type",
			desc:"The type of propagation action to take.",
		}],
	} **/
	_propagate: function (obj, type) {
		var forceDebug = false;
		
		if (obj.propagate_trace || forceDebug) { this.log('Propagate: Starting...'); }
		var typeAppend = '';
		
		switch (type) {
			case PROPAGATE_CREATE:
				typeAppend = 'sCreate';
			break;
			case PROPAGATE_UPDATE:
				typeAppend = 'sUpdate';
			break;
			case PROPAGATE_DELETE:
				typeAppend = 'sRemove';
			break;
		}
		
		// Setup the still create flag which is set to true in certain situations as outlined below
		var stillProcess = false;
		
		// Set the default locale if required
		if (obj[this.collectionId + '_locale'] == null) {
			obj[this.collectionId + '_locale'] = LOCALE_DEFAULT;
			if (obj.propagate_trace || forceDebug) { this.log('Propagate: Applied default locale because none existed'); }
		}
		
		/* CEXCLUDE */
		// Are we the server?
		if (this.engine.isServer) {
			// Set the test locale variable
			var locale = obj[this.collectionId + '_locale'];
			
			// Have we been sent an override locale as the third hidden argument?
			if (arguments[2] != null) {
				locale = arguments[2];
			}
			
			// Switch behaviour by the _locale property
			switch (locale) {
				case LOCALE_EVERYWHERE:
					// Propagate everywhere
					if (obj.propagate_trace || forceDebug) { this.log('Propagate: Propagating everywhere...', 'info', [this.collectionId + typeAppend, obj]); }
					this.engine.network.send(this.collectionId + typeAppend, obj);
					stillProcess = true;
				break;
				
				case LOCALE_ALL_CLIENTS:
					// Propagate ONLY to clients, not the server
					if (obj.propagate_trace || forceDebug) { this.log('Propagate: Propagating to all clients...'); }
					this.engine.network.send(this.collectionId + typeAppend, obj);
					stillProcess = false;
				break;
				
				case LOCALE_SINGLE_CLIENT:
					// Propagate ONLY to one client, not the server
					// Check that the object we're propagating has a user_id property
					if (obj.user_id) {
						// Check the client actually exists and is connected
						var user = this.engine.users.byUserId[obj.user_id];
						if (user && user.session_id) {
							if (obj.propagate_trace || forceDebug) { this.log('Propagate: Propagating to a single client...'); }
							this.engine.network.send(this.collectionId + typeAppend, obj, user.session_id);
						}
					} else {
						this.log('Cannot propagate data to LOCALE_SINGLE_CLIENT for object because no user_id is set.', 'warning', obj);
					}
					stillProcess = false;
				break;
				
				case LOCALE_SERVER_ONLY:
					// Propagate ONLY to the server
					if (obj.propagate_trace || forceDebug) { this.log('Propagate: Propagating to server only...'); }
					stillProcess = true;
				break;
				
				case LOCALE_EVERYWHERE + LOCALE_DB:
				case LOCALE_ALL_CLIENTS + LOCALE_DB:
				case LOCALE_SINGLE_CLIENT + LOCALE_DB:
				case LOCALE_SERVER_ONLY + LOCALE_DB:
					if (obj.propagate_trace || forceDebug) { this.log('Propagate: Doing DB operations...'); }
					switch (type) {
						case PROPAGATE_CREATE:
							// The item has a locale_db setting so first insert it into the db
							// but if the item already has a _db_id then we don't need to add it to the database!
							if (!obj[this.collectionId + '_db_id']) {
								if (obj.propagate_trace || forceDebug) { this.log('Propagate: Creating DB record...'); }
								// Make sure there is a persist property present in the object
								this.ensurePersistExists(obj);
								
								// The item has no id so insert it into the database
								this.dbInsert(obj, this.bind(function (err, doc) {
									if (!err) {
										// No error so propagate by removing LOCALE_DB from the locale value
										var newLocaleVal = doc[this.collectionId + '_locale'] - LOCALE_DB;
										// Set the obj _db_id to the returned doc property of the same name
										obj[this.collectionId + '_db_id'] = doc[this.collectionId + '_db_id'];
										if (obj.propagate_trace || forceDebug) { this.log('Propagate: DB Record created!'); }
										// Re-call this function without the database locale flag
										this._propagate(obj, type, newLocaleVal);
									} else {
										this.log('Error creating database entry.', 'error', err);
									}
								}));
							} else {
								// The item already has an id from the database so propagate by removing
								// LOCALE_DB from the locale value
								if (obj.propagate_trace || forceDebug) { this.log('Propagate: Not creating DB record, already exists...'); }
								var newLocaleVal = obj[this.collectionId + '_locale'] - LOCALE_DB;
								this._propagate(obj, type, newLocaleVal);
							}
						break;
						
						case PROPAGATE_UPDATE:
							if (obj[this.collectionId + '_db_id'] != null) {
								if (obj.propagate_trace || forceDebug) { this.log('Propagate: Updating DB record...'); }
								// The item has a db id so update it in the database
								this.dbUpdate(obj, this.bind(function (err, doc) {
									if (err) {
										this.log('Error updating database entry.', 'error', err);
									} else {
										if (obj.propagate_trace || forceDebug) { this.log('Propagate: DB Record updated!'); }
									}
								}));
							}
							
							// Propagate the update without the db flag
							var newLocaleVal = obj[this.collectionId + '_locale'] - LOCALE_DB;
							this._propagate(obj, type, newLocaleVal);
						break;
						
						case PROPAGATE_DELETE:
							if (obj[this.collectionId + '_db_id'] != null) {
								if (obj.propagate_trace || forceDebug) { this.log('Propagate: Deleting DB record...'); }
								// The item has a db id so remove it from the database
								this.dbRemove(obj, this.bind(function (err, doc) {
									if (err) {
										this.log('Error updating database entry.', 'error', err);
									} else {
										//console.log('DB remove said: ', err, doc);
										if (obj.propagate_trace || forceDebug) { this.log('Propagate: DB Record deleted!'); }
									}
								}));
							}
							
							// Propagate the delete without the db flag
							var newLocaleVal = obj[this.collectionId + '_locale'] - LOCALE_DB;
							this._propagate(obj, type, newLocaleVal);
						break;
					}
					
				break;
			}
			
		}
		/* CEXCLUDE */
		if (!this.engine.isServer) {
			stillProcess = true;
		}
		
		// Still process?
		if (stillProcess) {
			switch (type) {
				case PROPAGATE_CREATE:
					this._create(obj);
				break;
				case PROPAGATE_UPDATE:
					this._update(obj);
				break;
				case PROPAGATE_DELETE:
					this._remove(obj[this.collectionId + '_id']);
				break;
			}
		}
				
	},
});