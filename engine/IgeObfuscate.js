/** IgeObfuscate - Handles obfuscation tasks to create difficult-to-read versions of original JavaScript source code. {
	engine_ver:"0.1.2",
	category:"class",
} **/
IgeObfuscate = new IgeClass({
	
	/** engine - A reference object to the main engine instance. {
		category:"property",
		type:"object",
		instanceOf:"IgeEngine",
	} **/
	engine: null,
	
	/** init - The constructor for this class. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a new instance of this class",
		},
		argument: {
			type:"object",
			name:"engine",
			desc:"The active engine instance.",
			instanceOf:"IgeEngine",
		},
	} **/
	init: function (engine) {
		this._className = 'IgeObfuscate';
		
		this.engine = engine;
	},
	
	/** obfuscate - Generates and returns an obfuscated version of the JavaScript source code
	passed in by the source parameter. Will only obfuscate is the engine is currently set to
	release mode or the override parameter is set to true. {
		category:"method",
		return: {
			type:"string",
			desc:"Returns the final obfuscated code or the original if no obfuscation occurred.",
		},
		arguments: [{
			type:"string",
			name:"source",
			desc:"The JavaScript source to obfuscate.",
		}, {
			type:"string",
			name:"seed",
			desc:"Reserved.",
		}, {
			type:"string",
			name:"opts",
			desc:"Reserved.",
		}, {
			type:"bool",
			name:"override",
			desc:"Set to true to force obfuscation regardless of the engine's mode.",
		}],
	} **/
	obfuscate: function (source, seed, opts, override) {
		if (this.engine.config.mode == 'release' || override) {
			var jsp = require(igeConfig.mapUrl('/node_modules/uglify-js')).parser;
			var pro = require(igeConfig.mapUrl('/node_modules/uglify-js')).uglify;
			
			// Remove client-exclude marked code
			source = source.replace(/\/\* CEXCLUDE \*\/[\s\S.]*?\* CEXCLUDE \*\//g, '');
			
			// Pass to the uglify-js module
			var orig_code = source;
			var ast = jsp.parse(orig_code); // parse code and get the initial AST
			ast = pro.ast_mangle(ast); // get a new AST with mangled names
			ast = pro.ast_squeeze(ast); // get an AST with compression optimizations
			
			var finCode = pro.gen_code(ast); // compressed code here
			
			// Return final code
			return finCode;
		} else {
			return source;
		}
	},
	
	/** obfuscateGen - Generates obfuscated versions of all .js files inside the pathIn path, and
	saves them with the same name to the pathOut path. Will recurse sub-folders. If a non-js file
	is found, the contents of that file are copied verbatim to the destination. {
		category:"method",
		arguments: [{
			type:"string",
			name:"pathIn",
			desc:"The path to read the source files from to obfuscate.",
		}, {
			type:"string",
			name:"pathOut",
			desc:"The path to write the obfuscated files to.",
		}],
	} **/
	obfuscateGen: function (pathIn, pathOut, removeServerCode) {
		var fs = require('fs');
		var stats = require('fs').stat;
		var jsp = require(igeConfig.mapUrl('/node_modules/uglify-js')).parser;
		var pro = require(igeConfig.mapUrl('/node_modules/uglify-js')).uglify;
		
		if (removeServerCode) {
			this.log('Set to remove server code...');
		}
		
		// Make the destination folder
		try { fs.mkdirSync(pathOut, '0666'); } catch (err) {  }
		
		var files = fs.readdirSync(pathIn);
		
		for (var fileIndex in files) {
			var path = files[fileIndex];
			if (fs.statSync(pathIn + path).isFile()) {
				this.log('Processing obfuscation generator on: ' + pathIn + path);
				if (path.substr(path.length - 2, 2) == 'js') {
					var data = fs.readFileSync(pathIn + path, 'utf8');
					
					// Remove client-exclude marked code
					if (removeServerCode) {
						data = data.replace(/\/\* CEXCLUDE \*\/[\s\S.]*?\* CEXCLUDE \*\//g, '');
					}
					
					// Obfuscate and save
					// Pass to the uglify-js module
					var orig_code = data;
					var ast = jsp.parse(orig_code); // parse code and get the initial AST
					ast = pro.ast_mangle(ast); // get a new AST with mangled names
					ast = pro.ast_squeeze(ast); // get an AST with compression optimizations
					
					var finCode = pro.gen_code(ast); // compressed code here
					
					// Open the file in the out path and write the data then close file
					this.log('Writing js ' + pathOut + path);
					fs.writeFileSync(pathOut + path, finCode);
				} else {
					// Not a JS file so just copy it from source to dest
					this.log('Writing non-js ' + pathOut + path);
					fs.writeFileSync(pathOut + path, fs.readFileSync(pathIn + path));
				}
			} else if (fs.statSync(pathIn + path).isDirectory()) {
				this.obfuscateGen(pathIn + path + '/', pathOut + path + '/');
			}
		}
	},

});