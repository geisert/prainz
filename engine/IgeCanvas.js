IgeCanvas = new IgeClass({
	
	Extends: IgeEvents,
	
	engine: null,
	entities: null,
	assets: null,
	
	// Constructor
	init: function (engine) {
		this._className = 'IgeCanvas';
		
		this.engine = engine;
		
		this.entities = this.engine.entities;
		this.assets = this.engine.assets;
	},
	
	rectIntersect: function (sX1, sY1, sW, sH, dX1, dY1, dW, dH) {
		var sX2 = sX1 + sW;
		var sY2 = sY1 + sH;
		var dX2 = dX1 + dW;
		var dY2 = dY1 + dH;
		
		if (sX1 < dX2 && sX2 > dX1 && sY1 < dY2 && sY2 > dY1) {
			return true;
		}
		return false;
	},
	
	renderFull: function (viewport, layerIndex) {
		
		var renderCount = 0;
		
		// Check that the viewport has had precalculation data run
		if (viewport.$viewportAdjustX != null && viewport.$viewportAdjustY != null) {
			
			var map = viewport.$local.$map;
			var backBuffer = viewport.$local.$backBuffer; //$('#' + viewport.viewport_id + '_backBuffer')[0];
			var backCtx = viewport.$local.$backBufferCtx;
			
			var frontBuffer = viewport.$local.$frontBuffer[layerIndex];
			var frontCtx = viewport.$local.$frontBufferCtx[layerIndex];
			
			frontCtx.clearRect(0, 0, frontBuffer.width, frontBuffer.height);
			
			// Loop through all the entities on this layer and render them
			if (this.entities.byMapIdAndLayer[viewport.map_id] != null
			&& this.entities.byMapIdAndLayer[viewport.map_id][layerIndex]) {
				
				var tempEntArray = this.entities.byMapIdAndLayer[viewport.map_id][layerIndex];
				var entCount = tempEntArray.length;
				var cellRatioX = 0;
				var cellRatioY = 0;
				var backWidth = 0;
				var backHeight = 0;
				var depth = 0;
				var tileCordsToDepth = this.engine.renderer.tileCordsToDepth;
				var entity = null;
				var renderMode = viewport.$local.$map.map_render_mode; // 2d = 0, iso = 1
				
				var camera = viewport.$local.$camera;
				var cameraScale = camera.camera_scale;
				
				// Define the redraw list array which will store the entities to redraw
				var redrawList = [];
				var dupList = [];
				
				// Define the redraw area clipping zone
				if (!viewport.viewport_clipping) {
					var rzX = 0;
					var rzY = 0;
					var rzW = viewport.panLayer.width;
					var rzH = viewport.panLayer.height;
				} else {
					var rzX = viewport.$local.$renderX;
					var rzY = viewport.$local.$renderY;
					var rzW = viewport.$local.$renderWidth;
					var rzH = viewport.$local.$renderHeight;
				}
				
				// Loop through the entities array
				while (entCount--) {
					
					// Store the current entity and get it's dimensions
					entity = tempEntArray[entCount];
					
					// Check if the entity is already on the duplist
					if (!dupList[entity.entity_id]) {
						
						// Check if the entity is actually on screen in the viewport!
						var entSize = this.entities.getSize(entity);
						var entPos = this.entities.getPosition(entity);
						
						if (this.rectIntersect(entPos[renderMode][0] + viewport.$viewportAdjustX, entPos[renderMode][1] + viewport.$viewportAdjustY, entSize[2], entSize[3], rzX, rzY, rzW, rzH)) {
							// If the entity needs redrawing...
							//if (entity.$local.$entity_dirty) {
								
							// Calculate depth by screen co-ordinates
							depth = tileCordsToDepth(entity.entity_x, entity.entity_y, entity.entity_z, entity.entity_tile_width, entity.entity_tile_height, 0);
							// Add entity to the redraw list
							dupList[entity.entity_id] = true;
							redrawList.push([entity, depth]);
						}
							
						//}
					}
					
				}
				
				redrawList.sort(function (a, b) { return b[1] - a[1]; });
				var redrawCount = redrawList.length;
				
				while (redrawCount--) {
					entity = redrawList[redrawCount][0];
					var assetSize = entity.$local.$asset.$local.$size;
					var entSize = this.entities.getSize(entity);
					var entPos = this.entities.getPosition(entity);
					
					var fp = [entPos[renderMode][0] + viewport.$viewportAdjustX, entPos[renderMode][1] + viewport.$viewportAdjustY, entSize[2], entSize[3]];
					
					frontCtx.save();
					frontCtx.scale(cameraScale, cameraScale);
					
					if (entity.entity_base_draw) {
						// Draw the tiles that the entity SHOULD be occupying
						var XY1 = this.engine.renderer.viewToScreen[renderMode](entity.entity_x, entity.entity_y, viewport, TILE_CORNER_1);
						var XY2 = this.engine.renderer.viewToScreen[renderMode](entity.entity_x + (entity.entity_tile_width - 1), entity.entity_y, viewport, TILE_CORNER_2);
						var XY3 = this.engine.renderer.viewToScreen[renderMode](entity.entity_x + (entity.entity_tile_width - 1), entity.entity_y + (entity.entity_tile_height - 1), viewport, TILE_CORNER_3);
						var XY4 = this.engine.renderer.viewToScreen[renderMode](entity.entity_x, entity.entity_y + (entity.entity_tile_height - 1), viewport, TILE_CORNER_4);
						
						frontCtx.beginPath();
						frontCtx.moveTo(XY1[0], XY1[1]);
						frontCtx.lineTo(XY2[0], XY2[1]);
						frontCtx.lineTo(XY3[0], XY3[1]);
						frontCtx.lineTo(XY4[0], XY4[1]);
						frontCtx.lineTo(XY1[0], XY1[1]);
						if (entity.entity_base_draw[0] == 0) {
							frontCtx.strokeStyle = entity.entity_base_draw[1];
							frontCtx.stroke();
						}
						if (entity.entity_base_draw[0] == 1) {
							frontCtx.fillStyle = entity.entity_base_draw[1];
							frontCtx.fill();
						}
					}
					
					frontCtx.drawImage(entity.$local.$assetImage, entSize[0], entSize[1], assetSize[0], assetSize[1], fp[0], fp[1], fp[2], fp[3]);
					
					if (entity.entity_highlight) {
						frontCtx.save();
						frontCtx.globalCompositeOperation = 'lighter';
						frontCtx.drawImage(entity.$local.$assetImage, entSize[0], entSize[1], assetSize[0], assetSize[1], fp[0], fp[1], fp[2], fp[3]);
						frontCtx.restore();
					}
					
					if (map.map_debug_entities || entity.$local.entity_debug) {
						// Draw bounding boxes around the entity on the designated debug layer
						frontCtx.strokeStyle = '#00c6ff';
						frontCtx.strokeRect(fp[0], fp[1], fp[2], fp[3]);
						
						frontCtx.fillStyle = '#333333';
						frontCtx.fillRect(fp[0] + 3, fp[1] + 3, 25, 15);
						
						frontCtx.fillStyle = '#ffffff';
						frontCtx.fillText(parseFloat(redrawList[redrawCount][1]).toFixed(2), fp[0] + 5, fp[1] + 14);
					}
					
					frontCtx.restore();
					
					// Mark the entity as clean
					entity.$local.$entity_dirty = false;
					
					// Increment the render count
					renderCount++;
					
				}
				
			}
			
		} else {
			this.log('Viewport precalculations not available!', 'warning', viewport);
		}
		
		return renderCount;
		
	},
	
	renderRects: function (viewport, layerIndex, tickTime, map, dirtyRectArray) {
		
		var renderCount = 0;
		
		// Check that the viewport has had precalculation data run
		if (viewport.$viewportAdjustX != null && viewport.$viewportAdjustY != null) {
			
			// Grab the buffers
			var backBuffer = viewport.$local.$backBuffer,
			backCtx = viewport.$local.$backBufferCtx,
			frontBuffer = viewport.$local.$frontBuffer[layerIndex],
			frontCtx = viewport.$local.$frontBufferCtx[layerIndex],
			// Grab the camera data
			camera = viewport.$local.$camera,
			cameraScale = viewport.$local.$camera.camera_scale,
			
			// Define the redraw list array which will store the entities to redraw
			redrawList = [],
			dupList = [],
			
			// Get dirty rects object
			dr = map.$local.$dirtyRects,
			drRects = dirtyRectArray, //dr.layer[layerIndex].dirty;
			drCount = dirtyRectArray.length,
			entityCache = map.$local.$entityCache[layerIndex],
			
			tileCordsToDepth = this.engine.renderer.tileCordsToDepth,
			entity = null,
			entSize = null,
			entPos = null,
			assetSize = null,
			depth = 0,
			rect = null,
			screenX = 0,
			screenY = 0,
			tileWidth = map.$local.$dirtyRects.tileWidth,
			tileHeight = map.$local.$dirtyRects.tileHeight,
			finalWidth = 0,
			finalHeight = 0,
			centerX = viewport.$viewportAdjustX,
			centerY = viewport.$viewportAdjustY,
			renderMode = viewport.$local.$map.map_render_mode; // 2d = 0, iso = 1
			
			// Define the redraw area clipping zone
			if (!viewport.viewport_clipping) {
				var rzX = 0;
				var rzY = 0;
				var rzW = viewport.panLayer.width;
				var rzH = viewport.panLayer.height;
			} else {
				var rzX = viewport.$local.$renderX;
				var rzY = viewport.$local.$renderY;
				var rzW = viewport.$local.$renderWidth;
				var rzH = viewport.$local.$renderHeight;
			}
			
			// Scale the buffers before doing clear and draw functions
			frontCtx.save();
			backCtx.save();
			frontCtx.scale(cameraScale, cameraScale);
			backCtx.scale(cameraScale, cameraScale);
			
			// Check if the dirty mode wants us to output the dirty rectangle debug grid
			if (map.map_use_dirty && map.map_debug_dirty) {
				this.engine.dirtyRects.drawGridRects(viewport, 3, dirtyRectArray);
			}
			
			// Loop dirty rects
			while (drCount--) {
				
				// Clear back & front buffer rect and store redraw entities
				// Get the individual rect co-ordinates
				rect = drRects[drCount];
				
				// Store entities
				if (entityCache && entityCache[rect[0]]) {
					var entList = entityCache[rect[0]][rect[1]];
					
					if (entList != null) {
						var entCount = entList.length;
						while (entCount--) {
							// Grab the entity
							entity = entList[entCount];
							
							// Check if the entity exists in the redraw list
							if (!dupList[entity.entity_id]) {
								// Check if the entity is actually on screen in the viewport!
								entSize = this.entities.getSize(entity);
								entPos = this.entities.getPosition(entity);
								
								if (this.rectIntersect(entPos[renderMode][0] + viewport.$viewportAdjustX, entPos[renderMode][1] + viewport.$viewportAdjustY, entSize[2], entSize[3], rzX, rzY, rzW, rzH)) {
									// Calculate depth by screen co-ordinates
									depth = tileCordsToDepth(entity.entity_x, entity.entity_y, entity.entity_z, entity.entity_tile_width, entity.entity_tile_height, 0);
									// Add entity to the redraw list
									dupList[entity.entity_id] = true;
									redrawList.push([entity, depth]);
								}
							}
						}
					}
				}
				
				// Calculate the screen co-ordinates
				screenX = (rect[0] * tileWidth) + centerX;
				screenY = (rect[1] * tileHeight) + centerY;
				
				// Clear the rect
				frontCtx.clearRect(screenX, screenY, tileWidth, tileHeight);
				backCtx.clearRect(screenX, screenY, tileWidth, tileHeight);
				
			}
			
			// Do we have any entities to redraw?
			var redrawCount = redrawList.length;
			if (redrawCount) {
				// Sort the entities by depth
				redrawList.sort(function (a, b) { return b[1] - a[1]; });
				
				// Draw the entities
				while (redrawCount--) {
					// Grab the entity to draw
					entity = redrawList[redrawCount][0];
					
					// Calculate the entity's dimensions and position
					assetSize = entity.$local.$asset.$local.$size;
					entSize = this.entities.getSize(entity);
					entPos = this.entities.getPosition(entity);
					
					if (entity.entity_base_draw) {
						// Draw the tiles that the entity SHOULD be occupying
						var XY1 = this.engine.renderer.viewToScreen[renderMode](entity.entity_x, entity.entity_y, viewport, TILE_CORNER_1);
						var XY2 = this.engine.renderer.viewToScreen[renderMode](entity.entity_x + (entity.entity_tile_width - 1), entity.entity_y, viewport, TILE_CORNER_2);
						var XY3 = this.engine.renderer.viewToScreen[renderMode](entity.entity_x + (entity.entity_tile_width - 1), entity.entity_y + (entity.entity_tile_height - 1), viewport, TILE_CORNER_3);
						var XY4 = this.engine.renderer.viewToScreen[renderMode](entity.entity_x, entity.entity_y + (entity.entity_tile_height - 1), viewport, TILE_CORNER_4);
						
						backCtx.beginPath();
						backCtx.moveTo(XY1[0], XY1[1]);
						backCtx.lineTo(XY2[0], XY2[1]);
						backCtx.lineTo(XY3[0], XY3[1]);
						backCtx.lineTo(XY4[0], XY4[1]);
						backCtx.lineTo(XY1[0], XY1[1]);
						if (entity.entity_base_draw[0] == 0) {
							backCtx.strokeStyle = entity.entity_base_draw[1];
							backCtx.stroke();
						}
						if (entity.entity_base_draw[0] == 1) {
							backCtx.fillStyle = entity.entity_base_draw[1];
							backCtx.fill();
						}
					}
					
					var fp = [entPos[renderMode][0] + viewport.$viewportAdjustX, entPos[renderMode][1] + viewport.$viewportAdjustY, entSize[2], entSize[3]];
					
					backCtx.drawImage(entity.$local.$assetImage, entSize[0], entSize[1], assetSize[0], assetSize[1], fp[0], fp[1], fp[2], fp[3]);
					
					if (entity.entity_highlight) {
						backCtx.save();
						backCtx.globalCompositeOperation = 'lighter';
						backCtx.drawImage(entity.$local.$assetImage, entSize[0], entSize[1], assetSize[0], assetSize[1], fp[0], fp[1], fp[2], fp[3]);
						backCtx.restore();
					}
					
					if (map.map_debug_entities || entity.$local.entity_debug) {
						// Draw bounding boxes around the entity on the designated debug layer
						backCtx.strokeStyle = '#00c6ff';
						backCtx.strokeRect(fp[0], fp[1], fp[2], fp[3]);
						
						backCtx.fillStyle = '#333333';
						backCtx.fillRect(fp[0] + 3, fp[1] + 3, 25, 15);
						
						backCtx.fillStyle = '#ffffff';
						backCtx.fillText(parseFloat(redrawList[redrawCount][1]).toFixed(2), fp[0] + 5, fp[1] + 14);
					}
					
					// Mark the entity as clean
					entity.$local.$entity_dirty = false;
					
					// Increment the render count
					renderCount++;
					
				}
				
				// Copy back-buffer rects to front buffer
				drCount = drRects.length;
				while (drCount--) {
					// Get the individual rect co-ordinates
					rect = drRects[drCount];
					
					// Calculate the screen co-ordinates
					screenX = (rect[0] * tileWidth) + centerX;
					screenY = (rect[1] * tileHeight) + centerY;
					finalWidth = tileWidth;
					finalHeight = tileHeight;
					
					// Adjust for camera scale
					screenX = (screenX * cameraScale);
					screenY = (screenY * cameraScale);
					finalWidth = (finalWidth * cameraScale);
					finalHeight = (finalHeight * cameraScale);
					
					// Check the bounds of the copy to ensure we don't try to copy outside the canvas and throw an INDEX_SIZE_ERR
					if (screenX < 0) { // Check screen x
						finalWidth += screenX;
						screenX = 0;
					}
					if (screenY < 0) { // Check screen x
						finalHeight += screenY;
						screenY = 0;
					}
					if ((screenX + finalWidth) > backBuffer.width) { // Width check
						finalWidth = backBuffer.width - screenX;
					}
					if ((screenY + finalHeight) > backBuffer.height) { // Height check
						finalHeight = backBuffer.height - screenY;
					}
					
					if (finalWidth > 0 && finalHeight > 0) {
						// Draw the back-buffer data to the front-buffer!
						screenX = Math.floor(screenX);
						screenY = Math.floor(screenY);
						finalWidth = Math.ceil(finalWidth);
						finalHeight = Math.ceil(finalHeight);
						frontCtx.drawImage(backBuffer, screenX, screenY, finalWidth, finalHeight, screenX, screenY, finalWidth, finalHeight);
					}
					
				}
				
			}
			
			// Restore the buffer states
			frontCtx.restore();
			backCtx.restore();			
			
		} else {
			this.log('Viewport precalculations not available!', 'warning', viewport);
		}
		
		return renderCount;
		
	},
	
});