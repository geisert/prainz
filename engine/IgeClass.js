/*
	Isogenic Game Engine - Class generator
	--------------------------------------
	
	This code is part of the Isogenic Game Engine, however it is released
	under a separate license from other parts of the engine and can be used,
	reproduced, copied and distributed for all types of projects.
	
	Rob Evans
	Irrelon Software Limited
	http://www.isogenicengine.com
	------------------------------------------------------------------------
	
	Usage:
	
	// Define a new class
	var YourClass = new IgeClass({
		
		_tx: null, // Simple property
		
		init: function (arg1, arg2) {
			
			// Your constructor code here
			
		},
		
		setTx: function (tx) {
			
			// A simple method
			this._tx = tx;
			console.log(tx);
			
		},
		
		getTx: function () {
			
			return this._tx;
			
		}
		
	});
	
	// Create a new instance of your class
	var classInstance = new YourClass(arg1, arg2);
*/

/** IgeClass - Allows the creation of classes that follow the same definition as MooTools classes without the MooTools dependency. {
	engine_ver:"0.0.3",
	category:"class",
} **/
IgeClass = function (params) {
	var newClass = this.create(params);
	
	if (params['Depends'])
	{
		this.loadDepends(params['Depends']);
	} else {
		// Check if the class extends any other class
		if (params['Extends']) {
			// If the extends parameter is an array or string
			if (typeof(params['Extends']) == 'function') {
				// The extend is a single string item
				this.process(newClass, params['Extends'].prototype);
			}
			if (params['Extends'] instanceof Array) {
				// The extends is an array of classes so loop it and extend
				var extendArray = params['Extends'];
				for (var i = 0; i < extendArray.length; i++) {
					if (extendArray[i] && extendArray[i].prototype) {
						this.process(newClass, extendArray[i].prototype);
					} else {
						this.log('Cannot extend class because class prototype does not exist.', 'error');
					}
				}
			}
		}
		
		this.process(newClass, params);
	}
	
	// Return our new class
	return newClass;
}

/* IgeClass.prototype.create - Define the new class with params.initialize or blank function */
IgeClass.prototype.create = function (params) {
	return function () { this.bind = IgeClass.prototype.bind; this.log = IgeClass.prototype.log; params['init'].apply(this, arguments); this.safeCall = IgeClass.prototype.safeCall; this.absorbClass = IgeClass.prototype.absorbClass; } || function () {};
}

/* IgeClass.prototype.loadDepends - Load any scripts that this class depends on */
IgeClass.prototype.loadDepends = function (depends) {
	var script = new IgeElement('script', {src: source, type: 'text/javascript'});	
}

/* IgeClass.prototype.process - Process passed parameters and add them to newClass.prototype */
IgeClass.prototype.process = function (newClass, params) {
	
	for (var param in params)
	{
		
		switch (typeof params[param])
		{
			
			case 'function' && param != 'initialize' && param != 'Extends':
				newClass.prototype[param] = params[param];
			break;
			
			default:
				newClass.prototype[param] = params[param];
			break;
			
		}
		
	}
	
	return newClass;
	
}

/* IgeClass.prototype.absorbClass - Process passed parameters and add them to this.prototype except
where the param already exists in this. */
IgeClass.prototype.absorbClass = function (params) {
	//console.log('Absorbing...', this._className, params['networkProvider']);
	for (var param in params)
	{
		if (typeof(this[param]) == 'undefined') {
			switch (typeof(params[param]))
			{
				
				case 'function' && param != 'initialize' && param != 'Extends':
					this[param] = params[param];
				break;
				
				default:
					this[param] = params[param];
				break;
				
			}
		}
		
	}
	
}

/* bind - Function to replace the "this" of the method by the "this" of the caller */
IgeClass.prototype.bind = function(Method, ignoreNullMethod) {
	
	if (typeof Method == 'function') {
		var _this = this;
		return(
			function(){
				return( Method.apply( _this, arguments ) );
			}
		);
	} else {
		if (!ignoreNullMethod) {
			this.log('An attempt to use bind against a method that does not exist was made!', 'warning');
		}
		return (function () { });
	}
	
},

/* safeCall - Attempts to call the method specified by the methodName argument in the
passed obj object but if the method does not exist no error will be thrown. */
IgeClass.prototype.safeCall = function (obj, methodName, args) {
	if (obj && typeof obj[methodName] == 'function') {
		return obj[methodName].apply(obj, args || []);
	} else {
		// No method
		//this.log('Cannot call method "' + methodName + '" of passed object.', 'info');
		return false;
	}
}

/* log - Logs the param argument to the console based upon the level parameter */
IgeClass.prototype.log = function (param, level, obj, ignoreStack) {
	
	var dtObj = new Date();
	var dt = '[' + dtObj.toDateString() + ' ' + dtObj.toTimeString().substr(0,8) + '] ';
	
	var output = '';
	var debugOn = false;
	var debugLevel = [];
	var breakOnError = false;
	var isServer = false;
	var isSandboxed = false;
	
	if (!level) { level = 'info'; }
	//console.log('moo', typeof this.ige);
	if ((this.engine && this.engine.config) || (this.config && this.config.debug) || (typeof window === 'undefined' && typeof this.ige == 'object')) {
		/* CEXCLUDE */
		if (this.engine != null) { 
			// Use settings from engine config
			debugOn = this.engine.config.debug;
			debugLevel = this.engine.config.debugLevel;
			breakOnError = this.engine.config.debugBreakOnError;
			isSandboxed = this.engine.config.sandbox;
			
		} else if (this.config != null) {
			// Use settings from config
			debugOn = this.config.debug;
			debugLevel = this.config.debugLevel;
			breakOnError = this.config.debugBreakOnError;
			isSandboxed = this.config.sandbox;
		} else if (typeof this.ige == 'object') {
			// Use settings from ige config
			debugOn = this.ige.config.debug;
			debugLevel = this.ige.config.debugLevel;
			breakOnError = this.ige.config.debugBreakOnError;
			isSandboxed = this.ige.config.sandbox;
		} else {
			// Set some defaults
			debugOn = true;
			debugLevel = ['info', 'warning', 'error', 'log'];
			breakOnError = true;
			isSandboxed = true;
		}
		var color = require(igeConfig.mapUrl('/node_modules/cli-color'));
		isServer = true;
		/* CEXCLUDE */
	} else {
		// Client-side debugging
		debugOn = window.igeDebug;
		debugLevel = window.igeDebugLevel;
		breakOnError = window.igeDebugBreakOnError;
	}
	if (!isSandboxed) {
		
		var className = this._className || "Unnamed Class";
			
		if (debugOn && valueIn(level, debugLevel))
		{
			
			if (console != null) {
				switch (level)
				{
					
					case 'log':
						if (obj) { console.log(obj); }
						
						if (isServer) {
							output = color.magenta(dt) + color.white("IGE ") + color.cyan("*" + level + "*") + color.yellow(" [" + className + "]") + " :";
						} else {
							output = dt + "IGE *" + level + "* [" + className + "] :";
						}
						
						console.log(output);
						console.log(param);
					break;
					
					case 'info':
						if (obj) { console.log(obj); }
						if (isServer) {
							output = color.magenta(dt) + color.white("IGE ") + color.cyan("*" + level + "*") + color.yellow(" [" + className + "]") + " : " + param;
						} else {
							output = dt + "IGE *" + level + "* [" + className + "] : " + param;
						}
						
						console.info(output);
					break;
					
					case 'error':
						if (!ignoreStack) {
							if (isServer) {
								var stack = new Error().stack;
								console.log(color.magenta('Stack:'), color.red(stack));
							} else {
								if (typeof printStackTrace == 'function') {
									console.log('Stack:', printStackTrace().join('\n ---- '));
								}
							}
						}
						if (obj) { console.log(obj); }
						
						if (isServer) {
							output = color.magenta(dt) + color.white("IGE ") + color.red("*" + level + "*") + color.yellow(" [" + className + "]") + " : " + color.red(param);
						} else {
							output = dt + "IGE *" + level + "* [" + className + "] : " + param;
						}
						
						if (breakOnError) {
							throw(output);
						} else {
							console.log(output);
						}
					break;
					
					case 'warning':
						if (!ignoreStack) {
							if (isServer) {
								var stack = new Error().stack;
								console.log(color.magenta('Stack:'), color.red(stack));
							} else {
								if (typeof printStackTrace == 'function') {
									console.log('Stack:', printStackTrace().join('\n ---- '));
								}
							}
						}
						if (obj) { console.log(obj); }
						
						if (isServer) {
							output = color.magenta(dt) + color.white("IGE ") + color.magenta("*" + level + "*") + color.yellow(" [" + className + "]") + " : " + color.magenta(param);
						} else {
							output = dt + "IGE *" + level + "* [" + className + "] : " + param;
						}
						
						console.warn(output);
					break;
					
				}
			}
			
		}
	}
	
}

/* valueIn - Checks if a value is contained in an array */
var valueIn = function (value, arr) {
	for (var index =0; index < arr.length; index++) {
		if (arr[index] == value) { return true; }
	}
	
	return false;
}