IgeTime = new IgeClass({
	
	Extends: IgeEvents,
	
	engine: null,
	network: null,
	
	clientSync: 0,
	clientNetLag: 0,
	clientNetLatency: 0,
	clientNetDiff: 0,
	netSyncCount: 0,
	
	intervalNetSync: null,
	
	byIndex:[],
	
	init: function (engine) {
		this._className = 'IgeTime';
		
		this.engine = engine;
		this.byIndex = [];
		
		// Check if we have a network
		if (this.engine._IgeNetwork) {
			// Call the networkInit method of this class
			this.networkInit();
		}
	},
	
	networkInit: function () {
		this.network = this.engine.network;
		this.network.registerCommand('netSync', this.bind(this.netSyncReceived));
	},
	
	// Methods
	netSyncStart: function (delay, sampleCount) {
		if (!sampleCount) { sampleCount = 1; } // Default to 1 sample
		this.network.send('netSync', new Date().getTime());
		if (this.netSyncCount < sampleCount) {
			this.netSyncCount++;
			setTimeout(this.bind(this.netSyncStart), 1000);
		} else {
			this.netSyncCount = 0;
		}
		
		if (delay) {
			this.intervalNetSync = setInterval(this.bind(this.netSyncStart), delay);
			this.log('Network time sync is set to interval every ' + delay + ' milliseconds');
		}
	},
	
	netSyncReceived: function (data, client) {
		if (this.engine.isServer) {
			/* CEXCLUDE */
			// Send back the client's sent time and our current time
			this.network.send('netSync', [[data, new Date().getTime()]], client);
			/* CEXCLUDE */
		} else {
			// Received the response from the server, work out the delta
			var latency = (new Date().getTime() - data[0]) / 2; // Current time - client sent time divided by two (the entire round trip / 2)
			var timeDiff = (new Date().getTime() - data[1]); // Current time - server sent time (the time difference from server to client clocks)
			this.log('Network clock sync returned Latency: ' + latency + ', TimeDiff: ' + timeDiff, 'info');
			
			this.clientNetLatency = latency;
			this.clientNetLag = timeDiff;
			
			// Set the value that all engine functions will use when computing time difference between server and client
			this.clientNetDiff = timeDiff + latency;
		}
	},
	
});