/** IgeTemplates - The template management class. {
	engine_ver:"0.1.2",
	category:"class",
} **/
IgeTemplates = new IgeClass({
	
	Extends: [IgeCollection, IgeItem, IgeEvents],
	
	/** engine - A reference object to the main engine instance. {
		category:"property",
		type:"object",
		instanceOf:"IgeEngine",
	} **/
	engine: null,
	
	/** byIndex - An array with an integer index that allows you to access every
	item created using the create method in the order it was created. {
		category:"property",
		type:"array",
		index:"integer",
	} **/
	byIndex: null,
	
	/** byId - An array with a string index that allows you to access every item
	created using the create method by its id. {
		category:"property",
		type:"array",
		index:"string",
	} **/
	byId: null,
	
	/** init - The constructor for this class. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a new instance of this class",
		},
		argument: {
			type:"object",
			name:"engine",
			desc:"The active engine instance.",
			instanceOf:"IgeEngine",
		},
	} **/
	init: function (engine) {
		this._className = 'IgeTemplates';
		this.collectionId = 'template';
		this.engine = engine;
		
		this.byIndex = [];
		this.byId = [];
		
		// Check if we have a network
		if (this.engine._IgeNetwork) {
			// Call the networkInit method of this class
			this.networkInit();
		}
	},
	
	networkInit: function () {
		// Network CRUD Commands
		this.engine.network.registerCommand('templatesCreate', this.bind(this.receiveCreate));
		this.engine.network.registerCommand('templatesRead', this.read);
		this.engine.network.registerCommand('templatesUpdate', this.update);
		this.engine.network.registerCommand('templatesRemove', this.remove);
		
		// Register standard property names to the network manifest see the API manual for more details
		this.engine.network.addToManifest([
			'template_id',
			'template_contents',
		]);
	},
	
	/** create - Create a new template. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns the newly created item on success or false on failure.",
		},
		argument: {
			type:"object",
			name:"template",
			desc:"The template object to be created.",
			link:"templateData",
		},
	} **/
	create: function (template) {
		
		if (template != null) {
			if (template.template_contents != null) {
				// Check if we need to auto-generate an id
				this.ensureIdExists(template);
				
				template.$local = template.$local || {};
				
				this.byIndex.push(template);
				this.byId[template.template_id] = template;
				
				this.log('Template created: ' + template.template_id);
				
				return template;
			} else {
				this.log("Cannot create template because the template has no template_contents object!", "error", template);
				return false;
			}
		} else {
			this.log("Cannot create template because the passed template object is null!", "error", template);
			return false;
		}
		
	},
	
	update: function () {},
	remove: function () {},
	
	/** applyTemplate - Applies the contents of the template identified by templateId to the passed object. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true on success or false on failure.",
		},
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object to apply the template to.",
		}, {
			type:"multi",
			name:"templateId",
			desc:"The id of the template or the template object itself to apply to the passed object (obj). If this argument is not supplied the method will attempt to use the template_id parameter in the object from argument 1.",
			flags:"optional",
		}],
	} **/
	applyTemplate: function (obj, templateId) {
		// Check for a passed template ID and if none, see if the object itself has one
		if (!templateId) {
			if (obj.template_id) {
				templateId = obj.template_id;
			}
		}
		
		if (templateId) {
			
			// Get the template
			var tpl = this.read(templateId);
			
			// Check for the template
			if (tpl != null) {
				var tplContents = tpl.template_contents;
				
				// Clone the template contents properties into a new object
				var tplClone = this.engine.objClone(tplContents);
				
				if (tplClone) {
					// Check if the two objects have an asset_id
					if (tplClone.asset_id != null && obj.asset_id != null) {
						// Check if the receiving object is an entity
						var tempEnt = this.engine.entities.read(obj);
						if (tempEnt && obj.asset_id != tplClone.asset_id) {
							// The object is an entity and the source object has a new asset id so assign the new asset to the entity
							//this.engine.entities.setAsset(tempEnt, tplClone.asset_id);
						}
					}
					
					// Now iterate through the obj object and replace any template properties with
					// the object's properties
					for (var i in tplClone) {
						if (typeof(obj[i]) == 'undefined' || obj[i] == null) {
							obj[i] = tplClone[i];
						}
					}
				}
				
				return true;
			} else {
				this.log('Attempted to apply template that does not exist with id: ' + templateId, 'error', obj);
				return false;
			}
			
		} else {
			this.log('Attempted to apply template to an object without specifying a template_id!', 'error', obj);
			return false;
		}
	},
	
	/** applyTemplateByVoodoo - Applies the contents of the template identified by templateId to the passed object
	by altering the fundamental getter and setter methods of the properties. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true on success or false on failure.",
		},
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object to apply the template to.",
		}, {
			type:"multi",
			name:"templateId",
			desc:"The id of the template or the template object itself to apply to the passed object (obj).",
		}],
	} **/
	applyTemplateByVoodoo: function (obj, templateId) {
		
		if (!obj.$local.__igeTplOn) {
			// Get the template
			var tpl = this.read(templateId);
			
			// Check for the template
			if (tpl != null) {
				var tplContents = tpl.template_contents;
				obj.$local.__igeTplCts = tplContents;
				obj.$local.__igeTplOn = true;
				
				for (var i in tplContents) {
					eval('var templateGetter = function () { return this.__' + i + ' || this.$local.__igeTplCts.' + i + '; }');
					eval('var templateSetter = function (val) { this.__' + i + ' = val; }');
					
					if (Object.defineProperty) {
						
						// Use the ES5 defineProperty to set/get define
						Object.defineProperty(obj, i, {get: templateGetter, set: templateSetter});
						
					} else if (obj.__defineGetter__) {
						
						// Use older versions of the get/set define
						obj.__defineGetter__(i, templateGetter);
						obj.__defineSetter__(i, templateSetter);
						
					} else {
						
						// This is the worst-case scenario, fall back to copying the data
						obj[i] = tplContents[i];
						
					}
				}
				
				return true;
			} else {
				this.log('Attempted to apply template that does not exist named: ' + templateId, 'error');
				return false;
			}
			
		} else {
			this.log('Attempted to apply template to an object that already has a template applied!', 'error', obj);
			return false;
		}
	},
	
});