/* useBison - Sets a flag to determin if we are to encode and decode packets via bison. */
IgeNetwork.prototype.useBison = function (flag) {
	this.log('Use BISON to encode / decode network packets: ' + flag);
	this._bisonEnabled = flag;
},

/* createPacket - Creates and returns a new network packet. */
IgeNetwork.prototype.createPacket = function (cmdId, seqId, data) {
	return new this._packet(cmdId, seqId, data);
}

/* encodePacket - Creates, encodes and returns a new network packet's data. */
IgeNetwork.prototype.encodePacket = function (cmdId, seqId, data) {
	if (seqId == null) { seqId = -1; }
	
	// Check if we need to manifest compress
	if (this._manifestEnabled && typeof(data) == 'object') {
		// Replace each instance of a property name with it's new index
		var objectJson = JSON.stringify(data);
		var propIndex = this._manifest._reverse;
		for (var i in propIndex) {
			var regex1 = new RegExp('"' + propIndex[i] + '"', 'g');
			objectJson = objectJson.replace(regex1, 'º' + i + '');
		}
		
		// Replace true and false with 1 and 0 to get compression of those values
		var regex = new RegExp(':true', 'g');
		objectJson = objectJson.replace(regex, ':1');
		
		var regex = new RegExp(':false', 'g');
		objectJson = objectJson.replace(regex, ':0');
		
		data = 'º' + objectJson;
	}
	
	// Check if we need to BISON encode
	if (this._bisonEnabled) {
		return this.bison.encode(new this._packet(cmdId, seqId, data).getData());
	} else {
		return new this._packet(cmdId, seqId, data).getData();
	}
}

IgeNetwork.prototype.decodePacket = function (packetData) {
	var tempPacket = null;
	
	// Check if we need to BISON decode
	if (this._bisonEnabled) {
		tempPacket = this.bison.decode(packetData);
	} else {
		tempPacket = packetData;
	}
	
	var data = tempPacket[1];
	
	// Check if we need to manifest decompress
	if (this._manifestEnabled && typeof(data) == 'string') {
		// Check if this string was originally encoded with manifest data
		if (data.substr(0, 1) == 'º') {
			// Remove the starting byte
			data = data.substr(1, data.length - 1);
			
			// Replace each instance of an index with it's property name
			var objectJson = data;
			var propIndex = this._manifest._reverse;
			for (var i in propIndex) {
				var regex1 = new RegExp('º' + i + ':', 'g');
				objectJson = objectJson.replace(regex1, '"' + propIndex[i] + '":');
				
				var regex2 = new RegExp('º' + i + ',', 'g');
				objectJson = objectJson.replace(regex2, '"' + propIndex[i] + '",');
				
				var regex3 = new RegExp('º' + i + '}', 'g');
				objectJson = objectJson.replace(regex3, '"' + propIndex[i] + '"}');
				
				var regex4 = new RegExp('º' + i + ']', 'g');
				objectJson = objectJson.replace(regex4, '"' + propIndex[i] + '"]');
			}
			
			/*
			if (this._statsEnabled) {
				var jData = JSON.parse(data);
				this._nonCompressed = this._nonCompressed || [];
				for (var i in jData) {
					if (i.substr(0, 1) != 'º' && i.length > 2 && (parseInt(i) != i)) {
						this._nonCompressed[i] = this._nonCompressed[i] || 0;
						this._nonCompressed[i]++;
					}
					if (typeof(jData[i]) == 'string' && jData[i].substr(0, 1) != 'º' && jData[i].length > 2 && (parseInt(jData[i]) != jData[i])) {
						this._nonCompressed[jData[i]] = this._nonCompressed[jData[i]] || 0;
						this._nonCompressed[jData[i]]++;
					}
				}
			}
			*/
			// Convert the JSON back into a real object
			try { data = JSON.parse(objectJson); } catch (err) { console.log(objectJson); }
			
			// We do not reverse the compression of the "true" and "false" values since
			// they should be OK mapping to == true and == false anyway with 1 and 0	
		}
	}
	
	return {cmdId:tempPacket[0], data: data, seqId: tempPacket[2], msgTime: tempPacket[3]};
}

IgeNetwork.prototype._packet = function (cmdId, seqId, data) {
	this.cmdId = cmdId;
	this.seqId = seqId;
	this.data = data;
	this.msgTime = new Date().getTime();
}

/* packet.prototype - Define a network packet object prototype. */
IgeNetwork.prototype._packet.prototype = {
	// Packet properties
	cmdId: -1, // The packet command id of the command we are going to send
	seqId: -1, // The sequence number of this packet
	msgTime: -1, // The time at which this packet was created
	data: {}, // The packet's payload data
	
	/* getData - Returns sequenced array of the data that should be sent over
	the network. This excludes some properties of the packet that are not required
	by the receiver. The returned data is in an ordered array so the receiver
	needs to know which order the properties are sent in. If you modify this data
	or the data order, you must ensure that the receiving script is able to
	de-sequence the data (know which property is at array index 0, 1, 2 etc) */
	getData: function () {
		return [this.cmdId, this.data, this.seqId, this.msgTime];
	},
}

IgeNetwork.prototype._nextSequenceId = function () {
	return this._sequenceId++;
}