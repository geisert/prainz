/** IgeAiEngine - The AI control class. {
	engine_ver:"0.2.3",
	category:"class",
} **/
IgeAiEngine = new IgeClass({
	
	Extends: [IgeEvents, IgeItem, IgeNetworkItem],
	
	engine: null,
	
	definitionByIndex: [],
	definitionById: [],
	
	agentByIndex: [],
	agentById: [],
	
	// Constructor
	init: function (engine) {
		this._className = 'IgeAi';
		
		this.engine = engine;
		
		this.definitionByIndex = [];
		this.definitionById = [];
		
		this.agentByIndex = [];
		this.agentById = [];
		
		// Check if we have a network
		if (this.engine._IgeNetwork) {
			// Call the networkInit method of this class
			this.networkInit();
		}
	},
	
	networkInit: function () {
		// Network CRUD Commands
		this.engine.network.registerCommand('aiCreate', this.bind(this.create));
		this.engine.network.registerCommand('aiRead', this.bind(this.read));
		this.engine.network.registerCommand('aiUpdate', this.bind(this.update));
		this.engine.network.registerCommand('aiRemove', this.bind(this.remove));
	},
	
	createDefinition: function (aiDef) {
		if (this.engine.isServer) {
			/* CEXCLUDE */
			this.definitionByIndex.push(aiDef);
			this.definitionById[aiDef.ai_id] = aiDef;
			/* CEXCLUDE */
		} else {
			// Client code
			this.definitionByIndex.push(aiDef);
			this.definitionById[aiDef.ai_id] = aiDef;
		}
	},
	
	createAgent: function (aiAgent) {
		if (this.engine.isServer) {
			/* CEXCLUDE */
			this.agentByIndex.push(aiAgent);
			this.agentById[aiAgent.ai_id] = aiAgent;
			/* CEXCLUDE */
		} else {
			// Client code
			this.agentByIndex.push(aiAgent);
			this.agentById[aiAgent.ai_id] = aiAgent;
		}
	},
	
	read: function () {},
	_update: function () {},
	_remove: function () {},
	
	processAi: function (time) {
		for (var i = 0; i < this.agentByIndex.length; i++) {
			var aiAgent = this.agentByIndex[i];
			// Check the ai has a brain mode, if not, request one!
			if (aiAgent.brain.state == null) {
				aiAgent[aiAgent.agent.getNewState].apply(aiAgent, [aiDef]);
			}
		}
	},
	
	dispatchMessage: function (sender, recipient, messageType, info) {
		// Call the receiver method depending upon the messageType and pass the sender and info
		
	},
	
});

var ai = { // This is an AI definition object that defines the AI attributes
	ai_id: 'standardPerson',
	brain: {
		bornOnTimeStamp: 0,
		state: null,
		init: function () {
			// Process initial needs values with random values where required
			for (var i in this.needs) {
				var need = this.needs[i];
				
				if (typeof(need[2]) == 'undefined') {
					var lowValue = need[0];
					var highValue = need[1];
					
					// Setup a new value for this need randomly
					var finalNeedValue = Math.floor(lowValue + (Math.random() * (highValue - lowValue)));
					
					// Store the final value in the array at index 2
					this.need[2] = finalNeedValue;
				}
			}
			
			// Process has values just as with the needs above
			for (var i in this.has) {
				var has = this.has[i];
				
				if (typeof(has[2]) == 'undefined') {
					var lowValue = has[0];
					var highValue = has[1];
					
					// Setup a new value for this has randomly
					var finalHasValue = Math.floor(lowValue + (Math.random() * (highValue - lowValue)));
					
					// Store the final value in the array at index 2
					this.has[2] = finalHasValue;
				}
			}
		},
		
		needs: { // All the needs of a "standardPerson" AI
			bread: [1, 5], // Between 1 and 5
			milk: [1, 5], // Between 1 and 5
			toys: [1, 3], // Between 1 and 3
			sleep: [8, 12], // Between 8 and 12
			walkingDistance: [1000, 2000], // Between 1000 and 2000
		},
		
		has: { // All the needs that the AI currently has
			bread: [0, 5, 0],  // Between 0 and 5
			milk: [0, 5, 0], // Between 0 and 5
			toys: [0, 3, 0], // Between 0 and 3
			sleep: [8, 8, 8], // Always start with 8
			walkingDistance: [0, 500], // Between 0 and 2000
		},
		
		states: {
			initial: 'walker',
			waiting: { // When waiting for a while... replenish walking tolerance
				modifiers: {
					walkingDistance: {
						mod: -20,
						time: 1000,
					},
				},
			},
			walking: { // When walking...
				priorities: { // prioritise
					sleep: 10, // sleeping at level 10
					walkingDistance: 1, // walking at level 1
				},
				modifiers: { // During the walker mode, modify the...
					walkingDistance: { // walkingDistance value
						mod: 50, // by +50
						time: 1000, // every 1 second (1000 milliseconds)
					},
					sleep: { // sleep value
						mod: -0.25, // by -0.25
						time: 60000, // every minute (60,000 milliseconds)
					}
				},
			},
			shopping: { // When shopping...
				priorities: { // prioritise
					10:[sleep],
					1:[bread, milk],
					0.2:[toys]
				},
				modifiers: { // During the shopper mode, modify the...
					sleep: { // sleep value
						mod: -0.5, // by -0.5
						time: 60000, // every minute (60,000 milliseconds)
					}
				},
			},
		},
	},
	agent: { // Actions the AI should take when asked
		// Agent states
		getNewState: function () {
			// Set brain mode based upon current needs and the objectives they produce
			if (typeof(this.state) == 'undefined') {
				// No current mode so set the initial mode
				this.state = this.brain.states.initial;
			} else {
				// We already have a state so set a new one
				if (typeof(this.states[this.state].priorities) == 'object') {
					// Create a new array of priorities and the current values
					// so we can sort them and determine most needed
					var sortArray = [];
					
					for (var i in this.states[this.state].priorities) {
						
					}
				}
			}
		},
		walker: { // when walking
			sleep: 'getSleep', // if need sleep, call this function
			walkingDistance: 'getRandomDestination', // if need walkingDistance, call this function
		},
		shopper: { // when shopping
			sleep: 'getSleep',
			bread: 'getBread',
			milk: 'getMilk',
			toys: 'getToys',
		}
	},
}

var ai = {
	ai_id: 'standardKiller',
	brain: {
		currentAgent: null,
		needs: {
			kills: 3,
			health: 100,
			points: 200,
		},
		has: {
			kills: 0,
			health: 93,
			points: 0,
		},
		priorities: {
			kills: 1,
			health: 2,
			points: 1,
		}
	},
	agents: {
		kills: 'locateVictim',
		health: 'locateHealth',
		points: 'locateHighValueVictim',
	},
}