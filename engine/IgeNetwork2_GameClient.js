IgeNetwork.prototype.setHostAndPort = function (host, port) {
	if (this.engine.isServer) {
		this.emit('error', 'Cannot set host and port because we are not a client!');
	} else {
		this._serverHost = host;
		this._serverPort = port;
	}
}

IgeNetwork.prototype.startGameClient = function () {
	// Create the socket connection to the server
	this.socket = io.connect(null, {
		'port': this._serverPort,
		'remember transport': false,
		'reconnect':false,
	});
	
	this.socket.on('connect', this.bind(this._connect));
	//this.setState(5);
}

IgeNetwork.prototype._connect = function () {
	
	if (this.socket.socket.connected && !this.socket.socket.connecting) {
		this.socket.on('message', this.bind(this._receive));
		this.socket.on('disconnect', this.bind(this._disconnect));
		
		// Store the current session id
		this.sessionId = this.socket.socket.sessionid;
		
		// Emit an event that we are connected
		this.emit('serverConnect', this.socket);
		this.log('Connected to server with session id: ' + this.sessionId);
	}
}
	
IgeNetwork.prototype._disconnect = function () {
	this.log('Disconnected from server!');
	this.emit('serverDisconnect', this.socket);
}

IgeNetwork.prototype._receive = function (data) {
	// Decode the data packet
	var packet = this.decodePacket(data);
	var command = this._commandList._reverse[packet.cmdId];
		
	if (typeof command == 'undefined') {
		this.log('Error in network packet, command is undefined!', 'error', packet);
	} else {
		var packetData = packet.data || {};
		if (command == 'serverResponse') {
			var requestId = packetData.__igeRI;
			var requestObj = this._requests[requestId];
			//delete finalData.data.__igeRI;
			if (typeof requestObj.callback == 'function') { requestObj.callback.apply(requestObj.callback, [packetData]); }
		} else {
			this.emit(command, packetData);
		}
	}
}

/* request - Client-side function that sends a client request to the server to
be handled by the game logic and evaluated. The game logic can then send a response
using the server-side function "response". */
IgeNetwork.prototype.request = function (command, sendData, callback) {
	// Store the request so we can act upon the response
	if (sendData && sendData.$local != null) { 
		var strippedData = this.engine.stripLocal(sendData);
	} else {
		sendData = sendData || {};
		var strippedData = sendData;
	}
	
	var requestIndex = this._requests.length;
	strippedData.__igeRI = requestIndex;
	this._requests[requestIndex] = {command: command, data: strippedData, callback: callback};
	
	// Send the request to the server
	this.send('clientRequest', {command: command, data: strippedData});
}