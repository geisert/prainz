/* CEXCLUDE */
IgeNetwork.prototype.startFileServer = function (callback) {
	
	if (this.engine.isServer) {
		
		// Generate a client deployment of all the required javascript files into one script, maintaining load order
		if (igeConfig.clientDeploy != null) {
			var packageData = '';
			
			if (igeConfig.mode == 'debug') {
				// Deploy for debug
				this.log('WARNING - CURRENTLY DEPLOYING IN DEBUG MODE -- DO NOT USE ON OPEN PRODUCTION SERVER');
				packageData = 'window.igeBootstrap.init(';
				packageData += JSON.stringify(igeConfig.clientDeploy);
				packageData += ');';
			} else {
				// Deploy for release
				for (var i in igeConfig.clientDeploy) {
					// Get the next file to add to the deployment file
					var localPath = igeConfig.mapUrl(igeConfig.clientDeploy[i] + '.js');
					// Read the file's data
					//this.log('Reading data to protect from: ' + localPath);
					var data = fs.readFileSync(localPath, 'utf8');
					// If we got the file's data
					if (data) {
						// Obfuscate the data
						//this.log('Packaging protected script file: ' + localPath);
						try {
							var finData = this.engine.obfuscator.obfuscate(data, null, null, true);
						} catch (err) {
							this.log('Failed to obfuscate file: ' + localPath, 'error', err);
						}
						packageData += finData + ";";
					} else {
						this.log('Failed attempt to load file from path: ' + localPath);
					}
				}
			}
			
			// Write the data to the deployment file
			fs.writeFileSync(igeConfig.mapUrl('/client/_deploy.js'), packageData);
		} else {
			//this.log('Error - You must define a string-array of files in your config.js that tells the server what files to wrap into a single deployment for the client. See http://www.isogenicengine.com/documentation/general-engine-information/game-configuration-file-config-js/', 'error');
		}
		
		// Server code
		this.log('Starting networking for the server...');
		
		// Define node requirements
		this.http = require('http');
		this.url = require('url');
		this.fs = require('fs');
		this.sys = require('sys');
		this.path = require('path');
		//this.multipart = require('../server/node/node-multipart/multipart');
		
		// Node plugin modules
		this.static = require(igeConfig.mapUrl('/node_modules/node-static'));
		this.mime = require(igeConfig.mapUrl('/node_modules/node-static/lib/node-static/mime'));
		var clientFileServer = new (this.static.Server)(igeConfig.mapUrl('/client/'));
		var assetFileServer = new (this.static.Server)(igeConfig.mapUrl('/assets/'));
		var engineFileServer = new (this.static.Server)(igeConfig.mapUrl('/engine/'));
		var moduleFileServer = new (this.static.Server)(igeConfig.mapUrl('/modules/'));
		
		// Add htm to the mime types!
		this.mime.contentTypes['htm'] = this.mime.contentTypes['html'];
		
		this.emit('serverStarting');
		
		// Create the server object
		this.httpServer = this.http.createServer(this.bind(function(req, res){
			
			if (!this._serveClients) {
				this.emit('serveFile_deny', req.url);
				
				res.writeHead(404);
				res.write('404');
				res.end();
			} else {
				this.emit('serveFile_accept', req.url);
				
				var serveEngineFile = false;
				var serveAssetFile = false;
				var serveModuleFile = false;
				var serveClientFile = false;
				var served = false;
				
				// Set some serving defaults
				if (!igeConfig.serveClient) { igeConfig.serveClient = '/'; }
				if (!igeConfig.serveAssets) { igeConfig.serveAssets = '/assets/'; }
				if (!igeConfig.serveEngine) { igeConfig.serveEngine = '/engine/'; }
				
				// Get the path of the requested resource / file
				var path = this.url.parse(req.url).pathname;
				
				// Remove any serverFrom string data from the path
				if (igeConfig.serveClient != '/') {
					path = path.replace(igeConfig.serveClient, '');
					if (path == '') { path = '/'; }
				}
				
				// Remove the ability to get lower down the dir chain
				if (path.replace('..', '') != path) {
					// Access attempted to a folder that we don't want to serve from
					this.log('Failed attempt to load file from path because url contains ".." (' + req.connection.remoteAddress + '): ' + path);
					res.writeHead(404);
					res.write('404');
					res.end();
				}
				
				// Check for an engine file request
				var mappedPath = igeConfig.mapUrl(path, true);
				var pathType = mappedPath[0];
				var localPath = mappedPath[1];
				
				// Define the path that the static file server would use to access the file,
				// given that the static server is "listening" in a path already
				var adjustedPath = localPath.replace(mappedPath[2], '/');
				
				switch (pathType) {
					case 'engine':
						serveEngineFile = true;
					break;
					
					case 'assets':
						serveAssetFile = true;
					break;
					
					case 'modules':
						serveModuleFile = true;
					break;				
					
					case 'client':
						serveClientFile = true;
						if (path == '/') {
							path = '/index.htm';
							mappedPath = igeConfig.mapUrl(path, true);
							localPath = mappedPath[1];
							adjustedPath = '/index.htm';
						}
					break;				
				}
				
				/*
				console.log('-----------------------------------------');
				console.log('request url: ' + req.url);
				console.log('localPath:', localPath);
				console.log('adjustedPath:', adjustedPath);
				console.log('type', pathType);
				console.log('-----------------------------------------');
				*/
				
				this.path.exists(localPath, this.bind(function (exists) {
					if (exists) {
						// File exists
						if (this.engine.config.mode == 'release' && path.substr(path.length - 3, 3) == '.js') {
							// The file is JavaScript and we're in release mode so we need to obfuscate and pack all JS data
							this.serveObfuscated(req, res, localPath);
						} else {
							// Serve the file via static
							switch (pathType) {
								case 'engine':
									this.log('Serving engine data file (' + req.connection.remoteAddress + '): ' + localPath);
									engineFileServer.serveFile(adjustedPath, 200, {}, req, res);
								break;
								
								case 'assets':
									this.log('Serving asset data file (' + req.connection.remoteAddress + '): ' + localPath);
									assetFileServer.serveFile(adjustedPath, 200, {}, req, res);
								break;
								
								case 'modules':
									this.log('Serving module data file (' + req.connection.remoteAddress + '): ' + localPath);
									moduleFileServer.serveFile(adjustedPath, 200, {}, req, res);
								break;
								
								case 'client':
									this.log('Serving game data file (' + req.connection.remoteAddress + '): ' + localPath);
									clientFileServer.serveFile(adjustedPath, 200, {}, req, res);
								break;
							}
						}
					} else {
						// File does not exist
						// Check the server configuration for the file to see if we should serve it
						for (var i in this.engine.config.serveFile) {
							if (this.engine.config.serveFile[i].file != null) {
								if (this.engine.config.serveFile[i].file == path) {
									//console.log('path exists in config');
									if (this.engine.config.serveFile[i].type != null) {
										// We are serving a file so output it!
										fs.readFile(__dirname + path, function(err, data) {
											if (!err) {
												this.log('Serving config-registered static file data (' + req.connection.remoteAddress + '): ' + path);
												res.writeHead(200, {'Content-Type': this.engine.config.serveFile[i].type})
												res.write(data, 'utf8');
												res.end();
												served = true;
											} else {
												// Access attempted to a folder that we don't want to serve from
												this.log('Failed attempt to load config-registered static file from path (' + req.connection.remoteAddress + '): ' + path, 'warning');
												res.writeHead(404);
												res.write('404');
												res.end();
												served = true;
											}
										});
									}
									
									if (this.engine.config.serveFile[i].call != null) {
										// We need to call a function so do it
										this.log('Serving config-registered dynamic file data (' + req.connection.remoteAddress + '): ' + path);
										this.engine.config.serveFile[i].call.apply(this.engine.config.serveFile[i].context || this, [req, res]);
										served = true;
									}
								}
							}
						}
						
						// Check if we STILL haven't served a file... if so then 404 the request!
						if (!served) {
							try {
								// Access attempted to a folder that we don't want to serve from
								this.log('Failed attempt to load file from path (' + req.connection.remoteAddress + '): ' + path, 'warning');
								res.writeHead(404);
								res.write('404');
								res.end();
								served = true;
							} catch (err) {
								// There might have been an error so output it and continue. We don't want to crash here!
								this.log('Error when a file serve request was being processed in IgeNetwork2_FileServer.js: ' + err, 'warning');
							}
						}
					}
				}));
			}
		}));
		
		// Starting listener
		this.setState(4);
		this.httpServer.listen(this.engine.config.port, null, this.bind(function () {
			this.emit('fileServerStarted');
			if (typeof callback == 'function') {
				callback.apply(this);
			}
		}));
		
	}
	
}

IgeNetwork.prototype.serveObfuscated = function (req, res, localPath) {
	fs.readFile(localPath, 'utf8', this.bind(function(err, data) {
		if (!err) {
			// Server obfuscated js
			if (this.engine.config.mode == 'release') {
				this.log('Serving protected JavaScript file (' + req.connection.remoteAddress + '): ' + localPath);
			} else {
				this.log('Serving JavaScript file (' + req.connection.remoteAddress + '): ' + localPath);
			}
			var finData = this.engine.obfuscator.obfuscate(data);
			res.writeHead(200, {'Content-Type': 'text/javascript'});
			res.write(finData, 'utf8');
			res.end();
		} else {
			this.log('Failed attempt to load JavaScript file from path (' + req.connection.remoteAddress + '): ' + localPath, 'warning');
			res.writeHead(404);
			res.write('404');
			res.end();
		}
	}));
},

IgeNetwork.prototype.setState = function (state) {
	this.state = state;
	this.emit('stateChanged', state);
}
/* CEXCLUDE */