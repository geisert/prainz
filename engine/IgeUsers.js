/** IgeUsers - Manages user registration, authentication and user-specific data storage. {
	engine_ver:"0.2.2",
	category:"class",
} **/
/** registerAccepted - Fired when a user has successfully registered. {
	category: "event",
	arguments: [{
		type:"object",
		name:"user",
		desc:"The user data that emitted the event.",
	}],
	engine_ver:"0.2.2",
} **/
/** registerFailed - Fired when a user has failed registration. {
	category: "event",
	arguments: [{
		type:"object",
		name:"user",
		desc:"The user data that emitted the event.",
	}],
	engine_ver:"0.2.2",
} **/
/** authAccepted - Fired when a user has authenticated successfully. {
	category: "event",
	arguments: [{
		type:"object",
		name:"user",
		desc:"The user data that emitted the event.",
	}],
	engine_ver:"0.2.2",
} **/
/** authFailed - Fired when a user has failed authentication. {
	category: "event",
	arguments: [{
		type:"object",
		name:"user",
		desc:"The user data that emitted the event.",
	}],
	engine_ver:"0.2.2",
} **/
IgeUsers = new IgeClass({
	
	Extends: [IgeCollection, IgeNetworkItem, IgeItem, IgeEvents],
	
	/** engine - A reference object to the main engine instance. {
		category:"property",
		type:"object",
		instanceOf:"IgeEngine",
	} **/
	engine: null,
	
	/** byIndex - An array with an integer index that allows you to access every
	item created using the create method in the order it was created. {
		category:"property",
		type:"array",
		index:"integer",
	} **/
	byIndex: null,
	byId: null,
	bySessionId: null,
	byUserDbId: null,
	
	/** init - The constructor for this class. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a new instance of this class",
		},
		argument: {
			type:"object",
			name:"engine",
			desc:"The active engine instance.",
			instanceOf:"IgeEngine",
		},
	} **/
	init: function (engine) {
		this._className = 'IgeUsers';
		
		this.engine = engine;
		
		this.byIndex = [];
		this.byId = [];
		this.bySessionId = [];
		this.byUserId = [];
		
		this.collectionId = 'user';
		// Check if we have a network
		if (this.engine._IgeNetwork) {
			// Call the networkInit method of this class
			this.networkInit();
		}	
	},
	
	networkInit: function () {
		// Network User Commands
		this.engine.network.registerCommand('userRegister', null, this.bind(this._register, true));
		this.engine.network.registerCommand('userRegisterAccepted', this.bind(this._registerAccepted));
		this.engine.network.registerCommand('userRegisterFailed', this.bind(this._registerFailed));
		this.engine.network.registerCommand('userAuth', null, this.bind(this._auth, true));
		this.engine.network.registerCommand('userAuthAccepted', this.bind(this._authAccepted));
		this.engine.network.registerCommand('userAuthFailed', this.bind(this._authFailed));
		
		// Register default entity network properties - the properties that are automatically updated to 
		// clients if they change in an entity's data
		this.engine.network.registerNetProps(this._className, [
			'user_online',
		]);
		
		// Register standard property names to the network manifest see the API manual for more details
		this.engine.network.addToManifest([
			'user_id',
		]);
	},
	
	/** _itemDefaults - Called by the IgeItem class which this class extends when creating a new item. Here you can define
	any object properties that are default across all items of this class. If this method returns false the create action
	will be cancelled. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns false on failure or true on success.",
		},
		arguments: [{
			type:"object",
			name:"pItem",
			desc:"The item object to be checked.",
		}],
	} **/
	_itemDefaults: function (pItem) {
		this.ensureLocalExists(pItem);
		/* CEXCLUDE */
		if (this.engine.isServer) {
			//this.ensurePersistExists(pItem);
			// Ensure the user is not set to propagate past the server
			pItem.user_locale = LOCALE_SERVER_ONLY + LOCALE_DB;
			pItem.user_persist = true;
		}
		/* CEXCLUDE */
		this.ensureIdExists(pItem);
		
		return true;
	},
	
	/** _itemIntegrity - Called by the IgeItem class which this class extends when creating a new item. Checks the
	integrity of an item before it is allowed to be created. Here you can define custom checks to ensure an item being
	created conforms to the standard your class requires. If this method returns false the create action will be cancelled. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns false on failure or true on success.",
		},
		arguments: [{
			type:"object",
			name:"pItem",
			desc:"The item object to be checked.",
		}],
	} **/
	_itemIntegrity: function (pItem) {
		// Check that this item does not already exist
		if (this.byId[pItem.user_id]) {
			this.log('Attempted to create ' + this.collectionId + ' that already exists with id: ' + pItem[this.collectionId + '_id'], 'info', pItem);
			return false;
		}
		
		// Check that this item has a session_id
		if (!pItem.session_id) {
			this.log('Attempted to create ' + this.collectionId + ' that has no session_id: ' + pItem[this.collectionId + '_id'], 'info', pItem);
			return false;
		}
		
		// Check that this item's session id is not already registered
		if (this.bySessionId[pItem.session_id]) {
			this.log('Attempted to create ' + this.collectionId + ' that already exists with session_id: ' + pItem['session_id'], 'info', pItem);
			return false;
		}
		
		// No integrity checks failed so return true to continue processing this item
		return true;
	},
	
	/** _create - The private method that is called by IgeItem.create and does the actual creation part. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns false on failure or null for any other reason.",
		},
		arguments: [{
			type:"object",
			name:"pItem",
			desc:"The background object to be created.",
			link:"backgroundData",
		}],
	} **/
	_create: function (pItem) {
		this.byIndex.push(pItem);
		this.byId[pItem[this.collectionId + '_id']] = pItem;
		this.bySessionId[pItem.session_id] = pItem;
		
		// Check for a callback function embedded in the entity
		if (typeof(pItem.$local.create_callback) == 'function') {
			pItem.$local.create_callback.apply(this, [pItem]);
			pItem.$local.create_callback = null;
		}
		
		this.emit('afterCreate', pItem);
	},
	
	/** _update - Updates the class collection item with the matching id specified in the updData
	parameter with all properties of the pData parameter. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"bool",
			desc:"Returns false on failure or the updated item on success.",
		},
		arguments: [{
			type:"object",
			name:"pItem",
			desc:"The object to receive updated property values.",
		}, {
			type:"object",
			name:"pData",
			desc:"The property values to apply to the pItem object.",
		}],
	} **/
	_update: function (pItem, pData) {
		// Grab the current object
		var pItem = this.read(pItem);

		if (pItem != null) {
			// Emit the beforeUpdate event and halt if any listener returns the cancel flag
			if (!this.emit('beforeUpdate', pItem)) {
				
				
				// Update the current object with the new object's properties
				for (var i in pData) {
					// Remove the item from any lookup arrays if the keys have changed
					switch (i) {
						case this.collectionId + '_id':
							// id update
							delete this.byId[pItem[i]];
							this.byId[pData[i]] = pItem;
						break;
						
						case 'session_id':
							// session_id update
							delete this.bySessionId[pItem[i]];
							this.bySessionId[pData[i]] = pItem;
						break;
					}
					
					pItem[i] = pData[i];
				}
				
				this.emit('afterUpdate', pItem);
				
				if (typeof(pItem.$local.update_callback) == 'function') {
					pItem.$local.update_callback(pItem);
					pItem.$local.update_callback = null;
				}
				
				return pItem;
			} else {
				// Call the callback with no argument meaning there was a cancellation or error
				if (typeof(pItem.$local.update_callback) == 'function') {
					pItem.$local.update_callback();
					pItem.$local.update_callback = null;
				}
			}
		} else {
			this.log('Cannot update item because it could not be located by calling this.read().', 'error', pItem);
			return false;
		}
		
	},	
	
	readBySessionId: function (sessionId) {
		if (typeof(sessionId) == 'string' || typeof(sessionId) == 'number') {
			return this.bySessionId[sessionId] || null;
		} else {
			this.log('Cannot find user by sessionId because the passed id is not a string or integer!', 'warning', sessionId);
			return false;
		}
	},
	
	/** register - Asks the server to register a user with the passed username, password and data. {
		category:"method",
		engine_ver:"0.2.2",
		arguments: [{
			type:"string",
			name:"username",
			desc:"The username to register the user by.",
		}, {
			type:"string",
			name:"password",
			desc:"The password to register the user by.",
		}, {
			type:"object",
			name:"data",
			desc:"Arbitrary data to store in the user object. Can only be a valid JavaScript variable and contain no functions or recursive references.",
		}],
	} **/
	register: function (username, password, data) {
		this.engine.network.send('userRegister', {username:username, password:password, data:data});
	},
	
	/** auth - Asks the server to authenticate a user based upon the passed username and password. {
		category:"method",
		engine_ver:"0.2.2",
		arguments: [{
			type:"string",
			name:"username",
			desc:"The username to authenticate the user by.",
		}, {
			type:"string",
			name:"password",
			desc:"The password to authenticate the user by.",
		}],
	} **/
	auth: function (username, password) {
		this.engine.network.send('userAuth', {username:username, password:password});
	},
	
	/** store - Stores aribtrary data in the user object (and database entry). The data will be stored in the user
	object by the property name give. {
		category:"method",
		engine_ver:"0.2.2",
		arguments: [{
			type:"object",
			name:"user",
			desc:"The user object to store the data in.",
		}, {
			type:"string",
			name:"prop",
			desc:"The property to store the data under.",
		}, {
			type:"object",
			name:"data",
			desc:"Arbitrary data to store in the user object. Can only be a valid JavaScript variable and contain no functions or recursive references. If null, will instruct the engine to completely delete the existing property and any data stored in it.",
		}],
	} **/
	store: function (user, prop, data) {
		if (data == null) {
			// Remove the property
			delete user[prop];
		} else {
			user[prop] = data;
		}
		
		this.dbUpdate(user, this.bind(function (err, insertedDocs) {
			if (err) {
				this.log('Could not update user in DB with specified data!', 'warning', user);
			}
		}));
	},
	
	/* CEXCLUDE */
	/* _register - Called when a client requests a user registration. */
	_register: function (data, client) {
		// Check the database to make sure this username is not already taken
		this.engine.database.findAll(this.collectionId, {user_username:data.username}, this.bind(function (gotData, userData) {
			if (gotData) {
				// User was found, fail the registration
				this.engine.network.send('userRegisterFailed', {reason:'userInUse'}, client.id);
				this.emit('registerFailed', data);
			} else {
				// No user found, create a new record
				var user = {
					user_username:data.username,
					user_password:data.password,
					user_data:data.data
				}
				
				this.engine.database.insert(this.collectionId, user, this.bind(function (err, insertedDocs) {
					if (!err) {
						// Send a register success net message
						this.engine.network.send('userRegisterAccepted', null, client.id);
						this.emit('registerAccepted', insertedDocs);
					} else {
						this.log('Error inserting new registered user:' + err, 'warning', user);
						this.engine.network.send('userRegisterFailed', {reason:'dbError', errText:err}, client.id);
						this.emit('registerFailed', user);
					}
				}));
			}
		}));
	},
	
	/* _auth - Called when a client requests a user authorisation. */
	_auth: function (data, client) {
		// Ask the database for the user's details if it exists
		this.engine.database.findAll(this.collectionId, {user_username:data.username, user_password:data.password}, this.bind(function (gotData, userData) {
			if (gotData) {
				// User was found, store the user data in the class
				this.log('User authorised with data: ', userData);
				userData[0].session_id = client.session_id;
				
				this.create(client, this.bind(function () {
					// Send an auth accepted net message
					this.engine.network.send('userAuthAccepted', null, client.id);
					this.emit('authAccepted', data);
				}));
			} else {
				// No user found
				this.log('User not authorised with auth data: ', data);
				
				// Send an auth failed net message
				this.engine.network.send('userAuthFailed', null, client.id);
				this.emit('authFailed', data);
			}
		}));
	},
	
	/** saveUser - Saves user data to the database. If the user already exists (by username) the user is
	updated rather than inserted. {
		category:"method",
		engine_ver:"0.2.3",
		arguments: [{
			type:"object",
			name:"data",
			desc:"The user object to store the data in.",
		}, {
			type:"object",
			name:"createData",
			desc:"An object that will be mixed into the data object if the save is a create. Enter null for no mixin properties.",
		}, {
			type:"object",
			name:"updateData",
			desc:"An object that will be mixed into the data object if the save is an update. Enter null for no mixin properties.",
		}, {
			type:"function",
			name:"callback",
			desc:"A method to callback when the operation is complete or has experienced an error.",
		}],
	} **/
	saveUser: function (data, createData, updateData, callback) {
		// Check if the user is in memnory
		if (!this.readBySessionId(data.session_id)) {
			
		}
		
		// Check the database to make sure this username is not already taken
		this.engine.database.findAll(this.collectionId, {user_username:data.user_username}, this.bind(function (gotData, tempData) {
			if (gotData) {
				// User was found, update!
				if (typeof(updateData) == 'object') {
					for (var i in updateData) {
						data[i] = updateData[i];
					}
				}
				this.engine.database.flexUpdate(this.collectionId, {user_username:data.user_username}, data, this.bind(function (err, doc) {
					if (!err) {
						// No error, callback!
						this.log('User ' + data.user_username + ' data record updated');
						
						// Now grab the updated record to return!
						this.engine.database.findOne(this.collectionId, {user_username:data.user_username}, this.bind(function (gotData, userData) {
							if (gotData) {
								if (typeof(callback) == 'function') {
									callback(err, userData);
								}
							} else {
								this.log('Error getting user data from the DB after updating user data!', 'warning', data);
							}
						}));
					} else {
						this.log('Error updating existing user record:' + err, 'warning', data);
						if (typeof(callback) == 'function') {
							callback(err, data);
						}
					}
				}));
			} else {
				// No user found, create a new record
				if (typeof(createData) == 'object') {
					for (var i in createData) {
						data[i] = createData[i];
					}
				}
				this.engine.database.insert(this.collectionId, data, this.bind(function (err, doc) {
					if (!err) {
						this.log('User ' + data.user_username + ' data record created');
						// Now grab the updated record to return!
						this.engine.database.findOne(this.collectionId, {user_username:data.user_username}, this.bind(function (gotData, userData) {
							if (gotData) {
								if (typeof(callback) == 'function') {
									callback(err, userData);
								}
							} else {
								this.log('Error getting user data from the DB after creating user data!', 'warning', data);
							}
						}));
					} else {
						this.log('Error inserting new user record:' + err, 'warning', data);
						if (typeof(callback) == 'function') {
							callback(err, data);
						}
					}
				}));
			}
		}));
	},
	/* CEXCLUDE */
	
	_registerAccepted: function (data) {
		this.emit('registerAccepted', data);
	},
	
	_registerFailed: function (data) {
		this.emit('registerFailed', data);
	},
	
	_authAccepted: function (data) {
		this.emit('authAccepted', data);
	},
	
	_authFailed: function (data) {
		this.emit('authFailed', data);
	},
	
	login:function () {},
	logout:function () {},
	
});