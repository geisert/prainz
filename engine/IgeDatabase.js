IgeDatabase = new IgeClass({
	
	Extends: IgeEvents,
	
	engine: null,
	wrapper: null,
	
	byIndex: [],
	byId: [],
	currentWrapper: '',
	
	// Constructor
	init: function (engine) {
		this._className = 'IgeDatabase';
		
		this.engine = engine;
		
		this.byIndex = [];
		this.byId = [];
		this.currentWrapper = '';
	},
	
	setProvider: function (provider) {
		this.log('Selecting new database provider "' + provider + '"...');
		if (this._currentProvider) {
			// A provider is already in place
			this.log('An existing provider is already in place, removing: ' + this._currentProvider);
			this._currentProvider = null;
		}
		
		if (this._providers[provider] && this._providers[provider].classMethod) {
			this.log('Provider found, absorbing provider...');
			this.absorbClass(this._providers[provider].classMethod.prototype);
			if (this._currentProvider) {
				this.log('Provider absorbed successfully!');
			} else {
				this.log('Error absorbing provider class properties!', 'error');
			}
		} else {
			this.log('Cannot select provider "' + provider + '" because it does not exist.', 'error');
		}
	},
	
});