/* CEXCLUDE */
IgeNetwork.prototype.startGameServer = function (callback) {
	this.log("Starting game server listening on port: " + this.engine.config.port);
	this.socketio = require(igeConfig.mapUrl('/node_modules/socket.io'));
	
	// Create the socket.io object and a buffer for our data
	this.socket = this.socketio.listen(this.httpServer);
	this.socket.configure(this.bind(function () {
		this.socket.set('log level', 0);
		this.socket.enable('browser client minification');
		this.socket.set('transports', [
			'websocket',
			'flashsocket',
			'htmlfile',
			'xhr-polling',
			'jsonp-polling',
		]);
	}));
	
	//this.clients = this.socket.clients;
	
	this.socket.sockets.on('connection', this.bind(this._connect));
	//this.socket.sockets.on('clientDisconnect', this.bind(this._disconnect));
	//this.socket.sockets.on('clientMessage', this.bind(this._receive));
	
	// Server is UP!!
	this.setState(5);
	this.emit('gameServerStarted');
	
	// Setup the sync tick
	setInterval(this.bind(this._sendSyncData), Math.floor(1000 / this._cl_updaterate));
	
	if (typeof callback == 'function') {
		callback.apply(this);
	}
}

IgeNetwork.prototype._connect = function (client) {
	//console.log(client.handshake.address.address);
	if (!this._serveClients) {
		client.disconnect();
	} else {
		client.on('message', this.bind(this._receive));
		client.on('disconnect', this.bind(this._disconnect));
		
		this.log('Client connected with session id ' + client.id + ' from IP ' + client.handshake.address.address + ':' + client.handshake.address.port);
		this.emit('clientConnect', client);
	}
}
	
IgeNetwork.prototype._disconnect = function (reason, clientSessionId) {
	console.log('_disconnect method args:', arguments);
	this.log('Client disconnected from session id ' + clientSessionId);
	this.emit('clientDisconnect', clientSessionId);
}

IgeNetwork.prototype._receive = function (data, client) {
	// Decode the data packet
	var packet = this.decodePacket(data);
	var command = this._commandList._reverse[packet.cmdId];
	
	if (typeof command == 'undefined') {
		this.log('Error in network packet, command is undefined!', 'error', packet);
	} else {
		var packetData = packet.data || {};
		if (command == 'clientRequest') {
			//console.log('IGEN REQ DATA:', packet);
			var requestId = packetData.data.__igeRI;
			delete packetData.data.__igeRI;
			this.emit('clientRequest', {command:packetData.command, data:packetData.data, requestId:requestId, client:client});
		} else {
			this.emit(command, [packetData, client]);
		}
	}
}

/* registerSyncProvider - Stores a reference to a method that can be called to return
sync data from a collection. */
IgeNetwork.prototype.registerSyncProvider = function (collectionName, func) {
	this._syncProvider = this._syncProvider || [];
	this._syncProvider.push([collectionName, func]);
}

/* registerSyncComplete - Stores a reference to a method that can be called to clear
sync data from a collection. */
IgeNetwork.prototype.registerSyncComplete = function (collectionName, func) {
	this._syncComplete = this._syncComplete || [];
	this._syncComplete.push([collectionName, func]);
}

/* _sendSyncData - Asks all registered provider methods for sync data which is then
turned into data packets ready for transmission. The method returns an array of data
packets inside an array of collection names. */
IgeNetwork.prototype._sendSyncData = function () {
	var syncProviders = this._syncProvider;
	var syncProviderCount = syncProviders.length;
	
	// Loop through all the sync data providers
	while (syncProviderCount--) {
		var provider = syncProviders[syncProviderCount];
		// Get the sync data from the provider
		var tmpData = provider[1].call();
		// Get the delta data command id for this collection from the command list
		var cmdId = this._commandList[provider[0] + 'sDeltaData'];
		// Send the data to all clients
		for (var i in tmpData) {
			//this.log('Sending sync data to client', 'info', tmpData[i]);
			this.send(provider[0] + 'sDeltaData', tmpData[i]);
		}
		
		// Tell the sync provider we have completed the sync
		this._syncComplete[syncProviderCount][1].call();
	}
}

/** response - Sends a network response to a recieved network request. {
	category:"method",
	flag:"server",
	arguments: [{
		type:"string",
		name:"requestId",
		desc:"The request id that you want to send a response to.",
	}, {
		type:"object",
		name:"data",
		desc:"The data to send back with the response message.",
	}, {
		type:"multi",
		name:"client",
		desc:"Either a socket.io client (object) or a client session id (string) to send the message to.",
	}],
} **/
IgeNetwork.prototype.response = function (requestId, data, client) {
	// Send the request reponse to the client
	this.send('serverResponse', {__igeRI: requestId, data: data}, client);
}
/* CEXCLUDE */