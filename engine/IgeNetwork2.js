/** IgeNetwork2 - The network management class. {
	engine_ver:"0.2.0",
	category:"class",
} **/

/** serverStarted - Fired after the http server has started up. {
	category: "event",
} **/
IgeNetwork = new IgeClass({
	
	Extends: IgeEvents,
	
	/** engine - A reference object to the main engine instance. {
		category:"property",
		engine_ver:"0.1.0",
		type:"object",
		instanceOf:"IgeEngine",
	} **/
	engine: null,
	
	/** bison - An instance of the BiSON.js library (lib_bison.js). {
		category:"property",
		engine_ver:"0.2.0",
		type:"object",
	} **/
	bison: null,
	
	/** io - An object holding connection provider libraries such as socket.io and pusher. {
		category:"property",
		type:"object",
	} **/
	io: null,
	
	/** socketio - The socket.io library instance. {
		category:"property",
		type:"object",
	} **/
	socketio: null,
	
	/** socket - The socket.io connection instance object. {
		category:"property",
		type:"object",
		
	} **/
	socket: null,
	
	/** _commandList - An enum of network commands, each command is set by using the 'registerCommand' method. {
		category:"property",
		engine_ver:"0.1.0",
		type:"object",
		instanceOf:"IgeEnum",
	} **/	
	_commandList: null,
	
	/** _requests - An array of request ids for tracking and responding to async requests (internal use). {
		category:"property",
		engine_ver:"0.1.0",
		type:"array",
	} **/
	_requests: null,
	
	/** _sequenceId - An integer counter for use when generating sequence ids (internal use). {
		category:"property",
		engine_ver:"0.1.0",
		type:"integer",
	} **/
	_sequenceId: null,
	
	/** _timeStep - The time in milliseconds between networking time steps (internal use). {
		category:"property",
		type:"integer",
	} **/	
	_timeStep: null,
	
	/** _syncProvider - An array of methods that provide sync data before a time step (internal use). {
		category:"property",
		type:"array",
	} **/
	_syncProvider: null,
	
	/** _syncComplete - An array of methods that provide callbacks after data has been used in a time step (internal use). {
		category:"property",
		type:"array",
	} **/
	_syncComplete: null,
	
	/** _serveClients - Determines if connecting clients are allowed to continue with conection or are just dropped. (internal use). {
		category:"property",
		type:"bool",
	} **/	
	_serveClients: null,
	
	/** netProps - An enum containing data about which item properties of a class should
	propagate to connected clients. {
		engine_ver:"0.2.0",
		category:"property",
		type:"array",
		index:"multi",
		instanceOf:"IgeEnum",
	} **/
	netProps: null,
	
	/** _netClassByPropId - An array containing data about network property ids and the classes
	they map to (internal use). {
		engine_ver:"0.2.0",
		category:"property",
		type:"array",
		index:"integer",
	} **/
	_netClassByPropId: null,
	
	/** _netPropByPropId - An array containing data about network property ids and the properties
	they map to (internal use). {
		engine_ver:"0.2.0",
		category:"property",
		type:"array",
		index:"integer",
	} **/
	_netPropByPropId: null,
	
	/** init - The constructor for this class. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a new instance of this class",
		},
		argument: {
			type:"object",
			name:"engine",
			desc:"The active engine instance.",
			instanceOf:"IgeEngine",
		},
	} **/
	init: function (engine) {
		this._className = 'IgeNetwork2';
		
		this.engine = engine;
		this.engine.network = this;
		this.netProps = new IgeEnum();
		
		/* CEXCLUDE */
		if (this.engine.isServer) {
			this.bison = BISON;
		}
		/* CEXCLUDE */
		
		if (!this.engine.isServer) {
			this.bison = window.BISON;
		}
		
		// Set internal variable init values
		this._commandList = new IgeEnum();
		this._manifest = new IgeEnum();
		this.io = this.io || [];
		this._requests = [];
		this._syncProvider = [];
		this._syncComplete = [];
		this._dataRecv = [];
		this._dataRecvAll = 0;
		this._dataSend = [];
		this._dataSendAll = 0;
		
		this._sequenceId = 0;
		this._tickrate = 15; // Number of milliseconds between timesteps (ticks)
		this._cl_updaterate = 20; // The number of updates per second to send to the client
		this._cl_minimumrate = 10; // The minimum number of updates per second a client can handle before being auto-disconnected
		this._cl_cmdrate = 30; // The number of command packets a client will send per second
		
		// Register some global network commands
		this._commandList.add(['clientConnect', 'clientReady', 'clientDisconnect', 'clientRequest', 'serverResponse']);
	},
	
	/** registerCommand - Register a client / server network command. {
		category:"method",
		engine_ver:"0.1.2",
		arguments: [{
			type:"object",
			name:"commandName",
			desc:"The name of the command to register.",
		}, {
			type:"method",
			name:"callback1",
			desc:"When only callback1 is specified, this is the callback method to fire when the network command is received by the engine on either client or server. When callback1 and callback2 are specified together, callback1 will be fired when this network command is received on the client and callback2 will be fired when received on the server.",
		}, {
			type:"method",
			name:"callback2",
			desc:"When callback1 and callback2 are specified together, callback1 will be fired when this network command is received on the client and callback2 will be fired when received on the server.",
			flags:"optional",
		}],
	} **/
	registerCommand: function (commandName, callback1, callback2) {
		// Check that the command does not already exist
		if (typeof this._commandList[commandName] == 'undefined') {
			this._commandList.add([commandName]);
			
			// Check what mode we're in (1 callback is standard, 2 means client/server)
			if (callback2 == null) {
				// Standard mode
				if (typeof callback1 == 'function') { this.on(commandName, callback1, this); }
			} else {
				// Client-server mode
				if (!this.engine.isServer) {
					// Callback 1 is client-side
					if (typeof callback1 == 'function') { this.on(commandName, callback1, this); }
				} else {
					// Callback 2 is server-side
					if (typeof callback2 == 'function') { this.on(commandName, callback2, this); }
				}
			}
		} else {
			this.log('An attempt to register network command "' + commandName + '" failed because a command by the same name has already been registered.', 'warning');
		}
	},
	
	/** start - On a client, will start client networking. On the server, will start the file server and then the game server. {
		category:"method",
		engine_ver:"0.1.0",
	} **/
	start: function () {
		/* CEXCLUDE */
		if (this.engine.isServer) {
			this.log('Starting server...');
			this.startFileServer(this.bind(function () {
				this._start(this.bind(function () {
					// Tell listeners that the entire server has started
					this.emit('serverStarted');
				}));
			}));
			
		}
		/* CEXCLUDE */
		
		if (!this.engine.isServer) {
			this.log('Starting networking for the client...');
			this._start();
		}		
	},
	
	/** send - Sends a network command using the current socket.io connection. {
		category:"method",
		engine_ver:"0.1.0",
		arguments: [{
			type:"string",
			name:"command",
			desc:"The name of the command to send.",
		}, {
			type:"object",
			name:"sendData",
			desc:"When only callback1 is specified, this is the callback method to fire when the network command is received by the engine on either client or server. When callback1 and callback2 are specified together, callback1 will be fired when this network command is received on the client and callback2 will be fired when received on the server. If omitted, will send a blank object as the data to the listener.",
			flags:"optional",
		}, {
			type:"multi",
			name:"clientId",
			desc:"Only alters behaviour of the 'send' method if running as server. Either a socket.io client (object) or a client session id (string) to send the message to. If omitted, will broadcast to all clients.",
			flags:"optional, server",
		}, {
			type:"string",
			name:"debug",
			desc:"If set and the command parameter matches the value of this parameter, the packet data being sent will be output via the console.",
			flags:"optional",
		}],
	} **/
	sendold: function (command, sendData, clientId, debug) {
		
		// Check that we have a socket object to work with
		if (this.socket != null) {
			if (sendData && sendData.$local != null) {
				var strippedData = this.engine.stripLocal(sendData);
			} else {
				var strippedData = sendData;
			}
			
			var cmdId = this._commandList[command];
			
			if (cmdId != null) {
				// Create and encode a new data packet
				var finalPacketData = this.encodePacket(this._commandList[command], null, strippedData);
				if (command == debug) {
					console.log('Network original:', strippedData);
					/*if (strippedData != null && strippedData.template_contents != null) {
						console.log('Template object:', typeof(strippedData.template_contents));
						for (var i in strippedData.template_contents) {
							console.log(i, typeof(strippedData.template_contents[i]));
						}
					}*/
					console.log('Network sending:', this.decodePacket(finalPacketData));
				}
				// Depending upon if we are client or server, send the packet accordingly
				/* CEXCLUDE */
				if (this.engine.isServer) {
					// Server code
					if (clientId) {
						if (clientId instanceof Array) {
							// An array of client id's to send to
							//console.log('Sending data to clients ID by array: ', clientId);
							for (var i in clientId) {
								this.socket.sockets.socket(clientId[i]).json.send(finalPacketData);
							}
						} else if (typeof clientId == 'object') {
							// A single client object to send to
							//console.log('Sending data to client ID by object: ', clientId.id);
							clientId.json.send(finalPacketData);
						} else {
							// A single client id to send to
							//console.log('Sending data to client ID: ', clientId);
							this.socket.sockets.socket(clientId).json.send(finalPacketData);
						}
					} else {
						//console.log('Sending data by broadcast');
						//this.socket.sockets.json.send('test');
						this.socket.sockets.json.send(finalPacketData);
					}
				}
				/* CEXCLUDE */
				if (!this.engine.isServer){
					// Client code
					//console.log('Sending data to server...');
					//console.log(this.socket.send);
					this.socket.json.send(finalPacketData);
				}
				return true;
			} else {
				this.log('Attempted to send a network packet using a command that does not exist!', 'warning', command);
				return false;
			}
		}
	},
	
	/** getNetIdFromProp - Returns the integer id of the class + property name string. Used to map class and property names to
	an integer id for use when communicating changes across the network with efficient packet data. {
		category:"method",
		engine_ver:"0.2.0",
		return: {
			type:"integer",
			desc:"Returns a unique integer value that represents the class and property provided.",
		},
		arguments: [{
			type:"string",
			name:"className",
			desc:"The name of the class that the item properties belong to. This is usually the value stored in this._className.",
		}, {
			type:"array",
			name:"propName",
			desc:"The property name to return an id for.",
			index:"integer",
		}],
	} **/	
	getNetIdFromProp: function (className, propName) {
		return this.netProps[className + '_' + propName];
	},
	
	/** getNetPropFromId - Returns the string name of the network property id given. Used to map network property ids to
	a string. {
		category:"method",
		engine_ver:"0.2.0",
		return: {
			type:"array",
			desc:"Returns an array containing two string items in this format - [0] = class name [1] = property name.",
		},
		arguments: [{
			type:"string",
			name:"className",
			desc:"The name of the class that the item properties belong to. This is usually the value stored in this._className.",
		}, {
			type:"array",
			name:"propName",
			desc:"The property name to return an id for.",
			index:"integer",
		}],
	} **/	
	getNetPropFromId: function (netPropId) {
		return [this._netClassByPropId[netPropId], this._netPropByPropId[netPropId]];
	},
	
	/** registerNetProps - Registers properties of a class item that are to be monitored and propagated
	across the network when they change. The propArray argument must be an array regardless of if you
	are registering one property or multiple properties. {
		category:"method",
		engine_ver:"0.2.0",
		arguments: [{
			type:"string",
			name:"className",
			desc:"The name of the class that the item properties belong to. This is usually the value stored in this._className.",
		}, {
			type:"array",
			name:"propArray",
			desc:"An array of property names to register as networked properties.",
			index:"integer",
		}],
	} **/
	registerNetProps: function (className, propArray) {
		// Append the class name to the property array items
		for (var i in propArray) {
			propArray[i] = className + '_' + propArray[i];
		}
		
		// Add the properties to the netProps enum
		this.netProps.add(propArray);
		
		// Register the class name that the properties belong to for reverse lookup
		this._netClassByPropId = this._netClassByPropId || [];
		this._netPropByPropId = this._netPropByPropId || [];
		for (var i in propArray) {
			var netPropId = this.getNetIdFromProp(className, propArray);
			this._netClassByPropId[netPropId] = className;
			this._netPropByPropId[netPropId] = propArray[i];
		}
	},
	
	/** allowConnections - Sets the internal flag that determines if files and client connections
	are served or not. If passed true this will turn serving on, passing false will turn it off. {
		category:"method",
		engine_ver:"0.2.0",
		arguments: [{
			type:"bool",
			name:"setting",
			desc:"True or false to determine if the server will accept client and file request connections.",
		}],
	} **/
	allowConnections: function (setting) {
		this._serveClients = setting;
	},
	
	setProvider: function (provider) {
		this.log('Selecting new network provider "' + provider + '"...');
		if (this.networkProvider) {
			// A network provider is already in place
			this.log('An existing provider is already in place, removing: ' + this.networkProvider);
			
		}
		
		if (typeof(this.io[provider]) == 'object' && typeof(this.io[provider].classMethod) == 'function') {
			this.log('Network provider found, absorbing provider...');
			this.absorbClass(this.io[provider].classMethod.prototype);
			if (this.networkProvider) {
				this.log('Network provider "' + this.networkProvider + '" absorbed successfully!');
			} else {
				this.log('Error absorbing network provider class properties!', 'error');
			}
		} else {
			this.log('Cannot select networking provider "' + provider + '" because it does not exist.', 'error');
		}
	},
	
	// Sets use manifest flag to true or false
	useManifest: function (flag) {
		this.log('Use manifest lookup to compress network packets: ' + flag);
		this._manifestEnabled = flag;
	},
	
	addToManifest: function (name) {
		if (typeof(name) == 'string') {
			if (name.length > 0) {
				// The add method of IgeEnum expects an array even for one value so give it one
				this._manifest.add([name]);
			}
		} else {
			// Add the array of names to the IgeEnum manifest
			this._manifest.add(name);
		}
	},
	
	showManifestTargets: function (threshold) {
		var finalTargetList = [];
		if (!threshold) { threshold = 2; }
		for (var i in this._nonCompressed) {
			if (this._nonCompressed[i] >= threshold) {
				finalTargetList.push(i);
			}
		}
		return finalTargetList;
	},
	
	useStats: function (flag) {
		this.log('Record network transfer stats: ' + flag);
		this._statsEnabled = flag;
		
		if (flag) {
			for (var i = 0; i < 60; i++) {
				this._dataRecv[i] = 0;
				this._dataSend[i] = 0;
			}
		}
	},
	
	disableNetworking: function () {
		// Since the user doesn't want networking simply emit the provider up
		// event without actually loading the provider. This will bypass all networking
		// code!
		this.emit('networkProviderReady');
		this.emit('networkProviderUp');
	},
	
});