/** IgeCollection - Base class for classes that use MongoDB to store their collection data. Provides
helper methods for MongoDB and networking CRUD for any class that extends it. {
	engine_ver:"0.1.0",
	category:"class",
} **/

/** loadAllComplete - Fired when the dbLoadAll method has completed. {
	category: "event",
} **/
/** loadAllFailed - Fired if the dbLoadAll method failed. {
	category: "event",
} **/
/** netSendStarted - Fired when the collection has started to receive networked data. {
	category: "event",
	engine_ver:"0.3.0",
} **/
IgeCollection = new IgeClass({
	
	/** collectionId - The id of the collection that this class has been extended into. {
		category:"property",
		type:"string",
	} **/
	collectionId: '',
	
	/** init - A prototype method extended by the extending class. {
		category:"method",
	} **/
	init: function () {
		
	},
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// SERVER METHODS
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/* CEXCLUDE */
	
	/** dbLoadAll - Loads all data for this collection and passes each record to the
	'create' method	of the extending class. {
		category:"method",
		flags:"server",
		arguments: [{
			type:"object",
			name:"filter",
			desc:"A JSON object containing properties that MongoDB will use to filter the returned results by.",
		}, {
			type:"method",
			name:"callback",
			desc:"A callback method to call with all data loaded by this method.",
			flags:"optional",
		}],
	} **/
	dbLoadAll: function (filter, callback) {
		if (this.engine.isServer) {
			
			if (!this.collectionId) { console.log('No collection id, cannot do a loadAll!'); process.exit(1); }
			// Load all the data in this collection from the database!
			if (!filter) { filter = {}; }
			this.engine.database.findAll(this.collectionId, filter, this.bind(function (gotData, data) {
				// Check if any data was returned
				if (gotData) {
					// Loop through each entry and create it in the engine
					for (var i in data) { this.create(data[i]);	}
					this.log('Loaded ' + data.length + ' rows for the ' + this.collectionId + ' collection');
					this.emit('loadAllComplete');
				} else {
					this.log('No data returned from the ' + this.collectionId + ' collection when loading all data');
					this.emit('loadAllFailed');
				}
				
				// If a callback function was passed, call it now
				if (typeof callback == 'function') { callback.apply(this, [gotData, data]); }
				
			}));
			
		}
	},
	
	/** netSendAll - Sends all data in the collection array 'this.byIndex' to the specified client. {
		category:"method",
		flags:"server",
		arguments: [{
			type:"object",
			name:"client",
			desc:"The socket.io networking client to send the data to.",
		}],
	} **/
	netSendAll: function (client) {
		if (this.engine.isServer) {
			
			// Send all the data in this collection to a client!
			if (!this.collectionId) { console.log('No collection id, cannot do a sendAll!'); process.exit(1); }
			
			var count = this.byIndex.length;
			this.log('Sending all ' + this.collectionId + ' data (' + count + ' entries)');
			// TO-DO - Implement this network call and an end one too so that the client can display
			// loading progress etc
			//this.engine.network.send(this.collectionId + 'sStartSend', count, client);
			
			for (var i = 0; i < count; i++) {
				this.engine.network.send(this.collectionId + 'sCreate', this.byIndex[i], client);
			}
			
		}
	},
	
	/** netSendGroup - Sends specific data from the collection array 'this.byIndex' to the specified client. {
		category:"method",
		flags:"server",
		arguments: [{
			type:"object",
			name:"client",
			desc:"The socket.io networking client to send the data to.",
		}, {
			type:"array",
			name:"indexes",
			desc:"An array of item indexes to identify which records from 'this.byIndex' to send to the client.",
		}],
	} **/	
	netSendGroup: function (client, indexes) {
		if (this.engine.isServer) {
			
			if (!this.collectionId) { this.log('No collection id, cannot do a sendGroup!'); process.exit(1); }
			
			var count = indexes.length;
			//this.engine.network.send(this.collectionId + 'sStartSend', count, client);
			
			for (var i = 0; i < count; i++) {
				this.engine.network.send(this.collectionId + 'sCreate', indexes[i], client);
			}
			
		}
	},
	
	/** netSendOne - Sends a single specific data item from the collection array 'this.byIndex' to the specified client. {
		category:"method",
		flags:"server",
		arguments: [{
			type:"object",
			name:"client",
			desc:"The socket.io networking client to send the data to.",
		}, {
			type:"integer",
			name:"index",
			desc:"An integer to identify which record from 'this.byIndex' to send to the client.",
		}],
	} **/
	netSendOne: function (client, index) {
		if (this.engine.isServer) {
			
			// Send all the data in this collection to a client!
			if (!this.collectionId) { console.log('No collection id, cannot do a sendOne!'); process.exit(1); }
			this.engine.network.send(this.collectionId + 'sCreate', this.byIndex[index], client);
			
		}
	},
	
	/** dbInsert - Insert a new record into the MongoDB collection associated with the extending class. {
		category:"method",
		flags:"server",
		arguments: [{
			type:"object",
			name:"json",
			desc:"The JSON object to insert into the MongoDB collection.",
		}, {
			type:"method",
			name:"callback",
			desc:"A callback method to call with the outcome of the insert.",
		}],
	} **/
	dbInsert: function (json, callback) {
		if (this.engine.isServer) {
			
			// Insert a new record into the collection
			this.engine.database.insert(this.collectionId, [this.engine.stripLocal(json)], this.bind(function (err, doc) {
				callback.apply(this, [err, doc]);
			}));
			
		}
	},
	
	/** dbUpdate - Update an existing record in the MongoDB collection associated with the extending class. {
		category:"method",
		flags:"server",
		arguments: [{
			type:"object",
			name:"json",
			desc:"The JSON object to update into the MongoDB collection. The item to update is identified by the '*collectionName*_db_id' property which corresponds to the MongoDB _id key value.",
		}, {
			type:"method",
			name:"callback",
			desc:"A callback method to call with the outcome of the update.",
		}],
	} **/
	dbUpdate: function (json, callback) {
		if (this.engine.isServer) {
			var tempObj = {};
			tempObj[this.collectionId + '_db_id'] = json[this.collectionId + '_db_id'];
		
			this.engine.database.update(this.collectionId, this.engine.stripLocal(json), this.bind(function (err, doc) {
				callback.apply(this, [err, doc]);
			}));
			
		}
	},
	
	/** dbRemove - Remove an existing record in the MongoDB collection associated with the extending class. {
		category:"method",
		flags:"server",
		arguments: [{
			type:"object",
			name:"json",
			desc:"The JSON object to remove from the MongoDB collection. The item to remove is identified by the '*collectionName*_db_id' property which corresponds to the MongoDB _id key value.",
		}, {
			type:"method",
			name:"callback",
			desc:"A callback method to call with the outcome of the remove.",
		}],
	} **/
	dbRemove: function (obj, callback) {
		// Remove records from the collection
		var tempObj = {};
		tempObj[this.collectionId + '_db_id'] = obj[this.collectionId + '_db_id'];
		
		this.engine.database.remove(this.collectionId, tempObj, this.bind(function (err, doc) {
			callback.apply(this, [err, doc]);
		}));
	},
	
	/** updateDifferenceStore - Updates the difference store for this collection, collecting any property
	changes that are about to occur to the item identified by it's id. {
		category:"method",
		arguments: [{
			type:"object",
			name:"itemObj",
			desc:"The JSON object to update the difference store with.",
		}],
	} **/
	updateDifferenceStore: function (itemObj) {
		// Check if we are dealing with a non-object
		if (itemObj == null || !itemObj[this.collectionId + '_id']) {
			this.log('Diff store exit 1', 'info', itemObj);
			return false;
		}
		
		var itemId = itemObj[this.collectionId + '_id'];
		this._diffStore = this._diffStore || [];
		
		// Check for an existing store object
		var currentItemObject = this.byId[itemId];
		
		if (currentItemObject == null) {
			// The item doesn't exist in the byId array for this collection so return
			this.log('Diff store exit 2', 'info', currentItemObject);
			return false;
		}
		
		var diffStoreObject = null;
		diffStoreObject = this._diffStore[itemId];
		
		// Does the diffStore object currently exist?
		if (diffStoreObject != null) {
			// Update the diffStore object for this item
			for (var i in itemObj) {
				// Check if the itemObj property is different from the current object's property of the same name
				if (currentItemObject[i] == null || itemObj[i] != currentItemObject[i]) {
					// Add this property to the diffStore object
					diffStoreObject[i] = this.engine.objClone(itemObj[i]);
				} else {
					//this.log('Diff obj prop: ' + i + ' is the same', 'info', itemObj[i]);
				}
			}
		} else {
			// There is no diffStore entry for this item
			diffStoreObject = {};
			
			// Create a new diffStore object for this item
			for (var i in itemObj) {
				// Check if the itemObj property is different from the current object's property of the same name
				//if (currentItemObject[i] == null || itemObj[i] != currentItemObject[i]) {
					// Add this property to the diffStore object
					var cln = this.engine.objClone(itemObj[i]);
					var clnType = typeof(cln);
					if (clnType != 'undefined' && clnType != 'null') {
						diffStoreObject[i] = cln;
					} else {
						this.log('Error cloning diff store data from source, in property: ' + i, 'warning', [itemObj, diffStoreObject, cln]);
					}
				//}
			}
			
			// Assign the new diffStore object to the diffStore for this item
			this._diffStore[itemId] = diffStoreObject;			
		}
	},
	
	/** getDifferenceStore - Gets the collection difference store object. {
		category:"method",
		return: {
			type:"object",
			desc:"The collection difference store object.",
		},
	} **/
	getDifferenceStore: function () {
		return this._diffStore;
	},
	
	/** clearDifferenceStore - Clears the collection difference store object. {
		category:"method",
	} **/	
	clearDifferenceStore: function () {
		this._diffStore = [];
	},
	/* CEXCLUDE */
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// CLIENT METHODS
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/** netSendStarted - Emits an event from the class that has extended this class telling any listeners that the
	collection has started to receive data. {
		category:"method",
		engine_ver:"0.3.0",
		arguments: [{
			type:"integer",
			name:"count",
			desc:"The number of items being sent to the collection from the server.",
		}],
	} **/
	netSendStarted: function (count) {
		if (!this.engine.isServer) {
			this.emit('netSendStarted', {collection:this.collectionId, count:count});
		}
	},
	
	/** netSendUpdate - Sends an object to be updated over the network. {
		category:"method",
		engine_ver:"0.3.0",
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object to send to the client to be updated.",
		}, {
			type:"object",
			name:"client",
			desc:"The socket.io networking client to send the data to.",
		}],
	} **/
	netSendUpdate: function (obj, client) {
		if (this.engine.isServer) {
			/* CEXCLUDE */
			// Server code - send an update to the client
			this.engine.network.send(this.collectionId + 'Update', obj, client);
			/* CEXCLUDE */
		} else {
			// Client code - send an update to the server
			this.engine.network.send(this.collectionId + 'Update', obj);
		}
	},
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// UNIVERSAL METHODS
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/** receiveCreate - Called when a create has been received over the network. Executes the extending class method
	'create' with the passed object. {
		category:"method",
		engine_ver:"0.1.2",
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object receieved over the network.",
		}, {
			type:"string",
			name:"sender",
			desc:"The session id of the sender (usually blank because the server has sent the command).",
			flags:"optional",
		}],
	} **/
	receiveCreate: function (obj, sender) {
		//this.log('Receiving data create...');
		this.create(obj);
	},
	
	/** receiveCreate - Called when an update has been received over the network. Executes the extending class method
	'update' with the passed object. {
		category:"method",
		engine_ver:"0.1.2",
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object receieved over the network.",
		}, {
			type:"string",
			name:"sender",
			desc:"The session id of the sender (usually blank because the server has sent the command).",
			flags:"optional",
		}],
	} **/
	receiveUpdate: function (obj, sender) {
		//this.log('Receiving data update...');
		this.update(obj);
	},
	
	/** receiveRemove - Called when a remove has been received over the network. Executes the extending class method
	'remove' with the passed object. {
		category:"method",
		engine_ver:"0.1.2",
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object receieved over the network.",
		}, {
			type:"string",
			name:"sender",
			desc:"The session id of the sender (usually blank because the server has sent the command).",
			flags:"optional",
		}],
	} **/
	receiveRemove: function (obj, sender) {
		this.remove(obj[this.collectionId + '_id']);
	},
	
	/** removeBySearch - Removes objects from the class collection by matching properties against the
	specified JSON object properties. {
		category:"method",
		engine_ver:"0.1.2",
		arguments: [{
			type:"object",
			name:"json",
			desc:"The JSON object whos properties should be matched on any collection item for it to be removed from the collection.",
		}],
	} **/
	removeBySearch: function (json) {
		console.log('Removing by search', json);
		var entCount = this.byIndex.length;
		var matchCount = null;
		var tmpItem = null;
		var matchTotal = null;
		var removeCount = 0;
		
		for (var i in json) {
			matchTotal++;
		}
		
		while (entCount--) {
			matchCount = 0;
			tmpItem = this.byIndex[entCount];
			
			for (var i in json) {
				if (tmpItem[i] == json[i]) { matchCount++; }
			}
			
			if (matchCount == matchTotal) {
				// The entry matches the search data so remove it!
				if (this.remove(tmpItem)) {
					removeCount++;
				}
			}
		}
		
		return removeCount;
	},
	
});