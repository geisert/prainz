/** IgeSound - The sound manager that allows the engine to output sounds. This is an
optional engine module that must be loaded before it can be accessed. {
	engine_ver:"0.1.3",
	category:"class",
} **/

/** ready - Fired when the sound class is ready to use. {
	category: "event",
} **/
/** created - Fired when a soundItem is created. {
	category: "event",
	argument: {
		type:"object",
		name:"soundItem",
		desc:"The soundItem object this event was fired for.",
	},
} **/
/** buffered - Fired when a soundItem has buffered data up to the specified buffer time. {
	category: "event",
	argument: {
		type:"object",
		name:"soundItem",
		desc:"The soundItem object this event was fired for.",
	},
} **/
/** loaded - Fired when a soundItem has loaded all it's data. {
	category: "event",
	argument: {
		type:"object",
		name:"soundItem",
		desc:"The soundItem object this event was fired for.",
	},
} **/
/** almostFinished - Fired when a soundItem has reached the specified 'almost finished' time. {
	category: "event",
	argument: {
		type:"object",
		name:"soundItem",
		desc:"The soundItem object this event was fired for.",
	},
} **/
/** finished - Fired when a soundItem has finished playback. {
	category: "event",
	argument: {
		type:"object",
		name:"soundItem",
		desc:"The soundItem object this event was fired for.",
	},
} **/
IgeSound = new IgeClass({
	
	Extends: IgeEvents,
	
	/** engine - A reference object to the main engine instance. {
		category:"property",
		type:"object",
		instanceOf:"IgeEngine",
	} **/
	engine: null,
	
	/** _playlists - An array for storing playlists. {
		category:"property",
		type:"array",
		index:"integer",
	} **/
	_playlists: null,
	
	/** byIndex - An array with an integer index that allows you to access every
	item created using the create method in the order it was created. {
		category:"property",
		type:"array",
		index:"integer",
	} **/
	byIndex: null,
	
	/** byId - An array with a string index that allows you to access every item
	created using the create method by its id. {
		category:"property",
		type:"array",
		index:"string",
	} **/
	byId: null,
	
	/** _ready - A boolean flag representing the current state of the sound manager class. {
		category:"property",
		type:"bool",
	} **/
	_ready: null,
	
	/** init - The constructor for this class. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a new instance of this class",
		},
		argument: {
			type:"object",
			name:"engine",
			desc:"The active engine instance.",
			instanceOf:"IgeEngine",
		},
	} **/
	init: function (engine) {
		this._className = 'IgeSound';
		
		this.engine = engine;
		this.engine.sound = this;
		
		this.byIndex = [];
		this.byId = [];
		
		this.engine.registerRequirement(this.bind(this.state));
		
		// Define some constants
		window.SOUND_TYPE_PLAYLIST = 1;
		window.SOUND_TYPE_TRACK = 2;
		window.SOUND_TYPE_SOUND = 3;
		
		if (!this.engine.isServer) {
			// Load the required js libraries
			SM2_DEFER = true
			$('<script id="igeSound_sm2" type="text/javascript" src="/engine/modules/IgeSound/js/soundmanager2-jsmin.js" />').appendTo("head");
			this._waitForSm2();
		}
		
		this.log('Init complete');
	},
	
	/** _waitForSm2 - Waits for the sm2 library to load, then fires the _sm2Loaded method. {
		category:"method",
	} **/
	_waitForSm2: function () {
		if (window.SoundManager) {
			this._sm2Loaded();
		} else {
			setTimeout(this.bind(this._waitForSm2), 100);
		}
	},
	
	/** _sm2Loaded - Fired once the sm2 library is loaded. Registers our options and
	a callback to _setReady once the sm2 library is ready to use. { category:"method", } **/
	_sm2Loaded: function () {
		window.soundManager = new SoundManager();
		window.soundManager.url = '/engine/modules/IgeSound/swf/';
		window.soundManager.useHTML5Audio = true;
		window.soundManager.useFlashAudio = true;
		window.soundManager.debugMode = false;
		window.soundManager.consoleOnly = true;
		
		window.soundManager.beginDelayedInit();
		window.soundManager.onready(this.bind(this._setReady));
	},
	
	/** _setReady - Sets the state of the module to ready = true. {
		category:"method",
	} **/
	_setReady: function () {
		this._ready = true;
		this.emit('ready');
		
		this.log('Sound module ready!');
	},
	
	/** ready - Called by the engine after all other module framework methods have been called. {
		category:"method",
	} **/
	ready: function () {
		
	},
	
	/** state - Check the current state of the module.
	Returns false if we're still waiting on something. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true if the module is ready, false if not.",
		},
	} **/
	state: function () {
		return this._ready;
	},
	
	/** _soundCreated - Called when a sound has been created in the engine. {
		category:"method",
		argument: {
			type:"object",
			name:"sound",
			desc:"The soundItem object that has been created.",
		},
	} **/
	_soundCreated: function (sound) {
		if (typeof sound.$local.callbacks.onCreated == 'function') {
			sound.$local.callbacks.onCreated.apply(sound.$local.callbacks.scope || this, [sound]);
		}
		this.emit('created', sound);
	},
	
	/** _soundBuffered - Called when a sound has finished buffering. {
		category:"method",
		argument: {
			type:"object",
			name:"sound",
			desc:"The soundItem object that has finished buffering.",
		},
	} **/
	_soundBuffered: function (sound) {
		if (typeof sound.$local.callbacks.onBuffered == 'function') {
			sound.$local.callbacks.onBuffered.apply(sound.$local.callbacks.scope || this, [sound]);
		}
		this.emit('buffered', sound);
	},
	
	/** _soundLoaded - Called when a sound has finished loading all data. {
		category:"method",
		argument: {
			type:"object",
			name:"sound",
			desc:"The soundItem object that has finished loading.",
		},
	} **/
	_soundLoaded: function (sound) {
		this.log('Sound loaded with id: ' + sound.sound_id);
		if (typeof sound.$local.callbacks.onLoaded == 'function') {
			sound.$local.callbacks.onLoaded.apply(sound.$local.callbacks.scope || this, [sound]);
		}
		this.emit('loaded', sound);
	},
	
	/** _soundAlmostFinished - Called when a sound has almost finished playing. {
		category:"method",
		argument: {
			type:"object",
			name:"sound",
			desc:"The soundItem object that has almost finished loading.",
		},
	} **/
	_soundAlmostFinished: function (sound) {
		if (typeof sound.$local.callbacks.onBeforeFinish == 'function') {
			sound.$local.callbacks.onBeforeFinish.apply(sound.$local.callbacks.scope || this, [sound]);
		}
		this.emit('almostFinished', sound);
	},
	
	/** _soundFinished - Called when a sound has finished playing. {
		category:"method",
		argument: {
			type:"object",
			name:"sound",
			desc:"The soundItem object that has finished playing.",
		},
	} **/
	_soundFinished: function (sound) {
		if (typeof sound.$local.callbacks.onFinish == 'function') {
			sound.$local.callbacks.onFinish.apply(sound.$local.callbacks.scope || this, [sound]);
		}
		this.emit('finished', sound);
	},
		
	/** create - Create a new soundItem to be used for playback. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true on success or false on failure.",
		},
		arguments: [{
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to be created.",
			link:"soundItemData",
		}, {
			type:"object",
			name:"callbacks",
			desc:"An object of callback methods that will be fired when your soundItem emits an event. Valid object property names are",
		}],
	} **/
	create: function (soundItem, callbacks) {
		if (soundItem != null) {
			
			if (soundItem.sound_id) {
				
				if (soundItem.sound_type != null) {
					
					switch (soundItem.sound_type) {
						case SOUND_TYPE_PLAYLIST:
							soundItem.list = [];
							soundItem.index = null;
							
							this.byIndex.push(soundItem);
							this.byId[soundItem.sound_id] = soundItem;
							
							return true;
						break;
						
						case SOUND_TYPE_TRACK:
							// Set some defaults in the soundItem object for this TRACK
							if (soundItem.sound_stream == null) { soundItem.sound_stream = true; }
							if (soundItem.sound_pan == null) { soundItem.sound_pan = 0; }
							if (soundItem.sound_position == null) { soundItem.sound_position = 0; }
							if (soundItem.sound_volume == null) { soundItem.sound_volume = 50; }
							if (soundItem.sound_buffer_time == null) { soundItem.sound_buffer_time = 5; }
							if (soundItem.sound_auto_load == null) { soundItem.sound_auto_load = true; }
							if (soundItem.sound_multi_play == null) { soundItem.sound_multi_play = false; }
						break;
						
						case SOUND_TYPE_EFFECT:
							// Set some defaults in the soundItem object for this EFFECT
							if (soundItem.sound_stream == null) { soundItem.sound_stream = false; }
							if (soundItem.sound_pan == null) { soundItem.sound_pan = 0; }
							if (soundItem.sound_position == null) { soundItem.sound_position = 0; }
							if (soundItem.sound_volume == null) { soundItem.sound_volume = 50; }
							if (soundItem.sound_buffer_time == null) { soundItem.sound_buffer_time = 0; }
							if (soundItem.sound_auto_load == null) { soundItem.sound_auto_load = true; }
							if (soundItem.sound_multi_play == null) { soundItem.sound_multi_play = true; }
						break;
						
						default:
							this.log('Attempted to create a soundItem without a valid sound_type property!', 'warning', soundItem);
							return false;
						break;
					}
					
					// Create the sm2 sound from the soundItem data and store it
					var tmpSound = soundManager.createSound({
						// sm2 properties
						id: soundItem.sound_id,
						url: soundItem.sound_url,
						volume: soundItem.sound_volume, 
						autoLoad: soundItem.sound_auto_load,
						autoPlay: soundItem.sound_auto_play,
						bufferTime: soundItem.sound_buffer_time,
						stream: soundItem.sound_stream,
						pan: soundItem.sound_pan,
						position: soundItem.sound_position,
						multiShot: soundItem.sound_multi_play,
						// Events
						onload: this.bind(function () { this._soundLoaded(soundItem); }),
						onbeforefinish: this.bind(function () { this._soundAlmostFinished(soundItem); }),
						onfinish: this.bind(function () { this._soundFinished(soundItem); }),
						// Ige specific properties
						_igeSnd:soundItem,
					});
					
					// Create the $local object
					soundItem.$local = soundItem.$local || {};
					
					// Add the soundItem to the class arrays
					this.byIndex.push(soundItem);
					this.byId[soundItem.sound_id] = soundItem;
					
					// Add the sm2 sound to the soundItem object
					soundItem.$local.sndObject = tmpSound;
					
					// Store the callbacks if there are any
					soundItem.$local.callbacks = {};
					if (callbacks != null) {
						soundItem.$local.callbacks = callbacks;
					}
					
					// Fire the onCreated event
					this._soundCreated(soundItem);
					
					return true;
					
				} else {
					this.log('Cannot create sound because no sound_type was specified.', 'error', soundItem);
				}
			} else {
				this.log('Cannot create sound because no sound_id was specified.', 'error', soundItem);
				return false;
			}
		}
		
		return false;
	},
	
	/** addToPlaylist - Add the soundItem to the specified playlist. {
		category:"method",
		engine_ver:"0.2.0",
		return: {
			type:"bool",
			desc:"Returns true on success, false on failure.",
		},
		arguments: [{
			type:"object",
			name:"playlist",
			desc:"The playlist to queue the soundItem object to.",
		}, {
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to add to the playlist.",
			link:"soundItemData",
		}],
	} **/
	addToPlaylist: function (playlist, soundItem) {
		if (playlist.sound_type == SOUND_TYPE_PLAYLIST) {
			if (soundItem && soundItem.$local && soundItem.$local.sndObject) {
				// Add the item to the playlist
				playlist.list = playlist.list || [];
				playlist.list.push(soundItem);
				// Set the index to this item if it is the first to be added
				if (playlist.index == null) { playlist.index = 0; }
				// Register some event listeners!
				this.on('almostFinished', this.bind(this._playlistSoundItemAlmostFinished), this);
				this.on('finished', this.bind(this._playlistSoundItemFinished), this);
				
				return true;
			} else {
				this.log('Attempted to add a soundItem to a playlist but the soundItem passed is invalid!', 'warning', soundItem);
				return false;
			}
		} else {
			this.log('Attempted to add a soundItem to a playlist object but the object passed is not a playlist!', 'warning', playlist);
			return false;
		}
	},
	
	/** removeFromPlaylist - Remove the soundItem from the specified playlist. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true on success, false on failure.",
		},
		arguments: [{
			type:"object",
			name:"playlist",
			desc:"The playlist to remove the soundItem object from.",
		}, {
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to remove from the playlist.",
			link:"soundItemData",
		}],
	} **/
	removeFromPlaylist: function (playlist, soundItem) {
		if (playlist.sound_type == SOUND_TYPE_PLAYLIST) {
			playlist.list = playlist.list || [];
			playlist.list.splice(plalist.list.indexOf(soundItem), 1);
			
			return true;
		} else {
			this.log('Attempted to remove a soundItem from a playlist object but the object passed is not a playlist!', 'warning', playlist);
			return false;
		}
	},
	
	/** playlistCurrentSoundItem - Returns the current playlist soundItem object. {
		category:"method",
		engine_ver:"0.2.0",
		return: {
			type:"object",
			desc:"Returns the current soundItem in the playlist queue or false on error.",
		},
		argument: {
			type:"object",
			name:"playlist",
			desc:"The playlist to get the current soundItem from.",
		},
	} **/
	playlistCurrentSoundItem: function (playlist) {
		if (playlist.sound_type == SOUND_TYPE_PLAYLIST) {
			var currentItem = null;
			if (playlist.index != null) {
				currentItem = playlist.list[playlist.index];
			} else {
				if (playlist.list.length > 0) {
					currentItem = playlist.list[0];
				} else {
					currentItem = null;
				}
			}
			
			if (currentItem != null) {
				return currentItem;
			}
		} else {
			this.log('Attempted to get the current item of a playlist object but the object passed is not a playlist!', 'warning', playlist);
		}
		
		return false;
	},
	
	_playlistSoundItemAlmostFinished: function () {
		console.log('Playlist item almost finished', arguments);
	},
	
	_playlistSoundItemFinished: function () {
		console.log('Playlist item finished', arguments);
	},
	
	/** load - Start loading a soundItem from it's URL. {
		category:"method",
		argument: {
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to start loading.",
			link:"soundItemData",
		},
	} **/
	load: function (soundItem) {
		if (soundItem.sound_type == SOUND_TYPE_PLAYLIST) {
			// Load a playlist?
		} else {
			soundManager.load(soundItem.sound_id);
		}
	},
	
	/** play - Start playing or resumes a paused soundItem. If the soundItem is not yet loaded, this will trigger loading of the data. {
		category:"method",
		argument: {
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to start playing / resume.",
			link:"soundItemData",
		},
	} **/
	play: function (soundItem) {
		if (soundItem.sound_type == SOUND_TYPE_PLAYLIST) {
			// Start or resume playlist playback
			this.log('Playing playlist with id: ' + soundItem.sound_id);
			var currentItem = this.playlistCurrentSoundItem(soundItem);
			if (currentItem && currentItem.$local && currentItem.$local.sndObject) {
				currentItem.$local.sndObject.play();
			}
		} else {
			this.log('Playing sound with id: ' + soundItem.sound_id);
			soundItem.$local.sndObject.play();
		}
	},
	
	/** pause - Pause playback of the soundItem. Pausing preserves the current playback position of the sound. {
		category:"method",
		argument: {
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to pause.",
			link:"soundItemData",
		},
	} **/
	pause: function (soundItem) {
		if (soundItem.sound_type == SOUND_TYPE_PLAYLIST) {
			// Pause playlist playback
			var currentItem = this.playlistCurrentSoundItem(soundItem);
			if (currentItem && currentItem.$local && currentItem.$local.sndObject) {
				currentItem.$local.sndObject.pause();
			}
		} else {
			soundItem.$local.sndObject.pause();
		}
	},
	
	/** stop - Stop playback of the soundItem and return the playback position of the sound to zero (beginning). {
		category:"method",
		argument: {
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to stop.",
			link:"soundItemData",
		},
	} **/
	stop: function (soundItem) {
		if (soundItem.sound_type == SOUND_TYPE_PLAYLIST) {
			// Stop playlist playback
			var currentItem = this.playlistCurrentSoundItem(soundItem);
			if (currentItem && currentItem.$local && currentItem.$local.sndObject) {
				currentItem.$local.sndObject.stop();
			}
		} else {
			soundItem.$local.sndObject.stop();
		}
	},
	
	/** isLoaded - Returns true if the sound is fully loaded, or false. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true if the sound has been fully loaded, false if not.",
		},
		argument: {
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to inspect.",
			link:"soundItemData",
		},
	} **/
	isLoaded: function (soundItem) {
		if (currentItem.$local.sndObject.readyState != 3) {
			return false;
		} else {
			return true;
		}
	},
	
	/** getState - Returns the current readyState as an integer. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns the current ready state as an integer. The integer maps to SOUND_STATE constants declared in IgeConstants.js. See the example on how to use these constants in your code.",
		},
		argument: {
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to inspect.",
			link:"soundItemData",
		},
	} **/
	getState: function (soundItem) {
		return currentItem.$local.sndObject.readyState;
	},
	
	/** fadeTo - Fades the volume of the specified soundItem to the
	specified value in the given time in milliseconds. {
		category:"method",
		engine_ver:"0.2.0",
		arguments: [{
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to use in the operation.",
			link:"soundItemData",
		}, {
			type:"integer",
			name:"targetVolume",
			desc:"The volume to fade to (value from 0 to 100).",
		}, {
			type:"integer",
			name:"timeMs",
			desc:"The time in milliseconds to perform the operation in.",
		}, {
			type:"function",
			name:"callback",
			desc:"A method to call when the operation is complete.",
		}],		
	} **/
	fadeTo: function (soundItem, targetVolume, timeMs, callback) {
		
	},
	
	/** fadeIn - Fades the volume of the specified soundItem up to 100 (top-volume) in the given time in milliseconds. {
		category:"method",
		engine_ver:"0.2.0",
		arguments: [{
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to use in the operation.",
			link:"soundItemData",
		}, {
			type:"integer",
			name:"timeMs",
			desc:"The time in milliseconds to perform the operation in.",
		}, {
			type:"function",
			name:"callback",
			desc:"A method to call when the operation is complete.",
		}],		
	} **/
	fadeIn: function (soundItem, timeMs, callback) {
		
	},
	
	/** fadeOut - Fades the volume of the specified soundItem down to 0 (silent) in the given time in milliseconds. {
		category:"method",
		engine_ver:"0.2.0",
		arguments: [{
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to use in the operation.",
			link:"soundItemData",
		}, {
			type:"integer",
			name:"timeMs",
			desc:"The time in milliseconds to perform the operation in.",
		}, {
			type:"function",
			name:"callback",
			desc:"A method to call when the operation is complete.",
		}],		
	} **/
	fadeOut: function (soundItem, timeMs, callback) {
		
	},
	
	/** fadeToPause - Fades the volume of the specified soundItem down to 0 (silent) in the given time in milliseconds
	and then pauses playback of the sound.  {
		category:"method",
		engine_ver:"0.2.0",
		arguments: [{
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to use in the operation.",
			link:"soundItemData",
		}, {
			type:"integer",
			name:"timeMs",
			desc:"The time in milliseconds to perform the operation in.",
		}, {
			type:"function",
			name:"callback",
			desc:"A method to call when the operation is complete.",
		}],		
	} **/
	fadeToPause: function (soundItem, timeMs, callback) {
		
	},
	
	/** fadeToStop - Fades the volume of the specified soundItem down to 0 (silent) in the given time in milliseconds
	and then stops playback of the sound. {
		category:"method",
		engine_ver:"0.2.0",
		arguments: [{
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to use in the operation.",
			link:"soundItemData",
		}, {
			type:"integer",
			name:"timeMs",
			desc:"The time in milliseconds to perform the operation in.",
		}, {
			type:"function",
			name:"callback",
			desc:"A method to call when the operation is complete.",
		}],		
	} **/
	fadeToStop: function (soundItem, timeMs, callback) {
		
	},
	
	/** crossFade - Fades the volume of soundItem1 down to 0 (silent) and fades the volume of soundItem2 to 100
	(top-volume) in the given amount of time. soundItem1 will be stopped after it reaches 0 volume. {
		category:"method",
		engine_ver:"0.2.0",
		arguments: [{
			type:"object",
			name:"soundItem1",
			desc:"The soundItem object to fade to zero volume.",
			link:"soundItemData",
		}, {
			type:"object",
			name:"soundItem2",
			desc:"The soundItem object to fade to full volume.",
			link:"soundItemData",
		}, {
			type:"integer",
			name:"timeMs",
			desc:"The time in milliseconds to perform the operation in.",
		}, {
			type:"function",
			name:"callback",
			desc:"A method to call when the operation is complete.",
		}],
	} **/
	crossFade: function (soundItem1, soundItem2, timeMs, callback) {
		
	},
	
	/** setVolume - Sets the volume of soundItem between 0 (silent) and 100 (top-volume). {
		category:"method",
		arguments: [{
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to use in the operation.",
			link:"soundItemData",
		}, {
			type:"integer",
			name:"volume",
			desc:"The volume from 0 to 100 to set the soundItem to.",
		}],	
	} **/
	setVolume: function (soundItem, volume) {
		currentItem.$local.sndObject.setVolume(volume);
	},
	
	/** getDuration - Returns the duration in milliseconds of the soundItem. Can use an estimate if required.
	If a sound is fully loaded, the returned duration will be absolutely correct. Estimates cannot be
	considered fully accurate. {
		category:"method",
		return: {
			type:"integer",
			desc:"Returns the duration of the soundItem in milliseconds.",
		},
		arguments: [{
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to use in the operation.",
			link:"soundItemData",
		}, {
			type:"bool",
			name:"estimate",
			flags:"optional",
			desc:"If you do not specify estimate or you specify false for this argument, the returned duration
			will be the length of the loaded portion of the soundItem. If you specify true for estimate, the
			returned duration will be an attempt to extrapolate the length of the sound based upon the overall
			byte size of the file being loaded. Only works correctly on constant-bitrate files and will give
			weird results on variable-bitrates.",
		}],	
	} **/
	getDuration: function (soundItem, estimate) {
		if (currentItem.$local.sndObject.readyState != 3) {
			if (!estimate) {
				// Return the duration of the sound. This will fluctuate
				// whilst the sound file is still loading and will only
				// reflect the length of the loaded portion of the sound.
				return currentItem.$local.sndObject.duration;
			} else {
				// The estimate will attempt to extrapolate the length of
				// the sound based upon the overall byte size of the file
				// being loaded. Only works correctly on constant-bitrate
				// files and will give weird results on variable-bitrates.
				return currentItem.$local.sndObject.durationEstimate;
			}
		} else {
			// The sound is fully loaded so don't return an estimate,
			// even if we were asked for one.
			return currentItem.$local.sndObject.duration;
		}
	},
	
	/** skipTo - Skips the soundItem playback position to the time specified in milliseconds. {
		category:"method",
		arguments: [{
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to use in this operation.",
			link:"soundItemData",
		}, {
			type:"integer",
			name:"positionMs",
			desc:"The time in milliseconds to skip the playback position to.",
		}],
	} **/
	skipTo: function (soundItem, positionMs) {
		currentItem.$local.sndObject.setPosition(positionMs);
	},
	
	/** panTo - Pans the soundItem from its current pan position to 
	the given value in the specified time (in milliseconds). Pan 
	values are given in percent between -100 (all the way to the left) 
	and 100 (all the way to the right). Zero is dead centre. {
		category:"method",
		engine_ver:"0.2.0",
		arguments: [{
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to use in the operation.",
			link:"soundItemData",
		}, {
			type:"integer",
			name:"panPosition",
			desc:"An integer between -100 and 100 which represents the position to pan to.",
		}, {
			type:"integer",
			name:"timeMs",
			desc:"The time in milliseconds to perform the operation in.",
		}, {
			type:"function",
			name:"callback",
			desc:"A method to call when the operation is complete.",
		}],		
	} **/
	panTo: function (soundItem, panPosition, timeMs, callback) {
		
	},
	
	/** setPan - Sets the soundItem pan to the given value which is a
	value between -100 (all the way to the left) and 100 (all the way
	to the right). Zero is dead centre. {
		category:"method",
		arguments: [{
			type:"object",
			name:"soundItem",
			desc:"The soundItem object to use in the operation.",
			link:"soundItemData",
		}, {
			type:"integer",
			name:"panPosition",
			desc:"An integer between -100 and 100 which represents the position to pan to.",
		}],	
	} **/
	setPan: function (soundItem, panPosition) {
		currentItem.$local.sndObject.setPan(panPosition);
	},
	
});