/** IgeEnum - Takes an array of strings and creates an array with the string value as the key and 
the value as an auto-incremented integer. {
	engine_ver:"0.0.5",
	category:"class",
} **/
IgeEnum = new IgeClass({
	
	/** init - The constructor for this class. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a new instance of this class",
		},
		argument: {
			type:"array",
			name:"vals",
			desc:"An array of string values to create the enum from.",
		},
	} **/
	init: function (vals) {
		this._className = 'IgeEnum';
		
		if (vals == null) { vals = []; }
		var id = null;
		var tempEn = [];
		var enumVar = this;
		enumVar._reverse = [];
		
		var startPoint = 0;
		var endPoint = vals.length;
		
		for (v = startPoint; v < endPoint; v++)
		{
			
			if (vals[v])
			{
				
				tempEn.push(v);
				id = (tempEn.length - 1);
				enumVar[vals[v]] = id;
				enumVar._reverse[id] = vals[v];
				
			}
			
		}
		
		enumVar._lastIndex = id;
		enumVar._vals = vals;
		
		return enumVar;
		
	},
	
	/** add - Adds new values to an existing Enum. {
		category:"method",
		engine_ver:"0.0.5",
		arguments: [{
			type:"array",
			name:"vals",
			desc:"An array of string values to add to the enum.",
		}],
	} **/	
	add: function (vals) {
		var currentEnum = this;
		
		for (var val = 0; val < vals.length; val++) {
			currentEnum._vals.push(vals[val]);
		}
		
		vals = currentEnum._vals;
		var newEnum = new IgeEnum(vals);
		
		for (var thing in newEnum) {
			currentEnum[thing] = newEnum[thing];
		}
	}
	
});