/** IgeEntities - The entity management class. {
	engine_ver:"0.0.6",
	category:"class",
	definitions: [{
		name:"entityData",
		contents: {
			entity_id: {
				type:"string",
				desc:"The entity's unique id.",
			},
			entity_type: {
				type:"integer",
				desc:"The type of entity this data represents.",
				valueOf: {
					ENTITY_TYPE_BACKGROUND: {
						type:"integer",
						desc:"The entity is a background image.",
					},
					ENTITY_TYPE_TILE: {
						type:"integer",
						desc:"The entity is a background tile.",
					},
					ENTITY_TYPE_OBJECT: {
						type:"integer",
						desc:"The entity tile x co-ordinate.",
					},
					ENTITY_TYPE_SPRITE: {
						type:"integer",
						desc:"The entity is a sprite.",
					},
					ENTITY_TYPE_UI: {
						type:"integer",
						desc:"The entity is a user-interface element.",
					},
				},
			},
			entity_layer: {
				type:"integer",
				desc:"Which layer to render the entity on. Entities on layer zero are always drawn below entities on layer 1.",
			},
			entity_x: {
				type:"integer",
				desc:"The entity tile x co-ordinate.",
			},
			entity_y: {
				type:"integer",
				desc:"The entity tile y co-ordinate.",
			},
			entity_z: {
				type:"integer",
				desc:"The entity z co-ordinate.",
			},
			entity_actual_x: {
				type:"float",
				desc:"The entity map x co-ordinate - used for pixel-perfect positioning.",
			},
			entity_actual_y: {
				type:"float",
				desc:"The entity map y co-ordinate - used for pixel-perfect positioning.",
			},
			entity_tile_width: {
				type:"integer",
				desc:"The width in tiles that this entity takes up.",
			},
			entity_tile_height: {
				type:"integer",
				desc:"The height in tiles that this entity takes up.",
			},
			entity_tile_block: {
				type:"integer",
				desc:"Used to tell the engine how this entity interacts with other entities on the tile map. If an entity blocks, it will not allow any entities that use checking to be created on the same tiles as the blocking entity occupies. If an entity checks, before it is created it will check the tile map to see if a blocking entity already occupies one or more of the tiles that the checking entity wants to take up.",
				valueOf: {
					ENTITY_TB_NOBLOCK_NOCHECK: {
						type:"integer",
						desc:"The entity will not block other entities, will not check for other entities blocking it.",
					},
					ENTITY_TB_NOBLOCK_CHECK: {
						type:"integer",
						desc:"The entity will not block other entities, will check for other entities blocking it.",
					},
					ENTITY_TB_BLOCK_NOCHECK: {
						type:"integer",
						desc:"The entity will block other entities, will not check for other entities blocking it.",
					},
					ENTITY_TB_BLOCK_CHECK: {
						type:"integer",
						desc:"The entity will block other entities, will check for other entities blocking it.",
					},
				},
			},
			entity_persist: {
				type:"integer",
				desc:"The persist flag determines if this data will be removed from the engine, the server, the database and all connected clients when either the server starts up or the client that created the data disconnects from the server. When set to true, the data will NOT be automatically removed.",
				valueOf: {
					PERSIST_ENABLED:  {
						type:"integer",
						desc:"The entity will persist across disconnects and server restarts.",
					},
					PERSIST_DISABLED:  {
						type:"integer",
						desc:"The entity will not persist across disconnects and server restarts.",
					},
				},
			},
			entity_locale: {
				type:"integer",
				desc:"Determines where the entity exists and where it should be propagated to. To specify a locale AND database storage, use the locale + db for example - LOCALE_EVERYWHERE + LOCALE_DB.",
				valueOf: {
					LOCALE_EVERYWHERE:  {
						type:"integer",
						desc:"The entity exists everywhere.",
					},
					LOCALE_ALL_CLIENTS:  {
						type:"integer",
						desc:"The entity only exists on clients.",
					},
					LOCALE_SINGLE_CLIENT:  {
						type:"integer",
						desc:"The entity only exists on a single client.",
					},
					LOCALE_SERVER_ONLY:  {
						type:"integer",
						desc:"The entity will only exist server-side.",
					},
					LOCALE_DB:  {
						type:"integer",
						desc:"The entity will be stored in the database.",
					},
					LOCALE_DEFAULT:  {
						type:"integer",
						desc:"The entity will use the engine default locale setting configured in IgeConstants.js.",
					},
				},
			},
			asset_id: {
				type:"string",
				desc:"The id of the asset to use when rendering this entity to the screen.",
			},
			animation_id: {
				type:"string",
				desc:"The id of the animation to apply to the entity whilst it is being rendered.",
			},
			animation_dirty: {
				type:"bool",
				desc:"Defaults to true if not specified. When true, causes the entity to be set to dirty (to be redrawn) each time the entity's asset_sheet_frame is updated by the entity animation system.",
			},
			path_class: {
				type:"array",
				desc:"Only set on tile entities. An array of string names that denote the types of paths that can use this tile. This allows you to control which tiles paths can route through.",
			},
			map_id: {
				type:"string",
				desc:"The id of the map that this entity will exist on.",
			},
			template_id: {
				type:"string",
				desc:"The id of the template from which to derive the bulk of the entity's properties from.",
			},
		},
	}],
} **/

/** createDeferred - Fired when the create method defers creation of an entity because it's designated asset has not yet become ready. {
	category: "event",
} **/
/** deferCreated - Fired when an entity who's creation was deferred during the 'create' method is finally created. {
	category: "event",
} **/
/** beforeCreate - Fired before an entity is created. {
	category: "event",
} **/
/** afterCreate - Fired after an entity is created. {
	category: "event",
} **/
/** beforeUpdate - Fired before updating any data in an entity using the 'update' method. {
	category: "event",
} **/
/** afterUpdate - Fired after updating any data in an entity using the 'update' method. {
	category: "event",
} **/
/** beforeMove - Fired before moving an entity. {
	category: "event",
} **/
/** afterMove - Fired after moving an entity. {
	category: "event",
} **/
/** directionChange - Fired when an entity's direction is changed. {
	category: "event",
} **/
/** receiveDelta - Fired after a delta update has been received from the server. {
	category: "event",
	engine_ver:"0.2.2",
	flags:"client",
	arguments: [{
		type:"object",
		name:"data",
		desc:"The data that the server has sent over.",
	}],
} **/
IgeEntities = new IgeClass({
	
	Extends: [IgeCollection, IgeNetworkItem, IgeItem, IgeEvents],
	
	/** engine -  A reference object to the main engine instance. {
		category:"property",
		type:"object",
		instanceOf:"IgeEngine",
	} **/
	engine: null,
	
	/** viewports - A reference to this.engine.viewports for fast access. {
		category:"property",
		type:"object",
		instanceOf:"IgeViewports",
	} **/
	viewports: null,
	
	/** viewportsByMapId - A reference to this.engine.viewports.byMapId for fast access. {
		category:"property",
		type:"object",
	} **/	
	viewportsByMapId: null,
	
	/** mapsById - A reference to this.engine.maps.byId for fast access. {
		category:"property",
		type:"object",
	} **/	
	mapsById: null,
	
	/** assetsById - A reference to this.engine.assets.byId for fast access. {
		category:"property",
		type:"object",
	} **/
	assetsById: null,
	
	/** assetImageById - A reference to this.engine.assets.assetImage for fast access. {
		category:"property",
		type:"object",
	} **/
	assetImageById: null,
	
	/** camerasById - A reference to this.engine.cameras.byId for fast access. {
		category:"property",
		type:"object",
	} **/
	camerasById: null,
	
	/** renderer - A reference to this.engine.renderer for fast access. {
		category:"property",
		type:"object",
		instanceOf:"IgeRenderer",
	} **/	
	renderer: null,
	
	/** deferList - The events controller for this class. {
		category:"property",
		type:"array",
		instanceOf:"IgeEvents",
	} **/
	deferList: [],
	
	/** byIndex - An array with an integer index that allows you to access every
	item created using the create method in the order it was created. {
		category:"property",
		type:"array",
		index:"integer",
	} **/
	byIndex: null,
	
	/** byId - An array with a string index that allows you to access every item
	created using the create method by its id. {
		category:"property",
		type:"array",
		index:"string",
	} **/
	byId: null,
	
	/** byMapId - A multi-dimensional array with a string index that allows you to
	access every item created using the create method grouped by the map_id property
	value. {
		category:"property",
		type:"array",
		index:"string",
	} **/
	byMapId: [],
	
	/** byMapIdAndLayer - A multi-dimensional array with a string index that allows
	you to access every	item created using the create method grouped by the map_id, then
	the entity_layer property values. {
		category:"property",
		type:"array",
		index:"string",
	} **/
	byMapIdAndLayer: [],
	
	/** byAnimate - An array with a string index that allows you to access every item created
	using the create method that has animation data. {
		category:"property",
		type:"array",
		index:"integer",
	} **/
	byAnimate: [],
	
	/** byPath - An array with a string index that allows you to access every item created
	using the create method that has path data. {
		category:"property",
		type:"array",
		index:"integer",
	} **/	
	byPath: [],
	
	/** tileMapCache - A multi-dimensional array that holds data about the tile map for internal
	use. {
		category:"property",
		type:"array",
		index:"string",
	} **/
	tileMapCache: [],
	
	/** firstTime - Is true if this is the first time that the class has created an entity during
	this session. {
		category:"property",
		type:"bool",
	} **/
	firstTime: null,
	
	/** init - The constructor for this class. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a new instance of this class",
		},
		argument: {
			type:"object",
			name:"engine",
			desc:"The active engine instance.",
			instanceOf:"IgeEngine",
		},
	} **/	
	init: function (engine) {
		this._className = 'IgeEntities';
		this.collectionId = 'entity';
		this.engine = engine;
		
		this.byIndex = [];
		this.byId = [];
		this.byMapId = [];
		this.byMapIdAndLayer = [];
		this.byCords = [];
		this.byAnimate = [];
		this.byPath = [];
		this.tileMapCache = [];
		this.firstTime = true;
		
		// Process the entity defer list when a new asset is created
		this.engine.assets.on('assetLoaded', this.bind(this._processDeferList), this);
		
		// Check if we have a network
		if (this.engine._IgeNetwork) {
			// Call the networkInit method of this class
			this.networkInit();
		}	
	},
	
	networkInit: function () {
		// Network CRUD Commands
		this.engine.network.registerCommand('entitysCreate', this.bind(this.receiveCreate));
		this.engine.network.registerCommand('entitysRead', this.bind(this.read));
		this.engine.network.registerCommand('entitysUpdate', this.bind(this.receiveUpdate));
		this.engine.network.registerCommand('entitysRemove', this.bind(this.receiveRemove));
		
		// Network2 Commands
		this.engine.network.registerCommand('entitysDeltaData', this.bind(this._receiveDelta));
		
		/* CEXCLUDE */
		if (this.engine.isServer) {
			// Register this collection as part of the auto network update
			if (this.engine.network.registerSyncProvider != null) { this.engine.network.registerSyncProvider('entity', this.bind(this.getDifferenceStore)); }
			if (this.engine.network.registerSyncComplete != null) { this.engine.network.registerSyncComplete('entity', this.bind(this.clearDifferenceStore)); }
		}
		/* CEXCLUDE */
		
		// Register default entity network properties - the properties that are automatically updated to 
		// clients if they change in an entity's data
		this.engine.network.registerNetProps(this._className, [
			/*'entity_x',
			'entity_y',
			'entity_z',*/ /* no need to check these as changes to these auto-update the "actual" values anyway,
			and updating the actual values will set the tile values too! */
			'entity_actual_x',
			'entity_actual_y',
			'entity_actual_z',
			'asset_id',
			'asset_sheet_frame',
			'animation_id',
		]);
		
		// Register standard property names to the network manifest see the API manual for more details
		this.engine.network.addToManifest([
			'entity_id',
			'entity_x',
			'entity_y',
			'entity_z',
			'entity_actual_x',
			'entity_actual_y',
			'entity_tile_width',
			'entity_tile_height',
			'entity_actual_width',
			'entity_actual_height',
			'entity_moved',
			'entity_layer',
			'entity_locale',
			'entity_tile_block',
			'entity_type',
			'entity_persist',
			'entity_db_id',
			'session_id',
		]);
	},
	
	/** _receiveDelta - Called with data from the server detailing delta updates for real-time networking. Each
	class that extends IgeCollection and registers network properties must implement this method and deal with
	the data being sent to it. {
		category:"method",
		engine_ver:"0.2.2",
		argument: {
			type:"object",
			name:"data",
			desc:"The data from the server detailing updates to the engine state.",
		},
	} **/
	_receiveDelta: function (data) {
		// Run through the data and call the appropriate methods based upon the properties that have changed
		
		// Check for movement
		if (data.entity_actual_x != null || data.entity_actual_y != null) {
			this.moveToActual(data, data.entity_actual_x, data.entity_actual_y);
		}
		
		if (data.entity_x != null || data.entity_y != null) {
			this.moveToTile(data, data.entity_x, data.entity_y);
		}
		
		// Check for asset change
		if (data.asset_id != null) {
			this.setAsset(data, data.asset_id);
		}
		
		// Check for sheet frame change
		if (data.asset_sheet_frame != null) {
			this.setSheetFrame(data, data.asset_id);
		}
		
		// Check for animation change
		if (data.animation_id != null) {
			this.setAnimation(data, data.animation_id);
		}
		
		this.emit('receiveDelta', [data]);
	},
	
	/** _itemDefaults - Called by the IgeItem class which this class extends when creating a new item. Here you can define
	any object properties that are default across all items of this class. If this method returns false the create action
	will be cancelled. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns false on failure or true on success.",
		},
		arguments: [{
			type:"object",
			name:"pItem",
			desc:"The item object to be checked.",
		}],
	} **/
	_itemDefaults: function (pItem) {
		this.ensureLocalExists(pItem);
		/* CEXCLUDE */
		if (this.engine.isServer) { this.ensurePersistExists(pItem); }
		/* CEXCLUDE */
		this.ensureIdExists(pItem);
		
		return true;
	},
	
	/** _itemIntegrity - Called by the IgeItem class which this class extends when creating a new item. Checks the
	integrity of an item before it is allowed to be created. Here you can define custom checks to ensure an item being
	created conforms to the standard your class requires. If this method returns false the create action will be cancelled. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns false on failure or true on success.",
		},
		arguments: [{
			type:"object",
			name:"pItem",
			desc:"The item object to be checked.",
		}],
	} **/
	_itemIntegrity: function (pItem) {
		// Check that this entity does not already exist
		if (this.byId[pItem.entity_id]) {
			this.log('Attempted to create an entity that already exists with id: ' + pItem[this.collectionId + '_id'], 'info');
			return false;
		}
		// Check the entity has a layer defined
		if (typeof(pItem.entity_layer) == 'undefined') {
			this.log('Attempted to create an entity without an entity_layer property!', 'warning', pItem, true);
			return false;
		}
		// Check that the asset that this entity uses actually exists!
		if (!this.engine.assets.byId[pItem.asset_id]) {
			this.log('Cannot create entity because the asset it is trying to use does not exist: ' + pItem.asset_id, 'warning', pItem);
			return false;
		}
		// Is the entity's tile blocking check enabled?
		if (pItem.entity_tile_block == ENTITY_TB_NOBLOCK_CHECK || pItem.entity_tile_block == ENTITY_TB_BLOCK_CHECK) {
			// Check that the entitys tile is not blocked by another entity
			//this.log('Blocking check enabled on tile ' + pItem.entity_id + ' with ' + pItem.entity_tile_block, 'info', pItem);
			var blockingEntity = this.isEntityTileBlocked(pItem);
			if (blockingEntity) {
				this.log('Cannot create ' + pItem.entity_id + ' (' + pItem.entity_x + ', ' + pItem.entity_y + ') is blocked by other entity ' + blockingEntity.entity_id + ' at (' + blockingEntity.entity_x + ', ' + blockingEntity.entity_y + ')', 'info');
				return false;
			}
		}
		// Is the defer list empty and the entity's asset loaded?
		if (this.deferList.length || !this.engine.assets.assetReady(pItem.asset_id)) {
			// Either the defer list is not empty or the asset is not ready so add the entity to the defer list
			this.deferList.push(pItem);
			this.emit('createDeferred', pItem);
			return false;
		}
		
		// No integrity checks failed so return true to continue processing this item
		return true;
	},
	
	/** _processDeferList - Processes the entities stored in the deferList array and if the
	assets that an entity in the list uses are now available, the entity is created and removed
	from the deferList array. {
		category:"method",
	} **/
	_processDeferList: function () {
		
		if (!this.deferListProcessing) {
			
			this.deferListProcessing = true;
			
			while (this.deferList.length) {
				var entity = this.deferList[0];
				
				if (!this.engine.assets.assetReady(entity.asset_id)) {
					break;
				} else {
					this._propagate(entity, PROPAGATE_CREATE);
					this.deferList.splice(0, 1);
					
					this.emit('deferCreated', entity);
				}
			}
			
			this.deferListProcessing = false;
			
			if (this.deferListFollowUp) {
				//this.log('Defer list processed but follow-up flag set so processing again!');
				this.deferListFollowUp = false;
				this._processDeferList();
			}
			
		} else {
			//this.log('Already processing defer list so setting follow-up flag.');
			this.deferListFollowUp = true;
		}
		
	},
	
	/** create - Create a new entity on a map. {
		category:"method",
		engine_ver:"0.0.1",
		return: {
			type:"bool",
			desc:"Returns false on failure or null for any other reason.",
		},
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity object to be created.",
			link:"entityData",
		}, {
			type:"method",
			name:"callback",
			desc:"The method to call when the entity has been created successfully.",
		}],
	} **/
	//create: function (entity, callback) {}, // This is declared and inherited from IgeItem.js
	
	/** _create - The private method that is called by 'this.create' and does the actual creation part. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"bool",
			desc:"Returns false on failure or null for any other reason.",
		},
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity object to be created.",
			link:"entityData",
		}],
	} **/
	_create: function (entity) {
		
		// Delay the definition of some references until they are available in the engine
		if (this.firstTime) {
			
			// Define some references to speed up access
			this.viewports = this.engine.viewports;
			this.viewportsByMapId = this.engine.viewports.byMapId;
			this.mapsById = this.engine.maps.byId;
			this.assetsById = this.engine.assets.byId;
			this.assetImageById = this.engine.assets.assetImage;
			this.camerasById = this.engine.cameras.byId;
			this.renderer = this.engine.renderer;
			
			this.firstTime = false;
		}
		
		// If the entity has an ID..
		if (entity.entity_id) {
			
			// Check that this entity does not already exist
			if (!this.byId[entity.entity_id]) {
				
				// Set some useful local variables to speed up access to related engine items
				if (entity.animation_id) {
					this.setAnimation(entity, entity.animation_id);
					// Defaults
					if (typeof(entity.animation_dirty) == 'undefined') { entity.animation_dirty = true; }
				}
				
				if (entity.map_id) {
					
					if (this.engine.maps.byId[entity.map_id]) {
						entity.$local.$map = this.engine.maps.byId[entity.map_id];
						
						// Set the asset references
						this.setAsset(entity, entity.asset_id);
						
						// Now if we are not the server, check entity bounds
						var stillCreate = true;
						
						if (!this.engine.isServer) {
							// Check that the entity is within the bounds of a viewport (or culling is disabled)
							if (!entity.entity_disable_cull && this.isOutOfBounds(entity)) {
								stillCreate = false;
							}
						}
						
						if (stillCreate) {
							// Add the entity to the dirty rect system's cache
							var map = entity.$local.$map;
							
							if (map) {
								
								if (map.map_use_dirty) {
									this.engine.dirtyRects.addEntityToCache(entity, map);
								}
								
								// Set the size of the entity - called even if the entity does not use sprite sheets
								this.setSheetFrame(entity, entity.asset_sheet_frame);
								
								// Mark the entity as dirty so it gets redrawn after being created
								if (!this.engine.isServer) { this.markDirty(entity); }
								
								// Check if this entity blocks the tile map
								this.blockTiles(entity);
								
								// Add the entity to the engine
								this.byIndex.push(entity);
								this.byId[entity.entity_id] = entity;
								this.byMapId[entity.map_id] = this.byMapId[entity.map_id] || [];
								this.byMapId[entity.map_id].push(entity);
								this.byMapIdAndLayer[entity.map_id] = this.byMapIdAndLayer[entity.map_id] || [];
								this.byMapIdAndLayer[entity.map_id][entity.entity_layer] = this.byMapIdAndLayer[entity.map_id][entity.entity_layer] || [];
								this.byMapIdAndLayer[entity.map_id][entity.entity_layer].push(entity);
								
								// If this entity has animation, add it to the animation list
								if (entity.animation_id) { this.byAnimate.push(entity); }
								
								this.addEntityToTileMap(entity);
								
								// If the entity has path data, ensure that the pathing system knows this!
								if (entity.path != null) { this.engine.paths.resumePath(entity, PATH_TYPE_ENTITY); }
								
								// Calc the opposing positioning type for the entity
								if (entity.entity_actual_x != null && entity.entity_actual_y != null) {
									entity.entity_x = Math.floor(entity.entity_actual_x / map.map_tilesize);
									entity.entity_y = Math.floor(entity.entity_actual_y / map.map_tilesize);
								} else if (entity.entity_x != null && entity.entity_y != null) {
									entity.entity_actual_x = Math.floor(entity.entity_x * map.map_tilesize);
									entity.entity_actual_y = Math.floor(entity.entity_y * map.map_tilesize);
								}
								
								// Check for a callback function embedded in the entity
								if (typeof(entity.$local.create_callback) == 'function') {
									entity.$local.create_callback.apply(this, [entity]);
									entity.$local.create_callback = null;
								}
								
								this.emit('afterCreate', entity);
								
								return entity;
								
							} else {
								this.log('Cannot create entity because the entity does not have a map assigned to it! Specify a map_id!', 'error', entity);
								return false;
							}
						} else {
							this.log('Cannot create entity because the bounds-check failed.', 'warning', entity);
							return false;
						}
						
					} else {
						this.log('Cannot create entity because the map specified by map_id does not exist!', 'error', entity);
						return false;
					}
					
				} else {
					this.log('Cannot create entity because the entity does not have a map assigned to it! Specify a map_id!', 'warning', entity);
					return false;
				}
				
			}
			
		} else {
			this.log('Cannot create entity because it has no ID!', 'error', entity);
			return false;
		}
		
	},
	
	/** update - Updates the pItem object with the property values held in pData.
	DO NOT use this method to alter the position of	an entity, use the moveTo* methods instead. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"bool",
			desc:"Returns false on failure or the updated item on success.",
		},
		arguments: [{
			type:"object",
			name:"pItem",
			desc:"The object to receive updated property values.",
		}, {
			type:"object",
			name:"pData",
			desc:"The property values to apply to the pItem object.",
		}, {
			type:"method",
			name:"pCb",
			desc:"The method to call when the update is complete.",
			flags:"optional",
		}],
	} **/
	//update: function (pItem, pData, pCb) {}, // This is declared and inherited from IgeItem.js
	
	/** _update - Updates the class collection item with the matching id specified in the updData
	parameter with all properties of the pData parameter. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"bool",
			desc:"Returns false on failure or the updated item on success.",
		},
		arguments: [{
			type:"object",
			name:"pItem",
			desc:"The object to receive updated property values.",
		}, {
			type:"object",
			name:"pData",
			desc:"The property values to apply to the pItem object.",
		}],
	} **/
	_update: function (pItem, pData) {
		// Grab the current entity object
		var pItem = this.read(pItem);

		if (pItem != null) {
			// Emit the beforeUpdate event and halt if any listener returns the cancel flag
			if (!this.emit('beforeUpdate', pItem)) {
				// Update the current object with the new object's properties
				for (var i in pData) {
					pItem[i] = pData[i];
				}
				
				this.emit('afterUpdate', pItem);
				
				if (typeof(pItem.$local.update_callback) == 'function') {
					pItem.$local.update_callback(pItem);
					pItem.$local.update_callback = null;
				}
				
				return pItem;
			} else {
				// Call the callback with no argument meaning there was a cancellation or error
				if (typeof(pItem.$local.update_callback) == 'function') {
					pItem.$local.update_callback();
					pItem.$local.update_callback = null;
				}
			}
		} else {
			this.log('Cannot update item because it could not be located by calling this.read().', 'error', pItem);
			return false;
		}
		
	},
	
	propUpdate: function (itemObj, propName, propValue) {
		// 
	},
	
	/** _remove - Private method that removes the entity identified by the passed
	id from the engine. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"bool",
			desc:"Returns false on failure or true on success.",
		},
		arguments: [{
			type:"object",
			name:"pItem",
			desc:"The entity object to be removed.",
			link:"entityData",
		}],
	} **/
	_remove: function (pItem) {
		var pItem = this.read(pItem);
		
		if (pItem && pItem != null) {
			
			// Check if the pItem has a callback method and if so, save a reference to it, then fire after removal of the pItem
			var cbMethod = null;
			if (typeof pItem.$local.remove_callback == 'function') {
				cbMethod = pItem.$local.remove_callback;
			}
			
			// Remove it from all the arrays
				// TO-DO - Don't like this implementation - should store the index at create
				// and avoid these costly lookups! - But memory is important too... oh the woe.
			try { var arr1 = this.byMapIdAndLayer[pItem.map_id][pItem.entity_layer]; } catch (err) {
				console.log(pItem);
				throw(err);
			}
			
			var arr2 = this.byMapId[pItem.map_id];
			var arr5 = this.byIndex;
			
			// Mark the position of the entity as dirty
			this.markDirty(pItem);
			
			// Remove the entity from the dirty rect cache
			if (pItem.$local.$map.map_use_dirty) { this.engine.dirtyRects.removeEntityFromCache(pItem); }
			
			// Remove the entity from the tile map cache
			this.removeEntityFromTileMap(pItem);
			this.unBlockTiles(pItem);
			
			// If this entity has a path, remove it from the processing list
			this.engine.paths.stopPath(pItem, PATH_TYPE_ENTITY);
			
			// Remove the entity from all the lookup arrays
			delete this.byId[pItem.entity_id];
			var arr1Index = arr1.indexOf(pItem);
			var arr2Index = arr2.indexOf(pItem);
			var arr5Index = arr5.indexOf(pItem);
			arr1.splice(arr1Index, 1);
			arr2.splice(arr2Index, 1);
			arr5.splice(arr5Index, 1);
			
			// If the entity is animated, remove it from the animation lookup array
			if (pItem.animation_id) {
				var arr3 = this.byAnimate;
				var arr3Index = arr3.indexOf(pItem);
				arr3.splice(arr3Index, 1);
			}
			
			if (typeof(cbMethod) == 'function') {
				cbMethod.apply(this);
			}
			
			return true;
			
		} else {
			return false;
		}
		
	},
	
	/** getEntity - Takes either an entity object or an entity id and returns an entity object. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"bool",
			desc:"Returns an entity or false on failure.",
		},
		arguments: [{
			type:"multi",
			name:"entity",
			desc:"Either a string or an object containing a string property 'entity_id' that identifies the entity to be returned.",
			link:"entityData",
		}],
	} **/
	getEntity: function (entity) {
		this.log('IgeEntities.getEntity is depreciated, please use IgeEntities.read instead.', 'warning');
		return this.read(entity);
	},
	
	/** addEntityToTileMap - Adds an entity to the tile map cache (internal use). {
		category:"method",
		engine_ver:"0.1.2",
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity to add to the tile map cache.",
			link:"entityData",
		}],
	} **/
	addEntityToTileMap: function (entity) {
		// Init the type and cords lookup array
		var map = entity.$local.$map;
		this.tileMapCache[map.map_id] = this.tileMapCache[map.map_id] || [];
		this.tileMapCache[map.map_id][entity.entity_type] = this.tileMapCache[map.map_id][entity.entity_type] || [];
		this.tileMapCache[map.map_id][entity.entity_type][entity.entity_x] = this.tileMapCache[map.map_id][entity.entity_type][entity.entity_x] || [];
		
		// Now some special checks if this entity is a tile
		if (entity.entity_type == ENTITY_TYPE_TILE) {
			// Because we are a tile, only one of us can exist on the tile position, so
			// check if a tile already exists at these co-ordinates
			if (this.tileMapCache[map.map_id][entity.entity_type][entity.entity_x][entity.entity_y]) {
				// Tile already exists so remove it
				this.remove(this.tileMapCache[map.map_id][entity.entity_type][entity.entity_x][entity.entity_y].entity_id);
			}
			
			// Add the tile to the cords
			this.tileMapCache[map.map_id][entity.entity_type][entity.entity_x][entity.entity_y] = entity;
		} else {
			// Add the new entity to the type-co-ordinate lookup array
			this.tileMapCache[map.map_id][entity.entity_type][entity.entity_x][entity.entity_y] = this.tileMapCache[map.map_id][entity.entity_type][entity.entity_x][entity.entity_y] || [];
			this.tileMapCache[map.map_id][entity.entity_type][entity.entity_x][entity.entity_y].push(entity);
		}	
	},
	
	/** removeEntityFromTileMap - Removes an entity from the tile map cache (internal use). {
		category:"method",
		engine_ver:"0.1.2",
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity to remove from the tile map cache.",
			link:"entityData",
		}],
	} **/	
	removeEntityFromTileMap: function (entity) {
		// This could either be an array of objects or a single object so check!
		var map = entity.$local.$map;
		if (this.tileMapCache[map.map_id] != null && this.tileMapCache[map.map_id][entity.entity_type] != null && this.tileMapCache[map.map_id][entity.entity_type][entity.entity_x]  != null && this.tileMapCache[map.map_id][entity.entity_type][entity.entity_x][entity.entity_y]) {
			var arr = this.tileMapCache[map.map_id][entity.entity_type][entity.entity_x][entity.entity_y];
			
			if (arr instanceof Array) {
				var arrIndex = arr.indexOf(entity);
				if (arrIndex > -1) {
					arr.splice(arrIndex, 1);
				}
			} else {
				delete this.tileMapCache[map.map_id][entity.entity_type][entity.entity_x][entity.entity_y];
			}
		}
	},
	
	/** tileOnMapHasClass - Checks if a tile at the given co-ordinates has a particular class assigned to it. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"bool",
			desc:"Returns true if the tile position has the specified class or false if not.",
		},
		arguments: [{
			type:"string",
			name:"mapId",
			desc:"The id of the map to check against.",
		}, {
			type:"integer",
			name:"entityType",
			desc:"The type of entity that we want to check.",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate of the map tile to check.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate of the map tile to check.",
		}, {
			type:"string",
			name:"className",
			desc:"The class name to test the tile against.",
		}],
	} **/	
	tileOnMapHasClass: function (mapId, entityType, x, y, className) {
		if (this.tileMapCache[mapId] && this.tileMapCache[mapId][entityType] && this.tileMapCache[mapId][entityType][x] && this.tileMapCache[mapId][entityType][x][y]) {
			var pointEntList = this.tileMapCache[mapId][entityType][x][y];
			var entCount = pointEntList.length;
			while (entCount--) {
				var entity = pointEntList[entCount];
				if (entity.path_class) {
					if (entity.path_class.indexOf(className)) {
						return true;
					}
				}
			}
		}
		
		return false;
	},
	
	/** moveToTile - Moves the entity to the tile specified by x and y. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"bool",
			desc:"Returns true if the move was successful, false if not.",
		},
		arguments: [{
			type:"multi",
			name:"entity",
			desc:"The entity to move. Can either be the actual entity object or the string entity id.",
			link:"entityData",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate of the map tile to move the entity to.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate of the map tile to move the entity to.",
		}],
	} **/
	moveToTile:function (entity, x, y) {
		var curEntity = this.read(entity);
		
		if (curEntity != null) {
			// Check if the entity has ACTUALLY moved before running expensive code
			if (curEntity.entity_x != x || curEntity.entity_y != y) {
				if (curEntity.$local && curEntity.$local.$map) {
					this.emit('beforeMove', curEntity);
					var map = curEntity.$local.$map;
					
					// Check if the entity has blocking and if so, if the move is valid based upon the tile blocking map data
					if (!this.isEntityTileBlocked(curEntity, x, y)) {
						
						this.removeEntityFromTileMap(curEntity);
						this.unBlockTiles(curEntity);
						if (map.map_use_dirty) { this.engine.dirtyRects.removeEntityFromCache(curEntity); }
						
						if (!this.engine.isServer) { this.markDirty(curEntity); }
						
						var oldDir = curEntity.entity_direction;
						curEntity.entity_direction = this.directionFromMove(curEntity.entity_x, curEntity.entity_y, x, y, map);
						
						/* CEXCLUDE */
						// If the entity's net_mode tells us to update clients about every position change, 
						// update the diffStore so this change will be propagated to the network on next tick.
						// Do this BEFORE updating the entity so that a comparison between the current object
						// and the new values can determine if an update packet is required.
						if (this.engine.isServer && curEntity.entity_net_mode == NET_MODE_FREE_MOTION) {
							this.updateDifferenceStore({
								entity_id: curEntity.entity_id,
								entity_x: x,
								entity_y: y,
								entity_moved: true,
							});
						}
						/* CEXCLUDE */
						
						// Update the entity tile co-ordinates
						curEntity.entity_x = x;
						curEntity.entity_y = y;
						curEntity.entity_actual_x = Math.floor(curEntity.entity_x * map.map_tilesize);
						curEntity.entity_actual_y = Math.floor(curEntity.entity_y * map.map_tilesize);
						curEntity.entity_moved = true;
						
						//this.log('Moved entity to tile with ac-cords:' + curEntity.entity_actual_x + ',' + curEntity.entity_actual_y);
						
						curEntity.$local.$entity_dirty = true;
						
						this.addEntityToTileMap(curEntity);
						this.blockTiles(curEntity);
						if (map.map_use_dirty) { this.engine.dirtyRects.addEntityToCache(curEntity); }
						if (!this.engine.isServer) { this.markDirty(curEntity); }
						if (oldDir != curEntity.entity_direction) { this.emit('directionChange', curEntity); }
						
						return true;
						
					} else {
						this.log('Attempted to move entity to a blocked position!', 'warning');
						return false;
					}
					
					this.emit('afterMove', curEntity);
				} else {
					this.log('Attempted to move entity but entity does not have $local.$map data!', 'warning');
				}
			}
		} else {
			this.log('Attempted to move entity but null object was passed as the entity to move!', 'warning');
		}
	},
	
	/** moveByTile - Moves the entity by the number of tiles specified by x and y. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"bool",
			desc:"Returns true if the move was successful, false if not.",
		},
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity to move.",
			link:"entityData",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate of the map tile to move the entity by.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate of the map tile to move the entity by.",
		}],
	} **/
	moveByTile:function (entity, x, y) {
		var curEntity = this.read(entity);
		
		if (curEntity != null) {
			// Check if the entity has ACTUALLY moved before running expensive code
			if (x || y) {
				if (curEntity.$local && curEntity.$local.$map) {
					this.emit('beforeMove', curEntity);
					var map = curEntity.$local.$map;
					
					// Check if the entity has blocking and if so, if the move is valid based upon the tile blocking map data
					if (!this.isEntityTileBlocked(curEntity, x, y)) {
						
						this.removeEntityFromTileMap(curEntity);
						this.unBlockTiles(curEntity);
						if (map.map_use_dirty) { this.engine.dirtyRects.removeEntityFromCache(curEntity); }
						
						if (!this.engine.isServer) { this.markDirty(curEntity); }
						
						var oldDir = curEntity.entity_direction;
						curEntity.entity_direction = this.directionFromMove(curEntity.entity_x, curEntity.entity_y, curEntity.entity_x + x, curEntity.entity_y + y, map);
						
						/* CEXCLUDE */
						// If the entity's net_mode tells us to update clients about every position change, 
						// update the diffStore so this change will be propagated to the network on next tick.
						// Do this BEFORE updating the entity so that a comparison between the current object
						// and the new values can determine if an update packet is required.
						if (this.engine.isServer && curEntity.entity_net_mode == NET_MODE_FREE_MOTION) {
							this.updateDifferenceStore({
								entity_id: curEntity.entity_id,
								entity_x: curEntity.entity_x + x,
								entity_y: curEntity.entity_y + y,
								entity_moved: true,
							});
						}
						/* CEXCLUDE */
						
						// Update the entity tile co-ordinates
						curEntity.entity_x += x;
						curEntity.entity_y += y;
						curEntity.entity_actual_x = Math.floor(curEntity.entity_x * map.map_tilesize);
						curEntity.entity_actual_y = Math.floor(curEntity.entity_y * map.map_tilesize);
						curEntity.entity_moved = true;
						
						// If the entity's net_mode tells us to update clients about every position change, 
						// update the diffStore so this change will be propagated to the network on next tick
						if (curEntity.entity_net_mode == NET_MODE_FREE_MOTION) {
							this.updateDifferenceStore({
								entity_id: curEntity.entity_id,
								entity_actual_x: curEntity.entity_actual_x,
								entity_actual_y: curEntity.entity_actual_y,
								entity_moved: curEntity.entity_moved,
							});
						}
						
						curEntity.$local.$entity_dirty = true;
						
						this.addEntityToTileMap(curEntity);
						this.blockTiles(curEntity);
						if (map.map_use_dirty) { this.engine.dirtyRects.addEntityToCache(curEntity); }
						if (!this.engine.isServer) { this.markDirty(curEntity); }
						if (oldDir != curEntity.entity_direction) { this.emit('directionChange', curEntity); }
						
						return true;
						
					} else {
						return false;
					}
					
					this.emit('afterMove', curEntity);
				}
			}
		}
	},
	
	/** moveToActual - Moves the entity to the specified x and y in actual co-ordinates. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"bool",
			desc:"Returns true if the move was successful, false if not.",
		},
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity to move.",
			link:"entityData",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate of the actual map position to move the entity to.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate of the actual map position to move the entity to.",
		}],
	} **/
	moveToActual:function (entity, x, y) {
		var curEntity = this.read(entity);
		
		if (curEntity != null) {
			// Check if the entity has ACTUALLY moved before running expensive code
			if (curEntity.entity_actual_x != x || curEntity.entity_actual_y != y) {
				if (curEntity.$local && curEntity.$local.$map) {
					this.emit('beforeMove', curEntity);
					var map = curEntity.$local.$map;
					if (map.map_use_dirty) { this.engine.dirtyRects.removeEntityFromCache(curEntity); }
					
					if (!this.engine.isServer) { this.markDirty(curEntity); }
					
					// Determine the new direction if it has changed
					var oldDir = curEntity.entity_direction;
					curEntity.entity_direction = this.directionFromMove(curEntity.entity_actual_x, curEntity.entity_actual_y, x, y, map);
					
					/* CEXCLUDE */
					// If the entity's net_mode tells us to update clients about every position change, 
					// update the diffStore so this change will be propagated to the network on next tick.
					// Do this BEFORE updating the entity so that a comparison between the current object
					// and the new values can determine if an update packet is required.
					if (this.engine.isServer && curEntity.entity_net_mode == NET_MODE_FREE_MOTION) {
						this.updateDifferenceStore({
							entity_id: curEntity.entity_id,
							entity_actual_x: x,
							entity_actual_y: y,
							entity_moved: true,
						});
					}
					/* CEXCLUDE */
					
					// Update the entity actual co-ordinates
					curEntity.entity_actual_x = x;
					curEntity.entity_actual_y = y;
					
					// Because we are moving by actual co-ordinates, calculate which tile we are over for things like depth sort
					curEntity.entity_x = Math.floor(curEntity.entity_actual_x / map.map_tilesize);
					curEntity.entity_y = Math.floor(curEntity.entity_actual_y / map.map_tilesize);
					curEntity.entity_moved = true;
					
					curEntity.$local.$entity_dirty = true;
					
					if (map.map_use_dirty) { this.engine.dirtyRects.addEntityToCache(curEntity); }
					if (!this.engine.isServer) { this.markDirty(curEntity); }
					if (oldDir != curEntity.entity_direction) { this.emit('directionChange', curEntity); }
					
					this.emit('afterMove', curEntity);
				}
			}
		}
	},
	
	/** moveByActual - Moves the entity by the specified x and y in actual co-ordinates. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"bool",
			desc:"Returns true if the move was successful, false if not.",
		},
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity to move.",
			link:"entityData",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate of the actual map position to move the entity by.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate of the actual map position to move the entity by.",
		}],
	} **/	
	moveByActual:function (entity, x, y) {
		var curEntity = this.read(entity);
		
		if (curEntity != null) {
			// Check if the entity has ACTUALLY moved before running expensive code
			if (x || y) {
				if (curEntity.$local && curEntity.$local.$map) {
					this.emit('beforeMove', curEntity);
					var map = curEntity.$local.$map;
					if (map.map_use_dirty) { this.engine.dirtyRects.removeEntityFromCache(curEntity); }
					
					if (!this.engine.isServer) { this.markDirty(curEntity); }
					
					// Determine the new direction if it has changed
					var oldDir = curEntity.entity_direction;
					curEntity.entity_direction = this.directionFromMove(curEntity.entity_actual_x, curEntity.entity_actual_y, curEntity.entity_actual_x + x, curEntity.entity_actual_y + y, map);
					
					/* CEXCLUDE */
					// If the entity's net_mode tells us to update clients about every position change, 
					// update the diffStore so this change will be propagated to the network on next tick.
					// Do this BEFORE updating the entity so that a comparison between the current object
					// and the new values can determine if an update packet is required.
					if (this.engine.isServer && curEntity.entity_net_mode == NET_MODE_FREE_MOTION) {
						this.updateDifferenceStore({
							entity_id: curEntity.entity_id,
							entity_actual_x: curEntity.entity_actual_x + x,
							entity_actual_y: curEntity.entity_actual_y + y,
							entity_moved: true,
						});
					}
					/* CEXCLUDE */
					
					// Update the entity actual co-ordinates
					curEntity.entity_actual_x += x;
					curEntity.entity_actual_y += y;
					
					// Because we are moving by actual co-ordinates, calculate which tile we are over for things like depth sort
					curEntity.entity_x = Math.floor(curEntity.entity_actual_x / map.map_tilesize);
					curEntity.entity_y = Math.floor(curEntity.entity_actual_y / map.map_tilesize);
					curEntity.entity_moved = true;
					
					curEntity.$local.$entity_dirty = true;
					
					if (map.map_use_dirty) { this.engine.dirtyRects.addEntityToCache(curEntity); }
					if (!this.engine.isServer) { this.markDirty(curEntity); }
					if (oldDir != curEntity.entity_direction) { this.emit('directionChange', curEntity); }
					
					this.emit('afterMove', curEntity);
				} else {
					this.log('Attempted to move entity but entity does not have $local.$map data!', 'warning');
				}
			}
		} else {
			this.log('Attempted to move entity but null object was passed as the entity to move!', 'warning');
		}
	},
	
	/** directionFromMove - Determines the direction of movement between two points. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"integer",
			desc:"Returns a direction represented as an integer that maps to a direction constant. The direction of movement is calculated from the x1 and y1 co-ordinates towards the x2 and y2 co-ordinates.",
		},
		arguments: [{
			type:"integer",
			name:"x1",
			desc:"The x co-ordinate of the starting position.",
		}, {
			type:"integer",
			name:"y1",
			desc:"The y co-ordinate of the starting position.",
		}, {
			type:"integer",
			name:"x2",
			desc:"The x co-ordinate of the new position.",
		}, {
			type:"integer",
			name:"y2",
			desc:"The y co-ordinate of the new position.",
		}, {
			type:"object",
			name:"map",
			desc:"The map to use when determining direction values (2d and iso maps are calculated differently).",
			link:"mapData",
		}],
	} **/
	directionFromMove: function (x1, y1, x2, y2, map) {
		var finalDirection = DIRECTION_NONE;
		
		// The movement direction changes if the map is in 2d or iso because the co-ordinates
		// are always in 2d but movement in isometric is rotated, so we need to rotate into iso
		// if in iso mode by adjusting the final values here.
		if (map.map_render_mode == MAP_RENDER_MODE_2D) {
			// 2D calculations
			if (x2 > x1) { finalDirection += DIRECTION_E; }
			if (x2 < x1) { finalDirection += DIRECTION_W; }
			if (y2 < y1) { finalDirection += DIRECTION_N; }
			if (y2 > y1) { finalDirection += DIRECTION_S; }
		}
		
		if (map.map_render_mode == MAP_RENDER_MODE_ISOMETRIC) {
			// Iso calculations
			if (x2 > x1 && y2 == y1) { finalDirection = DIRECTION_SE; }
			if (x2 < x1 && y2 == y1) { finalDirection = DIRECTION_NW; }
			if (y2 < y1 && x2 == x1) { finalDirection = DIRECTION_NE; }
			if (y2 > y1 && x2 == x1) { finalDirection = DIRECTION_SW; }
			
			if (x2 < x1 && y2 < y1) { finalDirection = DIRECTION_N; }
			if (x2 > x1 && y2 < y1) { finalDirection = DIRECTION_E; }
			if (x2 > x1 && y2 > y1) { finalDirection = DIRECTION_S; }
			if (x2 < x1 && y2 > y1) { finalDirection = DIRECTION_W; }
		}
		
		return finalDirection;
	},
	
	/** entityBoundsByView - Get the bounds of an entity. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"integer",
			desc:"Returns an array with the co-ordinates of the entity bounds in screen view cords.",
		},
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity to evaluate the bounds data from.",
			link:"entityData",
		}, {
			type:"object",
			name:"viewport",
			desc:"The viewport that the entity's bounds are calculated with (different viewports can have different bounds data because of panning and scaling).",
			link:"viewportData",
		}],
	} **/
	entityBoundsByView: function (entity, viewport) {
		var dims = this.getDimensions(entity, viewport);
		return [dims.destinationX, dims.destinationY, dims.entityWidth, dims.entityHeight];
	},
	
	/** markDirty - Take an entity and make a dirty rect on the map from its dimensions. {
		category:"method",
		engine_ver:"0.1.2",
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity to evaluate the bounds data from in order to mark dirty rectangles.",
			link:"entityData",
		}],
	} **/
	markDirty: function (entity) {
		
		if (entity != null) {
			if (entity.map_id) {
				
				var map = entity.$local.$map;
				entity.$local.$entity_dirty = true;
				
				if (map.map_use_dirty) {
					
					// Map uses dirty rectangle system so mark dirty rects this entity intersects
					var entSize = this.getSize(entity);
					var entPos = this.getPosition(entity);
					
					var renderMode = map.map_render_mode;
					
					this.engine.dirtyRects.markDirtyByRect(map, [entPos[renderMode][0], entPos[renderMode][1], entSize[2], entSize[3]], entity.entity_layer);
					
				} else {
					
					// Map is set not to use dirty rectangle system so mark whole viewport as dirty
					// Get the list of viewports looking at the map that the entity exists on
					var vpList = this.engine.viewports.byMapId[entity.map_id];
					
					// If we have a list of viewports
					if (vpList != null) {
						
						// Loop through each viewport and add a dirty rect for this entity to it
						for (var vpI in vpList) {
							vpList[vpI].$local.$viewport_dirty = true;
							if (!this.engine.isSlave) { vpList[vpI].drawLayers[entity.entity_layer].$local.$layer_dirty = true; }
						}
						
					}
					
				}
				
			} else {
				this.log('No map_id property found in the entity passed.', 'error', entity);
			}
		} else {
			this.log('No entity passed.', 'warning', entity);
		}
		
	},
	
	/** markDirtyByAssetId - Takes an assetId and marks all entities that use the asset
	as dirty to force them to redraw. *** Not performance optimised, for debug only. {
		category:"method",
		engine_ver:"0.1.2",
		arguments: [{
			type:"string",
			name:"assetId",
			desc:"The id of the asset used to mark all entities using the asset as dirty.",
		}],
	} **/
	markDirtyByAssetId: function (assetId) {
		for (var i = 0; i < this.byIndex.length; i++) {
			var entity = this.byIndex[i];
			if (entity.asset_id == assetId) {
				delete entity.$local.$size;
				this.markDirty(entity);
			}
		}
	},
	
	/** detectTileIntersect - Detects the tile that an entity intersects. {
		category:"method",
		engine_ver:"0.3.0",
		return: {
			type:"integer",
			desc:"Returns an array with two entries [0] = x co-ordinate of the tile intersected, [1] = y co-ordinate of the tile intersected.",
		},
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity to use when calculating which tile is being intersected.",
			link:"entityData",
		}, {
			type:"object",
			name:"viewport",
			desc:"The viewport to use when calculating tile intersection.",
			link:"viewportData",
		}],
	} **/
	detectTileIntersect: function (entity, viewport) {
		
		if (entity.entity_actual_x != null && entity.entity_actual_y != null) {
			// Get the entity's asset
			var asset = this.engine.assets.byId[entity.asset_id]
			
			// Store the x and y of the entity
			var x = entity.entity_actual_x;
			var y = entity.entity_actual_y;
			
			// Check if the asset is sheet-based or a single image
			if (asset.asset_sheet_enabled && entity.asset_sheet_frame != null) {
				// Sheet-based asset so get the sheet source position
				var assetAnchorPoint = asset.asset_anchor_points[entity.asset_sheet_frame];
			} else {
				// Single image so use source as whole image
				var assetAnchorPoint = asset.asset_anchor_points[0];
			}
			
			// Add the asset anchor point to the entity x and y
			x += assetAnchorPoint[0];
			y += assetAnchorPoint[1];
			
			return [];
		} else {
			return false;
		}
	},
	
	/** setAnimation - Sets the animation currently being used when rendering the entity. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"integer",
			desc:"Returns true if successful or false on failure.",
		},
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity to use when changing the animation.",
			link:"entityData",
		}, {
			type:"string",
			name:"animationId",
			desc:"The id of the animation to apply to the entity.",
		}],
	} **/
	setAnimation: function (entity, animationId) {
		curEntity = this.read(entity);
		if (curEntity && curEntity.entity_id && curEntity.$local) {
			var animation = this.engine.animations.byId[animationId];
			
			if (animation != null) {
				curEntity.$local.$animation = animation;
				
				/* CEXCLUDE */
				// If the entity's net_mode tells us to update clients about every change, 
				// update the diffStore so this change will be propagated to the network on next tick.
				// Do this BEFORE updating the entity so that a comparison between the current object
				// and the new values can determine if an update packet is required.
				if (this.engine.isServer && curEntity.entity_net_mode == NET_MODE_FREE_MOTION) {
					this.updateDifferenceStore({
						entity_id: curEntity.entity_id,
						animation_id: animationId,
					});
				}
				/* CEXCLUDE */
				
				curEntity.animation_id = animationId;
				
				if (animation.animation_fps) {
					//curEntity.$local.$animation_unit = Math.floor(animation.$local.$fpsTime * animation.$local.$frameTime);
					curEntity.$local.$animation_time = 0;
					return true;
				} else {
					// Animation error
					this.log('Trying to set animation "' + curEntity.animation_id + '" for entity but animation does not have an FPS setting (animation_fps)!', 'error');
					return false;
				}
			} else {
				// Animation error
				this.log('Trying to set animation "' + animationId + '" for entity but animation does not exist in engine!', 'error');
				return false;
			}
		} else {
			//this.log('Trying to set animation "' + animationId + '" for entity but entity does not contain an entity_id property!', 'error', entity);
		}
	},
	
	/** setAsset - Sets the asset to be used when rendering the entity. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"integer",
			desc:"Returns true if successful or false on failure.",
		},
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity to use when changing the asset.",
			link:"entityData",
		}, {
			type:"string",
			name:"assetId",
			desc:"The id of the asset to apply to the entity.",
		}],
	} **/
	setAsset: function (entity, assetId) {
		if (entity && entity.$local) {
			// Check that the asset that this entity uses actually exists!
			if (!this.engine.assets.byId[assetId]) {
				this.log('setAsset: Cannot set entity asset because the asset specified does not exist: ' + assetId, 'warning', entity);
				return false;
			} else {
				var previousAsset = false;
				
				if (entity.$local != null && entity.$local.$asset) {
					var map = entity.$local.$map;
					
					this.removeEntityFromTileMap(entity);
					this.unBlockTiles(entity);
					if (map.map_use_dirty) { this.engine.dirtyRects.removeEntityFromCache(entity); }
					
					if (!this.engine.isServer) { this.markDirty(entity); }
					
					previousAsset = true;
				}
				
				entity.asset_id = assetId;
				entity.$local.$asset = this.engine.assets.byId[entity.asset_id];
				entity.$local.$assetImage = this.engine.assets.assetImage[entity.asset_id];
				
				entity.$local.$entity_dirty = true;
				
				// Update the pre-calc sheet data - called even if the entity does not use sprite sheets
				this.setSheetFrame(entity, entity.asset_sheet_frame);
				
				// Now call the getSize and getPosition methods which will update the data cached for this entity
				// because we set it as dirty above first which forces a re-calc of the cached data
				this.getSize(entity);
				this.getPosition(entity);
				
				if (previousAsset) {
					this.addEntityToTileMap(entity);
					this.blockTiles(entity);
					if (map.map_use_dirty) { this.engine.dirtyRects.addEntityToCache(entity); }
					if (!this.engine.isServer) { this.markDirty(entity); }
				}
				
				return true;
			}
		} else {
			this.log('Cannot set asset for entity because it does not have a $local!', 'error', entity);
			return false;
		}
	},
	
	/** processAnimation - Updates all entities with an active animation to the next frame
	based upon the delta timestep. {
		category:"method",
		engine_ver:"0.1.2",
		arguments: [{
			type:"integer",
			name:"delta",
			desc:"The delta timestep to use when calculating entity animation frames.",
			link:"entityData",
		}],
	} **/
	processAnimation: function  (delta) {
		if (!isNaN(delta)) {
			var arr = this.byAnimate;
			var count = arr.length;
			
			while (count--) {
				this.engine.animations.animateByDelta(arr[count], delta);
			}
		} else {
			this.log('Animation delta is NaN!', 'error', delta);
		}
	},
	
	/** setSheetFrame - Sets the sprite sheet frame index to use when rendering the entity. {
		category:"method",
		engine_ver:"0.1.2",
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity to apply the sheet frame update to.",
			link:"entityData",
		}, {
			type:"integer",
			name:"frameIndex",
			desc:"The index of the sprite-sheet frame to use.",
		}],
	} **/
	setSheetFrame: function (entity, frameIndex) {
		entity.$local.$size = entity.$local.$size || [];
		if (frameIndex) { entity.asset_sheet_frame = frameIndex; }
		
		// Retrieve the array of assets for this entity
		var asset = entity.$local.$asset;
		var assetImage = asset.$local.$image;
		var assetScale = asset.asset_scale;
		var yPos = 0;
		var xPos = 0;
		
		if (assetImage != null) {
			// Calculate the source X and Y of the entity
			if (entity.asset_sheet_frame && asset.asset_sheet_enabled) {
				yPos = (Math.ceil(entity.asset_sheet_frame / asset.asset_sheet_width) - 1);
				xPos = ((entity.asset_sheet_frame - (asset.asset_sheet_width * yPos)) - 1);
				entity.$local.$size[0] = asset.$local.$size[0] * xPos; // Source image starting x
				entity.$local.$size[1] = asset.$local.$size[1] * yPos; // Source image starting y
				entity.$local.$size[4] = asset.asset_anchor_points[entity.asset_sheet_frame - 1]; // Asset anchor point
				if (isNaN(entity.$local.$size[0])) { throw('Error with calculation'); }
			} else {
				entity.$local.$size[0] = 0; // Source image starting x
				entity.$local.$size[1] = 0; // Source image starting y
				entity.$local.$size[4] = asset.asset_anchor_points[0]; // Asset anchor point
			}
		}
	},
	
	/** getSize - Get the size (width / height dimensions) of the entity as well as
	some misc size-related properties. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"array",
			desc:"Returns an array of size-related properties.",
		},
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity to return the size data for.",
			link:"entityData",
		}],
	} **/
	getSize: function (entity) {
		// Check if the entity is dirty (or no cache data exists) then calculate size, otherwise return cached data
		if (!entity.$local.$size || !entity.$local.$size[2] || entity.$local.$size[3]) { //entity.$local.$entity_dirty || 
		//if (entity.$local.$entity_dirty || !entity.$local.$size) {
			// Check that the entity local size variable (array) exists
			entity.$local.$size = entity.$local.$size || [];
			
			// Retrieve the array of assets for this entity
			var asset = entity.$local.$asset;
			
			if (asset != null && asset.$local != null) {
				var assetImage = asset.$local.$image;
				var assetScale = asset.asset_scale;
				var tilesize = entity.$local.$map.map_tilesize;
				var tilesize2 = entity.$local.$map.map_tilesize2;
				var yPos = 0;
				var xPos = 0;
				var imageToRenderRatioWidth = 0;
				var imageToRenderRatioHeight = 0;
				
				if (assetImage != null) {
					// Calculate the source X and Y of the entity
					if (!entity.$local.$size[4] || entity.$local.$size[0] == null || entity.$local.$size[1] == null) { this.setSheetFrame(entity, entity.asset_sheet_frame); }
					
					// Calculate the width and height of the entity
					if (entity.entity_actual_width != null && entity.entity_actual_height != null) {
						// Actual width and height
						entity.$local.$size[2] = Math.floor(entity.entity_actual_width); // Entity width
						entity.$local.$size[5] = entity.$local.$size[2] / asset.$local.$size[0]; // Image to render ratio x
						entity.$local.$size[3] = Math.floor(entity.entity_actual_height); // Entity height
					} else if (entity.entity_tile_width && entity.entity_tile_height) {
						// Tile width and height
						if (asset.asset_render_mode == ASSET_RENDER_MODE_ISOMETRIC) {
							entity.$local.$size[2] = ((entity.entity_tile_width + entity.entity_tile_height) * assetScale * tilesize2); // Entity width
						} else {
							entity.$local.$size[2] = Math.floor(entity.entity_tile_width * assetScale * tilesize); // Entity width
						}
						entity.$local.$size[5] = entity.$local.$size[2] / asset.$local.$size[0]; // Image to render ratio x
						entity.$local.$size[3] = Math.floor((asset.$local.$size[1] * entity.$local.$size[5])); // Entity height
					} else {
						// Fallback to single tile width and height
						entity.$local.$size[2] = (Math.ceil(tilesize * assetScale)); // Entity width
						entity.$local.$size[5] = entity.$local.$size[2] / asset.$local.$size[0]; // Image to render ratio x
						entity.$local.$size[3] = Math.floor(tilesize2 * assetScale); // Entity height
					}
					
					entity.$local.$size[6] = entity.$local.$size[3] / asset.$local.$size[1]; // Image to render ratio y
					
					entity.$local.$size[7] = (entity.$local.$size[4][0] * entity.$local.$size[5]); // Asset anchor point x * ratio x
					entity.$local.$size[8] = (entity.$local.$size[4][1] * entity.$local.$size[6]); // Asset anchor point y * ratio y
				} else {
					this.log('No image data loaded for asset!', 'error', asset);
				}
				
			}
			
		}
		
		return entity.$local.$size;
	},
	
	/** getPosition - Return the position of the entity. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"array",
			desc:"Returns an array of position data in both 2d and isometric positions or false on error.",
		},
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity to return the position data for.",
			link:"entityData",
		}],
	} **/
	getPosition: function (entity) {
		if (entity != null) {
			// Check if the entity is dirty (or no cache data exists) then calculate position, otherwise return cached data
			if (entity.$local.$entity_dirty || !entity.$local.$position) {
				// Check that the entity local position variable (array) exists
				entity.$local.$position = entity.$local.$position || [];
				// Recalculate and store the entity position data
				if (entity.entity_actual_x != null && entity.entity_actual_y != null) {
					var pos = null;
					
					// Convert the 2d cords into iso cords
					var x2 = entity.entity_actual_x / 2;
					var y2 = entity.entity_actual_y / 2;
					
					// Store the position data in an entity local variable
					entity.$local.$position[0] = [parseInt(entity.entity_actual_x - entity.$local.$size[7]), parseInt(entity.entity_actual_y - (entity.$local.$size[8]))]; // 2d
					entity.$local.$position[1] = [parseInt((x2 - y2) - (entity.$local.$size[7])), parseInt(((x2 + y2) / 2) - (entity.$local.$size[8]))]; // iso
				} else if (entity.entity_x != null && entity.entity_y != null) {
					entity.$local.$position[0] = [parseInt((entity.entity_x * entity.$local.$map.map_tilesize) - entity.$local.$size[7]), parseInt((entity.entity_y * entity.$local.$map.map_tilesize) - entity.$local.$size[8])]; // 2d
					entity.$local.$position[1] = [parseInt(((entity.entity_x - entity.entity_y) * entity.$local.$map.map_tilesize2) - entity.$local.$size[7]), parseInt(((entity.entity_x + entity.entity_y) * entity.$local.$map.map_tilesize4) - entity.$local.$size[8])]; // iso
				}
			}
			
			return entity.$local.$position;
		} else {
			this.log('Attempting to get the position of an entity but you passed a null value as the entity!', 'warning');
			return false;
		}
	},
	
	/** cullOutOfBoundsEntities - Finds and removes any entities that do not reside inside a
	viewport viewing area. This method also accepts an entity parameter which will instruct
	the method to only check the entity passed for out-of-bounds. {
		category:"method",
		engine_ver:"0.1.3",
		return: {
			type:"integer",
			desc:"Returns the number of entities removed.",
		},
		arguments: [{
			type:"object",
			name:"entity",
			desc:"If specified, will limit the bounds check to only this entity.",
			link:"entityData",
			flags:"optional",
		}],
	} **/
	cullOutOfBoundsEntities: function (entity) {
		var removeArray = [];
		var removeCount =  0;
		
		if (!entity) {
			// No entity passed, find all out-of-bounds entities
			var entityArray = this.byIndex;
			var entityCount = entityArray.length;
			
			while (entityCount--) {
				var entity = entityArray[entityCount];
				removeArray[entity.entity_id] = this.isOutOfBounds(entity);
			}
			
			// Now loop through the remove array and remove any entity that is marked as true
			for (var i in removeArray) {
				if (removeArray[i]) {
					this.remove(i);
					removeCount++;
				}
			}
			
			this.log('Removed ' + removeCount + ' entities for out of bounds.');
		} else {
			// Entity passed, check only this one for out-of-bounds
			if (this.isOutOfBounds(entity)) {
				this.remove(entity.entity_id);
				removeCount++;
				console.log('Removed single entity for out of bounds.');
			}
		}
		
		return removeCount;
	},
	
	/** isOutOfBounds - Returns true if the entity is not inside the bounds of any viewport looking
	at the map it exists on, as long as the layer it is on has auto-culling enabled. {
		category:"method",
		engine_ver:"0.1.3",
		return: {
			type:"bool",
			desc:"Returns true if the passed entity is outside the bounds of any active viewport.",
		},
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity to check bounds against.",
			link:"entityData",
		}],
	} **/
	isOutOfBounds: function (entity) {
		// Start off with the view to remove this entity unless it is proven to need to stay
		var removeEntity = true;
		
		// Grab the map and viewports
		var map = entity.$local.$map;
		if (map != null) {
			if (map.map_layers != null && map.map_layers[entity.entity_layer] != null) {
				// Check if the layer this entity exists on has auto-culling enabled
				var layerAutoMode = map.map_layers[entity.entity_layer].layer_auto_mode;
				if (layerAutoMode == LAYER_AUTO_CULL || layerAutoMode == LAYER_AUTO_CULL + LAYER_AUTO_REQUEST) {
					
					var viewportList = this.viewportsByMapId[map.map_id];
					
					// Check if there are any viewports for this map
					if (viewportList) {
						var viewportCount = viewportList.length;
						
						// Loop through all the viewports looking at the map that this entity exits on
						while (viewportCount--) {
							// Get all the data needed to calculate the entity position within this viewport
							var viewport = viewportList[viewportCount];
							//var assetSize = entity.$local.$asset.$local.$size;
							var entSize = this.getSize(entity);
							var entPos = this.getPosition(entity);
							
							var renderMode = map.map_render_mode;
							
							var fp = [entPos[renderMode][0] + viewport.$viewportAdjustX, entPos[renderMode][1] + viewport.$viewportAdjustY, entSize[2], entSize[3]];
							
							// Check the entity position to see if it is outside of the viewport's bounds
							if ((fp[0] > viewport.panLayer.width || fp[1] > viewport.panLayer.height) || ((fp[0] + fp[2]) < 0 || (fp[1] + fp[3]) < 0)) {
								// The entity is outside the viewport
							} else {
								// The entity needs to stay so mark it as such as exit the viewport loop
								removeEntity = false;
								break;
							}
						}
					} else {
						// Because no viewports are setup yet, lets allow the entity to be created
						// TO-DO - Is this correct logic? Probably not!! Find and fix why an entity would ever
						// be created BEFORE a viewport is set up!
						removeEntity = false;
					}
					
				} else {
					// The map layer the entity exists on does not have auto-culling enabled so don't cull
					removeEntity = false;
				}
				
				return removeEntity;
				
			} else {
				// Map layers is null
				return false;
			}
		} else {
			// The map is null!
			return false;
		}
	},
	
	/** listInBounds - Returns an array of entities that are inside the bounds of the
	given viewport and camera duet. {
		category:"method",
		engine_ver:"0.2.1",
		return: {
			type:"array",
			desc:"Returns an array of entities inside viewport bounds.",
		},
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport to check bounds against.",
			link:"viewportData",
		}, {
			type:"object",
			name:"camera",
			desc:"The camera to check bounds against.",
			link:"cameraData",
		}],
	} **/
	listInBounds: function (viewport, camera) {
		var entityList = [];
		
		var entList = this.engine.entities.byIndex;
		var count = entList.length;
		
		var map = this.mapsById[viewport.map_id];
		//console.log('VP Dims: ', viewport.panLayer.width, viewport.panLayer.height);
		
		while (count--) {
			var entity = entList[count];
			
			var entSize = this.getSize(entity);
			var entPos = this.getPosition(entity);
			
			var renderMode = map.map_render_mode; 
			
			var fp = [entPos[renderMode][0] + viewport.$viewportAdjustX, entPos[renderMode][1] + viewport.$viewportAdjustY, entSize[2], entSize[3]];
			//console.log('Entity ' + entity.entity_id + ' is at ' + fp[0] + ' x ' + fp[1]);
			
			// Check the entity position to see if it is outside of the viewport's bounds
			if ((fp[0] > viewport.panLayer.width || fp[1] > viewport.panLayer.height) || ((fp[0] + fp[2]) < 0 || (fp[1] + fp[3]) < 0)) {
				// The entity is outside the viewport
			} else {
				// The entity is inside the viewport so add it to the entity list
				entityList.push(entity);
			}
		}
		
		return entityList;
	},
	
	/** blockTiles - Marks this entity's tiles as blocked so that no other entity can occupy them
	based upon the entity_tile_block property value. {
		category:"method",
		engine_ver:"0.1.2",
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity whose tile co-ordinates will be used to block tiles on the map.",
			link:"entityData",
		}],
	} **/
	blockTiles: function (entity) {
		if (entity.entity_tile_block == ENTITY_TB_BLOCK_NOCHECK || entity.entity_tile_block == ENTITY_TB_BLOCK_CHECK) {
			if (entity.entity_x != null && entity.entity_y != null) {
				var maps = this.engine.maps;
				
				// Does the entity occupy more than one tile?
				if (entity.entity_tile_width > 1 || entity.entity_tile_height > 1) {
					for (var x = 0; x < entity.entity_tile_width; x++) {
						for (var y = 0; y < entity.entity_tile_height; y++) {
							maps.markTile(entity.map_id, entity.entity_x + x, entity.entity_y + y, entity, 'igeBlock');
						}
					}
				} else {
					// Mark a single tile
					maps.markTile(entity.map_id, entity.entity_x, entity.entity_y, entity, 'igeBlock');
				}
				
			}
		}
	},
	
	/** unBlockTiles - Marks this entity's tiles as un-blocked so that other entity's can occupy them. {
		category:"method",
		engine_ver:"0.1.2",
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity whose tile co-ordinates will be used to block tiles on the map.",
			link:"entityData",
		}],
	} **/
	unBlockTiles: function (entity) {
		if (entity.entity_tile_block == ENTITY_TB_BLOCK_NOCHECK || entity.entity_tile_block == ENTITY_TB_BLOCK_CHECK) {
			if (entity.entity_x != null && entity.entity_y != null) {
				var maps = this.engine.maps;
				
				// Does the entity occupy more than one tile?
				if (entity.entity_tile_width > 1 || entity.entity_tile_height > 1) {
					for (var x = 0; x < entity.entity_tile_width; x++) {
						for (var y = 0; y < entity.entity_tile_height; y++) {
							maps.unMarkTile(entity.map_id, entity.entity_x + x, entity.entity_y + y, 'igeBlock');
						}
					}
				} else {
					// Mark a single tile
					maps.unMarkTile(entity.map_id, entity.entity_x, entity.entity_y, 'igeBlock');
				}
				
			}
		}
	},
	
	/** isEntityTileBlocked - Checks if a tile is currently blocked by an entity. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"object",
			desc:"Returns the entity that is currently blocking the specified tile area, or false if no blocking exists.",
		},
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity whose tile co-ordinates will be used to check if a tile is blocked.",
			link:"entityData",
		}, {
			type:"object",
			name:"tileX",
			desc:"If specified, will override the entity's x tile co-ordinate (entity_x) with this value when evaluating the tile map for a potential block.",
			flags:"optional",
		}, {
			type:"object",
			name:"tileY",
			desc:"If specified, will override the entity's y tile co-ordinate (entity_y) with this value when evaluating the tile map for a potential block.",
			flags:"optional",
		}],
	} **/
	isEntityTileBlocked: function (entity, tileX, tileY) {
		if ((entity.entity_x != null && entity.entity_y != null) || (tileX != null && tileY != null)) {
			var maps = this.engine.maps;
			
			var finalTileX = null;
			var finalTileY = null;
			
			if (tileX != null && tileY != null) {
				finalTileX = tileX;
				finalTileY = tileY;
			} else {
				finalTileX = entity.entity_x;
				finalTileY = entity.entity_y;
			}
			
			// Does the entity occupy more than one tile?
			if (entity.entity_tile_width > 1 || entity.entity_tile_height > 1) {
				for (var x = 0; x < entity.entity_tile_width; x++) {
					for (var y = 0; y < entity.entity_tile_height; y++) {
						var block = maps.getTileMark(entity.map_id, finalTileX + x, finalTileY + y, 'igeBlock');
						if (block && (block.entity_tile_block == ENTITY_TB_BLOCK_NOCHECK || block.entity_tile_block == ENTITY_TB_BLOCK_CHECK)) { 
							// Check that the block is not itself
							if (block != entity) {
								// Tile is blocked so return the blocking entity
								return block;
							}
						}
					}
				}
			} else {
				// Mark a single tile
				var block = maps.getTileMark(entity.map_id, finalTileX, finalTileY, 'igeBlock');
				if (block && (block.entity_tile_block == ENTITY_TB_BLOCK_NOCHECK || block.entity_tile_block == ENTITY_TB_BLOCK_CHECK)) { 
					// Check that the block is not itself
					if (block != entity) {
						// Tile is blocked so return the blocking entity
						return block;
					}
				}
			}
		}
		
		return false;
	},
	
	/** saveEntityDataAsJson - Saves map entity data as a JSON data file. {
		category:"method",
		engine_ver:"0.2.6",
		arguments: [{
			type:"string",
			name:"mapId",
			desc:"The id of the map to read entity data from.",
		}, {
			type:"string",
			name:"fileName",
			desc:"The path and name of the file to save the data to.",
		}],
	} **/
	saveEntityDataAsJson: function (mapId, fileName) {
		var data = this.engine.stripLocalDeep(this.byMapId[mapId]);
		
		// Scan through the objects, for each one with a template, check if the properties of the
		// object match those of the template and if so, remove the properties that match since
		// they are already defined in the template!
		for (var di in data) {
			var obj = data[di];
			// Delete entity_x and y since actual_x and y are also stored and will work anyway
			delete obj['entity_x'];
			delete obj['entity_y'];
			// Remove entity_db_id since we don't need to store it
			delete obj['entity_db_id'];
			delete obj['entity_persist'];
			delete obj['entity_locale'];
			// We are extracting a single map's data so we already know the map_id
			delete obj['map_id'];
			// Check for a template_id
			if (obj.template_id) {
				var template = this.engine.templates.read(obj.template_id);
				for (var ti in template.template_contents) {
					if (template.template_contents[ti] == obj[ti]) {
						// The object's property value matches the template's so remove it from the object
						delete obj[ti];
					}
				}
			}
		}
		
		var objectJson = JSON.stringify(data);
		
		// Build term lookup data
		var propIndex = this.engine.buildTermIndexCompressed(data);
		
		// Replace each instance of a property name with it's new index
		for (var i in propIndex) {
			var regex = new RegExp('"' + propIndex[i] + '"', 'g');
			objectJson = objectJson.replace(regex, '"¬' + i + '"');
		}
		
		// Replace true and false with 1 and 0 to get compression of those values
		var regex = new RegExp(':true', 'g');
		objectJson = objectJson.replace(regex, ':1');
		
		var regex = new RegExp(':false', 'g');
		objectJson = objectJson.replace(regex, ':0');		
		
		if (data != null) {
			fs.writeFile(igeConfig.mapUrl(fileName + '.json'), BISON.encode(data), this.bind(function (err) {
				if (!err) {
					this.log('Entity data saved successfully to ' + igeConfig.mapUrl(fileName + '.json'));
				} else {
					this.log('ERROR Saving entity data to ' + igeConfig.mapUrl(fileName + '.json'), 'warning', err);
				}
			}));
			
			fs.writeFile(igeConfig.mapUrl(fileName + '_fields.json'), BISON.encode(propIndex), this.bind(function (err) {
				if (!err) {
					this.log('Entity data saved successfully to ' + igeConfig.mapUrl(fileName + '_fields.json'));
				} else {
					this.log('ERROR Saving entity data to ' + igeConfig.mapUrl(fileName + '_fields.json'), 'warning', err);
				}
			}));
		}
	},
	
	/** atScreenXY - Determines the entities under a screen pixel co-ordinate. Useful
	for getting an array of entities that the mouse is hovering over. {
		category:"method",
		engine_ver:"0.2.5",
		return: {
			type:"array",
			desc:"Returns an array of entities sorted by their depth, whose pixels are intersected by the specified x and y screen co-ordinates.",
		},
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport that the x and y co-ordinates are taken from.",
			link:"viewportData",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate.",
		}],
	} **/
	atScreenXY: function (viewport, x, y) {
		// Convert event mouse co-ordinates into canvas co-ordinates
		var elementOffset = $('#' + viewport.viewport_id).offset();
		var renderMode = viewport.$local.$map.map_render_mode;
		var x = x - elementOffset.left - viewport.panLayer.centerX;
		var y = y - elementOffset.top - viewport.panLayer.centerY;
		
		var hitArray = [];
		
		// Loop entities and detect if our mouse is inside the entity bounding box
		// Get all the entities
		var ents = this.byIndex;
		var count = ents.length;
		
		while (count--) {
			var entity = ents[count];
			var entSize = this.getSize(entity);
			var entPos = this.getPosition(entity);
			var fp = [entPos[renderMode][0] + viewport.$viewportAdjustX, entPos[renderMode][1] + viewport.$viewportAdjustY, entSize[2], entSize[3]];
			var depth = 0;
			
			// Check mouse co-ordinates and see if they are inside our entity bounding box
			if (fp[0] <= x && fp[1] <= y && fp[0] + fp[2] > x && fp[1] + fp[3] > y) {
				// Grab the asset's image
				var img = this.engine.assets.assetImage[entity.asset_id];
				var assetSize = entity.$local.$asset.$local.$size;
				
				// Create a temporary canvas to use for image pixel scanning
				var canvas = document.createElement('canvas');
				canvas.width = entSize[2];
				canvas.height = entSize[3];
				
				var ctx = canvas.getContext('2d');
				ctx.drawImage(img, 0, 0, entSize[2], entSize[3]);
				ctx.drawImage(img, entSize[0], entSize[1], assetSize[0], assetSize[1], 0, 0, entSize[2], entSize[3]);
				
				var pixel = ctx.getImageData(x - fp[0], y - fp[1], 1, 1);
				
				if (pixel.data[3] > 127) {
					// Get the depth of the entity
					depth = this.engine.renderer.tileCordsToDepth(entity.entity_x, entity.entity_y, entity.entity_z, entity.entity_tile_width, entity.entity_tile_height, 0);
					
					// Add it to the hit array
					hitArray.push([entity, depth]);
				}
			}
		}
		
		if (hitArray.length > 1) {
			// Depth-sort the array
			hitArray.sort(function (a, b) { return b[1] - a[1]; });
		}
		return hitArray;
	},
	
	/** highlightAtScreenXY - Highlights all entities whose pixel data is drawn at
	the specified co-ordinates. Useful for pixel-perfect highlighting of entities. {
		category:"method",
		engine_ver:"0.2.5",
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport that the x and y co-ordinates are taken from.",
			link:"viewportData",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate.",
		}],
	} **/
	highlightAtScreenXY: function (viewport, x, y) {
		// Convert event mouse co-ordinates into canvas co-ordinates
		var elementOffset = $('#' + viewport.viewport_id).offset();
		var renderMode = viewport.$local.$map.map_render_mode;
		var x = x - elementOffset.left - viewport.panLayer.centerX;
		var y = y - elementOffset.top - viewport.panLayer.centerY;
		
		// Loop entities and detect if our mouse is inside the entity bounding box
		// Get all the entities
		var ents = this.byIndex;
		var count = ents.length;
		
		while (count--) {
			var entity = ents[count];
			var entSize = this.getSize(entity);
			var entPos = this.getPosition(entity);
			var fp = [entPos[renderMode][0] + viewport.$viewportAdjustX, entPos[renderMode][1] + viewport.$viewportAdjustY, entSize[2], entSize[3]];
			var depth = 0;
			
			// Check mouse co-ordinates and see if they are inside our entity bounding box
			if (fp[0] <= x && fp[1] <= y && fp[0] + fp[2] > x && fp[1] + fp[3] > y) {
				// Grab the asset's image
				var img = this.engine.assets.assetImage[entity.asset_id];
				var assetSize = entity.$local.$asset.$local.$size;
				
				// Create a temporary canvas to use for image pixel scanning
				var canvas = document.createElement('canvas');
				canvas.width = entSize[2];
				canvas.height = entSize[3];
				
				var ctx = canvas.getContext('2d');
				ctx.drawImage(img, 0, 0, entSize[2], entSize[3]);
				ctx.drawImage(img, entSize[0], entSize[1], assetSize[0], assetSize[1], 0, 0, entSize[2], entSize[3]);
				
				var pixel = ctx.getImageData(x - fp[0], y - fp[1], 1, 1);
				
				if (pixel.data[3] > 127) {
					this.highlight(entity);
				} else {
					this.unHighlight(entity);
				}
			} else {
				this.unHighlight(entity);
			}
		}
	},
	
	/** atTileXY - Determines all entities currently occupying the specified tile 
	co-ordinates. {
		category:"method",
		engine_ver:"0.2.5",
		return: {
			type:"array",
			desc:"Returns an array of two-dimensional arrays contaning an entity and its depth, sorted by their depths, that occupy the specified tile co-ordinates.",
			index:"integer",
		},
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport that the x and y co-ordinates are taken from.",
			link:"viewportData",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate.",
		}],
	} **/
	atTileXY: function (viewport, x, y) {
		var ents = this.byIndex;
		var count = ents.length;
		var hitArray = [];
		
		while (count--) {
			var entity = ents[count];
			var depth = 0;
			
			// Check mouse co-ordinates and see if they are inside our entity bounding box
			if (entity.entity_x <= x && entity.entity_y <= y && entity.entity_x + (entity.entity_tile_width) > x && entity.entity_y + (entity.entity_tile_height) > y) {
				// Get the depth of the entity
				depth = this.engine.renderer.tileCordsToDepth(entity.entity_x, entity.entity_y, entity.entity_z, entity.entity_tile_width, entity.entity_tile_height, 0);
				
				// Add it to the hit array
				hitArray.push([entity, depth]);
			}
		}
		
		if (hitArray.length > 1) {
			// Depth-sort the array
			hitArray.sort(function (a, b) { return b[1] - a[1]; });
		}
		return hitArray;
	},
	
	/** highlightAtTileXY - Highlights all entities occupying the specified tile
	co-ordinates. {
		category:"method",
		engine_ver:"0.2.5",
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport that the x and y co-ordinates are taken from.",
			link:"viewportData",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate.",
		}],
	} **/
	highlightAtTileXY: function (viewport, x, y) {
		var ents = this.byIndex;
		var count = ents.length;
		
		while (count--) {
			var entity = ents[count];
			
			// Check mouse co-ordinates and see if they are inside our entity bounding box
			if (entity.entity_x <= x && entity.entity_y <= y && entity.entity_x + (entity.entity_tile_width) > x && entity.entity_y + (entity.entity_tile_height) > y) {
				this.highlight(entity);
			} else {
				this.unHighlight(entity);
			}
		}
	},
	
	/** highlight - Sets the specified entity to be drawn with highlighting. {
		category:"method",
		engine_ver:"0.2.5",
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity to highlight.",
			link:"entityData",
		}],
	} **/
	highlight: function (entity) {
		if (!entity.entity_highlight) {
			entity.entity_highlight = true;
			this.markDirty(entity);
		}
	},
	
	/** highlightOne - Sets the specified entity to highlight and unhighlights
	all other entities. {
		category:"method",
		engine_ver:"0.2.5",
		arguments: [{
			type:"object",
			name:"entity",
			desc:"An entity to highlight.",
			link:"entityData",
		}],
	} **/
	highlightOne: function (entity) {
		var ents = this.byIndex;
		var count = ents.length;
		
		while (count--) {
			var tmpEnt = ents[count];
			if (tmpEnt == entity) {
				this.highlight(entity);
			} else {
				this.unHighlight(tmpEnt);
			}
		}
	},
	
	/** unHighlight - Sets the specified entity to be drawn without highlighting. {
		category:"method",
		engine_ver:"0.2.5",
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity to un-highlight.",
			link:"entityData",
		}],
	} **/
	unHighlight: function (entity) {
		if (entity.entity_highlight) {
			entity.entity_highlight = false;
			this.markDirty(entity);
		}
	},
	
	/** unHighlightAll - Sets all entities to be drawn without highlighting. {
		category:"method",
		engine_ver:"0.2.5",
	} **/
	unHighlightAll: function () {
		var ents = this.byIndex;
		var count = ents.length;
		
		while (count--) {
			this.unHighlight(ents[count]);
		}
	},	
	
});