IgeNetworkProvider_SocketIO = new IgeClass({
	
	networkProvider: 'socketio',
	state: 0,
	clientList: [],
	
	setOptions: function (obj) {
		this.io.socketio.options = obj;
	},
	
	providerInit: function () {
		this._state = 1;
		
		/* CEXCLUDE */
		if (this.engine.isServer) {
			// Load the socketio node module
			this.io.socketio.library = require(igeConfig.mapUrl('/node_modules/socket.io'));
			this.log('Network provider ready');
			this.emit('networkProviderReady');
		}
		/* CEXCLUDE */
		
		if (!this.engine.isServer) {
			// Ask the client to include our client-side library
			window.igeBootstrap.require('/socket.io/socket.io', 'socketioclient', this.bind(function () {
				this.log('Network provider ready');
				this.emit('networkProviderReady');
			}));
			
			if (!window.igeBootstrap.busy) {
				window.igeBootstrap.process.apply(window.igeBootstrap);
			}
		}
	},
	
	_start: function (callback) {
		var self = this;
		this.io.socketio.onStart = callback;
		
		/* CEXCLUDE */
		if (this.engine.isServer) {
			// Set the socketio pipe settings and create instance
			this.io.socketio.connection = this.io.socketio.library.listen(this.httpServer);
			this.io.socketio.connection.configure(this.bind(function () {
				if (this.debug) {
					this.io.socketio.connection.set('log level', 10);
				} else {
					this.io.socketio.connection.set('log level', 0);
				}
				this.io.socketio.connection.disable('browser client minification');
				this.io.socketio.connection.set('transports', [
					'websocket',
					'flashsocket',
					'htmlfile',
					'xhr-polling',
					'jsonp-polling',
				]);
			}));
			
			// Listen for socketio client events
			this.io.socketio.connection.sockets.on('connection', function (client) {
				if (!self._serveClients) {
					self.log('Rejecting client connect, we are not serving clients yet!');
					client.disconnect();
				} else {
					client.on('message', function (data) {
						self._socketioServer_ClientMessage(data, client);
					});
					
					client.on('disconnect', function () {
						self.log('Client disconnected from session id ' + client.id);
						self.emit('clientDisconnect', client.id);
					});
					
					self.log('Client connected with session id ' + client.id + ' from IP ' + client.handshake.address.address + ':' + client.handshake.address.port);
					self.emit('clientConnect', client.id);
				}
			});
			
			// Set the socket as ready
			this.io.socketio.state = 2;
			
			// Setup the sync tick
			setInterval(this.bind(this._sendSyncData), Math.floor(1000 / this._cl_updaterate));
			
			// Server is UP!!
			this.setState(5);
			this.emit('networkProviderUp');
			
			if (typeof(this.io.socketio.onStart) == 'function') {
				this.io.socketio.onStart.apply(this);
			}
		}
		/* CEXCLUDE */
		
		if (!this.engine.isServer) {
			// Create new socketio instance
			this.io.socketio.connection = new io.connect(null, this.io.socketio.options);
			
			// Listen for state changes
			this.io.socketio.connection.on('connect', function () {
				// Set the session ID as bug fixed by David Villareal
				self.engine.network.sessionId = this.socket.sessionid;
				
				this.on('message', function (data) {
					self._socketioClient_Receive(data, client);
				});
				
				// Set the socket as ready
				self.io.socketio.state = 2;
				
				self.emit('networkProviderUp');
				self.emit('serverConnect');
			});
			
			this.io.socketio.connection.on('disconnect', this.bind(function () {
				this.log('disconnect', 'info', arguments);
			}));
		}
	},
	
	/* CEXCLUDE */
	_socketioServer_ClientMessage: function(data, client) {
		// Decode the data packet
		var packet = this.decodePacket(data);
		var command = this._commandList._reverse[packet.cmdId];
		
		if (typeof(command) == 'undefined') {
			this.log('Error in network packet, command is undefined!', 'error', packet);
		} else {
			var packetData = packet.data || {};
			if (command == 'clientRequest') {
				//console.log('IGEN REQ DATA:', packet);
				var requestId = packetData.data.__igeRI;
				delete packetData.data.__igeRI;
				this.emit('clientRequest', {command:packetData.command, data:packetData.data, requestId:requestId, client:client.id});
			} else {
				this.emit(command, [packetData, client.id]);
			}
		}
	},
	/* CEXCLUDE */
	
	// If socketio connects with us
	_socketioClient_Connect: function () {
		this.log('SocketIO API connected!');
		this.io.socketio.state = 2;
		
		// Emit an event that we are connected
		this.emit('serverConnect');
	},
	
	// If socketio has an error
	_socketioClient_Error: function () {
		this.log('SocketIO API error!', 'error', arguments);
		this.io.socketio.state = -1;
	},
	
	_socketioClient_Receive: function (data, client) {
		if (this._statsEnabled) {
			var bytes = JSON.stringify(data).length;
			this._dataRecv[new Date().getSeconds()] += bytes;
			this._dataRecvAll += bytes;
		}
		
		// Decode the data packet
		var packet = this.decodePacket(data);
		var command = this._commandList._reverse[packet.cmdId];
		//console.log('Incomming server message:', command, packet);
		
		if (typeof command == 'undefined') {
			this.log('Error in network packet, command is undefined!', 'error', packet);
		} else {
			var packetData = packet.data || {};
			if (command == 'serverResponse') {
				var requestId = packetData.__igeRI;
				var requestObj = this._requests[requestId];
				//delete finalData.data.__igeRI;
				if (typeof requestObj.callback == 'function') { requestObj.callback.apply(requestObj.callback, [packetData]); }
			} else {
				this.emit(command, packetData);
			}
		}
	},
	
	_stop: function () {
		
	},
	
	send: function (command, sendData, clientId, debug) {
		// Check that we have a socket object to work with
		if (debug) { this.log('Send: Starting...'); }
		if (this.io.socketio.state == 2) {
			if (sendData && sendData.$local != null) {
				var strippedData = this.engine.stripLocal(sendData);
			} else {
				var strippedData = sendData;
			}
			
			var cmdId = this._commandList[command];
			if (debug) { this.log('Send: Command ID is: ' + cmdId); }
			if (cmdId != null) {
				// Create and encode a new data packet
				var finalPacketData = this.encodePacket(this._commandList[command], null, strippedData);
				
				if (this._statsEnabled) {
					if (debug) { this.log('Send: Recording stats...'); }
					var bytes = JSON.stringify(finalPacketData).length;
					this._dataSend[new Date().getSeconds()] += bytes;
					this._dataSendAll += bytes;
				}
				
				if (command == debug) {
					this.log('Network original:', 'info', strippedData);
					/*if (strippedData != null && strippedData.template_contents != null) {
						console.log('Template object:', typeof(strippedData.template_contents));
						for (var i in strippedData.template_contents) {
							console.log(i, typeof(strippedData.template_contents[i]));
						}
					}*/
					this.log('Network sending:', 'info', this.decodePacket(finalPacketData));
				}
				// Depending upon if we are client or server, send the packet accordingly
				/* CEXCLUDE */
				if (this.engine.isServer) {
					// Server code
					if (clientId) {
						if (clientId instanceof Array) {
							// An array of client id's to send to
							if (debug) { this.log('Send: Sending to array of socketId\'s...'); }
							//console.log('Sending data to clients ID by array: ', clientId);
							for (var i in clientId) {
								this.io.socketio.connection.sockets.socket(clientId[i]).json.send(finalPacketData);
							}
						} else if (typeof clientId == 'object') {
							// A single client object to send to
							if (debug) { this.log('Send: Sending to a single client with session: ' + socketId); }
							//console.log('Sending data to client ID by object: ', clientId.id);
							clientId.json.send(finalPacketData);
						} else {
							// A single client id to send to
							if (debug) { this.log('Send: Sending broadcast data...'); }
							//console.log('Sending data to client ID: ', clientId);
							this.io.socketio.connection.sockets.socket(clientId).json.send(finalPacketData);
						}
					} else {
						//console.log('Sending data by broadcast');
						//this.socket.sockets.json.send('test');
						this.io.socketio.connection.sockets.json.send(finalPacketData);
					}
				}
				/* CEXCLUDE */
				if (!this.engine.isServer){
					// Client code
					//console.log('Sending data to server...');
					//console.log(this.socket.send);
					this.io.socketio.connection.json.send(finalPacketData);
				}
				return true;
			} else {
				this.log('Attempted to send a network packet using a command that does not exist!', 'warning', command);
				return false;
			}
		} else {
			this.log('SocketIO state error, cannot send messages in this state!', 'warning', this.io.socketio.state);
		}
	},
	
});

// Register this provider with the network class
IgeNetwork.prototype.io = IgeNetwork.prototype.io || [];
IgeNetwork.prototype.io.socketio = {};
IgeNetwork.prototype.io.socketio.classMethod = IgeNetworkProvider_SocketIO;