/** IgeEngine - The main class of the engine, all other class instances are accessible through this. {
	engine_ver:"0.0.6",
	category:"class",
} **/

/** starting - Fired when the engine is starting up. {
	category: "event",
} **/
/** started - Fired when the engine has finished starting up. {
	category: "event",
} **/
/** stopped - Fired when the engine has been stopped. {
	category: "event",
} **/
/** loadingModule - Fired before a module is sent to the bootstrap clas to be loaded. {
	category: "event",
	engine_ver:"0.2.0",
	arguments: [{
		type:"string",
		name:"moduleName",
		desc:"The name of the module this event is about.",
	}, {
		type:"string",
		name:"moduleUrl",
		desc:"The url of the module this event is about.",
	}],
} **/
/** moduleLoaded - Fired after a module has loaded successfully. {
	category: "event",
	engine_ver:"0.2.0",
	arguments: [{
		type:"string",
		name:"moduleName",
		desc:"The name of the module this event is about.",
	}, {
		type:"string",
		name:"moduleUrl",
		desc:"The url of the module this event is about.",
	}],
} **/
IgeEngine = new IgeClass({
	
	Extends: IgeEvents,
	
	/** events - The events controller for this class. {
		category:"property",
		type:"object",
		instanceOf:"IgeEvents",
	} **/
	//events: null,
	
	/** events - The instance of this class. {
		category:"property",
		type:"object",
		instanceOf:"IgeEngine",
	} **/
	engine: null,
	
	/** database - Instance of the IgeDatabase class. {
		category:"property",
		type:"object",
		instanceOf:"IgeDatabase",
	} **/
	database: null,
	
	/** network - Instance of the IgeNetwork class. {
		category:"property",
		type:"object",
		instanceOf:"IgeNetwork",
	} **/	
	network: null,
	
	/** templates - Instance of the IgeTemplates class. {
		category:"property",
		type:"object",
		instanceOf:"IgeTemplates",
	} **/
	templates: null,
	
	/** users - Instance of the IgeUsers class. {
		category:"property",
		type:"object",
		instanceOf:"IgeUsers",
	} **/	
	users: null,
	
	/** assets - Instance of the IgeAssets class. {
		category:"property",
		type:"object",
		instanceOf:"IgeAssets",
	} **/
	assets: null,
	
	/** animations - Instance of the IgeAnimations class. {
		category:"property",
		type:"object",
		instanceOf:"IgeAnimations",
	} **/
	animations: null,
	
	/** dirtyRects - Instance of the IgeDirtyRects class. {
		category:"property",
		type:"object",
		instanceOf:"IgeDirtyRects",
	} **/
	dirtyRects: null,
	
	/** maps - Instance of the IgeMaps class. {
		category:"property",
		type:"object",
		instanceOf:"IgeMaps",
	} **/
	maps: null,
	
	/** entities - Instance of the IgeEntities class. {
		category:"property",
		type:"object",
		instanceOf:"IgeEntities",
	} **/
	entities: null,
	
	/** paths - Instance of the IgePaths class. {
		category:"property",
		type:"object",
		instanceOf:"IgePaths",
	} **/
	paths: null,
	
	/** screens - Instance of the IgeScreens class. {
		category:"property",
		type:"object",
		instanceOf:"IgeScreens",
	} **/
	screens: null,
	
	/** cameras - Instance of the IgeCameras class. {
		category:"property",
		type:"object",
		instanceOf:"IgeCameras",
	} **/
	cameras: null,
	
	/** viewports - Instance of the IgeViewports class. {
		category:"property",
		type:"object",
		instanceOf:"IgeViewports",
	} **/
	viewports: null,
	
	/** ui - Instance of the IgeUi class. {
		category:"property",
		type:"object",
		instanceOf:"IgeUi",
	} **/
	ui: null,
	
	/** renderer - Instance of the IgeRenderer class. {
		category:"property",
		type:"object",
		instanceOf:"IgeRenderer",
	} **/
	renderer: null,
	
	/** time - Instance of the IgeTime class. {
		category:"property",
		type:"object",
		instanceOf:"IgeTime",
	} **/
	time: null,
	
	/** idFactory - Instance of the IgeIdFactory class. {
		category:"property",
		type:"object",
		instanceOf:"IgeIdFactory",
	} **/
	idFactory: null,
	
	/** pallete - Instance of the IgePallete class. {
		category:"property",
		type:"object",
		instanceOf:"IgePallete",
	} **/
	pallete: null,
	
	/** windowManager - Instance of the IgeWindow class. {
		category:"property",
		type:"object",
		instanceOf:"IgeWindow",
	} **/
	windowManager: null,
	
	/** defaultContext - The default context to use when aquiring a context from a canvas element before drawing to it. {
		category:"property",
		type:"string",
	} **/
	defaultContext: '2d',
	
	/** config - A reference to the configuration object passed to this class instance when it is created. {
		category:"property",
		type:"string",
		flags:"server",
	} **/	
	config: null,
	
	/** isServer - Is set to true if the engine is running as a server, false if not. {
		category:"property",
		type:"bool",
	} **/		
	isServer: false,
	
	/** isSlave - Is set to true if the engine is running as a slave (usually because another process has taken over the renderer, audio and sometimes networking), false if not. {
		category:"property",
		type:"bool",
	} **/		
	isSlave: false,
	
	/** fpsMs - The number of milliseconds between rendering frames. {
		category:"property",
		type:"integer",
	} **/	
	fpsMs: 1,
	
	/** _requirementList - An array of methods that must all return true when called before the engine will start. {
		category:"property",
		type:"integer",
		seeAlso:"registerRequirement",
	} **/	
	_requirementList: null,
	
	/** _loadedModules - An array with a string index holding boolean values denoting if a module is loaded or not. {
		category:"property",
		type:"array",
		index:"string",
	} **/	
	_loadedModules: null,
	
	/** _moduleFrameworkMethods - An array with an integer index holding string values of method names to call when initialising a module. {
		category:"property",
		type:"array",
		index:"integer",
	} **/	
	_moduleFrameworkMethods: null,
	
	/** init - The constructor for this class. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a new instance of this class",
		},
		argument: {
			type:"object",
			name:"opts",
			desc:"The options object that gives the engine information about how what mode it is in and how to execute rendering etc.",
		},
	} **/
	init: function (opts) {
		
		this.engine = this;
		this._className = 'IgeEngine';
		this._requirementList = [];
		if (typeof(IgeNetwork) != 'undefined') { this._IgeNetwork = true; } else { this._IgeNetwork = false; }
		if (typeof(IgeUsers) != 'undefined') { this._IgeUsers = true; } else { this._IgeUsers = false; }
		
		if (opts != null) {
			var config = opts.config;
			var slave = opts.isSlave;
		}
		
		/* Set if we are a server or not - passing a config object to the engine init when running
		client-side will break a lot of engine code... don't do it! */
		/* CEXCLUDE */
		if (config) {
			this.isServer = true;
			this.config = config;
			this.obfuscator = new IgeObfuscate(this);
			this.log('+++++++++++++++++++++ ENGINE IS SERVER ++++++++++++++++++++++');
		}
		/* CEXCLUDE */
		
		if (slave) {
			this.isSlave = slave;
		}
		
		// Make sure that all the engine parts that the engine relies on are loaded
		// and fire an error if we find one that isn't!
		
		// The order that these objects is created is of importance, best not to play with it,
		// as some of them rely on the others existing when they init.
		if (this.isServer) { this.database = new IgeDatabase(this); }
		if (this._IgeNetwork) { this.network = new IgeNetwork(this); }
		this.templates = new IgeTemplates(this);
		if (this._IgeUsers) { this.users = new IgeUsers(this); }
		this.animations = new IgeAnimations(this);
		this.assets = new IgeAssets(this);
		this.dirtyRects = new IgeDirtyRects(this);
		this.entities = new IgeEntities(this);
		this.screens = new IgeScreens(this);
		this.maps = new IgeMaps(this);
		this.paths = new IgePaths(this);
		this.cameras = new IgeCameras(this);
		this.viewports = new IgeViewports(this);
		this.backgrounds = new IgeBackgrounds(this);
		this.time = new IgeTime(this);
		this.idFactory = new IgeIdFactory(this);
		this.pallete = new IgePallete(this);
		this.windowManager = new IgeWindow(this);
		this.bootstrap = new IgeBootstrap(null, true);
		
		/* CEXCLUDE */
		if (this.isServer) {
			this._moduleList = [];
			this._moduleListClientData = [];
		}
		/* CEXCLUDE */
		
		//this.ui = new IgeUi(this);
		this.renderer = new IgeRenderer(this);
		
		this.firstTick = 0;
		this.lastTick = 0;
		this._moduleCount = 0;
		this._moduleLoadedCount = 0;
		
		// Define the default module framework methods
		this._moduleFrameworkMethods = [];
		if (this._IgeNetwork) {
			this._moduleFrameworkMethods.push('networkInit');
		}
		this._moduleFrameworkMethods.push('engineHooks');
		this._moduleFrameworkMethods.push('networkCommands');
		this._moduleFrameworkMethods.push('networkProperties');
		this._moduleFrameworkMethods.push('modules');
		this._moduleFrameworkMethods.push('moduleHooks');
		this._moduleFrameworkMethods.push('ready');
		
		if (!this.isSlave && !this.isServer) {
			this.intervalSecond = setInterval(this.bind(this._secondTick), 1000);
		}
		
		// Check if we have a network
		if (this._IgeNetwork) {
			// Call the networkInit method of this class
			this.networkInit();
		}	
	},
	
	networkInit: function () {
		this.network.registerCommand('startClientEngine', this.bind(this.start));
		this.network.registerCommand('loadModule', this.bind(this._loadModule));
	},
	
	includeModule: function (modName) {
		/* CEXCLUDE */
		if (this.isServer) {
			// Load the config data for the named module
			var configDataSandbox = {};
			var fileData = fs.readFileSync(this.config.mapUrl('/modules/' + modName + '/config.js'));
			if (fileData != null) {
				vm.runInNewContext(fileData, configDataSandbox);
				if (configDataSandbox.config.moduleName) {
					if (configDataSandbox.config.serverSide && configDataSandbox.config.clientSide) {
						this.loadModule(configDataSandbox.config.moduleName, '/modules/' + modName + '/index', '/modules/' + modName + '/index');
					}
					if (configDataSandbox.config.serverSide && !configDataSandbox.config.clientSide) {
						this.loadModule(configDataSandbox.config.moduleName, '/modules/' + modName + '/index', null);
					}
					if (!configDataSandbox.config.serverSide && configDataSandbox.config.clientSide) {
						this.loadModule(configDataSandbox.config.moduleName, null, '/modules/' + modName + '/index');
					}
				} else {
					this.log('Module cannot be loaded because the config does not contain a moduleName property!', 'warning');
				}
			}
		}
		/* CEXCLUDE */
	},
	
	/** loadModule - Load a module into the engine. The path argument determines the
	server-side path of the module and the url is given to connecting clients to allow
	them to load the module. If path is omitted, the module will not be loaded server-
	side. If url is omitted, the module will not be loaded client-side. {
		category:"method",
		engine_ver:"0.1.3",
		arguments: [{
			type:"object",
			name:"modName",
			desc:"The name of the module class you are loading.",
		}, {
			type:"object",
			name:"path",
			desc:"The server-side path for Node.js to access and load the module's main script. Can be null if no server-side access is required for the module.",
		}, {
			type:"object",
			name:"url",
			desc:"The client-side path for clients to access and load the module's main script. Can be null if no client-side access is required for the module.",
			flags:"optional",
		}],
	} **/
	loadModule: function (modName, path, url) {
		this._moduleList = this._moduleList || [];
		this._moduleList.push([modName, path, url]);
		
		/* CEXCLUDE */
		if (this.isServer) {
			if (path != null) {
				this._moduleCount++;
				this.emit('loadingModule', [modName, path]);
				
				// Require module main file
				this.log('Loading module "' + modName + '" from path: ' + path);
				require(igeConfig.mapUrl(path));
				
				// Create module instance
				this.log('Init module "' + modName + '" from path: ' + path);
				var moduleInstanceName = 'module_' + this._moduleCount + new Date().getTime();
				eval(moduleInstanceName + ' = new ' + modName + '(this);');
				
				// Init module framework methods
				var methodName = '';
				for (var i in this._moduleFrameworkMethods) {
					methodName = this._moduleFrameworkMethods[i];
					if (!this.safeCall(eval(moduleInstanceName), methodName)) { this.log('Module "' + modName + '" does not implement framework method "' + methodName + '"', 'warning', null, true); }
				}
								
				this.emit('moduleLoaded', [modName, path]);
				this.setModuleLoaded(modName);
			}
			if (url != null) {
				this._moduleListClientData.push([modName, url]);
				this.log('Module added to client load queue with url: ' + url);
			}
		}
		/* CEXCLUDE */
		
		if (!this.isServer) {
			// We're client-side so just load the client-side module into the engine
			this._loadModule([[modName, url]]);
		}
	},
	
	/* CEXCLUDE */
	/** sendClientModuleList - Sends a network message to the specified client detailing
	all client-side modules that the server wants the client to load. {
		category:"method",
		engine_ver:"0.1.3",
		flags:"server",
		arguments: [{
			type:"object",
			name:"client",
			desc:"The socket.io client object to send the data to.",
		}],
	} **/
	sendClientModuleList: function (client) {
		if (this.isServer) {
			// Check if we've got any modules to send
			if (this._moduleListClientData.length) {
				this.network.send('loadModule', [this._moduleListClientData], client);
			}
		}
	},
	/* CEXCLUDE */
	
	/** _loadModule - Called when a network message is received by a client to load client-
	side module scripts. {
		category:"method",
		engine_ver:"0.1.3",
		arguments: [{
			type:"array",
			name:"data",
			desc:"A multi-dimensional array of module data.",
		}],
	} **/
	_loadModule: function (data) {
		// Load the module code
		this._moduleCount++;
		if (!this.isServer) {
			// Client-side
			for (var i = 0; i < data.length; i++) {
				// Ask the bootstrap class to require this module file and fire the callback
				// method we're supplying when the file is loaded.
				this.emit('loadingModule', [data[0], data[1]]);
				this.log('Asking bootstrap to load module file for ' + data[i][1]);
				this.log("Bootstrap Busy: " + igeBootstrap.busy, "Bootstrap Queue Length: " + igeBootstrap.queue.length);
				igeBootstrap.require(data[i][1], data[i][0], this.bind(function (fileData) {
					if (fileData[1]) {
						// We were passed the name of the class created by this module
						// so create a new instance of the class now. Many modules can
						// simply add themselves to the engine which is passed to the
						// module's init as an argument in the call below.
						this.setModuleLoaded(fileData[0]);
						this.log('Bootstrap Loaded Module: ' + fileData[0]);
						
						// Create the new module instance
						var moduleInstanceName = 'module_' + this._moduleCount + new Date().getTime();
						eval(moduleInstanceName + ' = new ' + fileData[1] + '(this);');
						var moduleInstance = eval(moduleInstanceName);
						
						// Init module framework methods
						var methodName = '';
						for (var i in this._moduleFrameworkMethods) {
							methodName = this._moduleFrameworkMethods[i];
							if (!this.safeCall(moduleInstance, methodName)) { this.log('Module "' + moduleInstance._className + '" does not implement framework method "' + methodName + '"', 'warning', null, true); }
						}
						
						this._moduleLoadedCount++;
						this.emit('moduleLoaded', [fileData[1], fileData[0]]);
						
						if (this._moduleLoadedCount == this._moduleCount) {
							this.emit('allModulesLoaded');
						}
					}
				}));
			}
			igeBootstrap.process();
		}
	},
	
	/** setModuleLoaded - Set the module's status to loaded. {
		category:"method",
		engine_ver:"0.2.0",
		arguments: [{
			type:"string",
			name:"moduleName",
			desc:"The name of the module to set as loaded.",
		}],
	} **/
	setModuleLoaded: function (moduleName) {
		this._loadedModules = this._loadedModules || [];
		this._loadedModules[moduleName] = true;
	},
	
	/** isModuleLoaded - Check if a module has loaded. {
		category:"method",
		engine_ver:"0.2.0",
		return: {
			type:"bool",
			desc:"Returns true if the module has been loaded or false otherwise.",
		},
		arguments: [{
			type:"string",
			name:"moduleName",
			desc:"The name of the module to check.",
		}],
	} **/
	isModuleLoaded: function (moduleName) {
		this._loadedModules = this._loadedModules || [];
		return this._loadedModules[moduleName] || false;
	},
	
	/** setSlave - Tells the engine to switch to slave mode. {
		category:"method",
	} **/	
	setSlave: function () {
		this.isSlave = true;
	},
	
	/** registerRequirement - Stores the passed method in an array that is checked before the 
	engine is allowed to start. Every method in the list must return true before the engine
	will start. This allows modules to delay engine startup until they have loaded their
	required assets, scripts, files etc. {
		category:"method",
		engine_ver:"0.1.3",
		arguments: [{
			type:"method",
			name:"func",
			desc:"A method to add to the requirementList array that must return true when called before the engine will start.",
		}],
	} **/	
	registerRequirement: function (func) {
		this._requirementList.push(func);
	},
	
	/** start - Asks the engine to start! {
		category:"method",
	} **/	
	start: function () {
		if (!this._requirementCheckInterval) {
			this._requirementCheckInterval = setInterval(this.bind(this._requirementCheck), 50);
		}
	},
	
	/** _requirementCheck - Checks the requirement list by calling each method and recording the
	response. If all methods respond with true, the check interval will be cancelled and the 
	engine will be told to start. {
		category:"method",
		engine_ver:"0.1.3",
	} **/	
	_requirementCheck: function () {
		var responseOkCount = 0;
		if (this._requirementList.length > 0) {
			for (var i = 0; i < this._requirementList.length; i++) {
				if (this._requirementList[i].call()) {
					responseOkCount++;
				}
			}
		}
		
		if (responseOkCount == this._requirementList.length) {
			// Cancel the check interval
			clearInterval(this._requirementCheckInterval);
			// Start the engine
			this._start();
		}
	},
	
	/** _start - Called when all requirment list methods return true. This is the real start
	method. The one names "start" will kick-off the requirement list checker which will only
	call this method (_start) once all requirement list methods return true. This allows us
	to delay the start of the engine until all the different modules check in that they have
	loaded their required assets, files, data etc ok. {
		category:"method",
	} **/	
	_start: function () {
		this.emit('starting', this);
		/* CEXCLUDE */
		if (this.isServer) {
			// Server code
			this.log('Starting engine as server...');
			
			if (this.config.db) {
				this._startDb();
			} else {
				this.log('No database configuration, assuming non-persistent game environment.');
				this._startServer();
			}
		}
		/* CEXCLUDE */
		
		if (!this.isServer) {
			// Client code
			this.log('Starting engine as client...');
			this._started();
		}
	},
	
	/* CEXCLUDE */
	
	/** startClient - Issue a network command to a client to start the client engine. When the
	client receives this command it will automatically start itself and start rendering any
	active viewports etc. Once this command is issued, no further action is required for the
	engine to start. {
		category:"method",
		flags:"server",
		arguments: [{
			type:"object",
			name:"client",
			desc:"The socket.io client object to send the data to.",
		}],
	} **/	
	startClient: function (client) {
		//if (client.igeReady) {
			this.network.send('startClientEngine', null, client);
		//} else {
			// Client hasn't signalled ready so shedule another check for a few ms away
			//setTimeout(this.bind(function () { this.startClient(client); }), 100);
		//}
	},
	
	/** _startDb - Starts the database connection process on the server. {
		category:"method",
		flags:"server",
	} **/	
	_startDb: function () {
		this.log('Starting database connect...');
		this.database.setProvider(this.config.db.type);
		this.database.setOptions(this.config.db);
		this.database.on('connected', this.bind(function () { this._startServer(); }));
		this.database.on('connectionError', this.bind(function () { this._databaseError(); }));
		this.database.connect();
	},
	
	/** _databaseError - Called by the database module when an error occurs with the database. {
		category:"method",
		flags:"server",
	} **/
	_databaseError: function () {
		console.log('Could not connect to the database!');
	},
	
	/** _startServer - Asks the network module to start up in server mode. {
		category:"method",
		flags:"server",
	} **/
	_startServer: function () {
		this.network.on('serverStarted', this.bind(function () { this._started(); }));
		this.log('Starting VM...');
		this.emit('initVM');
	},
	
	/** _serverStarted - Called when the network module has successfully started up in server mode. {
		category:"method",
		flags:"server",
	} **/
	_serverStarted: function () {
		this.intervalRender = setInterval(this.bind(this._renderTick), 10);
		this.log('Engine started as server!');
	},
	/* CEXCLUDE */
	
	/** _started - Called when the engine has started and will fire a 'started' event. {
		category:"method",
	} **/
	_started: function () {
		if (this.isServer) {
			/* CEXCLUDE */
			this._serverStarted();
			/* CEXCLUDE */
		} else {
			// Client-side startup
			this._clientStarted();
		}
		
		this.setState(ENGINE_STATE_STARTED);
		this.emit('started');
	},
	
	/** _clientStarted - Called when the engine has started and is in client mode. {
		category:"method",
	} **/
	_clientStarted: function () {
		this.log('Engine started as client!');
		
		this._renderTickCount = 0;
		this._aiTickCount = 0;
		this.renderer.statsOn = true;
		
		if (!this.isSlave) {
			this.intervalRender = setInterval(this.bind(this._renderTick), this.fpsMs);
			//setTimeout(this.bind(this._aiTick), 1);
		}
		
	},
	
	/** _renderTick - Called upon each render tick interval by a timer. Processes all the
	required logic for each render tick. {
		category:"method",
		arguments: [{
			type:"bool",
			name:"noSched",
			desc:"When set to true, the method will not schedule another call of itself after it finishes.",
		}],
	} **/
	_renderTick: function (noSched) {
		if (noSched != true) { noSched = false; }
		
		var tickStart = new Date().getTime();
		
		// Adjust the tickStart value by the difference between the server and the client
		tickStart -= this.time.clientNetDiff;
		
		this._renderTickCount++;
		
		if (!this.firstTick) {
			this.firstTick = tickStart;
		}
		
		if (!this.lastTick) {
			this.lastTick = 0;
			this.currentDelta = tickStart;
			if (!this.isServer) {
				// We're not the server so resize any auto-resizing viewports
				this.screens._doAutoSize();
			}
		} else {
			this.currentDelta = tickStart - this.lastTick;
		}
		
		this.lastTick = tickStart;
		
		// Execute on client and server
		this.paths.processPaths(this.lastTick); //this.currentDelta
		
		// If we're the client
		if (!this.isServer) {
			this.entities.processAnimation(this.currentDelta);
			this.renderer.render(this.currentDelta);
		}
		
	},
	
	/** _aiTick - Called upon each AI tick interval by a timer. Processes all the
	required logic for each AI tick. {
		category:"method",
	} **/
	_aiTick: function () {
		this._aiTickCount++;
		// Set another timeout!
		setTimeout(this.bind(this._aiTick), 1);
	},
	
	/** _secondTick - Called upon each second tick interval by a timer. Calculates
	current FPS and displays that on-screen. {
		category:"method",
	} **/	
	_secondTick: function () {
		this.currentRenderFps = this._renderTickCount;
		this.currentAiFps = this._aiTickCount;
		
		this._renderTickCount = 0;
		this._aiTickCount = 0;
		
		if (!this.isSlave && !this.isServer) {
			if (document.getElementById('igeLogDiv') != null) {
				var statusString = '';
				statusString += this.currentRenderFps + ' fps | ';
				//statusString += 'Net: ' + (this.time.clientNetLatency) + 'ms | ';
				if (this.network && this.network._statsEnabled) {
					statusString += 'Sent: ' + this.network._dataSendAll + ' bytes | ';
					statusString += 'Recv: ' + this.network._dataRecvAll + ' bytes | ';
				}
				statusString += this.entities.byIndex.length + ' entities | ';
				statusString += this.renderer.renderCount + ' draws/sec | ';
				document.getElementById('igeLogDiv').innerHTML = statusString;
			}
		}
		
		this.renderer.renderCount = 0;
		
	},
	
	/** stop - Stops the engine. {
		category:"method",
	} **/	
	stop: function () {
		/* CEXCLUDE */
		if (this.isServer) {
			// Server code
			this.server.stop();
		}
		/* CEXCLUDE */
		if (!this.isServer) {
			// Client code
			// kill all the timers! (except if we are slave, then only kill ones that slave uses)
			clearInterval(this.intervalSecond);
			clearInterval(this.intervalRender);
			this._stopped();
		}
	},
	
	/** _stopped - Called when the engine has stopped. Will fire a 'stopped' event. {
		category:"method",
	} **/		
	_stopped: function () {
		this.setState(ENGINE_STATE_STOPPED);
		this.emit('stopped');
	},
	
	/** diffObject - Takes a two objects and an optional array and finds the differences between the two
	objects and returns the result of the comparison. {
		category:"method",
		return: {
			type:"array",
			desc:"Returns an array with the first entry (index zero) as a new object containing only the properties and their values from argument 1 that are different from the object specified in argument 2.",
		},
		arguments: [{
			type:"object",
			name:"newObject",
			desc:"The new object to compare to the current object (argument 2).",
		}, {
			type:"object",
			name:"curObject",
			desc:"The current object to compare to the new object (argument 1).",
		}, {
			type:"array",
			name:"props",
			desc:"If set, will limit the comparison of each object to only the property names in the array.",
			flags:"optional",
		}, {
			type:"bool",
			name:"checkDollars",
			desc:"If set to true, the function will	also test properties whose names start with a dollar symbol ($). The default is to ignore them (dollar-prepended names are considered private properties and not to be included in network traffic, DB storage or object comparisons etc).",
			flags:"optional",
		}],
	} **/
	diffObject: function (newObject, curObject, props, checkDollars) {
		
		if (newObject != null && curObject != null) {
			var updObject = {};
			var changedProps = [];
			
			// Whatever changes have been made, we NEED to have the ID in the update object!
			// TO-DO - Not every object we pass into this method is an entity is it? If so this should
			// go in the IgeEntities class not here! Else this line is wrong because is assumes an entity!
			updObject.entity_id = newObject.entity_id;
			
			if (props) {
				// We have an array of properties to copy
				for (var i in props) {
					var propName = props[i];
					
					if (!checkDollars && propName.substr && propName.substr(0, 1) == '$') {
						continue;
					}
					
					if (curObject[propName] != newObject[propName]) {
						updObject[propName] = newObject[propName];
						changedProps.push(propName);
					}
				}
			} else {
				// Check for differences and copy changed data
				for (var i in newObject) {
					
					if (!checkDollars && i.substr && i.substr(0, 1) == '$') {
						continue;
					}
					
					if (curObject[i] != null && curObject[i] != newObject[i]) {
						updObject[i] = newObject[i];
						changedProps.push(i);
					}
				}
			}
		} else {
			this.log('Error whilst trying to diffObject, an object == null.', 'error', {newObject:newObject, curObject:curObject});
		}
		
		return [updObject, changedProps];
		
	},
	
	/** localSet - Stores a local / private property inside the passed object's .$local object. {
		category:"method",
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object to store the new property inside.",
		}, {
			type:"string",
			name:"prop",
			desc:"The property name to store the value against.",
		}, {
			type:"multi",
			name:"val",
			desc:"The value to store against the property name.",
		}],
	} **/
	localSet: function (obj, prop, val) {
		obj['$' + prop] = val;
	},
	
	/** localGet - Returns the specified local / private property from the passed object's .$local object that has 
	originally been set by the localSet method. {
		category:"method",
		return: {
			type:"multi",
			desc:"Returns the value of the specified prop.",
		},	
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object retrieve the property value from.",
		}, {
			type:"string",
			name:"prop",
			desc:"The property name to return the value from.",
		}],
	} **/
	localGet: function (obj, prop) {
		return obj['$' + prop];
	},
	
	/** getData - Returns the specified local / private property from the passed object's .$local object. {
		category:"method",
		return: {
			type:"multi",
			desc:"Returns the value of the specified key.",
		},		
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object retrieve the property value from.",
		}, {
			type:"string",
			name:"key",
			desc:"The property name to return the value from.",
		}],
	} **/
	getData: function (obj, key) {
		if (typeof(obj) == 'object' && obj != null) {
			obj.$local = obj.$local || {};
			return obj.$local[key];
		} else {
			this.log('Cannot get data from passed variable, it is not an object!', 'warning', obj);
		}
	},
	
	/** putData - Stores a local / private property inside the passed object's .$local object. {
		category:"method",
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object to store the new property inside.",
		}, {
			type:"string",
			name:"key",
			desc:"The property name to store the value against.",
		}, {
			type:"multi",
			name:"value",
			desc:"The value to store against the property name.",
		}],
	} **/
	putData: function (obj, key, value) {
		if (typeof(obj) == 'object' && obj != null) {
			obj.$local = obj.$local || {};
			obj.$local[key] = value;
		} else {
			this.log('Cannot put data to passed variable, it is not an object!', 'warning', obj);
		}
	},
	
	/** stripLocal - Returns a copy (not a reference) of the passed object with any dollar-prepended properties removed. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a copy (not a reference) of the passed object with any dollar-prepended properties removed.",
		},	
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object to copy and return stripped of properties that start with a dollar sign.",
		}],
	} **/
	stripLocal: function (obj) {
		var newObj = {};
		for (var i in obj) {
			if (i != '$local') {
				newObj[i] = obj[i];
			}
		}
		return newObj;
	},
	
	/** stripLocalDeep - Returns a copy (not a reference) of the passed object with any dollar-prepended properties removed.
	This method performs a deep scan so every property which is also an object will also be checked and have their dollar-
	prepended properties removed recursively. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a copy (not a reference) of the passed object with any dollar-prepended properties removed.",
		},	
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object to copy and return stripped of properties that start with a dollar sign.",
		}],
	} **/
	stripLocalDeep: function (obj) {
		var newObj = {};
		for (var i in obj) {
			if (i != '$local') {
				if (typeof (obj[i]) == 'object') {
					newObj[i] = this.stripLocalDeep(obj[i]);
				} else {
					newObj[i] = obj[i];
				}
			}
		}
		return newObj;
	},
	
	/** objClone - Returns a copy (not a reference) of the passed object. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a copy (not a reference) of the passed object.",
		},	
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object to copy and return.",
		}],
	} **/	
	objClone: function (obj) {
		var objType = typeof(obj);
		if (objType != 'undefined' && objType != 'null') {
			var retVal = {};
			
			try {
				retVal = JSON.parse(JSON.stringify(obj));
			} catch (err) {
				console.log(err, obj);
			}
			
			return retVal;
		} else {
			this.log('Cannot clone undefined object!', 'warning', obj);
		}
	},
	
	/** setState - Sets the current state of the engine to either started or stopped. {
		engine_ver:"0.2.0",
		category:"method",
		arguments: [{
			type:"integer",
			name:"newState",
			desc:"The state to set the engine to.",
		}],
	} **/	
	setState: function (newState) {
		this._state = newState;
	},
	
	/** state - Gets the current state of the engine. {
		engine_ver:"0.2.0",
		category:"method",
		return: {
			type:"integer",
			desc:"Returns the current engine state as an integer value represented by a constant.",
		},	
	} **/	
	getState: function (newState) {
		return this._state || ENGINE_STATE_STOPPED;
	},
	
	/* CEXCLUDE */
	/** loadAllDbData - Loads all the data from the database into memory. Optionally clears any
	non-persistent data from the database first. {
		engine_ver:"0.2.0",
		category:"method",
		arguments: [{
			type:"bool",
			name:"clearFirst",
			desc:"If set to true will clear all non-persistent data from the database before loading any data.",
		}, {
			type:"function",
			name:"callback",
			desc:"The method to call when all data has been successfully loaded from the database into the engine.",
			flags:"optional",
		}],
		flags:"server",
	} **/
	loadAllDbData: function (clearFirst, callback) {
		if (clearFirst) {
			// Clear all non-persist data first
			this.database.remove('asset', {asset_persist:false}, this.bind(function (err) {
			this.database.remove('map', {map_persist:false}, this.bind(function (err) {
			this.database.remove('viewport', {viewport_persist:false}, this.bind(function (err) {
			this.database.remove('screen', {screen_persist:false}, this.bind(function (err) {
			this.database.remove('camera', {camera_persist:false}, this.bind(function (err) {
			this.database.remove('entity', {entity_persist:false}, this.bind(function (err) {
				this._loadAllDbData(callback);
			}));
			}));
			}));
			}));
			}));
			}));
		} else {
			this._loadAllDbData(callback);
		}
	},
	
	/** _loadAllDbData - Called by loadAllDbData after non-persistent data is removed from the
	database or if no removal is required. This is used because calls to the DB are async. {
		engine_ver:"0.2.0",
		category:"method",
		arguments: [{
			type:"function",
			name:"callback",
			desc:"The method to call when all data has been successfully loaded from the database into the engine.",
			flags:"optional",
		}],
		flags:"server",
	} **/
	_loadAllDbData: function (callback) {
		// Load all the world data from the database!
		this.templates.dbLoadAll({}, this.bind(function () {
		this.screens.dbLoadAll({}, this.bind(function () {
		this.animations.dbLoadAll({}, this.bind(function () {
		this.assets.dbLoadAll({}, this.bind(function () {
		this.maps.dbLoadAll({}, this.bind(function () {
		this.cameras.dbLoadAll({}, this.bind(function () {
		this.viewports.dbLoadAll({}, this.bind(function () {
			callback();
		}));
		}));
		}));
		}));
		}));
		}));
		}));
	},
	
	/** loadData - Load data from a list of collections based upon the specified dataObj using the defined filters.
	The data will be loaded from each collection in order and only once each collection has finished loading its data
	will the next collection start loading its data. {
		engine_ver:"0.2.0",
		category:"method",
		arguments: [{
			type:"object",
			name:"dataObj",
			desc:"An object defining which collections to load data from and what filters to apply to the data that is loaded.",
		}],
		flags:"server",
	} **/
	loadData: function (dataObj) {
		if (dataObj != null && this.__loadDataObj == null) {
			this.__loadDataCollection = this.__loadDataCollection || [];
			this.__loadDataFilter = this.__loadDataFilter || [];
			this.__loadDataMax = 0;
			
			for (var i in dataObj) {
				this.log('[loadData] Queuing data retrieval from collection ' + i);
				this.__loadDataCollection.push(i);
				this.__loadDataFilter.push(dataObj[i] || {});
				this.__loadDataMax++;
			}
			
			this.__loadDataObj = dataObj;
			this.__loadDataIndex = 0;
			
			this._loadNextData();
			
			return true;
		} else {
			return false;
		}
	},
	
	/** _loadNextData - Called by loadData. Loads the next collection data in the loadData list. {
		engine_ver:"0.2.0",
		category:"method",
		flags:"server",
	} **/
	_loadNextData: function () {
		if (this.__loadDataIndex < this.__loadDataMax) {
			var collection = this.__loadDataCollection[this.__loadDataIndex];
			var filter = this.__loadDataFilter[this.__loadDataIndex];
			
			this[collection].dbLoadAll(filter, this.bind(function () { this.__loadDataIndex++; this._loadNextData(); }));
		} else {
			// All data has finished loading, emit an event
			this.log('[loadData] Completed');
			this.emit('_loadDataFinished');
		}
	},
	/* CEXCLUDE */
	
	waitForModule: function (modObj) {
		
	},
	
	/** buildTermIndex - Returns an array of property names and values used in the passed object. Scans object recursively. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns an array of property names and values used in the passed object.",
		},	
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object to scan for property names.",
		}],
	} **/
	buildTermIndex: function (obj, currentIndex, retMode) {
		// Build an array of unique property names as keys with the value as true
		var newIndex = currentIndex || [];
		for (var i in obj) {
			if (typeof(obj[i]) == 'object') {
				// Parse the object
				this.buildTermIndex(obj[i], newIndex, 1);
			} else if(typeof(obj[i]) == 'string') {
				if (parseInt(obj[i]) != obj[i]) {
					// Store the string value in the term array
					newIndex[obj[i]] = true;
				}
			}
			if (parseInt(i) != i) {
				newIndex[i] = true;
			}
			
		}
		
		if (!retMode) {
			// Now turn the index into an array where the keys are integers
			var finalIndex = [];
			for (var i in newIndex) {
				finalIndex.push(i);
			}
			return finalIndex;
		}
	},
	
	/** buildTermIndexCompressed - Returns an array of property names and values used in the passed object where the name or value
	appears more than once. Scans object recursively. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns an array of property names and values used in the passed object.",
		},	
		arguments: [{
			type:"object",
			name:"obj",
			desc:"The object to scan for property names.",
		}],
	} **/
	buildTermIndexCompressed: function (obj, currentIndex, retMode) {
		// Build an array of unique property names as keys with the value as true
		var newIndex = currentIndex || [];
		for (var i in obj) {
			// Check if this key is a non-number
			if (parseInt(i) != i) {
				// This is a key, not a value
				if (!newIndex[i]) { newIndex[i] = 1; } else { newIndex[i] = 2; }
			}
			
			// Check if the value for this key is an object or string
			if (typeof(obj[i]) == 'object') {
				// Parse the object
				this.buildTermIndexCompressed(obj[i], newIndex, 1);
			} else if(typeof(obj[i]) == 'string') {
				// This is a value, not a key
				if (parseInt(obj[i]) != obj[i]) {
					// Store the string value in the term array
					if (!newIndex[obj[i]]) { newIndex[obj[i]] = 1; } else { newIndex[obj[i]] = 2; }
				}
			}

			
		}
		
		if (!retMode) {
			// Now turn the index into an array where the keys are integers
			var finalIndex = [];
			for (var i in newIndex) {
				if (newIndex[i] > 1) {
					finalIndex.push(i);
				}
			}
			return finalIndex;
		}
	},
	
});