IgeNetworkProvider_Offline = new IgeClass({
	
	networkProvider: 'offline',
	
	setOptions: function (obj) {
		
	},
	
	providerInit: function () {
		this.log('Network provider "offline" is ready!');
		this.emit('networkProviderReady');
	},
	
	_start: function (callback) {
		this._state = 1;
		this.setState(5);
		this.emit('networkProviderUp');
	},
	
	_stop: function () {
		this._state = 0;
	},
	
	send: function (command, sendData, socketId, debug) {
		this.log('No network commands are enabled because we are using the "offline" network provider.');
		return false;
	},
	
});

// Register this provider with the network class
IgeNetwork.prototype.io = IgeNetwork.prototype.io || [];
IgeNetwork.prototype.io.offline = {};
IgeNetwork.prototype.io.offline.classMethod = IgeNetworkProvider_Offline;