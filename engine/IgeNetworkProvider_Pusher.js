IgeNetworkProvider_Pusher = new IgeClass({
	
	networkProvider: 'pusher',
	state: 0,
	clientList: [],
	
	setOptions: function (obj) {
		this.io.pusher.options = obj;
	},
	
	providerInit: function () {
		this._state = 1;
		this.io.pusher.channel = 'd';
		
		/* CEXCLUDE */
		if (this.engine.isServer) {
			// Set an array to hold socket ids
			this.io.pusher._sockets = this.io.pusher._sockets || [];
			
			// Load the pusher node module
			this.io.pusher.library = require(igeConfig.mapUrl('/node_modules/pusher-pipe'));
			
			// Set the pusher pipe settings and create instance
			this.io.pusher.connection = this.io.pusher.library.createClient(this.io.pusher.options);
			this.io.pusher.connection.subscribe(['socket_message', 'socket_existence']);
			this.log('Network provider ready');
			
			this.emit('networkProviderReady');
		}
		/* CEXCLUDE */
		
		if (!this.engine.isServer) {
			// Ask the client to include our client-side library // http://js.pusherapp.com/1.10.0-pre/pusher.min - http://js.pusherapp.com/1.9/pusher.min
			window.igeBootstrap.require('http://js.pusherapp.com/1.10.0-pre/pusher.min', 'pusherclient', this.bind(function () {
				Pusher.host = this.io.pusher.options.host;
				Pusher.ws_port = this.io.pusher.options.port;
				Pusher.wss_port = this.io.pusher.options.sslPort;
				
				// Create new pusher instance
				this.io.pusher.connection = new Pusher(this.io.pusher.options.key);
			
				// Listen for state changes
				this.io.pusher.connection.connection.bind('connected', this.bind(this._pusherClient_Connect));
				this.io.pusher.connection.connection.bind('disconnected', this.bind(this._pusherClient_Disconnect));
				this.io.pusher.connection.connection.bind('failed', this.bind(this._pusherClient_Error));
				
				// Listen for events
				this.io.pusher.connection.back_channel.bind(this.io.pusher.channel, this.bind(this._pusherClient_Receive));
				//this.io.pusher.connection.channel('g').on('event', this.bind(this._pusherClient_Receive));
				this.io.pusher.connection.subscribe('g');
				
				this.log('Network provider ready');
				this.emit('networkProviderReady');
			}));
			
			if (!window.igeBootstrap.busy) {
				window.igeBootstrap.process.apply(window.igeBootstrap);
			}
		}
	},
	
	_start: function (callback) {
		this.io.pusher.onStart = callback;
		
		/* CEXCLUDE */
		if (this.engine.isServer) {
			// Set the state to connecting
			this.io.pusher.state = 1;
			
			// Connect to the pusher pipe service
			this.io.pusher.connection.connect();
			
			// Listen for pusher main pipe events
			this.io.pusher.connection.on('connected', this.bind(this._pusherServer_Connect));
			this.io.pusher.connection.on('disconnected', this.bind(this._pusherServer_Disconnect));
			this.io.pusher.connection.on('error', this.bind(this._pusherServer_Error));
			
			// Listen for pusher client events
			this.io.pusher.connection.sockets.on('open', this.bind(this._pusherServer_ClientConnect));
			this.io.pusher.connection.sockets.on('close', this.bind(this._pusherServer_ClientDisconnect));
			this.io.pusher.connection.sockets.on('event', this.bind(this._pusherServer_ClientMessage));
		}
		/* CEXCLUDE */
		
		if (!this.engine.isServer) {
			// Connect!
			this.io.pusher.connection.connect();
		}
	},
	
	/* CEXCLUDE */
	_pusherServer_Connect: function () {
		this.log('Pusher API connected!');
		this.io.pusher.state = 2;
		
		// Server is UP!!
		this.setState(5);
		this.emit('networkProviderUp');
		
		// Setup the sync tick
		setInterval(this.bind(this._sendSyncData), Math.floor(1000 / this._cl_updaterate));
		
		if (typeof(this.io.pusher.onStart) == 'function') {
			this.io.pusher.onStart.apply(this);
		}
	},
	
	_pusherServer_Disconnect: function () {
		this.log('Pusher API disconnected!', 'warning', arguments);
		this.io.pusher.state = 0;
		
		this.emit('networkProviderDown');
	},
	
	// If pusher has an error
	_pusherServer_Error: function () {
		this.log('Pusher API error!', 'warning', arguments);
		this.io.pusher.state = -1;
		
		this.emit('networkProviderError');
	},
	
	// If pusher tells us a client has connected
	_pusherServer_ClientConnect: function(socket_id) {
		if (this._serveClients) {
			this.io.pusher._sockets.push(socket_id);
			this.log('Client connected with session id ' + socket_id);
			this.emit('clientConnect', socket_id);
		}
	},
	
	_pusherServer_ClientDisconnect: function (socket_id) {
		var spliceIndex = this.io.pusher._sockets.indexOf(socket_id);
		if (spliceIndex > -1) { this.io.pusher._sockets.splice(spliceIndex, 1); }
		this.log('Client disconnected from session id ' + socket_id);
		this.emit('clientDisconnect', socket_id);
	},
	
	_pusherServer_ClientMessage: function(eventName, socket_id, data) {
		//console.log('Incomming client message:', eventName, socket_id, data);
		if (eventName == this.io.pusher.channel) {
			// Decode the data packet
			var packet = this.decodePacket(data);
			var command = this._commandList._reverse[packet.cmdId];
			
			if (typeof(command) == 'undefined') {
				this.log('Error in network packet, command is undefined!', 'error', packet);
			} else {
				var packetData = packet.data || {};
				if (command == 'clientRequest') {
					//console.log('IGEN REQ DATA:', packet);
					var requestId = packetData.data.__igeRI;
					delete packetData.data.__igeRI;
					this.emit('clientRequest', {command:packetData.command, data:packetData.data, requestId:requestId, client:socket_id});
				} else {
					this.emit(command, [packetData, socket_id]);
				}
			}
		}
	},
	/* CEXCLUDE */
	
	// If pusher connects with us
	_pusherClient_Connect: function () {
		this.log('Pusher API connected!');
		this.io.pusher.state = 2;
		
		this.emit('networkProviderUp');
		
		// Emit an event that we are connected
		this.emit('serverConnect');
	},
	
	// If pusher disconnects from us
	_pusherClient_Disconnect: function () {
		this.log('Pusher API disconnected!', 'warning', arguments);
		this.io.pusher.state = 0;
		
		this.log('Disconnected from server!');
		this.emit('serverDisconnect');	
	},
	
	// If pusher has an error
	_pusherClient_Error: function () {
		this.log('Pusher API error!', 'error', arguments);
		this.io.pusher.state = -1;
	},
	
	_pusherClient_Receive: function (data) {
		var debug = false;
		//if (!this.engine.isServer) { debug= true; }
		if (this._statsEnabled) {
			var bytes = JSON.stringify(data).length;
			this._dataRecv[new Date().getSeconds()] += bytes;
			this._dataRecvAll += bytes;
		}
		
		// Decode the data packet
		var packet = this.decodePacket(data);
		var command = this._commandList._reverse[packet.cmdId];
		//console.log('Incomming server message:', command, packet);
		
		if (typeof command == 'undefined') {
			this.log('Error in network packet, command is undefined!', 'error', packet);
		} else {
			var packetData = packet.data || {};
			if (command == 'serverResponse') {
				var requestId = packetData.__igeRI;
				var requestObj = this._requests[requestId];
				//delete finalData.data.__igeRI;
				if (typeof requestObj.callback == 'function') { requestObj.callback.apply(requestObj.callback, [packetData]); }
			} else {
				if (debug) {
					console.log('receive', command, packetData);
				}
				this.emit(command, packetData);
			}
		}
	},
	
	_stop: function () {
		this.io.pusher.connection.disconnect();
	},
	
	send: function (command, sendData, socket_id, debug) {
		//if (!this.engine.isServer) { debug= true; }
		// Check that we have a socket object to work with
		if (debug) { console.log('send', command, sendData, socket_id, debug) }
		if (this.io.pusher.state == 2) {
			if (sendData && sendData.$local != null) {
				var strippedData = this.engine.stripLocal(sendData);
			} else {
				sendData = sendData || {};
				var strippedData = sendData;
			}
			
			var cmdId = this._commandList[command];
			if (debug) { this.log('Send: Command ID is: ' + cmdId); }
			if (cmdId != null) {
				// Create and encode a new data packet
				var finalPacketData = this.encodePacket(this._commandList[command], null, strippedData);
				
				if (this._statsEnabled) {
					if (debug) { this.log('Send: Recording stats...'); }
					var bytes = JSON.stringify(finalPacketData).length;
					this._dataSend[new Date().getSeconds()] += bytes;
					this._dataSendAll += bytes;
				}
				
				if (command == debug) {
					this.log('Network original:', 'info', strippedData);
					/*if (strippedData != null && strippedData.template_contents != null) {
						console.log('Template object:', typeof(strippedData.template_contents));
						for (var i in strippedData.template_contents) {
							console.log(i, typeof(strippedData.template_contents[i]));
						}
					}*/
					this.log('Network sending:', 'info', this.decodePacket(finalPacketData));
				}
				// Depending upon if we are client or server, send the packet accordingly
				/* CEXCLUDE */
				if (this.engine.isServer) {
					// Server code
					if (socket_id) {
						if (socket_id instanceof Array) {
							// An array of socket id's to send to
							if (debug) { this.log('Send: Sending to array of socket_id\'s...'); }
							for (var i in socket_id) {
								this.io.pusher.connection.socket(socket_id[i]).trigger(this.io.pusher.channel, finalPacketData);
							}
						} else {
							// A single client id to send to
							if (debug) { this.log('Send: Sending to a single client with session: ' + socket_id); }
							this.io.pusher.connection.socket(socket_id).trigger(this.io.pusher.channel, finalPacketData);
						}
					} else {
						//console.log('Sending data by broadcast');
						if (debug) { this.log('Send: Sending broadcast data for command ' + command); }
						var sockets = this.io.pusher._sockets;
						var socketCount = sockets.length;
						
						while (socketCount--) {
							var tmpSock = sockets[socketCount];
							this.io.pusher.connection.socket(tmpSock).trigger(this.io.pusher.channel, finalPacketData);
						}
					}
				}
				/* CEXCLUDE */
				if (!this.engine.isServer){
					// Client code
					this.io.pusher.connection.send_event(this.io.pusher.channel, finalPacketData);
				}
				return true;
			} else {
				this.log('Attempted to send a network packet using a command that does not exist!', 'warning', command);
				return false;
			}
		} else {
			this.log('Pusher state error, cannot send messages in this state!', 'warning', this.io.pusher.state);
		}
	},
	
});

// Register this provider with the network class
IgeNetwork.prototype.io = IgeNetwork.prototype.io || [];
IgeNetwork.prototype.io.pusher = {};
IgeNetwork.prototype.io.pusher.classMethod = IgeNetworkProvider_Pusher;

/* request - Client-side function that sends a client request to the server to
be handled by the game logic and evaluated. The game logic can then send a response
using the server-side function "response". */
/*IgeNetwork.prototype.request = function (command, sendData, callback) {
	// Store the request so we can act upon the response
	if (sendData && sendData.$local != null) { 
		var strippedData = this.engine.stripLocal(sendData);
	} else {
		sendData = sendData || {};
		var strippedData = sendData;
	}
	
	var requestIndex = this._requests.length;
	strippedData.__igeRI = requestIndex;
	this._requests[requestIndex] = {command: command, data: strippedData, callback: callback};
	
	// Send the request to the server
	this.send('clientRequest', {command: command, data: strippedData});
}*/