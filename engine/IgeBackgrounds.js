IgeBackgrounds = new IgeClass({
	Extends: [IgeCollection, IgeEvents, IgeNetworkItem, IgeItem],
	
	init: function (engine) {
		this._className = 'IgeBackgrounds';
		
		this.engine = engine;
		
		this.byIndex = [];
		this.byId = [];
		this.byMapId = [];
		
		this.collectionId = 'background';
		
		// Hook events!
		this.engine.cameras.on('afterLookAt', this.bind(this._cameraLookChange));
		this.engine.cameras.on('afterLookBy', this.bind(this._cameraLookChange));
		this.engine.cameras.on('afterResize', this.bind(this._viewportResize));
		
		// Check if we have a network
		if (this.engine._IgeNetwork) {
			// Call the networkInit method of this class
			this.networkInit();
		}
	},
	
	networkInit: function () {
		// Network CRUD Commands
		this.engine.network.registerCommand(this.collectionId + 'sCreate', this.bind(this.receiveCreate));
		this.engine.network.registerCommand(this.collectionId + 'sRead', this.bind(this.read));
		this.engine.network.registerCommand(this.collectionId + 'sUpdate', this.bind(this.receiveUpdate));
		this.engine.network.registerCommand(this.collectionId + 'sRemove', this.bind(this.receiveRemove));
		
		// Register standard property names to the network manifest see the API manual for more details
		this.engine.network.addToManifest([
			'background_id',
			'background_repeat',
			'background_layer',
			'background_cam_speed_x',
			'background_cam_speed_y',
			'background_offset_x',
			'background_offset_y',
		]);
	},
	
	/** _itemDefaults - Called by the IgeItem class which this class extends when creating a new item. Here you can define
	any object properties that are default across all items of this class. If this method returns false the create action
	will be cancelled. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns false on failure or true on success.",
		},
		arguments: [{
			type:"object",
			name:"pItem",
			desc:"The item object to be checked.",
		}],
	} **/
	_itemDefaults: function (pItem) {
		this.ensureLocalExists(pItem);
		/* CEXCLUDE */
		if (this.engine.isServer) { this.ensurePersistExists(pItem); }
		/* CEXCLUDE */
		this.ensureIdExists(pItem);
		
		return true;
	},
	
	/** _itemIntegrity - Called by the IgeItem class which this class extends when creating a new item. Checks the
	integrity of an item before it is allowed to be created. Here you can define custom checks to ensure an item being
	created conforms to the standard your class requires. If this method returns false the create action will be cancelled. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns false on failure or true on success.",
		},
		arguments: [{
			type:"object",
			name:"pItem",
			desc:"The item object to be checked.",
		}],
	} **/
	_itemIntegrity: function (pItem) {
		// Check that this entity does not already exist
		if (this.byId[pItem.background_id]) {
			this.log('Attempted to create ' + this.collectionId + ' that already exists with id: ' + pItem[this.collectionId + '_id'], 'info');
			return false;
		}
		// Check the entity has a layer defined
		if (typeof(pItem.background_layer) == 'undefined' && typeof(pItem.background_target) == 'undefined') {
			this.log('Attempted to create ' + this.collectionId + ' without a either a background_layer or background_target property. One or the other is required!', 'warning', pItem, true);
			return false;
		}
		// Check that the asset that this entity uses actually exists!
		if (!this.engine.assets.byId[pItem.asset_id]) {
			this.log('Cannot create ' + this.collectionId + ' because the asset it is trying to use does not exist: ' + pItem.asset_id, 'error', pItem);
			return false;
		}
		
		// No integrity checks failed so return true to continue processing this item
		return true;
	},
	
	/** _create - The private method that is called by IgeItem.create and does the actual creation part. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns false on failure or null for any other reason.",
		},
		arguments: [{
			type:"object",
			name:"pItem",
			desc:"The background object to be created.",
			link:"backgroundData",
		}],
	} **/
	_create: function (pItem) {
		this.byIndex.push(pItem);
		this.byId[pItem[this.collectionId + '_id']] = pItem;
		this.byMapId[pItem.map_id] = this.byMapId[pItem.map_id] || [];
		this.byMapId[pItem.map_id].push(pItem);
		
		// Only create backgrounds if we are on a client
		if (!this.engine.isServer) {
			// Backgrounds do not get created and destroyed in the DOM via the renderer,
			// they are manipulated directly on the DOM via jQuery
			var style = 'position:absolute; ';
			style += 'left:0px; ';
			style += 'top:0px; ';
			style += 'width:100%; ';
			style += 'height:100%; ';
			style += 'background:url(' + this.engine.assets.byId[pItem.asset_id].asset_image_url + ') ' + pItem.background_repeat + '; ';
			
			// Loop the viewports
			var vpArray = this.engine.viewports.byIndex;
			var vpCount = vpArray.length;
			
			while (vpCount--) {
				var viewport = vpArray[vpCount];
				var camera = viewport.$local.$camera;
				if (pItem.background_cam_speed_x || pItem.background_cam_speed_y) {
					var posX = (pItem.background_offset_x || 0) + (camera.camera_x * (pItem.background_cam_speed_x || 0));
					var posY = (pItem.background_offset_y || 0) + (camera.camera_y * (pItem.background_cam_speed_y || 0));
					var backpos = 'background-position: ' + (-posX) + 'px ' + (-posY) + 'px; '
				}
				
				// Create the new background element for this viewport
				var bgElem = $('<div id="' + viewport.viewport_id + '_' + pItem[this.collectionId + '_id'] + '" style="' + style + ' ' + backpos + '">');
				
				if (typeof(pItem.background_layer) != 'undefined') {
					// We are in layer mode so append this background element to the viewport layer
					bgElem.appendTo('#' + viewport.viewport_id + '_' + (pItem.background_layer || '0'));
				}
			}
			
			// Check for a callback function embedded in the entity
			if (typeof(pItem.$local.create_callback) == 'function') {
				pItem.$local.create_callback.apply(this, [pItem]);
				pItem.$local.create_callback = null;
			}
		}
		
		this.emit('afterCreate', pItem);
	},
	
	_cameraLookChange: function (camera) {
		var viewport = camera.$local.$viewport;
		
		// Check if we have any backgrounds operating on the map this viewport is looking at
		if (this.byMapId[viewport.map_id]) {
			var bgArray = this.byMapId[viewport.map_id];
			var bgCount = bgArray.length;
			
			// Loop the backgrounds and apply the position move based upon the background_cam_speed property
			while (bgCount--) {
				var background = bgArray[bgCount];
				if (background.background_cam_speed_x || background.background_cam_speed_y) {
					var anchorX = ((viewport.$viewportAdjustX || 0) + camera.camera_x);
					var anchorY = ((viewport.$viewportAdjustY || 0) + camera.camera_y);
					var newX = anchorX + (background.background_offset_x || 0) + (-camera.camera_x * (background.background_cam_speed_x || 0));
					var newY = anchorY + (background.background_offset_y || 0) + (-camera.camera_y * (background.background_cam_speed_y || 0));
					var bgElem = $('#' + viewport.viewport_id + '_' + background.background_id);
					bgElem.css('backgroundPosition', newX + 'px ' + newY + 'px');
				}
			}
		}
	},
	
	_viewportResize: function (viewport) {
		var camera = viewport.$local.$camera;
		// Check if we have any backgrounds operating on the map this viewport is looking at
		if (this.byMapId[viewport.map_id]) {
			var bgArray = this.byMapId[viewport.map_id];
			var bgCount = bgArray.length;
			
			// Loop the backgrounds and apply the position move based upon the background_cam_speed property
			while (bgCount--) {
				var background = bgArray[bgCount];
				if (background.background_cam_speed_x || background.background_cam_speed_y) {
					var anchorX = ((viewport.$viewportAdjustX || 0) + camera.camera_x);
					var anchorY = ((viewport.$viewportAdjustY || 0) + camera.camera_y);
					var newX = anchorX + (background.background_offset_x || 0) + (-camera.camera_x * (background.background_cam_speed_x || 0));
					var newY = anchorY + (background.background_offset_y || 0) + (-camera.camera_y * (background.background_cam_speed_y || 0));
					var bgElem = $('#' + viewport.viewport_id + '_' + background.background_id);
					bgElem.css('backgroundPosition', newX + 'px ' + newY + 'px');
				}
			}
		}
	},
	
});