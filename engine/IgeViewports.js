/** IgeViewports - The viewports manager. {
	engine_ver:"0.1.0",
	category:"class",
} **/

/** beforeCreate - Fired before a viewport is created. {
	category: "event",
	argument: {
		type:"object",
		name:"viewport",
		desc:"The viewport object this event was fired for.",
	},
} **/
/** afterCreate - Fired after a viewport is created. {
	category: "event",
	argument: {
		type:"object",
		name:"viewport",
		desc:"The viewport object this event was fired for.",
	},
} **/
/** mousedown - Fired when the player's mouse button goes down on a viewport element. {
	category: "event",
	argument: {
		type:"object",
		name:"evt",
		desc:"The HTML event object for this event. The event object will contain a 'viewport' property containing the viewport object relating to the event and a 'map' property containing the map objec that the viewport is looking at.",
	},
} **/
/** mousemove - Fired when the player's mouse moves on a viewport element. {
	category: "event",
	argument: {
		type:"object",
		name:"evt",
		desc:"The HTML event object for this event. The event object will contain a 'viewport' property containing the viewport object relating to the event and a 'map' property containing the map objec that the viewport is looking at.",
	},
} **/
/** mouseup - Fired when the player's mouse button goes up on a viewport element. {
	category: "event",
	argument: {
		type:"object",
		name:"evt",
		desc:"The HTML event object for this event. The event object will contain a 'viewport' property containing the viewport object relating to the event and a 'map' property containing the map objec that the viewport is looking at.",
	},
} **/
/** mousewheel - Fired when the player's mouse wheel is used on a viewport element. {
	category: "event",
	argument: {
		type:"object",
		name:"evt",
		desc:"The HTML event object for this event. The event object will contain a 'viewport' property containing the viewport object relating to the event and a 'map' property containing the map objec that the viewport is looking at.",
	},
} **/
/** beforePanEnd - Fired before a viewport pan operation is ended. {
	category: "event",
	argument: {
		type:"object",
		name:"viewport",
		desc:"The viewport object this event was fired for.",
	},
} **/
/** afterPanEnd - Fired after a viewport pan operation is ended. {
	category: "event",
	argument: {
		type:"object",
		name:"viewport",
		desc:"The viewport object this event was fired for.",
	},
} **/
IgeViewports = new IgeClass({
	
	Extends: [IgeCollection, IgeItem, IgeEvents],
	
	/** engine - A reference object to the main engine instance. {
		category:"property",
		type:"object",
		instanceOf:"IgeEngine",
	} **/
	engine: null,
	
	/** byIndex - An array with an integer index that allows you to access every
	item created using the create method in the order it was created. {
		category:"property",
		type:"array",
		index:"integer",
	} **/
	byIndex: null,
	
	/** byId - An array with a string index that allows you to access every item
	created using the create method by its id. {
		category:"property",
		type:"array",
		index:"string",
	} **/
	byId: null,
	
	/** byMapId - An array with a string index that allows you to access every item
	created using the create method by its map_id property. {
		category:"property",
		type:"array",
		index:"string",
	} **/
	byMapId: [],
	
	/** byScreenId - An array with a string index that allows you to access every item
	created using the create method by its screen_id property. {
		category:"property",
		type:"array",
		index:"string",
	} **/
	byScreenId: [],
	
	/** layerElementType - An array with an integer index containing the name of the HTML
	element to use when generating viewports that render using different methods. At time
	of build, these are canvas, html and WebGL. Canvas mode uses a canvas element to render
	engine entities to the screen. HTML mode uses the DOM to create div elements that represent
	the individual entities on screen. WebGL mode uses a canvas element but uses the webgl
	context to render entities in a 3D context. WebGL support is not currently implemented. {
		category:"property",
		type:"array",
		index:"integer",
	} **/
	layerElementType: [],
	
	/** init - The constructor for this class. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a new instance of this class",
		},
		argument: {
			type:"object",
			name:"engine",
			desc:"The active engine instance.",
			instanceOf:"IgeEngine",
		},
	} **/
	init: function (engine) {
		this._className = 'IgeViewports';
		
		this.engine = engine;
		
		this.byIndex = [];
		this.byId = [];
		this.byMapId = [];
		this.byScreenId = [];
		this.redrawZonesByMapIdAndLayer = [];
				
		// Define the layer element types
		this.layerElementType = [];
		this.layerElementType.push('canvas'); // Canvas 2D
		this.layerElementType.push('div'); // HTML
		this.layerElementType.push('canvas'); // WebGL
		
		this.collectionId = 'viewport';
		
		if (!this.engine.isServer) {
			// Set an event hook to resize screens and then any viewports with the autoresize property set to true
			this.engine.screens.on('resize', this.bind(function () { this._doAutoSize(); }), this);
		}		
		
		// Check if we have a network
		if (this.engine._IgeNetwork) {
			// Call the networkInit method of this class
			this.networkInit();
		}
	},
	
	networkInit: function () {
		// Network CRUD Commands
		this.engine.network.registerCommand('viewportsCreate', this.bind(this.receiveCreate));
		this.engine.network.registerCommand('viewportsRead', this.read);
		this.engine.network.registerCommand('viewportsUpdate', this.update);
		this.engine.network.registerCommand('viewportsRemove', this.remove);
	},
	
	/** create - Create a new viewport to be used for viewing a section of a map. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns the newly created viewport.",
		},
		argument: {
			type:"object",
			name:"viewport",
			desc:"The viewport object to be created.",
			link:"viewportData",
		},
	} **/
	create: function (viewport) {
		
		// Check if we need to auto-generate an id
		this.ensureIdExists(viewport);
		
		this.emit('beforeCreate', viewport);
		
		viewport.$local = viewport.$local || {};
		
		// Assign the local vars that are useful for the viewport to have (optimises other code by reducing object reference chains)
		this.setMap(viewport, viewport.map_id);
		this.setCamera(viewport, viewport.camera_id);
		
		// Set locals
		viewport.$local.$viewport_dirty = true;
		
		// Add the viewport to the engine
		this.byIndex.push(viewport);
		this.byId[viewport.viewport_id] = viewport;
		this.byMapId[viewport.map_id] = this.byMapId[viewport.map_id] || [];
		this.byMapId[viewport.map_id].push(viewport);
		this.byScreenId[viewport.screen_id] = this.byScreenId[viewport.screen_id] || [];
		this.byScreenId[viewport.screen_id].push(viewport);
		
		if (!this.engine.isServer && !this.engine.isSlave) {
			// Create the elements on the page to display the viewport
			var container = $('<div />');
			container.attr('id', viewport.viewport_id);
			container.css('width', viewport.viewport_container.width);
			container.css('height', viewport.viewport_container.height);
			
			if (viewport.viewport_background_color) {
				container.css('backgroundColor', viewport.viewport_background_color);
			}
			
			// Assign the object references to this container element for later access
			container.data('viewports', this);
			container.data('map', this.engine.maps.byId[viewport.map_id]);
			container.data('viewport', viewport);
			
			// Assign the container styles
			container.css('float', 'left');
			container.css('overflow', 'hidden');
			if (viewport.css_zindex) { container.css('zIndex', viewport.css_zindex); }
			
			// Stop users from getting a browser menu when they right-click
			disableContextMenu(container[0]);
			
			// Calculate the pan layer's center point on the viewport and width / height
			viewport.panLayer.centerX = -(~~(1 * ((viewport.viewport_container.width * viewport.panLayer.padding))));
			viewport.panLayer.centerY = -(~~(1 * ((viewport.viewport_container.height * viewport.panLayer.padding))));
			viewport.panLayer.width = (~~(1 * (viewport.viewport_container.width + ((viewport.viewport_container.width * viewport.panLayer.padding) * 2))));
			viewport.panLayer.height = (~~(1 * (viewport.viewport_container.height + ((viewport.viewport_container.height * viewport.panLayer.padding) * 2))));
			
			// Create the pan layer so we can move all the draw layers at once and negate
			// having to redraw the actual canvases until the mouse-up occurs
			var panLayer = $('<div />');
			panLayer.attr('id', viewport.viewport_id + '_' + viewport.panLayer.id);
			panLayer.css('width', viewport.panLayer.width);
			panLayer.css('height', viewport.panLayer.height);
			panLayer.css('position', 'relative');
			panLayer.css('left', 0);
			panLayer.css('top', 0);
			panLayer.css('overflow', 'hidden');
			
			// Reset the pan layer to the center position
			panLayer.css('marginLeft', viewport.panLayer.centerX);
			panLayer.css('marginTop', viewport.panLayer.centerY);
			
			// Stop users from getting a browser menu when they right-click
			disableContextMenu(panLayer[0]);
			
			// Add the pan layer to the container element
			container.append(panLayer);
			
			
			
			// Store a local reference to the most accessed elements in the viewport
			viewport.$local.pan_panLayerElement = panLayer;
			viewport.$local.containerElement = container;
			
			// Create the viewport layers
			var layerLevel = 0;
			var layerZIndex = 0;
			var canvasBased = false;
			var hasCanvasBased = false;
			var layerCount = viewport.drawLayers.length;
			
			// Create the front buffer arrays even if we're not using canvas
			viewport.$local.$frontBuffer = viewport.$local.$frontBuffer || [];
			viewport.$local.$frontBufferCtx = viewport.$local.$frontBufferCtx || [];
			
			for (layerId = 0; layerId < layerCount; layerId++) {
				// Get the layer definition
				var layerDef = viewport.drawLayers[layerId];
				
				// Assign a new zIndex
				layerZIndex++;
				
				// Set locals
				layerDef.$local = layerDef.$local || {};
				layerDef.$local.$layer_dirty = true;
				
				// Select the correct type of layer type
				var layerElement = this.layerElementType[layerDef.layer_type];
				
				if (layerElement == 'canvas') { canvasBased = true; hasCanvasBased = true; } else { canvasBased = false; }
				
				// Create a new layer
				var newLayer = $('<' + layerElement + '  class="igeFrontBuffer layer' + layerId + '" />');
				newLayer.attr('id', viewport.viewport_id + '_' + layerId);
				newLayer.attr('width', viewport.panLayer.width);
				newLayer.attr('height', viewport.panLayer.height);
				newLayer.css('width', viewport.panLayer.width);
				newLayer.css('height', viewport.panLayer.height);
				newLayer.css('position', 'relative');
				newLayer.css('top', -(0 + (layerLevel * viewport.panLayer.height)));
				newLayer.css('overflow', 'hidden');
				newLayer.css('zIndex', layerZIndex);
				
				// Stop users from getting a browser menu when they right-click
				disableContextMenu(newLayer[0]);
				
				panLayer.append(newLayer);
				
				if (canvasBased) {
					
					// Store layer in front-buffer array for later access
					viewport.$local.$frontBuffer[layerId] = newLayer[0];
					viewport.$local.$frontBufferCtx[layerId] = this._canvasContext(newLayer[0]);
					
				} else {
					
					viewport.$local.layerElement = viewport.$local.layerElement || [];
					viewport.$local.layerElement[layerId] = newLayer[0];
					
				}
				
				layerLevel++;
			}
			
			// If the viewport has canvas-based layers, create the viewport back buffer
			if (hasCanvasBased) {
				
				// Create a new layer
				var newLayer = $('<canvas class="igeBackBuffer" width="' + viewport.panLayer.width + '" height="' + viewport.panLayer.height + '"></canvas>');
				newLayer.attr('id', viewport.viewport_id + '_backBuffer');
				newLayer.css('position', 'relative');
				newLayer.css('top', -(0 + (layerLevel * viewport.panLayer.height)));
				newLayer.css('display', 'none');
				
				// Stop users from getting a browser menu when they right-click
				disableContextMenu(newLayer[0]);
				
				panLayer.append(newLayer);
				
				viewport.$local.$backBuffer = newLayer[0];
				viewport.$local.$backBufferCtx = this._canvasContext(newLayer[0]);
				
			}
			
			// Add the viewport elements to the viewport's parent element
			if (!viewport.viewport_container.parentElementId) { viewport.viewport_container.parentElementId = viewport.screen_id; }
			$('#' + viewport.viewport_container.parentElementId).append(container);
			
			// Setup events for the container
			// Viewport mouse events
			container.mousemove(this._mouseMove);
			container.mousedown(this._mouseDown);
			container.mouseup(this._mouseUp);
			container.bind('mousewheel', this._mouseWheel);
			
			// Viewport touch events - we always set the event.btn to 1 because we
			// cannot differentiate between fingers! So every touch is mouse button 1
			container.bind('touchmove', this._mouseMove);
			container.bind('touchstart', this._mouseDown);
			container.bind('touchend', this._mouseUp);
			
			this.setContainerDimensions(viewport, viewport.viewport_container.width, viewport.viewport_container.height);
			this.updateViewportPreCalc(viewport);
			
		}
		
		this.emit('afterCreate', viewport);
		
		return viewport;
		
	},
	
	/** _canvasContext - Get the engine default context for a canvas element. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns the canvas element context to draw to.",
		},
		argument: {
			type:"object",
			name:"elem",
			desc:"The canvas element to get the context from.",
		},
	} **/
	_canvasContext: function (elem) {
		if (elem != null) {
			var context = null;
			
			switch (this.engine.defaultContext) {
				case '2d':
					context = elem.getContext(this.engine.defaultContext);
				break;
				case 'webgl-2d':
					WebGL2D.enable(elem);
					context = elem.getContext(this.engine.defaultContext);
				break;
			}
			
			return context;
		} else {
			this.log('Cannot retrieve canvas context for null element', 'error');
		}
	},
	
	update: function (viewport) {
		
	},
	
	remove: function () {},
	
	/** _mouseDown - Called when a viewport fires a mouse-down event. {
		category:"method",
		argument: {
			type:"object",
			name:"e",
			desc:"The event object that was passed when the event was fired.",
		},
	} **/
	_mouseDown: function (e) {
		var touch = false;
		
		if(e.originalEvent.touches && e.originalEvent.touches.length) {
			e.button = 0;
			touch = true;
		}
		
		var curElem = $(e.currentTarget);
		var viewports = $(e.currentTarget).data('viewports');
		e.map = $(e.currentTarget).data('map');
		e.viewport = $(e.currentTarget).data('viewport');
		
		viewports.emit('mousedown', [e, touch]);
	},
	
	/** _mouseMove - Called when a viewport fires a mouse-move event. {
		category:"method",
		argument: {
			type:"object",
			name:"e",
			desc:"The event object that was passed when the event was fired.",
		},
	} **/	
	_mouseMove: function (e) {
		var touch = false;
		
		if(e.originalEvent.touches && e.originalEvent.touches.length) {
			e.button = 0;
			touch = true;
		}
		
		if (e.originalEvent.changedTouches && e.originalEvent.changedTouches.length) {
			e.pageX = e.originalEvent.changedTouches[0].pageX;
			e.pageY = e.originalEvent.changedTouches[0].pageY;
			e.button = 0;
			touch = true;
		}
		
		var curElem = $(e.currentTarget);
		var viewports = $(e.currentTarget).data('viewports');
		e.map = $(e.currentTarget).data('map');
		e.viewport = $(e.currentTarget).data('viewport');
		viewports.emit('mousemove', [e, touch]);
	},
	
	/** _mouseUp - Called when a viewport fires a mouse-up event. {
		category:"method",
		argument: {
			type:"object",
			name:"e",
			desc:"The event object that was passed when the event was fired.",
		},
	} **/
	_mouseUp: function (e) {
		var touch = false;
		
		if(e.originalEvent.touches && e.originalEvent.touches.length) {
			e.button = 0;
			touch = true;
		}
		
		if (e.originalEvent.changedTouches && e.originalEvent.changedTouches.length) {
			
			e.pageX = e.originalEvent.changedTouches[0].pageX;
			e.pageY = e.originalEvent.changedTouches[0].pageY;
			e.button = 0;
			touch = true;
		}
		
		var curElem = $(e.currentTarget);
		var viewports = $(e.currentTarget).data('viewports');
		e.map = $(e.currentTarget).data('map');
		e.viewport = $(e.currentTarget).data('viewport');
		
		viewports.emit('mouseup', [e, touch]);
	},	
	
	/** _mouseWheel - Called when a viewport fires a mouse-wheel event. {
		category:"method",
		argument: {
			type:"object",
			name:"e",
			desc:"The event object that was passed when the event was fired.",
		},
	} **/
	_mouseWheel: function (e) {
		var curElem = $(e.currentTarget);
		var viewports = $(e.currentTarget).data('viewports');
		e.map = $(e.currentTarget).data('map');
		e.viewport = $(e.currentTarget).data('viewport');
		
		viewports.emit('mousewheel', e);
	},	
	
	/** setMap - Set the map that a viewport is looking at. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true on success or false on failure.",
		},
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport to set the active map on.",
		}, {
			type:"string",
			name:"mapId",
			desc:"The id of the map to set the viewport to look at.",
		}],
	} **/
	setMap: function (viewport, mapId) {
		this.log('Setting map for viewport ' + viewport.viewport_id + ' to ' + mapId, 'info');
		var tempObj = this.engine.maps.byId[mapId];
		
		if (tempObj) {
			viewport.drawLayers = tempObj.map_layers;
			viewport.$local.$map = tempObj;
			viewport.map_id = mapId;
			viewport.$local.$tileWidth = viewport.$local.$map.map_tilesize;
			viewport.$local.$tileHeight = viewport.$local.$tileWidth * viewport.viewport_tile_ratio;
			viewport.$local.$tileWidth2 = Math.floor(viewport.$local.$tileWidth / 2);
			viewport.$local.$tileHeight2 = Math.floor(viewport.$local.$tileHeight / 2);
			
			// Set the viewport to be completely redrawn
			this.makeViewportDirty(viewport);
			
			return true;
		} else {
			this.log('Unable to set map for viewport "' + viewport.viewport_id + '" because the map with the name "' + mapId + '" does not exist.', 'error', viewport);
			return false;
		}
	},
	
	/** setCamera - Set the camera that a viewport is using for positioning data. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true on success or false on failure.",
		},
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport to set the active camera on.",
		}, {
			type:"string",
			name:"cameraId",
			desc:"The id of the camera to set the viewport to use.",
		}],
	} **/
	setCamera: function (viewport, cameraId) {
		var camera = this.engine.cameras.byId[cameraId];
		
		if (!camera) {
			
			this.log('Unable to set camera for viewport "' + viewport.viewport_id + '" because the camera with the name "' + cameraId + '" does not exist.', 'error');
			
			return false;
			
		} else {
			
			// Set local vars for viewport
			viewport.$local.$camera = camera;
			viewport.camera_id = cameraId;
			
			// Set local vars for camera
			camera.$local = camera.$local || {};
			camera.$local.$viewport = viewport;
			
			this.updateViewportPreCalc(viewport);
			
			return true;
			
		}
	},
	
	/** setContainerDimensions - Sets the dimensions of the container element that
	the viewport exists inside and calculates some useful maths for panning etc
	(internal use). {
		category:"method",
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport to set the container dimensions for.",
		}, {
			type:"integer",
			name:"width",
			desc:"The width to set the container to.",
		}, {
			type:"integer",
			name:"height",
			desc:"The height to set the container to.",
		}],
	} **/
	setContainerDimensions: function (viewport, width, height) {
		// Update the viewports dimensions
		var container = document.getElementById(viewport.viewport_id);
		//if (container != null) {
			container.style.width = width + 'px';
			container.style.height = height + 'px';
			viewport.viewport_container.width = width;
			viewport.viewport_container.height = height;
			
			viewport.$local.$width = width;
			viewport.$local.$height = height;
			
			viewport.$local.$width2 = Math.floor(viewport.$local.$width / 2);
			viewport.$local.$height2 = Math.floor(viewport.$local.$height / 2);
			
			// Calculate the pan layer's center point on the viewport and width / height
			viewport.panLayer.centerX = -(~~(1 * ((viewport.viewport_container.width * viewport.panLayer.padding))));
			viewport.panLayer.centerY = -(~~(1 * ((viewport.viewport_container.height * viewport.panLayer.padding))));
			viewport.panLayer.width = (~~(1 * (viewport.viewport_container.width + ((viewport.viewport_container.width * viewport.panLayer.padding) * 2))));
			viewport.panLayer.height = (~~(1 * (viewport.viewport_container.height + ((viewport.viewport_container.height * viewport.panLayer.padding) * 2))));
			
			// Resize the pan layer so we can move all the draw layers at once and negate
			// having to redraw the actual canvases until the mouse-up occurs
			var panLayer = $('#' + viewport.viewport_id + '_' + viewport.panLayer.id);
			if (panLayer != null) {
				// Set pan layer width and height	
				panLayer.css('width', viewport.panLayer.width);
				panLayer.css('height', viewport.panLayer.height);
				// Reset the pan layer to the center position
				panLayer.css('marginLeft', viewport.panLayer.centerX);
				panLayer.css('marginTop', viewport.panLayer.centerY);
			}
			
			// Resize the viewport layers
			var layerLevel = 0;
			var canvasBased = false;
			var layerCount = viewport.drawLayers.length;
			
			for (layerId = 0; layerId < layerCount; layerId++) {
				// Get the layer definition
				var layerDef = viewport.drawLayers[layerId];
				
				// Set locals
				layerDef.$local.$layer_dirty = true;
				
				// Select the correct type of layer type
				var layerElement = this.layerElementType[layerDef.layer_type];
				if (layerElement == 'canvas') { canvasBased = true; }
				
				// Get the layer element
				var layerElem = $('#' + viewport.viewport_id + '_' + layerId);
				if (layerElem != null) {
					layerElem.attr('width', viewport.panLayer.width);
					layerElem.attr('height', viewport.panLayer.height);
					layerElem.css('width', viewport.panLayer.width);
					layerElem.css('height', viewport.panLayer.height);
					layerElem.css('top', -(0 + (layerLevel * viewport.panLayer.height)));
				}
				layerLevel++;
			}
			
			// If the viewport has canvas-based layers, resize the viewport back buffer
			if (canvasBased) {
				// Get the back buffer element
				var bbElem = $('#' + viewport.viewport_id + '_backBuffer');
				if (bbElem != null) {
					bbElem.attr('width', viewport.panLayer.width);
					bbElem.attr('height', viewport.panLayer.height);
					bbElem.css('width', viewport.panLayer.width);
					bbElem.css('height', viewport.panLayer.height);
				}
			}
			
			this.updateViewportPreCalc(viewport);
			this.makeViewportDirty(viewport);
		//}
		// TO-DO - Resize the viewport layers as well and think about what else is affected. - LAYERS DONE AS ABOVE
	},
	
	/** makeViewportDirty - Adds a dirty rectangle that covers the whole viewport for every layer
	effectively forcing the renderer to redraw all layers of the viewport (internal use). {
		category:"method",
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport to mark as dirty.",
		}],
	} **/
	makeViewportDirty: function (viewport) {
		// Loop through the viewport layers
		var dls = viewport.drawLayers;
		var dlCount = dls.length;
		
		while (dlCount--) {
			dls[dlCount].$local = dls[dlCount].$local || [];
			dls[dlCount].$local.$layer_dirty = true;
		}
		
		viewport.$local.$viewport_dirty = true;
	},
	
	/** markViewportSectionDirty - Makes a section of the viewport dirty by calculating a bounding
	box based upon the passed diff parameters and then asks the renderer to redraw the section based
	upon dirty rectangle intersection (internal use). {
		category:"method",
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport whose dirty section data is to be modified.",
		}, {
			type:"integer",
			name:"xDiff",
			desc:"A positive or negative integer that denotes the horizontal area of the viewport in pixels to mark as dirty.",
		}, {
			type:"integer",
			name:"yDiff",
			desc:"A positive or negative integer that denotes the vertical area of the viewport in pixels to mark as dirty.",
		}],
	} **/
	markViewportSectionDirty: function (viewport, xDiff, yDiff) {
		// A positive xDiff means the redraw zone is to the right of the viewport
		// A positive yDiff means the redraw zone is to the bottom of the viewport
		var plWidth = viewport.panLayer.width;
		var plHeight = viewport.panLayer.height;
		var section1 = false;
		var section2 = false;
		
		// Calculate the first section based upon the xDiff
		if (xDiff != 0) {
			if (xDiff > 0) {
				// X starts at the right-side of the screen
				var section1X = plWidth - xDiff;
			} else {
				// X starts at the left-side of the screen
				var section1X = 0;
			}
			// Y starts at the top of the screen
			var section1Y = 0;
			
			// Width of section is the difference
			var section1Width = Math.abs(xDiff);
			var section1Height = plHeight;
			
			section1 = true;
		}
		
		// Calculate the second section based upon the yDiff
		if (yDiff != 0) {
			if (yDiff > 0) {
				// Y starts at the bottom of the screen
				var section2Y = plHeight - yDiff;
			} else {
				// Y starts at the top of the screen
				var section2Y = 0;
			}
			// X starts at the left of the screen
			var section2X = 0;
			
			// Height of section is the difference
			var section2Width = plWidth;
			var section2Height = Math.abs(yDiff);
			
			section2 = true;
		}
		
		// If both sections are active (a diagonal movement) then make sure we don't overlap them
		if (xDiff != 0 && yDiff != 0) {
			
		}
		
		// Clear any previous sections
		this.clearDirtySections(viewport);
		
		// Now add the new sections
		if (section1) { this.addDirtySection(viewport, [section1X, section1Y, section1Width, section1Height]); }
		if (section2) { this.addDirtySection(viewport, [section2X, section2Y, section2Width, section2Height]); }
		
	},
	
	/** clearDirtySections - Removes all dirty section data for the viewport (internal use). {
		category:"method",
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport whose dirty section data is to be modified.",
		}],
	} **/
	clearDirtySections: function (viewport) {
		viewport.$local.dirtySections = [];
	},
	
	/** addDirtySection - Adds a section to the viewport's dirty section data (internal use). {
		category:"method",
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport whose dirty section data is to be modified.",
		}, {
			type:"array",
			name:"section",
			desc:"An array containing four entries, an x, y, width and height of the section of the viewport to mark as dirty.",
		}],
	} **/
	addDirtySection: function (viewport, section) {
		viewport.$local.dirtySections.push(section);
	},
	
	/** updateViewportPreCalc - Updates the viewport's internal pre-calculated data that helps in
	fast rendering and entity positioning (internal use). {
		category:"method",
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport whose pre-calc data is to be updated.",
		}],
	} **/
	updateViewportPreCalc: function (viewport) {
		var camera = viewport.$local.$camera;
		var cameraScale = camera.camera_scale;
		var cameraX = camera.camera_x * cameraScale;
		var cameraY = camera.camera_y * cameraScale;
		
		var viewportWidth2 = viewport.$local.$width2 / cameraScale;
		var viewportHeight2 = viewport.$local.$height2 / cameraScale;
		
		viewport.$viewportAdjustX = viewportWidth2 - cameraX - viewport.panLayer.centerX;
		viewport.$viewportAdjustY = viewportHeight2 - cameraY - viewport.panLayer.centerY;
		
		viewport.$local.$renderX = viewport.$viewportAdjustX - viewport.$local.$width2 + viewport.$local.$camera.camera_x;
		viewport.$local.$renderY = viewport.$viewportAdjustY - viewport.$local.$height2 + viewport.$local.$camera.camera_y;
		viewport.$local.$renderWidth = viewport.$local.$width;
		viewport.$local.$renderHeight = viewport.$local.$height;
	},
	
	/** _doAutoSize - This method is called when the window resizes and is used to resize
	viewports and their	dependant elements if they are set to autoSize (fill the screen). {
		category:"method",
	} **/
	_doAutoSize: function ()
	{
		
		var viewport = null,
		panLayer = null,
		vpLayers = null,
		vpIndex = 0,
		vplIndex = 0,
		newLayer = null,
		newWidth = 0,
		newHeight = 0;
		
		// Go through all the viewports we are managing and set the width 
		// and height for the viewport and its layers
		for (vpIndex = 0; vpIndex < this.byIndex.length; vpIndex++)
		{
			
			viewport = this.byIndex[vpIndex];
			
			// Check if the viewport has its autoSize parameter set to true
			if (viewport.viewport_autoSize)
			{
				
				this.emit('beforeResize', viewport);
				
				// Update the viewports dimensions
				newWidth = $('#' + viewport.viewport_id).parent().width();
				newHeight = $('#' + viewport.viewport_id).parent().height();
				this.setContainerDimensions(viewport, newWidth, newHeight);
				this.engine.viewports.requestEntityData(viewport);
				
				this.emit('afterResize', viewport);
				
			}
			
		}
		
	},
	
	/** panStart - Initiates viewport panning which will cause the viewport's contents to pan
	without incurring any graphics redraws. Once you have initiated panning with this method,
	you can call the panTo and panBy methods to move the contents of the viewport. When your
	panning is complete, call the panEnd method to end panning and allow the viewport contents
	to be redrawn. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true on success or false on failure.",
		},
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport to use for panning.",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate at which the pan operation started.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate at which the pan operation started.",
		}],
	} **/
	panStart: function (viewport, x, y) {
		var local = viewport.$local;
		
		// Check we are not already performing a pan action
		if (!local.pan_panning) {
			// Get the pan layer element
			var pl = local.pan_panLayerElement;
			
			// Set the initial pan data
			local.pan_layerStart = [parseInt(pl.css('marginLeft')), parseInt(pl.css('marginTop'))];
			local.pan_start = [x, y];
			local.pan_current = [x, y];
			local.pan_change = [0, 0];
			
			// Set the panning flag to true
			local.pan_panning = true;
			
			return true;
		} else {
			// Panning has already been initiated so throw an error
			this.log('Cannot initiate a panning operation on the viewport because panning has already been initiated.', 'error', viewport);
			return false;
		}
	},
	
	/** panTo - Uses the viewport's pan layer to give the impression that the camera has been
	panned, however no redraw occurs whilst viewport panning is being done. This allows you
	to move the contents of a viewport around without incurring costly redraws until panning
	is ended. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true on success or false on failure.",
		},
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport to use for panning.",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate to pan to.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate to pan to.",
		}],
	} **/
	panTo: function (viewport, x, y) {
		var local = viewport.$local;
		
		// Check that a pan action has been initiated
		if (local.pan_panning) {
			// Update the pan current data
			local.pan_current[0] = x;
			local.pan_current[1] = y;
			local.pan_change[0] = local.pan_start[0] - local.pan_current[0];
			local.pan_change[1] = local.pan_start[1] - local.pan_current[1];
			
			this.panMoveLayer(viewport, local.pan_layerStart[0] - local.pan_change[0], local.pan_layerStart[1] - local.pan_change[1]);
			
			return true;
		} else {
			this.log('Attempted to use panTo on a viewport when no pan has been started with panStart!', 'warning');
			return false;
		}
	},
	
	/** panBy - Uses the viewport's pan layer to give the impression that the camera has been
	panned, however no redraw occurs whilst viewport panning is being done. This allows you
	to move the contents of a viewport around without incurring costly redraws until panning
	is ended. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true on success or false on failure.",
		},
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport to use for panning.",
		}, {
			type:"integer",
			name:"x",
			desc:"The x amount to pan by.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y amount to pan by.",
		}],
	} **/
	panBy: function (viewport, x, y) {
		var local = viewport.$local;
		
		// Check that a pan action has been initiated
		if (local.pan_panning) {
			// Update the pan current data
			local.pan_current[0] += x;
			local.pan_current[1] += y;
			local.pan_change[0] = local.pan_start[0] - local.pan_current[0];
			local.pan_change[1] = local.pan_start[1] - local.pan_current[1];
			
			return true;
		} else {
			this.log('Attempted to use panBy on a viewport when no pan has been started with panStart!', 'warning');
			return false;
		}
	},
	
	/** panEnd - Ends the current panning function initiated with a call to the panStart method
	to end, causing the viewport contents to be redrawn. If no pan was initiated with the panStart
	method, panEnd will return false. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true on success or false on failure.",
		},
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport to use for panning.",
		}, {
			type:"integer",
			name:"x",
			desc:"The final x co-ordinate that the pan ended on.",
		}, {
			type:"integer",
			name:"y",
			desc:"The final y co-ordinate that the pan ended on.",
		}],
	} **/
	panEnd: function (viewport, x, y) {
		var local = viewport.$local;
		
		// Check that a pan action has been initiated
		if (local.pan_panning) {
			this.emit('beforePanEnd', viewport);
			
			// Get the camera object
			var camera = local.$camera;
			var camSqr = (camera.camera_scale * camera.camera_scale);
			
			if (x != null && y != null) {
				// The x and y passed were not null so count them as the last pan position
				local.pan_current[0] = x;
				local.pan_current[1] = y;
				local.pan_change[0] = local.pan_start[0] - local.pan_current[0];
				local.pan_change[1] = local.pan_start[1] - local.pan_current[1];
			}
			
			// Turn off panning
			local.pan_panning = false;
			
			// Hide the viewport contents whilst we make a big jump in position etc
			this.hideContents(viewport);
			
			// Updating the viewport's camera to the new location
			this.engine.cameras.lookAt(local.$camera, camera.camera_x + (local.pan_change[0] / camSqr), camera.camera_y + (local.pan_change[1] / camSqr));
			
			// Cull any out-of-bounds entities
			this.engine.entities.cullOutOfBoundsEntities();
			
			// Render a frame so we avoid the "jumping" effect
			this.engine._renderTick(true);
			
			// Reset viewport pan layer
			this.panResetLayer(viewport);
			
			// Show the contents again!
			this.showContents(viewport);
			
			this.emit('afterPanEnd', viewport);
			
			return true;
		} else {
			this.log('Attempted to use panEnd on a viewport when no pan has been started with panStart!', 'warning');
			return false;
		}
	},
	
	/** panMoveLayer - Moves the viewport's internal pan layer element using the marginLeft
	and marginTop CSS styles set to the passed x and y values. This method is usually called
	from the panTo and panBy methods and is not usually useful outside of the pan layer methods
	(internal use). {
		category:"method",
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport to use for panning.",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate to move the pan layer to.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate to move the pan layer to.",
		}],
	} **/
	panMoveLayer: function (viewport, x, y) {
		var local = viewport.$local;
		var pl = local.pan_panLayerElement;
		
		// Update the pan layer CSS styles
		pl.css('marginLeft', x);
		pl.css('marginTop', y);
	},
	
	/** panResetLayer - Resets the viewport's pan layer back to its initial style settings. This
	method is usually called from the panEnd method and is not usually useful outside of the pan
	layer methods (internal use). {
		category:"method",
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport to use for panning.",
		}],
	} **/
	panResetLayer: function (viewport) {
		var local = viewport.$local;
		var pl = local.pan_panLayerElement;
		
		// Update the pan layer CSS styles
		pl.css('marginLeft', local.pan_layerStart[0]);
		pl.css('marginTop', local.pan_layerStart[1]);
	},
	
	/** isPanning - Check if the viewport has initiated a pan opertation. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true if the viewport has initiated a pan opertation, false if not.",
		},
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport to check.",
		}],
	} **/
	isPanning: function (viewport) {
		return viewport.$local.pan_panning;
	},
	
	/** hideContents - Hides the contents of a viewport by setting the CSS style "visibility". {
		category:"method",
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport whose contents should be hidden.",
		}],
	} **/
	hideContents: function (viewport) {
		viewport.$local.containerElement.css('visibility', 'hidden');
	},
	
	/** showContents - Shows the contents of a viewport by setting the CSS style "visibility". {
		category:"method",
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport whose contents should be shown.",
		}],
	} **/
	showContents: function (viewport) {
		viewport.$local.containerElement.css('visibility', 'visible');
	},
	
	/** requestEntityData - Using the viewport and camera settings, this method will ask the server
	to send all entity data that resides within the viewport's viewing area. This is useful when
	dealing with large maps that would require vast amounts of memory to store all the entity data.
	This command is best used with the method "IgeEntities.cullOutOfBoundsEntities" which will remove
	entity data from the engine if it no longer resides inside any viewport's viewing area. {
		category:"method",
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport to request entity data for.",
		}],
	} **/
	requestEntityData: function (viewport) {
		if (!this.engine.isServer) {
			//this.engine.network.request('updateViewport', {viewport:this.engine.stripLocal(viewport), camera:this.engine.stripLocal(viewport.$local.$camera)});
		}
	},
	
	/** _setCleanSectionMove - Sets data used by the renderer to process section panning
	without displaying rendering artefacts. {
		category:"method",
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport to request entity data for.",
		}, {
			type:"integer",
			name:"x",
			desc:"The x data.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y data.",
		}],
	} **/
	_setCleanSectionMove: function (viewport, x, y) {
		// Ensure that the x and y have actual integer values even if null
		if (!x) { x = 0; }
		if (!y) { y = 0; }
		
		// Store the values which will be used by the renderer on the next frame
		viewport.$local.cleanSectionMove = [x, y];
	},
	
});