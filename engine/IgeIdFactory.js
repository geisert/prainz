/** IgeIdFactory - The id factory class. {
	engine_ver:"0.1.2",
	category:"class",
} **/
IgeIdFactory = new IgeClass({
	
	/** engine - A reference object to the main engine instance. {
		category:"property",
		type:"object",
		instanceOf:"IgeEngine",
	} **/
	engine: null,
	
	/** counter - A counter used to produce unique ids. {
		category:"property",
		type:"integer",
	} **/
	counter: null,
	
	/** init - The constructor for this class. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a new instance of this class",
		},
		argument: {
			type:"object",
			name:"engine",
			desc:"The active engine instance.",
			instanceOf:"IgeEngine",
		},
	} **/
	init: function (engine) {
		this._className = 'IgeIdFactory';
		
		this.engine = engine;
		
		// Set the initial id as the current time in milliseconds. This ensures that under successive
		// restarts of the engine, new ids will still always be created compared to earlier runs.
		this.counter = new Date().getTime();
	},
	
	/** newId - Generates a new unique ID. {
		category:"method",
		return: {
			type:"string",
			desc:"A unique ID for use in any part of the engine that might need one.",
		},
	} **/
	newId: function () {
		this.counter++;
		return String(this.counter + (Math.random() * Math.pow(10, 17) + Math.random() * Math.pow(10, 17) + Math.random() * Math.pow(10, 17) + Math.random() * Math.pow(10, 17)));
	},
	
	/** newIdHex - Generates a new 16-character unique ID (string). {
		category:"method",
		return: {
			type:"string",
			desc:"A unique hexadecimal ID for use in any part of the engine that might need one.",
		},
	} **/
	newIdHex: function () {
		this.counter++;
		return this.counter.toString(16);
	},
	
});