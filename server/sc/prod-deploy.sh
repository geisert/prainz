set -x
ssh root@8.19.32.16 'cd frennzy-prod; git pull upstream master; git push'
ssh root@8.19.32.16 'pkill highland'
ssh root@8.19.32.16 'pkill node'
ssh root@8.19.32.16 '(~/frennzy-prod/server/highlander.sh &)'
