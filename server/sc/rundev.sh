set -x
cd frennzy-dev
git pull
pkill node
cd frennzy-dev/server && nohup /root/local/node/bin/node app.js > log/node.log &
