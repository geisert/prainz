function score_screen(){  
	fz.view('score_screen');  
	$('#dispPagename').html('Score Screen');  
	$('.hide').hide();
	$('#install').hide(); 
	for(i=0;i<fz.players.length;i++){ 
		$('#playerScore'+(i+1)).html(fz.players[i].name);  
		$('#scoreScore'+(i+1)).html(fz.players[i].overallScore);   
		$('#rowScore'+(i+1)).show();   
	} 
}
function game_menu(){
	fz.view('game_menu');
	$('#dispPagename').html('Game Menu');
}
function saveUser(){
	var name = document.getElementById('namePrompt').value;//get value from element
	fz.username = name;//store in fz
	fz.players[0].name = name;//set player0 aka 1 to you by default
	fz.save({'username':name,'players[0].name':fz.players[0].name});//update the database
	$('#uWelcome').html("<div class='fzTitle'>Welcome,</br>"+fz.username);//rewrite the wellcome to say your name
	$('#dispU').html(fz.username);//changes the header to say your name insetd of undefined
	$('#header').show();//turn on header
	$('#footer').show();//**** ** footer
	game_menu();//set the view to menu mode*
}
function chooseGame(gameName){
	fz.game=gameName;
	fz.save({game:gameName});
	lobby();
}
function lobby() {
	fz.view('lobby');
	$('#dispPagename').html('Lobby');
	$('.hide').hide();
	$('#install').hide();
	for( i = 0; i < fz.players.length; i++) {
		fz.players[i].icon = "<img id='player" + i + "_icon' src='" + fz.players[i].iconUrl + "' />";
		$('#icon' + (i + 1)).html(fz.players[i].icon);
		$('#player' + (i) + "_icon").css('width', '25px');
		$('#player' + (i) + "_icon").css('height', '25px');
		$('#edit' + (i + 1)).html(fz.players[i].name);
		$('#score' + (i + 1)).html(fz.players[i].overallScore);
		$('#row' + (i + 1)).show();
	}
	if(fz.players.length >= 8) {
		$('#addPlayer').hide();
	} else {
		$('#addPlayer').show();
	}
	$('#gameName').html(fz.game);
	$('#playbutton').html('play ' + fz.game);
}
function multi_lobby(){
	lobby();
	$('.fzScoreTableMoveUpCol').hide()
	$('.fzScoreTableMoveDownCol').hide()
	$('.fzScoreTableRemoveCol').hide()
	$('#addPlayer').hide();
	$('.fzScoreTableMoveUpCol').hide()
	$('.fzScoreTableMoveDownCol').hide()
	$('.fzScoreTableRemoveCol').hide()
}
function editPlayer(edit) {
	$('#editPlayerName').html(fz.players[edit - 1].name);
	$('#player_icon').html(fz.players[edit - 1].icon);
	$('#configure').show();
	fz.editing = edit - 1;
}
function deletePlayer(edit) {
	fz.players.splice((edit - 1), 1);
	fz.save({players:fz.players});
	lobby();
}
function changeName() {
	fz.players[fz.editing].name = prompt('new nickname', '');
	fz.logAct('name changed to ' + fz.players[fz.editing].name);
	$('#player' + (fz.editing + 1)).html(fz.players[fz.editing].name);
	$('#editPlayerName').html(fz.players[fz.editing].name);
}
function changeIconLeft() {
	if(fz.players[fz.editing].iconIndex < (fz.icons.length - 1)) {
		fz.players[fz.editing].iconIndex++;
	} else {
		fz.players[fz.editing].iconIndex = 0;
	}
	fz.players[fz.editing].iconName = fz.icons[fz.players[fz.editing].iconIndex];
	fz.players[fz.editing].iconUrl = "../images/icons/" + fz.players[fz.editing].iconName + ".png";
	fz.players[fz.editing].icon = "<img id='player" + fz.editing + "_icon' src='" + fz.players[fz.editing].iconUrl + "' />";
	$('#player_icon').html(fz.players[fz.editing].icon);
}
function changeIconRight() {
	if(fz.players[fz.editing].iconIndex == 0) {
		fz.players[fz.editing].iconIndex = (fz.icons.length - 1);
	} else {
		fz.players[fz.editing].iconIndex--;
	}
	fz.players[fz.editing].iconName = fz.icons[fz.players[fz.editing].iconIndex];
	fz.players[fz.editing].iconUrl = "../images/icons/" + fz.players[fz.editing].iconName + ".png";
	fz.players[fz.editing].icon = "<img id='player" + fz.editing + "_icon' src='" + fz.players[fz.editing].iconUrl + "' />";
	$('#player_icon').html(fz.players[fz.editing].icon);
}
function addPlayer() {
	newName = ('Player' + (fz.players.length + 1));
	fz.players.push({
		'name' : newName,
		'iconIndex' : 1,
		'overallScore' : 0,
		'score' : 0,
		'uId':null
	});
	fz.players[fz.players.length-1].iconName = fz.icons[fz.players.length-1]
	fz.players[fz.players.length-1].iconUrl = "../images/icons/" + fz.players[fz.players.length-1].iconName + ".png";
	fz.players[fz.players.length-1].icon = "<img id='player" + (fz.players.length-1) + "_icon' src='" + fz.players[fz.players.length-1].iconUrl + "' />";
	fz.save({players:fz.players});
	lobby();
}
function openGame() {
	if(fz.credits < fz.players.length) {
		alert("You need " + fz.players.length + " credits to play.\nLet's go get some more.");
		top_up();
	} else {
		fz.save({credits:(fz.credits-fz.players.length)});
		if(fz.game == "guesstimates") {
			if(fz.players.length < 3) {
				alert("This game needs at least 3 players.");
			}
			else{
				fz.advance('/games/' + fz.game + '/rules');
			}
		}
		else{
			fz.advance('/games/' + fz.game + '/rules');
		}
	}
}
function hideConfigure() {
	$('#configure').hide();
	fz.save({players:fz.players})
	lobby();
}
function movePlayerUp(edit) {
	if(edit != 1) {
		fz.players.splice(edit - 2, 0, fz.players.splice(edit-1, 1)[0]);
		fz.save({players:fz.players});
		lobby();
	}
}
function movePlayerDown(edit) {
	if(edit != fz.players.length) {
		fz.players.splice(edit - 1, 0, fz.players.splice(edit, 1)[0]);
		fz.save({players:fz.players});
		lobby();
	}
}
function share(){
	fz.view('share');
	$('#dispPagename').html('Share');
	$('#dispC').html('Credits: '+fz.credits.toString());
	$('canvas').css('width',50);
	$('canvas').css('height',50);
	$('#install').hide();
}
function tweet(){
	fz.credits+=1;
	fz.save({credits:fz.credits});
	share()
	alert('Twitter Integration Coming Soon\nHave a credit anyway!');
	fz.logAct('tweeting on twitter');
}
function fbShare(){
	fz.credits+=5;
	fz.save({credits:fz.credits});
	alert('FaceBook Integration Coming Soon\nHave 5 credits anyway!');
	share()
	fz.logAct('sharing on facebook');
}
var fPage = window.location.hostname + "/?rId=";
var qrHS = 0;
function hide(name){
	document.getElementById(name).style.display = 'none';
}
function show(name){
	document.getElementById(name).style.display = 'block';
}
function resizeQr(){
	if(qrHS == 0){
		$('canvas').css('width',256);
		$('canvas').css('height',256);
		qrHS = 1;
	}else{
		$('canvas').css('width',50);
		$('canvas').css('height',50);
		qrHS = 0;
	}
}function getMorecredits(){
	fz.credits+=10;
	fz.save({credits:fz.credits});
	alert('All Credits are free during testing\nHave 10 for clicking on the one that said "Buy"!');
	top_up();
	fz.logAct('just getting more credits for free');
}
function top_up(){
	fz.view('top_up');
	$('#dispPagename').html('Top Up');
	$('#dispC').html('Credits: '+fz.credits.toString());
	$('#credits').html(fz.credits.toString());
	if(fz.game!='none' && fz.isFromLobby && !(fz.credits<fz.players.length)){
		$('#toGameP').show();
		$('#toGame').show();
	}
	else{
		$('#toGameP').hide();
	}
	$('#install').hide();
}
function goToGame(){
	fz.isFromLobby=0;
	fz.save();
	window.location.assign('/games/'+fz.game+'/rules');
}
function offer(){
	fz.credits+=5;
	fz.save();
	alert('Offer of the Day Coming Soon\nHave 5 credits anyway!');
	top_up();
	fz.logAct('going to offer wall');
}
