var fz;
fz.icons=['construction','detective','fedora','fedorawhite','grad','madhat','mouse','party','police','redhat','santa','santa2','witch','wizard1','wizard2','wizard3'];

fz.init=function(){
	for(i=0;i<fz.players.length;i++){
		fz.players[i].score=0;
	}
}

fz.initIcons = function(){
	for(i=0;i<fz.players.length;i++){
		fz.players[i].iconIndex=fz.icons.indexOf(fz.players[i].iconName);
		fz.players[i].iconUrl="../images/icons/"+fz.players[i].iconName+".png";
		fz.players[i].icon="<img id='player"+i+"_icon' src='"+fz.players[i].iconUrl+"' />";
	}
}

fz.advance=function(path){
	if (fz.room!='none'&&fz.room!='undefined'&&fz.room!=null){
		var json={action:'advance',path:path};
		fz.toRoom(json);
		window.location.assign(path);
	}
}
socket.on('advance',function(json){
	window.location.assign(json.path);
});

fz.save=function(updates){
	updates.uId=fz.uId;
	socket.emit('save',updates);
}

fz.toRoom = function toRoom(json){
	json.room=fz.room;
	socket.emit('toRoom',json);
}

fz.joinRoom=function(room,multi){
	json={};
	json.uId=fz.uId;
	json.room=room;
	json.oldRoom=fz.room;
	json.multi=multi;
	socket.emit('joinRoom',json);
	fz.room=room;
}

function leaveRoom(nick){//not finished
	json.nickname=nick;
	json.room=fz.room;
	socket.emit('leaveRoom',json);
}

fz.view=function view(pageName){
	$('.page').hide();
	$('#'+pageName).show();
}

fz.gameover= function gameover(){
	for(i=0;i<fz.players.length;i++){
		fz.players[i].overallScore+=fz.players[i].score;
	}
	fz.save({players:fz.players});
	fz.advance('/menu/score_screen');
}

fz.logAct=function logAct(action){
	socket.emit('logActivity',{uId:fz.uId,activity:action});
}
