var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-25815757-1']);
_gaq.push(['_trackPageview']); (function() {
	var ga = document.createElement('script');
	ga.type = 'text/javascript';
	ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(ga, s);
})();
var socket = io.connect();
$(document).ready(function() {
	if( typeof (init) == 'function') {
		init();
		initHeader();
		clickToInstall();
		fz.joinRoom(fz.room);
	}
});
function openPage(path) {
	window.location.assign(path);
}
if(navigator.standalone) {
	$('#install').hide();
}
if('<%=pagename%>' != 'index') {
	document.body.ontouchmove = function(e) {
		e.preventDefault();
	}
}
function initHeader() {
	if(navigator.platform == 'iPhone') {
		$('#container').css('top', '20px')
	}
	$('#dispU').html(fz.username);
	$('#dispC').html("Credits: " + fz.credits.toString());
}

function clickToInstall() {
	var uagent = navigator.userAgent.toLowerCase();
	var instruct = "1) Click 'Forward', below<br><font size='4'>(It looks like this: <img src='/images/appleforwardbutton.jpeg' width='20' height='20'/>)</font></br>2) 'Add to Home Screen'";
	//untill there are some special phone-specific instructions, generic instructions will be used
	if(uagent.search("android") > -1) {
		//instructions for android
		instruct = "1) Click 'Add Bookmark'<br>2) Open 'Bookmarks'<br>3)'Add Shortcut to Home'.";
	} else if(uagent.search("ipad") > -1) {
		//instruct = "Add instructions for ipad";
	} else if(uagent.search("iphone") > -1) {
		//instruct = "Add instructions for iphone";
	} else if(uagent.search("ipod") > -1) {
		//instruct = "Add instructions for android";
	}
	//general instructions
	$('#instructions').html("TO INSTALL FRENNZY:</br>" + instruct);
}