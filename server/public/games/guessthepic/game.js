var drawx=[];
var drawy=[];
var mydrawx=[];
var mydrawy=[];
var pointx;
var pointy;
var mousedown=false;
var emit = {};
var playerIndex=0;
var drawer=fz.players[playerIndex].uId;

socket.on('clear',function(){
	drawx=[];
	drawy=[];
});
socket.on('setRole',function(json){
	playerIndex++;
	alert(json.drawer+' '+fz.uId);
	if(json.drawer==fz.uId){
		role='drawer';
	}
	else{
		role='guesser';
	}
});
function selectWinner(){
	fz.view('playerList');
	fz.toRoom({action:'clearCanvas',to:'all'})
}
function playerChosen(num){
	fz.players[num].score+=4;
	if(fz.players.length>(playerIndex+1)){
		playerIndex++;
	}
	else{
		playerIndex=0;
	}
	drawer=fz.players[playerIndex].uId
	fz.toRoom({action:'setRole',drawer:drawer});
	fz.view('drawScreen');
}

function init(){
	fz.init();
	if(fz.players[playerIndex].uId==fz.uId){
		role='drawer';
	}
	else{
		role='guesser';
	}
	$('#header').hide();
	$('#footer').hide();
	$('#install').hide();
	fz.view('drawScreen');
    var kin = new Kinetic_2d("drawing");
    var canvas = kin.getCanvas();
    var context = kin.getContext();
    socket.on('clearCanvas',function(){
		canvas.width=canvas.width;
    });
	socket.on('draw',function(json){
		drawx.push(json.x);
		drawy.push(json.y);
		if(drawx.length>1){
			var i=drawx.length-1;
 			context.moveTo(drawx[i-1],drawy[i-1]);
        	context.lineTo(drawx[i],drawy[i]);
        	context.stroke();
    	};
	});
	if(role=='drawer'){
		document.onmousedown=function(event){
			mousedown=true;
			fz.toRoom({'action':'draw','x':event.pageX,'y':event.pageY});
			mydrawx.push(event.pageX);
			mydrawy.push(event.pageY);
		};
		document.onmousemove= function(event){
			if(mousedown==true){
				emit = {'action':'draw','x':event.pageX,'y':event.pageY}
				fz.toRoom(emit);
				mydrawx.push(event.pageX);
				mydrawy.push(event.pageY);
				if(mydrawx.length>1){
					var i=mydrawx.length-1;
		 			context.moveTo(mydrawx[i-1],mydrawy[i-1]);
		        	context.lineTo(mydrawx[i],mydrawy[i]);
		        	context.stroke();
		    	};
			}
		}
		document.onmouseup= function(event){
			mousedown=false;
			fz.toRoom({'action':'clear'});
			mydrawx=[];
			mydrawy=[];
		};
		document.ontouchmove= function(event){
			event.preventDefault();
			emit = {'action':'draw','x':event.touches[0].pageX,'y':event.touches[0].pageY};
			fz.toRoom(emit);
			mydrawx.push(event.touches[0].pageX);
			mydrawy.push(event.touches[0].pageY);
			if(mydrawx.length>1){
				var i=mydrawx.length-1;
	 			context.moveTo(mydrawx[i-1],mydrawy[i-1]);
	        	context.lineTo(mydrawx[i],mydrawy[i]);
	        	context.stroke();
	    	};
		}
		document.ontouchend= function(event){
			fz.toRoom({'action':'clear'});
			mydrawx=[];
			mydrawy=[];
		};
	}
}