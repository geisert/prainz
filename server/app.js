/**
 * Module dependencies.
 */

var mongoose = require('mongoose')
, express = require('express')
, sio = require('socket.io')
, db=mongoose.connect('mongodb://localhost/frennzy');

function init(app){

	// Configuration

	app.configure(function(){
					  app.set('views', __dirname + '/views');
					  app.set('view engine', 'ejs');
					  app.use(express.bodyParser());
					  app.use(express.methodOverride());
					  app.use(app.router);
					  app.use(express.static(__dirname + '/public'));
					  console.log("dirname:" + __dirname);
				  });

	app.set('view options', {
				layout: "layout.ejs"
			});
	
	app.configure('development', function(){
					  app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
				  });

	app.configure('production', function(){
					  app.use(express.errorHandler()); 
				  });


	///////////////////////////////////////////////////////////
	function contentType(path) {
		if (path.match('.js$')) {
			return "text/javascript";
		} else if (path.match('.css$')) {
			return  "text/css";
		} else if (path.match('.manifest$')) {
			return  "text/cache-manifest";
		} else if (path.match('.svg$')) {// disabled
			return  "image/svg+xml";
//			return "text/html";
		}  else {
			return "text/html";
		}
	}

	///////////////////////////////////////////////////////////
	// Util methods

	function extend(destination, source) {
		for (var property in source) {
			if (source.hasOwnProperty(property)) {
				destination[property] = source[property];
			}
		}
		return destination;
	}

	function showPage(req, res, locals) {
		if (locals == null) {
			locals = {};
		}
		if(req.cookies.uid==null){
			if(req.params.rId==null){req.params.rId='none'}
			newPlayer(req.params.rId,function(doc){
				res.cookie('uid', doc._id, { maxAge: (1000*60*60*24*365) , path:'/'});
				initFz(req,res,locals,doc);
			})
		}
		else{
			Player.findOne({_id:req.cookies.uid},function(err,doc){
				initFz(req,res,locals,doc);
			});
		}
		function initFz(req,res,locals,doc){
			locals.fz={num:{},str:{}};
			locals.players=[{}];
			locals.fz.num.overallScore=doc.overallScore;
			locals.fz.num.credits=doc.credits;
			locals.fz.num.fbId=doc.fbId;
			locals.fz.str.username=doc.username;
			locals.fz.str.game=doc.game;
			locals.fz.str.uId=doc._id;
			locals.fz.str.twitter=doc.twitter;
			locals.fz.str.room=doc.room;
			for(i=0;i<doc.players.length;i++){
				locals.players[i]={num:{},str:{}};
				locals.players[i].num.overallScore=doc.players[i].overallScore;
				locals.players[i].num.score=doc.players[i].score;
				locals.players[i].str.iconName=doc.players[i].iconName;
				locals.players[i].str.name=doc.players[i].name;
				locals.players[i].str.uId=doc.players[i].uId;
			}
			renderPage(req,res,locals)
		}
		function renderPage(req,res,locals){
			var url = req.url.slice(1); // remove leading
			if (url.split('/')[0]=='menu'){//check if they are loading the menu
				url='menu'; 
				locals.page=(req.url.slice(1).split('/')[1]);
			}
			if (url=="/") { url = "/index"}
			url = url.split("?")[0];	// remove any params for matching
			var pagename = locals.pagename || url;
	
			var defaults = {
				pagename: pagename,
			};
			locals = extend(locals, defaults);
			//console.log(locals);
			res.render(url, {locals: locals});
		}
	}

	///////////////////////////////////////////////////////////
	// Routes

	app.get('/', function(req, res){
				showPage(req, res);
			});
	app.get('/debug', function(req, res) {
				showPage(req, res); 
			})
	app.get('/menu/:page', function(req,res) {
				var locals={'games':['hotpotato','ramjammin','guesstimates'],
							'gameTeasers':["Don't Get Burned!","Team Storytelling","What Would Your Friends Say?",]};
				showPage(req, res, locals);
			})
	app.get('/menu', function(req,res) {
				var locals={'games':['hotpotato','ramjammin','guesstimates'],
							'gameTeasers':["Don't Get Burned!","Team Storytelling","What Would Your Friends Say?",]};
			showPage(req, res, locals);
			})

	// single game
	app.get('/games/:gamename/rules', function(req,res) {
				var gamename = req.params.gamename;
				data = { gamename: gamename };
				showPage(req,res,data);
			})

	// play
	app.get('/games/:gamename/play?', function(req,res) {
				var gamename = req.params.gamename;
				data = {
					gamename: gamename,
					pagename: gamename,
					players: req.params.players,
					showPagename: false
				};
				console.log(data);
				showPage(req,res,data);
			})

	app.get('/query', function(req,res) {
				showPage(req,res);
			});

	var liburl = require('url');
	var fs = require('fs');

	app.get('/cache.manifest', function(req,res){
				
				var path = liburl.parse(req.url).pathname;
				console.log("path=" + path);

				fs.readFile(__dirname + path, function(err, data){
								if (err) {
									res.writeHead(404);
									res.write("cant find the manifest!")
									res.end();
								} else {
									res.writeHead(200, {'Content-Type': contentType(path)});
									res.write(data, 'utf8');
									res.end();
								}
							});
				// res.contentType("text/cache-manifest");
				// res.end("CACHE MANIFEST");
			});

	////// catchall/404 handler
	//app.get('*', function(req, res){
	//	str = req.url;
	//	res.send('404!!!<br/>' + str, 404);
	//});
	//

var io = sio.listen(app);

//initiate the socket connection
io.sockets.on('connection',function(socket){
	socket.on('save',function(json){
		modifyPlayer(json);
	});
	socket.on('toRoom',function(json){
		console.log(json);
		action=json.action;
		delete json.action;
		room=json.room;
		delete json.room;
		socket.broadcast.to(room).emit(action,json);
		if(json.to=='all'){
			socket.emit(action,json);
		}
	});
	socket.on('joinRoom',function(json){
		socket.leave(json.oldRoom);
		delete json.oldRoom;
		socket.join(json.room);
		socket.join(json.uId);
		modifyPlayer(json,function(doc){
			if(json.multi=='multi'){
				Player.find({room:json.room},function(err,docs){
					console.log(docs)
					var sendingPlayers=[];
					for(i=0;i<docs.length;i++){
						var uId=docs[i]._id.toString();
						var addPlayer={name:docs[i].username,overallScore:docs[i].overallScore,iconName:'wizard3'}
						addPlayer.uId=uId;
						console.log(addPlayer);
						sendingPlayers[i]=addPlayer;
						socket.emit('newPlayer',addPlayer);
					}
					modifyPlayer({uId:doc._id,players:sendingPlayers});
					socket.broadcast.to(json.room).emit('newPlayer',{name:doc.username,overallScore:doc.overallScore,iconName:'grad',uId:doc._id});
				});
			}
		});
	});
	socket.on('wisper',function(json){
		socket.broadcast.to(json.player).emit(json.action,{message:json.message,from:json.self});
	});
	socket.on('query',function(json){
		var find={};
		find[json.findWhat]=json.findValue;
		Player.find(find,function(err,docs){
			socket.emit('queryResult',{'results':docs})
		});
	});
	socket.on('logActivity',function(json){
		activity(json);
	});
	socket.on('disconnect',function(){
		console.log('disconnect'+socket.id);
	});
});

//DB connections, schema, and functions
var Schema=mongoose.Schema;
var PlayerSchema = new Schema({
	username:String,
	iconName:String,
	players:Array,
	credits:Number,
	overallScore:Number,
	game:String,
	refered:Array,
	email:String,
	fbId:Number,
	twitter:String,
	rId:String,
	startTime:Date,
	room:String,
	socket:Number
});
var GroupSchema = new Schema({
	nicks:String,
	icons:Array,
	players:Array
});
var IconSchema = new Schema({
	nicks:String,
	url:String
});
var ActivitySchema = new Schema({
	player:String,
	activity:String,
	timestamp:Date
});
function newPlayer(rId,cb){
	var d = new Date();
	var player = new Player({
		username:'undefined'
		, players:[{'name':'undefined','iconName':'wizard1','score':0,'overallScore':0},{'name':'Pirate','iconName':'grad','score':0,'overallScore':0}]
		, credits:10
		, overallScore:0
		, email:'none'
		, twitter:'none'
		, game:'none'
		, rId:rId
		, startTime:d.getTime()
		, fbId:0
		, room:'none'
		, socket:0
	});
	player.save(function(err){
		if(err){
			console.log(err);
		}
		cb(player);
	})
}

function modifyPlayer(json,cb){
	//console.log(json);
	var conditions = { _id: json.uId }
	if(json.players!=null){
		if(json.players[0]!=null){
			if(json.players[0].uId!=null){
				for(i=0;i<json.players.length;i++){
					if(json.players[i].uId!=json.uId){
						modifyPlayer(json.players[i]);
					}
				}
			}
		}
	}
	delete json.uId;
	Player.find(conditions,function(err,docs){
		//console.log(docs)
		for(i=0;i<docs.length;i++){
			for(j in json){
				var jItem=("'"+j+"'");
				var thisDoc=docs[i];
				thisDoc[j]=json[j];
			}
			thisDoc.save(function(err){
				if(typeof(cb)=='function'){
					cb(thisDoc);
				}
			});
		}
	})
}
function newGroup(json){
	
}
function newIcon(json){
	
}
function activity(action){
	var d=new Date();
	console.log(action.activity);
	var activityLog = new Activity({
		player:action.uId,
		activity:action.activity,
		timestamp:d.getTime()
	});
	activityLog.save(function(err){
		if(err){
			console.log(err);
		}
	})
}

var Player  = mongoose.model('Player', PlayerSchema);
var Group  = mongoose.model('Group', GroupSchema);
var Icon  = mongoose.model('Icon', IconSchema);
var Activity  = mongoose.model('Activity', ActivitySchema);
}

var app = module.exports = express.createServer(express.cookieParser(),express.session({secret:'joinTheFrennzy'}));
init(app);
app.listen(8004);

console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
